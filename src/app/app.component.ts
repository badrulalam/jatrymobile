import { Component, ViewChild } from '@angular/core';

import { Events, MenuController, Nav, Platform } from 'ionic-angular';
// import { Splashscreen, StatusBar } from 'ionic-native';


import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { HomePage } from '../pages/home/home';
import { ConferenceData } from '../providers/conference-data';
import { JatryStaticData } from '../providers/jatry-static-data';
import { UserData } from '../providers/user-data';
import { CurrentBookingPage } from '../pages/current-booking/self';
import { CanceledBookingPage } from '../pages/canceled-booking/self';
import { UpcommingFlightsPage } from '../pages/upcomming-flights/self';
import { CanceledFlightsPage } from '../pages/canceled-flights/self';
import { MyProfilePage } from '../pages/myprofile/self';
import { FellowTravellersPage } from '../pages/fellow-travellers/self';
import { ChangePasswordPage } from '../pages/change-password/self';
import { CreditCardsPage } from '../pages/credit-card/self';
import { ShippingAddressPage } from '../pages/shipping-address/self';

export interface PageObj {
  title: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
}

@Component({
  templateUrl: 'app.template.html'
})
export class ConferenceApp {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  // List of pages that can be navigated to from the left menu
  // the left menu only works after login
  // the login page disables the left menu
  appPages: PageObj[] = [
    { title: 'Home', component: HomePage, icon: 'calendar' }
  ];
  //appPagesMyFlights: PageObj[] = [
  //  { title: 'Upcoming Flights', component: TabsPage, icon: 'calendar' },
  //  { title: 'Previous Bookings', component: TabsPage, index: 1, icon: 'information-circle' },
  //  { title: 'Cancelled Bookings', component: TabsPage, index: 2, icon: 'information-circle' },
  //];
  //appPagesMyAccounts: PageObj[] = [
  //];
  loggedInPages: PageObj[] = [
    // { title: 'Profile', component: AccountPage, icon: 'person' },
    // { title: 'Travellers', component: TabsPage, index: 1, icon: 'information-circle' },
    // { title: 'Payment Info', component: TabsPage, index: 2, icon: 'information-circle' },
    // { title: 'Shipping Address', component: TabsPage, index: 2, icon: 'information-circle' },
    // { title: 'Billing Address', component: TabsPage, index: 2, icon: 'information-circle' },
    //{ title: 'Logout', component: TabsPage, icon: 'log-out', logsOut: true },
    { title: 'Logout', component: HomePage, icon: 'log-out', logsOut: true }
  ];

  myFlightPages: PageObj[] = [
    { title: 'Current Booking', component: CurrentBookingPage, index: 1, icon: 'information-circle' },
    { title: 'Canceled Booking', component: CanceledBookingPage, index: 1, icon: 'information-circle' },
    { title: 'Upcomming Flights', component: UpcommingFlightsPage, index: 1, icon: 'information-circle' },
    { title: 'Canceled Flights', component: CanceledFlightsPage, index: 1, icon: 'information-circle' },
  ];
  myAccountPages: PageObj[] = [
    { title: 'My Profile', component: MyProfilePage, index: 1, icon: 'information-circle' },
    { title: 'Fellow Travellers', component: FellowTravellersPage, index: 1, icon: 'information-circle' },
    { title: 'Change Password', component: ChangePasswordPage, index: 1, icon: 'information-circle' },
    { title: 'Credit Cards', component: CreditCardsPage, index: 1, icon: 'information-circle' },
    { title: 'Shipping Address', component: ShippingAddressPage, index: 1, icon: 'information-circle' },
  ];





  loggedOutPages: PageObj[] = [
    { title: 'Login', component: LoginPage, icon: 'log-in' },
    { title: 'Signup', component: SignupPage, icon: 'person-add' }
  ];
  //rootPage: any = TabsPage;
  rootPage: any = HomePage;
  //rootPage: any = TravellersPage;

  constructor(
    public events: Events,
    public userData: UserData,
    public menu: MenuController,
    platform: Platform,
    jatryStaticData: JatryStaticData,
    confData: ConferenceData
  ) {
    // Call any initial plugins when ready
    platform.ready().then(() => {
      // StatusBar.styleDefault();
      // Splashscreen.hide();



      // Here we will check if the user is already logged in
      // because we don't want to ask users to log in each time they open the app
      // let env = this;
      // NativeStorage.getItem('user')
      //   .then( function (data) {
      //     // user is previously logged and we have his data
      //     // we will let him access the app
      //     env.nav.push(UserPage);
      //     Splashscreen.hide();
      //   }, function (error) {
      //     //we don't have the user data so we will ask him to log in
      //     env.nav.push(LoginPage);
      //     Splashscreen.hide();
      //   });
      //
      // StatusBar.styleDefault();






    });

    // load the conference data
    //confData.load();

    // decide which menu items should be hidden by current login status stored in local storage
    this.userData.hasLoggedIn().then((hasLoggedIn) => {
      this.enableMenu(hasLoggedIn === true);
    });

    this.listenToLoginEvents();
  }

  openPage(page: PageObj) {
    // the nav component was found using @ViewChild(Nav)
    // reset the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario

    console.log('page', page);

    if (page.index) {
      this.nav.setRoot(page.component, {tabIndex: page.index});

    } else {
      this.nav.setRoot(page.component);
    }

    if (page.logsOut === true) {
      // Give the menu time to close before changing to logged out
      setTimeout(() => {
        this.userData.logout();
      }, 1000);
    }
  }

  listenToLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:signup', () => {
      this.enableMenu(true);
    });

    this.events.subscribe('user:logout', () => {
      this.enableMenu(false);
    });
  }

  enableMenu(loggedIn) {
    this.menu.enable(loggedIn, 'loggedInMenu');
    this.menu.enable(!loggedIn, 'loggedOutMenu');
  }
}
