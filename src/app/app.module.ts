import { NgModule } from '@angular/core';

import { IonicApp, IonicModule } from 'ionic-angular';
//import { AUTH_PROVIDERS } from 'angular2-jwt';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
// import { Observable } from "rxjs/Observable";
import { ConferenceApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { SchedulePage } from '../pages/schedule/schedule';
import { SearchresultPage } from '../pages/searchresult/searchresult';
import { SearchresultDetailPage } from '../pages/searchresult-detail/searchresult-detail';
import { HomePage } from '../pages/home/home';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { HomeFilterPage } from '../pages/home-filter/home-filter';
import { HomeSelectcityPage } from '../pages/home-selectcity/home-selectcity';
import { HomeModalPage } from '../pages/home-modal/home-selectcity';
import { HomeModalDomPage } from '../pages/home-modal-dom/home-selectcity';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
import { TabsPage } from '../pages/tabs/tabs';
import { MasterPage } from '../pages/master/master';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { TravellersPage } from '../pages/travellers/travellers';
import { TravellersFormPage } from '../pages/travellers-form/travellers-form';
import { PaymentPage } from '../pages/payment/payment';

import { ConferenceData } from '../providers/conference-data';
import { JatryStaticData } from '../providers/jatry-static-data';
import { UserData } from '../providers/user-data';


import { CurrentBookingPage } from '../pages/current-booking/self';
import { CurrentBookingDetailsPage } from '../pages/current-booking/self-details';
import { CanceledBookingPage } from '../pages/canceled-booking/self';
import { CancelBookingDetailsPage } from '../pages/canceled-booking/self-details';
import { UpcommingFlightsPage } from '../pages/upcomming-flights/self';
import { CanceledFlightsPage } from '../pages/canceled-flights/self';
import { MyProfilePage } from '../pages/myprofile/self';
import { FellowTravellersPage } from '../pages/fellow-travellers/self';
import { FellowTravellersDetailsPage } from '../pages/fellow-travellers/self-details';
import { FellowTravellersCreatePage } from '../pages/fellow-travellers/self-create';
import { ChangePasswordPage } from '../pages/change-password/self';
import { CreditCardsPage } from '../pages/credit-card/self';
import { CreditCardsDetailsPage } from '../pages/credit-card/self-details';
import { CreditCardsCreatePage } from '../pages/credit-card/self-create';
import { ShippingAddressPage } from '../pages/shipping-address/self';

let storage = new Storage();
let YOUR_HEADER_PREFIX = "bearer";

export function getAuthHttp(http) {
  return new AuthHttp(new AuthConfig({
    headerPrefix: YOUR_HEADER_PREFIX,
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => storage.get('id_token')),
  }), http);
}

@NgModule({
  declarations: [
    ConferenceApp,
    AboutPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    SearchresultPage,
    SearchresultDetailPage,
    HomePage,
    ScheduleFilterPage,
    HomeFilterPage,
    HomeSelectcityPage,
    HomeModalPage,
    HomeModalDomPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    MasterPage,
    TutorialPage,
    TravellersPage,
    TravellersFormPage,
    CurrentBookingPage,
    CurrentBookingDetailsPage,
    CanceledBookingPage,
    CancelBookingDetailsPage,
    UpcommingFlightsPage,
    CanceledFlightsPage,
    MyProfilePage,
    FellowTravellersPage,
    FellowTravellersDetailsPage,
    FellowTravellersCreatePage,
    ChangePasswordPage,
    CreditCardsPage,
    CreditCardsDetailsPage,
    CreditCardsCreatePage,
    ShippingAddressPage,
    PaymentPage
  ],
  imports: [
    IonicModule.forRoot(ConferenceApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    AboutPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    SearchresultPage,
    SearchresultDetailPage,
    HomePage,
    ScheduleFilterPage,
    HomeFilterPage,
    HomeSelectcityPage,
    HomeModalPage,
    HomeModalDomPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    MasterPage,
    TutorialPage,
    TravellersPage,
    TravellersFormPage,
    CurrentBookingPage,
    CurrentBookingDetailsPage,
    CanceledBookingPage,
    CancelBookingDetailsPage,
    UpcommingFlightsPage,
    CanceledFlightsPage,
    MyProfilePage,
    FellowTravellersPage,
    FellowTravellersDetailsPage,
    FellowTravellersCreatePage,
    ChangePasswordPage,
    CreditCardsPage,
    CreditCardsDetailsPage,
    CreditCardsCreatePage,
    ShippingAddressPage,
    PaymentPage
  ],
  providers: [
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http]
    },
    //AUTH_PROVIDERS,
    JatryStaticData,
    ConferenceData,
    UserData,
    Storage
  ]
})
export class AppModule {}
