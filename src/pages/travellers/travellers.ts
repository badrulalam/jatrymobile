import { Component } from '@angular/core';

import { App, ModalController, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

/*
 To learn how to use third party libs in an
 Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
 */
//import moment from 'moment';

import { TravellersFormPage } from '../travellers-form/travellers-form';
import { PaymentPage } from '../payment/payment';
import { JatryStaticData } from '../../providers/jatry-static-data';
import { AppSettings } from '../../services/AppSettings.service';

@Component({
  selector: 'page-travellers',
  templateUrl: 'travellers.html'
})
export class TravellersPage {
  // the list is a child of the schedule page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element
  //@ViewChild('scheduleList', {read: List}) scheduleList: List;

  public searchParam:any={}
  public flightItem:any={};
  public travellers:any={};
  public bookingItem:any={};

  constructor(
    public http: Http,
    public authHttp: AuthHttp,
    public app: App,
    public jatryStaticData: JatryStaticData,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams:NavParams
  ) {
    this.searchParam=navParams.data.searchParam;
    this.flightItem=navParams.data.flightItem;
    this.bookingItem=navParams.data.bookingItem;

    console.log('search params',this.searchParam);
    console.log('flight detail params',this.flightItem);
    console.log('booking',this.bookingItem);

    // let travellersOrder = 0;

    ////for adults
    if(this.searchParam.adults)
      this.travellers.adults=[];
    ////for childs
    if(this.searchParam.childs)
      this.travellers.childs=[];
    ////for infants
    if(this.searchParam.infant)
      this.travellers.infants=[];


    for(var i=0;i<this.bookingItem.passenger.length;i++){
      if(this.bookingItem.passenger[i].passenger_type == "adults"){
        this.travellers.adults.push(this.bookingItem.passenger[i])
      }
      if(this.bookingItem.passenger[i].passenger_type == "childs"){
        this.travellers.childs.push(this.bookingItem.passenger[i])
      }
      if(this.bookingItem.passenger[i].passenger_type == "infant"){
        this.travellers.infants.push(this.bookingItem.passenger[i])
      }
    }

    console.log(this.travellers);

    ////for adults
    //if(this.searchParam.adults)
    //  this.travellers.adults=[];
    //for(var i=0;i<this.searchParam.adults;i++){
    //  this.travellers.adults.push({passenger_type:"adults",passenger_order:travellersOrder});
    //  travellersOrder++;
    //}
    //
    ////for childs
    //if(this.searchParam.childs)
    //  this.travellers.childs=[];
    //for(var i=0;i<this.searchParam.childs;i++){
    //  this.travellers.childs.push({passenger_type:"childs",passenger_order:travellersOrder});
    //  travellersOrder++;
    //}
    //
    ////for infants
    //if(this.searchParam.infant)
    //  this.travellers.infants=[];
    //for(var i=0;i<this.searchParam.infant;i++){
    //  this.travellers.infants.push({passenger_type:"infant",passenger_order:travellersOrder});
    //  travellersOrder++;
    //}

  }

  openTravellersForm(travellerType){
    console.log("need to open the dialog for adding traveller fields");
    let modal = this.modalCtrl.create(TravellersFormPage, this.travellers);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data!='cancel') {
        this.travellers=data;
      }
    });
  }
  goToPaymentPage(){

    //this.bookingItem.passenger

    // let travellersInfo = [];

    ////for adults
    //if(this.searchParam.adults){
    //  for(var i=0;i<this.searchParam.adults;i++){
    //    travellersInfo.push(this.travellers.adults[i]);
    //  }
    //}
    //
    ////for childs
    //if(this.searchParam.childs){
    //  for(var i=0;i<this.searchParam.childs;i++){
    //    travellersInfo.push(this.travellers.childs[i]);
    //  }
    //}
    //
    ////for infants
    //if(this.searchParam.infant){
    //  for(var i=0;i<this.searchParam.infant;i++){
    //    travellersInfo.push(this.travellers.infant[i]);
    //  }
    //}

    console.log(this.bookingItem);
    let queryStr = JSON.stringify({passenger:this.bookingItem.passenger});
    //let queryStr = {passenger:travellersInfo,customeradd:{},custpayment:{}};
    //let myHeader = new Headers();
    //myHeader.append('Content-Type', 'application/x-www-form-urlencoded');

    console.log(queryStr);

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.authHttp.post(`${AppSettings.API_ENDPOINT}/api/booking-update`,queryStr, options).subscribe(res => {
      let travellersData = res.json();
      if(travellersData.passenger){
          this.navCtrl.push(PaymentPage, {
            searchParam: this.searchParam,
            flightItem:  this.flightItem,
            bookingItem: this.bookingItem
          });
      }

      //this.navCtrl.push(PaymentPage, {
      //  searchParam: this.searchParam,
      //  flightItem:this.flightItem,
      //  travellers:this.travellers
      //
      //});

    });

  }


  //airlinesByIata(iata) {
  //  var data =  this.airlines.find(function (row) {
  //    return row.iata == iata;
  //  });
  //
  //  return data ? data.airline : iata;
  //};

  //detailFlight(flightItem){
  //  this.navCtrl.push(SearchresultDetailPage, {
  //    flightItem: flightItem,
  //    searchParam:this.params
  //  });
  //}

}
