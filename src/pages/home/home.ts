import { Component } from '@angular/core';

import {App, ModalController, NavController } from 'ionic-angular';

/*
  To learn how to use third party libs in an
  Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
*/
//import moment from 'moment';

import { HomeSelectcityPage } from '../home-selectcity/home-selectcity';
import { HomeModalPage } from '../home-modal/home-selectcity';
import { HomeModalDomPage } from '../home-modal-dom/home-selectcity';
//import { SessionDetailPage } from '../session-detail/session-detail';
import { SearchresultPage } from '../searchresult/searchresult';
//import { TravellersPage } from '../travellers/travellers';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { AppSettings } from '../../services/AppSettings.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // the list is a child of the home page
  // @ViewChild('homeList') gets a reference to the list
  // with the variable #homeList, `read: List` tells it to return
  // the List and not a reference to the element
  //@ViewChild('homeList', {read: List}) homeList: List;

  public searchObj:any={};
  public domSearchObj:any={};
  public flightType:any;
  public domesticList:any;
  constructor(
    public app: App,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public authHttp: AuthHttp
    //public confData: ConferenceData,
    //public user: UserData
  ) {
    this.searchObj.origin="DAC";
    this.searchObj.destination="DEL";
    this.searchObj.departureDate = '1990-02-18';
    this.searchObj.arrivalDate = '1990-02-19';
    this.searchObj.adults = 1;
    this.searchObj.childs = 0;
    this.searchObj.infant = 0;
    this.searchObj.tripClass = 'Economy';
    this.searchObj.tripType = 'O';
    this.searchObj.noOfSegments = 1;
    this.domSearchObj.origin=1;
    this.domSearchObj.destination=2;
    this.domSearchObj.departureDate = '1990-02-19';
    this.domSearchObj.arrivalDate = '1990-02-19';
    this.domSearchObj.adults = 1;
    this.domSearchObj.childs = 0;
    this.domSearchObj.infant = 0;
    this.domSearchObj.tripClass = 'Economy';
    this.domSearchObj.tripType = 'O';
    this.domSearchObj.noOfSegments = 1;
    this.flightType = "I";
    var d = new Date();
    var strDate = d.getFullYear().toString();
    if(d.getMonth()+1<10)
      strDate+='-'+'0'+(d.getMonth()+1);
    else
      strDate+='-'+(d.getMonth()+1).toString();
    if(d.getDate()<10)
      strDate+='-'+'0'+d.getDate();
    else
      strDate+='-'+d.getDate().toString();

    this.searchObj.departureDate=strDate;
    this.searchObj.arrivalDate=strDate;

    this.searchObj.tripType='O';
    this.getDomesticList()

  }



  getDomesticList(){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.authHttp.get(`${AppSettings.API_ENDPOINT}/api/city-list`,options).subscribe(res => {
      console.log(res.json());
      this.domesticList = res.json();
console.log("this.domesticList", this.domesticList);
    });
  }



  ionViewDidEnter() {
    this.app.setTitle('Home');
  }

  selectCity(fromOrTo) {

    let modal = this.modalCtrl.create(HomeSelectcityPage, {fromOrTo:fromOrTo,tripClass:this.searchObj.tripClass});
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        if(fromOrTo=="from" && data != 'cancel'){
          this.searchObj.origin = data.ac;
        }
        else if(fromOrTo=="to" && data != 'cancel'){
          this.searchObj.destination = data.ac;
        }
      }
    });
  }
  openModal() {

    let modal = this.modalCtrl.create(HomeModalPage, {searchObj:this.searchObj});
    modal.present();

    modal.onDidDismiss((data: any) => {


    });
  }
  openModalDom() {

    let modal = this.modalCtrl.create(HomeModalDomPage, {searchObj:this.searchObj});
    modal.present();

    modal.onDidDismiss((data: any) => {


    });
  }

//  this.$watch('searchObj', function (newValue, oldValue) {
//  // console.log(newValue, oldValue);
//  if (newValue.adults + newValue.childs > 9) {
//    newValue.adults = oldValue.adults;
//    newValue.childs = oldValue.childs;
//  }
//  if (newValue.adults < 1) {
//    newValue.adults = 1;
//  }
//  if (newValue.adults < newValue.infant) {
//    newValue.infant = newValue.adults;
//  }
//}, true);

  changeTravellers(travellerType,opType){

    var oldAdult, oldChild, oldInfant;

    oldAdult=this.searchObj.adults;
    oldChild=this.searchObj.childs;
    oldInfant=this.searchObj.infant;

    if(opType=='+'){
      if(travellerType=='adult')
        this.searchObj.adults++;
      else if(travellerType=='child')
        this.searchObj.childs++;
      else if(travellerType=='infant')
        this.searchObj.infant++;
    }
    else{
        if(travellerType=='adult')
          this.searchObj.adults--;
        else if(travellerType=='child')
          this.searchObj.childs--;
        else if(travellerType=='infant')
          this.searchObj.infant--;
    }

    if (this.searchObj.adults + this.searchObj.childs > 9) {
      this.searchObj.adults = oldAdult;
      this.searchObj.childs = oldChild;
    }
    if (this.searchObj.adults < 1) {
      this.searchObj.adults = 1;
    }
    if (this.searchObj.adults < this.searchObj.infant) {
      this.searchObj.infant = this.searchObj.adults;
    }
  }

  searchMe() {
      //this.navCtrl.push(TravellersPage, {
      //  searchParam: this.searchObj
      //});
    //console.log(this.searchObj);

    //if(this.searchObj.tripType=='O'){
    //  delete this.searchObj.arrivalDate;
    //}
    //console.log(this.searchObj);

    this.navCtrl.push(SearchresultPage,this.searchObj);
  }
}
