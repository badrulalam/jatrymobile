import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';

import { AboutPage } from '../about/about';
import { MapPage } from '../map/map';
//import { SchedulePage } from '../schedule/schedule';
import { HomePage } from '../home/home';
import { SpeakerListPage } from '../speaker-list/speaker-list';


@Component({
  templateUrl: 'master.html'
})
export class MasterPage {
  // set the root pages for each tab
  //tab1Root: any = SchedulePage;
  tab1Root: any = HomePage;
  tab2Root: any = SpeakerListPage;
  tab3Root: any = MapPage;
  tab4Root: any = AboutPage;
  mySelectedIndex: number;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }
}
