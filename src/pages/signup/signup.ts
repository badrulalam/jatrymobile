import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

//import { TabsPage } from '../tabs/tabs';
import { HomePage } from '../home/home';
import { UserData } from '../../providers/user-data';


@Component({
  selector: 'page-login',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: {name?: string, email?: string, password?: string, confirmPassword?: string} = {};
  submitted = false;

  constructor(public navCtrl: NavController, public userData: UserData) {}

  onSignup(form) {
    this.submitted = true;
    console.log(this.signup);

    if (form.valid) {
      console.log("valid hoyechhe");
      this.userData.signup(this.signup).subscribe(
          x => console.log('onNext: %s', x),
          e => console.log('onError: %s', e),
        () => {
          console.log('onCompleted: registrationSuccess');
          //if(this.navParams.data.fromWhichPage)
          //  this.viewCtrl.dismiss("login successfull");
          //else
          //  this.navCtrl.setRoot(HomePage, {tabIndex: 1});
            this.navCtrl.setRoot(HomePage, {tabIndex: 1});
        }//console.log('onCompleted')
      );
      //this.navCtrl.push(TabsPage);
      //this.navCtrl.push(HomePage);
    }
  }
}
