import { Component } from '@angular/core';

import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
//import { TabsPage } from '../tabs/tabs';
//import { MasterPage } from '../master/master';
import { HomePage } from '../home/home';
import { UserData } from '../../providers/user-data';
import { Facebook } from 'ionic-native';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: {username?: string, password?: string} = {};
  submitted = false;
  FB_APP_ID: number = 1327341357289597;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public userData: UserData,
    public alertCtrl: AlertController
  ) {

    Facebook.browserInit(this.FB_APP_ID, "v2.8");
  }

  onLogin(form) {
    console.log('this.navParams');
    console.log(this.navParams);

    this.submitted = true;

    console.log('form.valid');
    console.log(form.valid);

    if (form.valid) {
      //this.navCtrl.push(TabsPage); //this.navCtrl.push(MasterPage);//this.navCtrl.push(HomePage);
      this.userData.login(this.login.username, this.login.password)
        .subscribe(
          x => console.log('onNext: %s', x),
          e => {
            console.log('onError: %s', e);
            this.showAlert("Error", e);
          },
          () => {
            console.log('onCompleted: loginSuccess');
            if(this.navParams.data.fromWhichPage)
              this.viewCtrl.dismiss("login successfull");
            else
              this.navCtrl.setRoot(HomePage, {tabIndex: 1});
          }//console.log('onCompleted')
        );
    }
  }
  fbLogin(user) {
   console.log("from fb user", user);
this.userData.fbLogin(user)
  .subscribe(
    x => console.log('fb onNext: %s', x),
    e => {
      console.log('fb onError: %s', e);

    },
    () => {
      console.log('onCompleted: loginSuccess');
      if(this.navParams.data.fromWhichPage)
        this.viewCtrl.dismiss("login successfull");
      else
        this.navCtrl.setRoot(HomePage, {tabIndex: 1});
     }
  );
    // if (form.valid) {
    //   //this.navCtrl.push(TabsPage); //this.navCtrl.push(MasterPage);//this.navCtrl.push(HomePage);
    //   this.userData.login(this.login.username, this.login.password)
    //     .subscribe(
    //       x => console.log('onNext: %s', x),
    //       e => {
    //         console.log('onError: %s', e);
    //         this.showAlert("Error", e);
    //       },
    //       () => {
    //         console.log('onCompleted: loginSuccess');
    //         if(this.navParams.data.fromWhichPage)
    //           this.viewCtrl.dismiss("login successfull");
    //         else
    //           this.navCtrl.setRoot(HomePage, {tabIndex: 1});
    //       }//console.log('onCompleted')
    //     );
    // }
  }

  onSignup() {
    this.navCtrl.push(SignupPage);
  }
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [{
        text: 'Close',
        handler: () => {
          console.log('Close clicked');
          // this.nav.pop();
        }
      }]
    });
    alert.present();
  }

  onSignupFacebook(){
    // let nav = this.navCtrl;
    //the permissions your facebook app needs from the user
    let permissions = ["public_profile","email","user_friends"];

    let self = this;
    Facebook.login(permissions)
      .then(function(response){ console.log("fb success response", response);
        // let userId = response.authResponse.userID;
        let params = new Array();

        //Getting name and gender properties
        Facebook.api("/me?fields=name,gender,email", params)
          .then(function(user) {

            console.log("before call a function");
            self.fbLogin(user);
            console.log("after call a function");
            // self.showAlert("Error", "a");


            // console.log("fb success user", user);
            // user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
            // //now we have the users info, let's save it in the NativeStorage
            // NativeStorage.setItem('user',
            //   {
            //     name: user.name,
            //     gender: user.gender,
            //     picture: user.picture
            //   })
            //   .then(function(){
            //     console.log('facebook: loginSuccess');
            //   }, function (error) {
            //     console.log(error);
            //   })




          })
      }, function(error){
        console.log(error);
      });
  }
}
