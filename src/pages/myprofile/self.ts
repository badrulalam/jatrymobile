import { Component } from '@angular/core';

import { AlertController, NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { UserData } from '../../providers/user-data';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { AppSettings } from '../../services/AppSettings.service';
@Component({
  selector: 'myprofile',
  templateUrl: 'self.html'
})
export class MyProfilePage {
  username: string;
  userObj:any={};
  profile:any = {};

  constructor(
    public alertCtrl: AlertController,
    public nav: NavController,
    public userData: UserData,
    public authHttp: AuthHttp
  ) {
    this.getProfile();
  }
  getProfile(){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.authHttp.get(`${AppSettings.API_ENDPOINT}/api/profile`,options).subscribe(res => {
      console.log(res.json());
      this.profile = res.json();

    });
  }
  getLoggedInUser(){
    //loggedin uses should be fetched from api
    this.userObj={};
  }

  ngAfterViewInit() {
    this.getUsername();
  }

  updatePicture() {
    console.log('Clicked to update picture');
  }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  changeUsername() {
    let alert = this.alertCtrl.create({
      title: 'Change Username',
      buttons: [
        'Cancel'
      ]
    });
    alert.addInput({
      name: 'username',
      value: this.username,
      placeholder: 'username'
    });
    alert.addButton({
      text: 'Ok',
      handler: data => {
        this.userData.setUsername(data.username);
        this.getUsername();
      }
    });

    alert.present();
  }

  getUsername() {
    this.userData.getUsername().then((username) => {
      this.username = username;
    });
  }

  changePassword() {
    console.log('Clicked to change password');
  }

  logout() {
    this.userData.logout();
    this.nav.setRoot(LoginPage);
  }

  profileUpdate(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let queryStr = JSON.stringify(this.profile);
    console.log("queryStr", queryStr);

    this.authHttp.put(`${AppSettings.API_ENDPOINT}/api/profile`,queryStr,options).subscribe(res => {
      console.log("update", res.json());
      // this.profile = res.json();

    });
    this.showAlert();
  }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Update!',
      subTitle: 'Your profile has been updated',
      buttons: [{
        text: 'Close',
        handler: () => {
          console.log('Close clicked');
          // this.nav.pop();
        }
      }]
    });
    alert.present();
  }
}
