import { Component } from '@angular/core';

import { AlertController, NavController } from 'ionic-angular';

import { UserData } from '../../providers/user-data';


import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { AppSettings } from '../../services/AppSettings.service';
@Component({
  selector: 'change-password',
  templateUrl: 'self.html'
})
export class ChangePasswordPage {
  username: string;
  userObj:any={};
  pass:any={};

  constructor(public alertCtrl: AlertController, public nav: NavController, public userData: UserData, public authHttp: AuthHttp) {

  }


  passwordUpdate(){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let queryStr = JSON.stringify(this.pass);
    console.log("queryStr", queryStr);

    this.authHttp.post(`${AppSettings.API_ENDPOINT}/api/change-password`,queryStr,options).subscribe(res => {
      console.log("update", res.json());
      this.showAlert('Update!', res.json());
    }, err =>{
      this.showAlert('Error!', err.json());
    });

  }
  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: [{
        text: 'Close',
        handler: () => {
          console.log('Close clicked');
          // this.nav.pop();
        }
      }]
    });
    alert.present();
  }
}
