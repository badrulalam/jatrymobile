import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

import { Http } from '@angular/http';

import { JatryStaticData } from '../../providers/jatry-static-data';

@Component({
  selector: 'page-home-selectcity',
  templateUrl: 'home-selectcity.html'
})
export class HomeSelectcityPage {
  tracks: Array<{name: string, isChecked: boolean}> = [];
  allCities = [];
  data: any;
  myInput:"";
  public airports : any;

  constructor(
    public http: Http,
    public navParams: NavParams,
    public viewCtrl: ViewController
    // public jatryStaticData: JatryStaticData
  ) {

    this.airports = new JatryStaticData().getAirports();
    this.myInput="";
    // passed in array of track names that should be excluded (unchecked)

    console.log(this.navParams.data.fromOrTo);

    // this.initializeItems();
  }

  // initializeItems(){
  //   this.data = this.jatryStaticData.getAirports();
  //   //console.log(this.data);
  // }

  //initializeItems(){
  //  this.http.get('/assets/data/place.json').subscribe(res => {
  //    this.data = res.json();
  //  });
  //}

  getItems(input) {
    // this.data = [];
    if(input.length >=3){
      if (input && input.trim() != '') {
        this.data = this.airports.filter((item) => {
          return (JSON.stringify(item).toLowerCase().indexOf(input.toLowerCase()) > -1);
        });
      }
    }else{
      this.data = [];
    }
    return;

  }

  meSelected(city){
    console.log("city",city);
    this.viewCtrl.dismiss(city);
  }

  dismiss(data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.viewCtrl.dismiss("cancel");
  }


}
