import { Component } from '@angular/core';

import { AlertController, NavController, NavParams } from 'ionic-angular';

// import { LoginPage } from '../login/login';
import { UserData } from '../../providers/user-data';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { JatryStaticData } from '../../providers/jatry-static-data';

import { AppSettings } from '../../services/AppSettings.service';
// import { Observable } from "rxjs/Observable";
@Component({
  selector: 'cancelbookingdetails',
  templateUrl: 'self-details.html'
})
export class CancelBookingDetailsPage {
  username: string;
  userObj:any={};
  cbItem:any = {};
  invoice:any = {};
  public flightItem:any={};
  public airlines=[];
  public airports=[];
  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userData: UserData,
    public authHttp: AuthHttp,
    public jatryStaticData: JatryStaticData
  ) {
    this.cbItem = navParams.data.cbItem;
    // this.getProfile();
    console.log("single cbItem", this.cbItem);
    this.getInvoice();
  }
  getInvoice(){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.authHttp.get(`${AppSettings.API_ENDPOINT}/api/flight/canceled-booking/view/${this.cbItem.order_number}`,options).subscribe(res => {
      console.log("invoice", res.json());
      this.invoice = res.json();

    });
  }
  dateDifference(date1, date2) {
    var d1:any = new Date(date1);
    var d2:any = new Date(date2);
    var difference_ms:any = d1 - d2;
    //console.log(difference_ms);
    //take out milliseconds
    difference_ms = difference_ms/1000;
    //var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var hours = Math.floor(difference_ms % 24);
    var days = Math.floor(difference_ms/24);

    var ret_data = "";
    if(days){
      ret_data += days + ' days';
    }
    if(hours){
      ret_data += hours + 'h ';
    }
    if(minutes){
      ret_data += minutes + 'm';
    }
    // return days + ' days, ' + hours + ' hours, ' + minutes + ' minutes, and ' + seconds + ' seconds';
    return ret_data;
  }

  getAirlines(){
    this.airlines = this.jatryStaticData.getAirlines();
  }

  getAirports(){
    this.airports = this.jatryStaticData.getAirports();
  }
  getLoggedInUser(){
    //loggedin uses should be fetched from api
    this.userObj={};
  }
  getAirportName(airport){
    var data =  this.airports.find(function (row) {
      return row.ac==airport;
    });
    return data ? data.an : airport;
  }
  airlinesByIata(iata) {
    var data =  this.airlines.find(function (row) {
      return row.iata == iata;
    });

    return data ? data.airline : iata;
  }

  ngAfterViewInit() {
    this.getUsername();
  }

  updatePicture() {
    console.log('Clicked to update picture');
  }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  changeUsername() {
    let alert = this.alertCtrl.create({
      title: 'Change Username',
      buttons: [
        'Cancel'
      ]
    });
    alert.addInput({
      name: 'username',
      value: this.username,
      placeholder: 'username'
    });
    alert.addButton({
      text: 'Ok',
      handler: data => {
        this.userData.setUsername(data.username);
        this.getUsername();
      }
    });

    alert.present();
  }

  getUsername() {
    this.userData.getUsername().then((username) => {
      this.username = username;
    });
  }

  changePassword() {
    console.log('Clicked to change password');
  }



  // travellerUpdate(){
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //   let queryStr = JSON.stringify(this.traveller);
  //   console.log("queryStr", queryStr);
  //
  //   this.authHttp.put(`${AppSettings.API_ENDPOINT}/api/traveller/${this.traveller.id}`,queryStr,options).subscribe(res => {
  //       console.log("update result", res.json());
  //       // this.profile = res.json();
  //       this.showAlert();
  //     },
  //     err => {
  //       console.log("update err", err);
  //     });
  //
  // }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Update!',
      subTitle: 'Your Traveller has been updated',
      buttons: [
        {
          text: 'Close',
          handler: () => {
            console.log('Close clicked');
            this.navCtrl.pop();
          }
        },

      ]
    });
    alert.present();
  }

}
