import { Component, ViewChild } from '@angular/core';

import { App, List, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import { AuthHttp } from 'angular2-jwt';
import { Headers, RequestOptions } from '@angular/http';
import { LoginPage } from '../login/login';

import { UserData } from '../../providers/user-data';

/*
 To learn how to use third party libs in an
 Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
 */
//import moment from 'moment';

import { JatryStaticData } from '../../providers/jatry-static-data';
import { TravellersPage } from '../travellers/travellers';
import { AppSettings } from '../../services/AppSettings.service';

@Component({
  selector: 'page-searchresult-detail',
  templateUrl: 'searchresult-detail.html'
})
export class SearchresultDetailPage {
  // the list is a child of the schedule page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element
  @ViewChild('scheduleList', {read: List}) scheduleList: List;

  public searchParam:any={}
  public flightItem:any={};
  public airports=[];
  public params:any ;
  public airlines=[];

  constructor(
    //public http: Http,
    public app: App,
    public authHttp: AuthHttp,
    public userData: UserData,
    public jatryStaticData: JatryStaticData,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams:NavParams,
    private toastCtrl: ToastController
  ) {
    this.searchParam=navParams.data.searchParam;
    this.flightItem=navParams.data.flightItem;
    console.log('search params',this.searchParam);
    console.log('flight detail params',this.flightItem);
    this.getAirports();
    this.getAirlines();
  }

  getAirlines(){
    this.airlines = this.jatryStaticData.getAirlines();
  }

  getAirports(){
    this.airports = this.jatryStaticData.getAirports();
  }

  getAirportName(airport){
    var data =  this.airports.find(function (row) {
      return row.ac==airport;
    });
    return data ? data.an : airport;
  }

  airlinesByIata(iata) {
    var data =  this.airlines.find(function (row) {
      return row.iata == iata;
    });

    return data ? data.airline : iata;
  }

  dateDifference(date1, date2) {
    var d1:any = new Date(date1);
    var d2:any = new Date(date2);
    var difference_ms:any = d1 - d2;
    //console.log(difference_ms);
    //take out milliseconds
    difference_ms = difference_ms/1000;
    //var seconds = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var minutes = Math.floor(difference_ms % 60);
    difference_ms = difference_ms/60;
    var hours = Math.floor(difference_ms % 24);
    var days = Math.floor(difference_ms/24);

    var ret_data = "";
    if(days){
      ret_data += days + ' days';
    }
    if(hours){
      ret_data += hours + 'h ';
    }
    if(minutes){
      ret_data += minutes + 'm';
    }
    // return days + ' days, ' + hours + ' hours, ' + minutes + ' minutes, and ' + seconds + ' seconds';
    return ret_data;
  }

  goToTravellersPage(){
    this.userData.hasLoggedIn().then((hasLoggedIn) => {
      if(!hasLoggedIn){
        //need to login and then go to traveller page
        //openning a modal for login
        let modal = this.modalCtrl.create(LoginPage, {fromWhichPage:'SearchResultPage'});
        modal.present();

        modal.onDidDismiss((data: any) => {
          console.log(data);
          if(data!="login successfull"){
            this.showTostrFailedLogin();
          }
        });
      }
      this.storeBookingAndGoTravellersPage();
    });
  }

  storeBookingAndGoTravellersPage(){

    console.log(this.searchParam,this.flightItem);

    let bookingQuery = {AirPricePoint:this.flightItem,SearchParams:this.searchParam}

    let queryStr = JSON.stringify(bookingQuery);

    //console.log(queryStr);

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.authHttp.post(`${AppSettings.API_ENDPOINT}/api/booking`,queryStr, options).subscribe(res => {
      console.log(res.json());

      this.navCtrl.push(TravellersPage, {
        flightItem: this.flightItem,
        searchParam:this.searchParam,
        bookingItem:res.json()
      });

    });
  }

  showTostrFailedLogin(){
    let toast = this.toastCtrl.create({
      message: 'Login Failed',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
}
