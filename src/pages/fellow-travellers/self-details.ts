import { Component } from '@angular/core';

import { AlertController, NavController, NavParams } from 'ionic-angular';

// import { LoginPage } from '../login/login';
import { UserData } from '../../providers/user-data';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { AppSettings } from '../../services/AppSettings.service';
// import { Observable } from "rxjs/Observable";
@Component({
  selector: 'ftravellerdetails',
  templateUrl: 'self-details.html'
})
export class FellowTravellersDetailsPage {
  username: string;
  userObj:any={};
  traveller:any = {};

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public userData: UserData,
    public authHttp: AuthHttp
  ) {
    this.traveller = navParams.data.travellerItem;
    // this.getProfile();
    console.log("single traveller", this.traveller);
  }
  // getProfile(){
  //
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //   this.authHttp.get(`${AppSettings.API_ENDPOINT}/api/traveller`,options).subscribe(res => {
  //     console.log(res.json());
  //     this.travellerList = res.json();
  //
  //   });
  // }
  getLoggedInUser(){
    //loggedin uses should be fetched from api
    this.userObj={};
  }

  ngAfterViewInit() {
    this.getUsername();
  }

  updatePicture() {
    console.log('Clicked to update picture');
  }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  changeUsername() {
    let alert = this.alertCtrl.create({
      title: 'Change Username',
      buttons: [
        'Cancel'
      ]
    });
    alert.addInput({
      name: 'username',
      value: this.username,
      placeholder: 'username'
    });
    alert.addButton({
      text: 'Ok',
      handler: data => {
        this.userData.setUsername(data.username);
        this.getUsername();
      }
    });

    alert.present();
  }

  getUsername() {
    this.userData.getUsername().then((username) => {
      this.username = username;
    });
  }

  changePassword() {
    console.log('Clicked to change password');
  }



  travellerUpdate(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let queryStr = JSON.stringify(this.traveller);
    console.log("queryStr", queryStr);

    this.authHttp.put(`${AppSettings.API_ENDPOINT}/api/traveller/${this.traveller.id}`,queryStr,options).subscribe(res => {
      console.log("update result", res.json());
      // this.profile = res.json();
        this.showAlert();
    },
    err => {
      console.log("update err", err);
    });

  }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Update!',
      subTitle: 'Your Traveller has been updated',
      buttons: [
        {
          text: 'Close',
          handler: () => {
            console.log('Close clicked');
            this.navCtrl.pop();
          }
        },

      ]
    });
    alert.present();
  }

}
