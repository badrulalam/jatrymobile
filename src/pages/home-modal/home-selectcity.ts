import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

import { Http } from '@angular/http';

import { JatryStaticData } from '../../providers/jatry-static-data';

@Component({
  selector: 'page-home-modal',
  templateUrl: 'home-modal.html'
})
export class HomeModalPage {
  tracks: Array<{name: string, isChecked: boolean}> = [];
  allCities = [];
  data: any;
  myInput:"";
  public airports : any;
  searchObj: any;
  constructor(
    public http: Http,
    public navParams: NavParams,
    public viewCtrl: ViewController
    // public jatryStaticData: JatryStaticData
  ) {

    this.airports = new JatryStaticData().getAirports();
    this.myInput="";
    // passed in array of track names that should be excluded (unchecked)
    this.searchObj = this.navParams.data.searchObj;
    console.log(this.searchObj);

    // this.initializeItems();
  }

  // initializeItems(){
  //   this.data = this.jatryStaticData.getAirports();
  //   //console.log(this.data);
  // }

  //initializeItems(){
  //  this.http.get('/assets/data/place.json').subscribe(res => {
  //    this.data = res.json();
  //  });
  //}

  getItems(input) {
    // this.data = [];
    if(input.length >=3){
      if (input && input.trim() != '') {
        this.data = this.airports.filter((item) => {
          return (JSON.stringify(item).toLowerCase().indexOf(input.toLowerCase()) > -1);
        });
      }
    }else{
      this.data = [];
    }
    return;

  }

  meSelected(city){
    console.log("city",city);
    this.viewCtrl.dismiss(city);
  }

  dismiss(data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.viewCtrl.dismiss("cancel");
  }

  changeTravellers(travellerType,opType){

    var oldAdult, oldChild, oldInfant;

    oldAdult=this.searchObj.adults;
    oldChild=this.searchObj.childs;
    oldInfant=this.searchObj.infant;

    if(opType=='+'){
      if(travellerType=='adult')
        this.searchObj.adults++;
      else if(travellerType=='child')
        this.searchObj.childs++;
      else if(travellerType=='infant')
        this.searchObj.infant++;
    }
    else{
      if(travellerType=='adult')
        this.searchObj.adults--;
      else if(travellerType=='child')
        this.searchObj.childs--;
      else if(travellerType=='infant')
        this.searchObj.infant--;
    }

    if (this.searchObj.adults + this.searchObj.childs > 9) {
      this.searchObj.adults = oldAdult;
      this.searchObj.childs = oldChild;
    }
    if (this.searchObj.adults < 1) {
      this.searchObj.adults = 1;
    }
    if (this.searchObj.adults < this.searchObj.infant) {
      this.searchObj.infant = this.searchObj.adults;
    }
  }
}
