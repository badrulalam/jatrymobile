import { Component } from '@angular/core';

import { App, ModalController, NavController, NavParams } from 'ionic-angular';
import { Http} from '@angular/http';

/*
 To learn how to use third party libs in an
 Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
 */
//import moment from 'moment';

//import { PaymentPage } from '../payment/payment';
import { JatryStaticData } from '../../providers/jatry-static-data';


@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html'
})
export class PaymentPage {
  // the list is a child of the schedule page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element
  //@ViewChild('scheduleList', {read: List}) scheduleList: List;

  public searchParam:any={}
  public flightItem:any={};
  public travellers:any={};

  constructor(
    public http: Http,
    public app: App,
    public jatryStaticData: JatryStaticData,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams:NavParams
  ) {
    this.searchParam=navParams.data.searchParam;
    this.flightItem=navParams.data.flightItem;
    this.travellers=navParams.data.travellers;
    console.log('search params',this.searchParam);
    console.log('flight detail params',this.flightItem);
    console.log('travellers',this.travellers);
  }

  //openTravellersForm(travellerType){
  //  console.log("need to open the dialog for adding traveller fields");
  //  let modal = this.modalCtrl.create(TravellersFormPage, this.travellers);
  //  modal.present();
  //
  //  modal.onDidDismiss((data: any) => {
  //    if (data!='cancel') {
  //      this.travellers=data;
  //    }
  //  });
  //}
  gotoConfirmationSummarypage(){
    console.log("Update payment and billing info to database, and go to confirmation summary page");

  }

}
