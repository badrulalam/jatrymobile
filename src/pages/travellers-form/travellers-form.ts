import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

import { Http } from '@angular/http';

@Component({
  selector: 'page-travellers-form',
  templateUrl: 'travellers-form.html'
})
export class TravellersFormPage {
  data: any;
  myInput:"";
  travellers={adults:{},childs:{},infants:{}};

  constructor(
    public http: Http,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.myInput="";
    // passed in array of track names that should be excluded (unchecked)

    console.log(this.navParams.data);
    this.travellers=this.navParams.data;
  }

  //getItems(ev) {
  //  // Reset items back to all of the items
  //  //this.initializeItems();
  //
  //  // set val to the value of the ev target
  //  var val = ev.target.value;
  //
  //  // if the value is an empty string don't filter the items
  //  if (val && val.trim() != '') {
  //    this.data = this.data.filter((item) => {
  //      return (item.ct.toLowerCase().indexOf(val.toLowerCase()) > -1);
  //    })
  //  }
  //}

  checkValidDone(){
    //need to check each fields validity and done
    console.log(this.travellers);
    this.viewCtrl.dismiss(this.travellers);
  }

  dismiss(data?: any) {
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.viewCtrl.dismiss("cancel");
  }
}
