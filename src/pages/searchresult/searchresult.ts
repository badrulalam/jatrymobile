import { Component } from '@angular/core';

import { App, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

/*
 To learn how to use third party libs in an
 Ionic app check out our docs here: http://ionicframework.com/docs/v2/resources/third-party-libs/
 */
//import moment from 'moment';

import { SearchresultDetailPage } from '../searchresult-detail/searchresult-detail';
import { JatryStaticData } from '../../providers/jatry-static-data';
import { AppSettings } from '../../services/AppSettings.service';

@Component({
  selector: 'page-searchresult',
  templateUrl: 'searchresult.html'
})
export class SearchresultPage {
  // the list is a child of the schedule page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element
  //@ViewChild('scheduleList', {read: List}) scheduleList: List;

  public airlines=[];

  public flights:any={};

  public params:any ;

  public loadingSearch=true;

  constructor(
    //public http: Http,
    public authHttp: AuthHttp,
    public app: App,
    public jatryStaticData: JatryStaticData,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams:NavParams,
    private toastCtrl: ToastController
  ) {
    console.log('params',navParams);
    this.params=navParams.data;
    this.getAirlines();
    this.getFlights();
    this.loadingSearch=true;
    //console.log(this.flights);
  }

  showTostr(totalFound){
    let toast = this.toastCtrl.create({
      message: totalFound + ' Fligths found',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }


  getAirlines(){
    this.airlines = this.jatryStaticData.getAirlines();
  }

  getFlights(){

    if(this.params.tripType=='O'){
      delete this.params.arrivalDate;
    }

    let queryStr = JSON.stringify(this.params);
    queryStr=queryStr.replace('tripClass','class');
    console.log(queryStr);

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.authHttp.post(`${AppSettings.API_ENDPOINT}/api/trigger/sample`,queryStr,options).subscribe(res => {
      this.flights = res.json();
      this.loadingSearch=false;
      this.showTostr(this.flights.Parse.length);

    });
  }

  airlinesByIata(iata) {
    var data =  this.airlines.find(function (row) {
      return row.iata == iata;
    });

    return data ? data.airline : iata;
  };

  detailFlight(flightItem){
    this.navCtrl.push(SearchresultDetailPage, {
      flightItem: flightItem,
      searchParam:this.params
    });
  }

}
