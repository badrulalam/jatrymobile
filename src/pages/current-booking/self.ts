import { Component } from '@angular/core';

import { AlertController, NavController, ItemSliding } from 'ionic-angular';

// import { LoginPage } from '../login/login';
import { UserData } from '../../providers/user-data';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { AppSettings } from '../../services/AppSettings.service';
import 'rxjs/add/operator/map';
// import { Observable } from "rxjs/Observable";
import { CurrentBookingDetailsPage } from '../current-booking/self-details';
@Component({
  selector: 'currentbooking',
  templateUrl: 'self.html'
})
export class CurrentBookingPage {
  username: string;
  userObj:any={};
  currentBooking:any = [];
  // traveller:any = {};

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public userData: UserData,
    public authHttp: AuthHttp
  ) {
    this.getProfile();
  }
  getProfile(){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.authHttp.get(`${AppSettings.API_ENDPOINT}/api/flight/current-booking`,options).subscribe(res => {
      console.log(res.json());
      this.currentBooking = res.json();

    });
  }
  getLoggedInUser(){
    //loggedin uses should be fetched from api
    this.userObj={};
  }

  ngAfterViewInit() {
    this.getUsername();
  }

  updatePicture() {
    console.log('Clicked to update picture');
  }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  changeUsername() {
    let alert = this.alertCtrl.create({
      title: 'Change Username',
      buttons: [
        'Cancel'
      ]
    });
    alert.addInput({
      name: 'username',
      value: this.username,
      placeholder: 'username'
    });
    alert.addButton({
      text: 'Ok',
      handler: data => {
        this.userData.setUsername(data.username);
        this.getUsername();
      }
    });

    alert.present();
  }

  getUsername() {
    this.userData.getUsername().then((username) => {
      this.username = username;
    });
  }

  changePassword() {
    console.log('Clicked to change password');
  }


  // profileUpdate(){
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //   let queryStr = JSON.stringify(this.profile);
  //   console.log("queryStr", queryStr);
  //
  //   this.authHttp.put(`${AppSettings.API_ENDPOINT}/api/profile`,queryStr,options).subscribe(res => {
  //     console.log("update", res.json());
  //     // this.profile = res.json();
  //
  //   });
  //   this.showAlert();
  // }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Update!',
      subTitle: 'Your profile has been updated',
      buttons: ['Close']
    });
    alert.present();
  }
  currentBookingDetails(cbItem){
    this.navCtrl.push(CurrentBookingDetailsPage, {
      cbItem: cbItem,
      
    });
  }

  onPageWillEnter() {
    this.getProfile();
  }
  deleteTraveller(item, slidingItem: ItemSliding){
    console.log(item);
    this.showConfirm(item, slidingItem);
    
  }
  showConfirm(item , slidingItem: ItemSliding) {
    let confirm = this.alertCtrl.create({
      title: 'Use this lightsaber?',
      message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
            slidingItem.close();
          }
        },
        {
          text: 'Agree',
          handler: () => {
            let travellerIndex =  this.currentBooking.indexOf(item);
            if(travellerIndex !== -1){
              this.deleteTravellerByIndex(item.id).subscribe(res => {
                  console.log(res.json());
                  this.currentBooking.splice(travellerIndex, 1);
                  confirm.dismiss();
                  return true;
                },
                err => {
                  console.log("something wrong", err);
                  confirm.dismiss();
                  return true;
                });

            }
            return false;
          }
        }
      ]
    });
    confirm.present();
  }

  deleteTravellerByIndex(id){

    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.authHttp.delete(`${AppSettings.API_ENDPOINT}/api/traveller/${id}`,options);

  }
}
