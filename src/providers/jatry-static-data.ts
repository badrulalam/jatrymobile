import { Injectable } from '@angular/core';


@Injectable()
export class JatryStaticData {

  constructor() {}

  getAirports() {
    return [{
      "ac": "DEL",
      "an": "Indira Gandhi",
      "han": "इंदिरा गांधी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "New Delhi",
      "hct": "नई दिल्ली"
    }, {
      "ac": "BOM",
      "an": "Chatrapati Shivaji",
      "han": "छत्रपति शिवाजी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Mumbai",
      "hct": "मुंबई"
    }, {
      "ac": "GOI",
      "an": "Dabolim",
      "han": "डाबोलीम",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Goa",
      "hct": "गोआ"
    }, {
      "ac": "BLR",
      "an": "Bengaluru",
      "han": "बेंगलुरु",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bangalore",
      "hct": "बंगलोर"
    }, {
      "ac": "MAA",
      "an": "Chennai",
      "han": "चेन्नाई",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Chennai",
      "hct": "चेन्नई"
    }, {
      "ac": "CCU",
      "an": "Netaji Subhas Chandra Bose",
      "han": "नेताजी सु�ाष चंद्र बोस",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kolkata",
      "hct": "कोलकटा"
    }, {
      "ac": "RDP",
      "an": "Kazi Nazrul Islam",
      "han": "काजी नजरूल इस्लाम",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Durgapur",
      "hct": "दुर्गापुर"
    }, {
      "ac": "HYD",
      "an": "Shamshabad Rajiv Gandhi",
      "han": "शम्शाबाद राजीव गांधी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Hyderabad",
      "hct": "हैदराबाद"
    }, {
      "ac": "PNQ",
      "an": "Lohegaon",
      "han": "लोहेगांव",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Pune",
      "hct": "पुणे"
    }, {
      "ac": "AMD",
      "an": "Sardar Vallabh Bhai Patel",
      "han": "सरदार वल� �ाई पटेल",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Ahmedabad",
      "hct": "अहमदाबाद"
    }, {
      "ac": "LKO",
      "an": "Amausi",
      "han": "अमौसी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Lucknow",
      "hct": "लखनऊ"
    }, {
      "ac": "COK",
      "an": "Cochin",
      "han": "कोचिन",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kochi",
      "hct": "कोची"
    }, {
      "ac": "DXB",
      "an": "Dubai",
      "han": "दुबाई",
      "cn": "United Arab Emirates",
      "hcn": "संयुक्त‌ अर‌ब‌ अमीरात‌",
      "cc": "AE",
      "ct": "Dubai",
      "hct": "दुबई"
    }, {
      "ac": "XNB",
      "an": "Dubai Bus Station",
      "han": "दुबाई बस स्टेशन",
      "cn": "United Arab Emirates",
      "hcn": "संयुक्त‌ अर‌ब‌ अमीरात‌",
      "cc": "AE",
      "ct": "Dubai Bus Station",
      "hct": "दुबई बस स्टेशन"
    }, {
      "ac": "PAT",
      "an": "Jai Prakash Narayan",
      "han": "जय प्रकाश नारायण",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Patna",
      "hct": "पटना"
    }, {
      "ac": "JAI",
      "an": "Sanganeer",
      "han": "संगानीर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Jaipur",
      "hct": "जयपुर"
    }, {
      "ac": "SXR",
      "an": "Srinagar",
      "han": "श्रीनगर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Srinagar",
      "hct": "श्रीनगर"
    }, {
      "ac": "BBI",
      "an": "Biju Patnaik",
      "han": "बीजू पटनायक",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bhubaneswar",
      "hct": "�ुबनेश्वर"
    }, {
      "ac": "GAU",
      "an": "Lokpriya Gopinath Bordoloi",
      "han": "लोकप्रिया गोपीनाथ बोर्डोलोई",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Guwahati",
      "hct": "गुवाहाटी"
    }, {
      "ac": "SIN",
      "an": "Changi",
      "han": "चांगी",
      "cn": "Singapore",
      "hcn": "सिंगापुर‌",
      "cc": "SG",
      "ct": "Singapore",
      "hct": "सिंगापुर"
    }, {
      "ac": "BKK",
      "an": "Suvarnabhumi",
      "han": "सुवर्णा�ूमि",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Bangkok",
      "hct": "बैंगकोक"
    }, {
      "ac": "IXC",
      "an": "Chandigarh",
      "han": "चंडीगढ़",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Chandigarh",
      "hct": "चंडीगढ़"
    }, {
      "ac": "CDP",
      "an": "Cuddapah",
      "han": "कुडप्पा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Cuddapah",
      "hct": "कुडप्पा"
    }, {
      "ac": "NAG",
      "an": "Dr Ambedkar",
      "han": "डा. आंबेडकर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Nagpur",
      "hct": "नागपूर"
    }, {
      "ac": "IDR",
      "an": "Devi Ahilya Bai Holkar",
      "han": "देवी अहिल्या बाई होल्कर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Indore",
      "hct": "इंदौर"
    }, {
      "ac": "CJB",
      "an": "Peelamedu",
      "han": "पीलेमदू",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Coimbatore",
      "hct": "कोइम्बटूर"
    }, {
      "ac": "VTZ",
      "an": "Vishakhapatnam",
      "han": "विशाखापट्नम",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Visakhapatnam",
      "hct": "विशाखपट्नम"
    }, {
      "ac": "TRV",
      "an": "Thiruvananthapuram",
      "han": "थीरुवनंथापूरम",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Trivandrum",
      "hct": "त्रिवेन्द्रम"
    }, {
      "ac": "IXJ",
      "an": "Satwari",
      "han": "सतवरी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Jammu",
      "hct": "जम्मु"
    }, {
      "ac": "RPR",
      "an": "Raipur",
      "han": "रायपूर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Raipur",
      "hct": "रायपूर"
    }, {
      "ac": "IXB",
      "an": "Bagdogra",
      "han": "बागडोगरा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bagdogra",
      "hct": "बागडोगरा"
    }, {
      "ac": "VNS",
      "an": "Lal Bahadur Shastri",
      "han": "लाल बहादुर शास्त्री",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Varanasi",
      "hct": "वाराणसी"
    }, {
      "ac": "BHO",
      "an": "Raja Bhoj",
      "han": "राज �ोज",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bhopal",
      "hct": "�ोपाल"
    }, {
      "ac": "IXE",
      "an": "Bajpe",
      "han": "बाजपे",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Mangalore",
      "hct": "मंगलोर"
    }, {
      "ac": "IXR",
      "an": "Birsa Munda",
      "han": " बिरसा मुंडा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Ranchi",
      "hct": "रांची"
    }, {
      "ac": "IXZ",
      "an": "Veer Savarkar",
      "han": "वीर सावरकर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Port Blair",
      "hct": "पोर्ट ब्येयर"
    }, {
      "ac": "NYC",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New York",
      "hct": "न्यू यार्क"
    }, {
      "ac": "JFK",
      "an": "John F Kennedy",
      "han": "जॉन एफ़ केन्नेडी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New York",
      "hct": "न्यू यार्क"
    }, {
      "ac": "LGA",
      "an": "La Guardia",
      "han": "ला गुआरदिआ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New York",
      "hct": "न्यू यार्क"
    }, {
      "ac": "BDQ",
      "an": "Vadodara",
      "han": "वडोदरा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Vadodara",
      "hct": "वडोदरा"
    }, {
      "ac": "KUL",
      "an": "Kuala Lumpur",
      "han": "कुआला लमपूर",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kuala Lumpur",
      "hct": "कुआला लमपूर"
    }, {
      "ac": "SZB",
      "an": "Sultan Abdul Aziz Shah",
      "han": "सुल्तान अब्दुल अज़ीज़ शाह",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kuala Lumpur",
      "hct": "कुआला लमपूर"
    }, {
      "ac": "IXM",
      "an": "Madurai",
      "han": "मदुराई",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Madurai",
      "hct": "मदुराई"
    }, {
      "ac": "KCZ",
      "an": "Kochi",
      "han": "कोची",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kochi",
      "hct": "कोची"
    }, {
      "ac": "KTM",
      "an": "Tribuvan",
      "han": "ट्रिबुवन",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Kathmandu",
      "hct": "काठमांडू"
    }, {
      "ac": "ATQ",
      "an": "Sri Guru Ram Dass Jee",
      "han": "श्री गुरू राम दास जी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Amritsar",
      "hct": "अमृतसर"
    }, {
      "ac": "LON",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "London",
      "hct": "लंडन"
    }, {
      "ac": "YXU",
      "an": "London Municipal",
      "han": "लंडन म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "London",
      "hct": "लंडन"
    }, {
      "ac": "LHR",
      "an": "Heathrow",
      "han": "हेथ्रौ",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "London",
      "hct": "लंडन"
    }, {
      "ac": "LGW",
      "an": "Gatwick",
      "han": "गट्विक",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "London",
      "hct": "लंडन"
    }, {
      "ac": "LCY",
      "an": "London City",
      "han": "लंडन सिटी",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "London",
      "hct": "लंडन"
    }, {
      "ac": "LTN",
      "an": "Luton",
      "han": "लुटन",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "London",
      "hct": "लंडन"
    }, {
      "ac": "STN",
      "an": "Stansted",
      "han": "स्टान्स्तेद",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "London",
      "hct": "लंडन"
    }, {
      "ac": "HKG",
      "an": "Hong Kong",
      "han": "होंग कोन्ग",
      "cn": "Hong Kong",
      "hcn": "हाँग‌ काँग‌",
      "cc": "HK",
      "ct": "Hong Kong",
      "hct": "होंग कोन्ग"
    }, {
      "ac": "DOH",
      "an": "Doha",
      "han": "दोहा",
      "cn": "Qatar",
      "hcn": "क‌त‌र‌",
      "cc": "QA",
      "ct": "Doha",
      "hct": "दोहा"
    }, {
      "ac": "DED",
      "an": "Jolly Grant",
      "han": "जॉली ग्रांट",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Dehradun",
      "hct": "देहरादून"
    }, {
      "ac": "TIR",
      "an": "Tirupati",
      "han": "तिरुपति",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Tirupati",
      "hct": "तिरुपति"
    }, {
      "ac": "CMB",
      "an": "Bandaranaike",
      "han": "बन्दारनाय्के",
      "cn": "Sri Lanka",
      "hcn": "श्री लंका",
      "cc": "LK",
      "ct": "Colombo",
      "hct": "कोलोंबो"
    }, {
      "ac": "CCJ",
      "an": "Kozhikode",
      "han": "कोऴीकोडे",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Calicut",
      "hct": "कालीकट"
    }, {
      "ac": "IMF",
      "an": "Tulihal",
      "han": "तुलिहल",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Imphal",
      "hct": "इंफाल"
    }, {
      "ac": "VGA",
      "an": "Vijayawada",
      "han": "विजयवाडा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Vijayawada",
      "hct": "विजयवाडा"
    }, {
      "ac": "UDR",
      "an": "Maharana Pratap",
      "han": "महाराणा प्रताप",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Udaipur",
      "hct": "उदयपुर"
    }, {
      "ac": "IXA",
      "an": "Singerbhil",
      "han": "सिंगर्�ील",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Agartala",
      "hct": "अगरतल्ला"
    }, {
      "ac": "IXU",
      "an": "Chikkalthana",
      "han": "चीक्कल्ठना",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Aurangabad",
      "hct": "औरंगाबाद"
    }, {
      "ac": "JDH",
      "an": "Jodhpur",
      "han": "जोधपुर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Jodhpur",
      "hct": "जोधपुर"
    }, {
      "ac": "MCT",
      "an": "Seeb",
      "han": "सीब",
      "cn": "Oman",
      "hcn": "ओमान",
      "cc": "OM",
      "ct": "Muscat",
      "hct": "मस्कट"
    }, {
      "ac": "STV",
      "an": "Surat",
      "han": "सूरत",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Surat",
      "hct": "सूरत"
    }, {
      "ac": "SFO",
      "an": "San Francisco",
      "han": "सेन फ़्रान्सिस्को",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Francisco",
      "hct": "सेन फ़्रान्सिस्को"
    }, {
      "ac": "YTO",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Toronto",
      "hct": "टोरोन्टो"
    }, {
      "ac": "YYZ",
      "an": "Lester B Pearson",
      "han": "लेस्टर बी पीयरसन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Toronto",
      "hct": "टोरोन्टो"
    }, {
      "ac": "YTZ",
      "an": "City Centre APT",
      "han": "सिटी सेन्टर अपार्टमेन्ट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Toronto",
      "hct": "टोरोन्टो"
    }, {
      "ac": "HKT",
      "an": "Phuket",
      "han": "फूकेट",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Phuket",
      "hct": "फूकेट"
    }, {
      "ac": "CHI",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chicago",
      "hct": "शिकागो"
    }, {
      "ac": "ORD",
      "an": "O'Hare",
      "han": "ओहरे",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chicago",
      "hct": "शिकागो"
    }, {
      "ac": "MDW",
      "an": "Midway",
      "han": "मिडवे",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chicago",
      "hct": "शिकागो"
    }, {
      "ac": "AUH",
      "an": "Dhabi",
      "han": "धाबी",
      "cn": "United Arab Emirates",
      "hcn": "संयुक्त‌ अर‌ब‌ अमीरात‌",
      "cc": "AE",
      "ct": "Abu Dhabi",
      "hct": "अबू धाबी"
    }, {
      "ac": "JLR",
      "an": "Jabalpur",
      "han": "जबलपुर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Jabalpur",
      "hct": "जबलपुर"
    }, {
      "ac": "KWI",
      "an": "Kuwait",
      "han": "कुवैत",
      "cn": "Kuwait",
      "hcn": "कुवैत",
      "cc": "KW",
      "ct": "Kuwait",
      "hct": "कुवैत"
    }, {
      "ac": "CDG",
      "an": "Charles De Gaulle",
      "han": "चार्ल्स दे गौले",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Paris",
      "hct": "पेरिस"
    }, {
      "ac": "PAR",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Paris",
      "hct": "पेरिस"
    }, {
      "ac": "ORY",
      "an": "Orly",
      "han": "ओर्ली",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Paris",
      "hct": "पेरिस"
    }, {
      "ac": "BVA",
      "an": "Tille",
      "han": "टिले",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Beauvais",
      "hct": "बीउवैस"
    }, {
      "ac": "DIB",
      "an": "Mohanbari",
      "han": "मोहनबारी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Dibrugarh",
      "hct": "डिब्रुगढ़"
    }, {
      "ac": "SYD",
      "an": "Sydney Kingsford Smith",
      "han": "सिडनी किंग्सफोर्ड स्मिथ",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Sydney",
      "hct": "सिडनी"
    }, {
      "ac": "YQY",
      "an": "Sydney",
      "han": "सिडनी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "SYDNEY",
      "hct": "सिडनी"
    }, {
      "ac": "RAJ",
      "an": "Rajkot Civil",
      "han": "राजकोट सिवील",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Rajkot",
      "hct": "राजकोट"
    }, {
      "ac": "MLE",
      "an": "Male",
      "han": "माले",
      "cn": "Maldives",
      "hcn": "माल‌दीव‌",
      "cc": "MV",
      "ct": "Male",
      "hct": "माले"
    }, {
      "ac": "RUH",
      "an": "King Khaled",
      "han": "किंग खालिद",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Riyadh",
      "hct": "रियाध"
    }, {
      "ac": "SHJ",
      "an": "Sharjah",
      "han": "शारजाह",
      "cn": "United Arab Emirates",
      "hcn": "संयुक्त‌ अर‌ब‌ अमीरात‌",
      "cc": "AE",
      "ct": "Sharjah",
      "hct": "शारजाह"
    }, {
      "ac": "IXD",
      "an": "Bamrauli",
      "han": "बरमौली",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Allahabad",
      "hct": "इलाहाबाद"
    }, {
      "ac": "EWR",
      "an": "Newark Liberty",
      "han": "नेवार्क लिबर्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Newark",
      "hct": "नेवार्क"
    }, {
      "ac": "FRA",
      "an": "Frankfurt",
      "han": "फ़्रान्क्फ़ूरट",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Frankfurt",
      "hct": "फ़्रान्क्फ़ूरट"
    }, {
      "ac": "HHN",
      "an": "Frankfurt Hahn",
      "han": "फ़्रान्क्फ़ूरट हाह्न",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Hahn",
      "hct": "हाह्न"
    }, {
      "ac": "MEL",
      "an": "Tullamarine",
      "han": "तुलामरीन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Melbourne",
      "hct": "मेलबार्न"
    }, {
      "ac": "MLB",
      "an": "Melbourne Regional",
      "han": "मेल्बौर्णे रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Melbourne",
      "hct": "मेलबार्न"
    }, {
      "ac": "DMM",
      "an": "King Fahad",
      "han": "किंग फ़हद",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Dammam",
      "hct": "दमाम"
    }, {
      "ac": "MRU",
      "an": "Plaisancet",
      "han": "प्लाईसन्सेट",
      "cn": "Mauritius",
      "hcn": "मोरिशिय‌स‌",
      "cc": "MU",
      "ct": "Mauritius",
      "hct": "मॉरिशयस"
    }, {
      "ac": "IXS",
      "an": "Kumbhigram",
      "han": "कूम्�ीग्राम",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Silchar",
      "hct": "सिलचार"
    }, {
      "ac": "CAN",
      "an": "Baiyun",
      "han": "बाईयून",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Guangzhou",
      "hct": "गुआंग्झ़ौ"
    }, {
      "ac": "ZRH",
      "an": "Zurich",
      "han": "ज़ुरिच",
      "cn": "Switzerland",
      "hcn": "स्विट्ज़र‌लैंड‌",
      "cc": "CH",
      "ct": "Zurich",
      "hct": "ज़ुरिच"
    }, {
      "ac": "LAX",
      "an": "Los Angeles",
      "han": "लोस अंजेल्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Los Angeles",
      "hct": "लोस अंजेल्स"
    }, {
      "ac": "RJA",
      "an": "Rajahmundry",
      "han": "राजमंड्रि",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Rajahmundry",
      "hct": "राजामुंड्रि"
    }, {
      "ac": "JED",
      "an": "Jeddah",
      "han": "जेड्डाह",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Jeddah",
      "hct": "जेद्दाह"
    }, {
      "ac": "SHA",
      "an": "Hongqiao",
      "han": "होंग्क़ियाओ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Shanghai",
      "hct": "शंघाई"
    }, {
      "ac": "PVG",
      "an": "Pudong",
      "han": "पूडोंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Shanghai",
      "hct": "शंघाई"
    }, {
      "ac": "DPS",
      "an": "Ngurah Rai",
      "han": "न्गूरा राय",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Denpasar Bali",
      "hct": "द्न्पासर बाली"
    }, {
      "ac": "IST",
      "an": "Ataturk",
      "han": "अटाट्रुक",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Istanbul",
      "hct": "इस्तान्बुल"
    }, {
      "ac": "SAW",
      "an": "Sabiha Gokcen",
      "han": "सबीहा गोक्केन",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Istanbul",
      "hct": "इस्तान्बुल"
    }, {
      "ac": "AJL",
      "an": "Lengpui",
      "han": "लेन्ग्पुइ",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Aizawl",
      "hct": "ऐज़ौल"
    }, {
      "ac": "BAH",
      "an": "Bahrain",
      "han": "बहारेन",
      "cn": "Bahrain",
      "hcn": "बाहरैन",
      "cc": "BH",
      "ct": "Bahrain",
      "hct": "बहारेन"
    }, {
      "ac": "AMS",
      "an": "Schiphol",
      "han": "शिफोल",
      "cn": "Netherlands",
      "hcn": "नेद‌र‌लैंड‌",
      "cc": "NL",
      "ct": "Amsterdam",
      "hct": "अम्स्टेर्दम"
    }, {
      "ac": "AKL",
      "an": "Auckland",
      "han": "अऊक्लांद",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Auckland",
      "hct": "अऊक्लांद"
    }, {
      "ac": "IAD",
      "an": "Washington Dulles",
      "han": "वाशींग्टन दूलल्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Washington DC",
      "hct": "वाशींग्टन डीसी"
    }, {
      "ac": "WAS",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Washington",
      "hct": "वाशींग्टन"
    }, {
      "ac": "DCA",
      "an": "Ronald Reagan Washington Natl",
      "han": "रोनाल्ड रीगन वाशींग्टन नट्ल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Washington DC",
      "hct": "वाशींग्टन डीसी"
    }, {
      "ac": "GOP",
      "an": "Gorakhpur",
      "han": "गोरखपुर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Gorakhpur",
      "hct": "गोरखपुर"
    }, {
      "ac": "MNL",
      "an": "Ninoy Aquino",
      "han": "निनोय अक़िनो",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Manila",
      "hct": "मणीला"
    }, {
      "ac": "HBX",
      "an": "Hubli",
      "han": "हुबली",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Hubli",
      "hct": "हुबली"
    }, {
      "ac": "BJS",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Beijing",
      "hct": "बीजिंग"
    }, {
      "ac": "PEK",
      "an": "Beijing Capital",
      "han": "बीजिंग कॅपिटल",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Beijing",
      "hct": "बीजिंग"
    }, {
      "ac": "NAY",
      "an": "Beijing Nanyuan",
      "han": "बीजिंग नाण्युआन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Beijing",
      "hct": "बीजिंग"
    }, {
      "ac": "JKT",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Jakarta",
      "hct": "जकार्ता"
    }, {
      "ac": "CGK",
      "an": "Soekarno Hatta",
      "han": "सोयकर्नो हट्टा",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Jakarta",
      "hct": "जकार्ता"
    }, {
      "ac": "DMU",
      "an": "Dimapur",
      "han": "डिमपूर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Dimapur",
      "hct": "डिमपूर"
    }, {
      "ac": "BHJ",
      "an": "Rudra Mata",
      "han": "रूद्र माता",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bhuj",
      "hct": "�ुज"
    }, {
      "ac": "KNU",
      "an": "Chakeri",
      "han": "चकेरी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kanpur",
      "hct": "कानपुर"
    }, {
      "ac": "BOS",
      "an": "Logan",
      "han": "लोगन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Boston",
      "hct": "ब़ॉस्टन"
    }, {
      "ac": "TCR",
      "an": "Tuticorin",
      "han": "टूटिकुरीन",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Tuticorin",
      "hct": "टूटिकुरीन"
    }, {
      "ac": "DAC",
      "an": "Zia",
      "han": "ज़िआ",
      "cn": "Bangladesh",
      "hcn": "बांग्लादेश",
      "cc": "BD",
      "ct": "DHAKA",
      "hct": "ढाका"
    }, {
      "ac": "JRH",
      "an": "Rowriah",
      "han": "रौरिआ",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Jorhat",
      "hct": "जोर्हाट"
    }, {
      "ac": "JNB",
      "an": "O R Tambo",
      "han": "ओ आर तम्बो",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Johannesburg",
      "hct": "जोहान्नेस्बूरग"
    }, {
      "ac": "YVR",
      "an": "Vancouver",
      "han": "वन्कौवर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Vancouver",
      "hct": "वन्कौवर"
    }, {
      "ac": "CXH",
      "an": "Vancouver Coal Harbour",
      "han": "वन्कौवर कोल हर्बौर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Vancouver",
      "hct": "वन्कौवर"
    }, {
      "ac": "ATL",
      "an": "Hartsfield Jackson",
      "han": "हर्ट्सफ़ील्ड जैकसन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Atlanta",
      "hct": "ॲटलांटा"
    }, {
      "ac": "MYQ",
      "an": "Mysore",
      "han": "मैसूर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Mysore",
      "hct": "मैसूर"
    }, {
      "ac": "MUC",
      "an": "Munich",
      "han": "मूणीच",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Munich",
      "hct": "मूणीच"
    }, {
      "ac": "NBO",
      "an": "Jomo kenyatta ",
      "han": "जोमो केन्याटा",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Nairobi",
      "hct": "नाय्रोबी"
    }, {
      "ac": "WIL",
      "an": "Wilson",
      "han": "विलसन",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Nairobi",
      "hct": "नाय्रोबी"
    }, {
      "ac": "MOW",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Moscow",
      "hct": "मोस्को"
    }, {
      "ac": "SVO",
      "an": "Sheremetyevo",
      "han": "शेरेमेट्येवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Moscow",
      "hct": "मोस्को"
    }, {
      "ac": "DME",
      "an": "Domodedovo",
      "han": "दोमोडेदोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Moscow",
      "hct": "मोस्को"
    }, {
      "ac": "VKO",
      "an": "Vnukovo",
      "han": "व्नुकोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Moscow",
      "hct": "मोस्को"
    }, {
      "ac": "BKA",
      "an": "Bykovo",
      "han": "ब्य्कोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Moscow",
      "hct": "मोस्को"
    }, {
      "ac": "AGX",
      "an": "Agatti",
      "han": "आगटी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Agatti Island",
      "hct": "आगटी आयलॅन्ड"
    }, {
      "ac": "MIL",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Milan",
      "hct": "मिलन"
    }, {
      "ac": "MXP",
      "an": "Malpensa",
      "han": "माल्पेन्सा",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Milan",
      "hct": "मिलन"
    }, {
      "ac": "PMF",
      "an": "Parma",
      "han": "परमा",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Parma",
      "hct": "परमा"
    }, {
      "ac": "BGY",
      "an": "Orio Al Serio",
      "han": "ओरिओ अल सेरिओ",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Milan",
      "hct": "मिलन"
    }, {
      "ac": "LIN",
      "an": "Linate",
      "han": "लीनाटे",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Milan",
      "hct": "मिलन"
    }, {
      "ac": "MAN",
      "an": "Manchester",
      "han": "मॅन्चेस्टर",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Manchester",
      "hct": "मॅन्चेस्टर"
    }, {
      "ac": "MHT",
      "an": "Manchester",
      "han": "मॅन्चेस्टर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "MANCHESTER",
      "hct": "मॅन्चेस्टर"
    }, {
      "ac": "SEL",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Seoul",
      "hct": "सियोउल"
    }, {
      "ac": "ICN",
      "an": "Incheon",
      "han": "इन्चियोन",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Seoul",
      "hct": "सियोउल"
    }, {
      "ac": "GMP",
      "an": "Gimpo",
      "han": "गिम्पो",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Seoul",
      "hct": "सियोउल"
    }, {
      "ac": "BCN",
      "an": "Barcelona",
      "han": "बार्केलोना",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Barcelona",
      "hct": "बार्केलोना"
    }, {
      "ac": "BLA",
      "an": "Gen J A Anzoategui",
      "han": "जन जे ए अन्ज़ोआटेगुइ",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Barcelona",
      "hct": "बार्केलोना"
    }, {
      "ac": "JGA",
      "an": "Govardhanpur",
      "han": "गोवर्धनपुर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Jamnagar",
      "hct": "जामनगर"
    }, {
      "ac": "BRU",
      "an": "Brussels National",
      "han": "ब्रस्सेल्स नेशनल",
      "cn": "Belgium",
      "hcn": "बेल्जिय‌म‌",
      "cc": "BE",
      "ct": "Brussels",
      "hct": "ब्रस्सेल्स"
    }, {
      "ac": "CRL",
      "an": "Brussels South",
      "han": "ब्रस्सेल्स साउथ",
      "cn": "Belgium",
      "hcn": "बेल्जिय‌म‌",
      "cc": "BE",
      "ct": "Charleroi",
      "hct": "चार्लरॉय"
    }, {
      "ac": "KUU",
      "an": "Bhuntar",
      "han": "�ुंतर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kulu",
      "hct": "कुल्लु"
    }, {
      "ac": "ROM",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Rome",
      "hct": "रोम"
    }, {
      "ac": "FCO",
      "an": "Fiumicino",
      "han": "फ़ीऊमिसिनो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Rome",
      "hct": "रोम"
    }, {
      "ac": "CIA",
      "an": "",
      "han": "",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Rome",
      "hct": "रोम"
    }, {
      "ac": "AGR",
      "an": "Kheria",
      "han": "खेरिया",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Agra",
      "hct": "आगरा"
    }, {
      "ac": "TYO",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Tokyo",
      "hct": "तोक्यो"
    }, {
      "ac": "NRT",
      "an": "Narita",
      "han": "नरिता",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Tokyo",
      "hct": "तोक्यो"
    }, {
      "ac": "HND",
      "an": "Haneda",
      "han": "हनेदा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Tokyo",
      "hct": "तोक्यो"
    }, {
      "ac": "ATH",
      "an": "Eleftherios Venizelos",
      "han": "एलेफ़्ठेरिओस वेनिज़ेलोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Athens",
      "hct": "अठेन्स"
    }, {
      "ac": "AHN",
      "an": "Athens Municipal",
      "han": "अठेन्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Athens",
      "hct": "अठेन्स"
    }, {
      "ac": "SEA",
      "an": "Seattle Tacoma",
      "han": "सीत्ल तकोमा",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Seattle",
      "hct": "सीत्ल"
    }, {
      "ac": "LKE",
      "an": "Kenmore Air Harbor Seaplane Base",
      "han": "केनमोरे एअर हरबोर सीपलाने बेस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Seattle",
      "hct": "सीत्ल"
    }, {
      "ac": "PNY",
      "an": "Pondicherry",
      "han": "पोन्डिचेर्री",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Pondicherry",
      "hct": "पोन्डिचेर्री"
    }, {
      "ac": "USM",
      "an": "Koh Samui",
      "han": "कोह समुइ",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Koh Samui",
      "hct": "कोह समुइ"
    }, {
      "ac": "IXG",
      "an": "Sambre",
      "han": "साम्ब्रे",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Belgaum",
      "hct": "बेलगांव"
    }, {
      "ac": "VIE",
      "an": "Vienna",
      "han": "विएन्ना",
      "cn": "Austria",
      "hcn": "ऑस्ट्रिया",
      "cc": "AT",
      "ct": "Vienna",
      "hct": "विएन्ना"
    }, {
      "ac": "GVA",
      "an": "Geneve Cointrin",
      "han": "जेनेवे कोईंट्रिन",
      "cn": "Switzerland",
      "hcn": "स्विट्ज़र‌लैंड‌",
      "cc": "CH",
      "ct": "Geneva",
      "hct": "जीनीवा"
    }, {
      "ac": "CAI",
      "an": "Cairo",
      "han": "काईरो",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Cairo",
      "hct": "काईरो"
    }, {
      "ac": "DTT",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Detroit",
      "hct": "डेट्रॉईट"
    }, {
      "ac": "DTW",
      "an": "Detroit Metro Wayne Cnty",
      "han": "डेट्रॉईट मेट्रो वेन क्न्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Detroit",
      "hct": "डेट्रॉईट"
    }, {
      "ac": "BER",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Berlin",
      "hct": "बर्लिन"
    }, {
      "ac": "TXL",
      "an": "Tegel",
      "han": "टेगेल",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Berlin",
      "hct": "बर्लिन"
    }, {
      "ac": "SXF",
      "an": "Schoenefeld",
      "han": "शोयनेफ़ेल्ड",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Berlin",
      "hct": "बर्लिन"
    }, {
      "ac": "THF",
      "an": "Tempelhof",
      "han": "टेंपेल्होफ़",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Berlin",
      "hct": "बर्लिन"
    }, {
      "ac": "MAD",
      "an": "Barajas",
      "han": "बाड़ाजस",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Madrid",
      "hct": "मद्रिद"
    }, {
      "ac": "NDC",
      "an": "Nanded",
      "han": "नान्डेद",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Nanded",
      "hct": "नान्डेद"
    }, {
      "ac": "BHU",
      "an": "Bhavnagar",
      "han": "�ावनगर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bhavnagar",
      "hct": "�ावनगर"
    }, {
      "ac": "STO",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Stockholm",
      "hct": "स्टोखोल्म"
    }, {
      "ac": "ARN",
      "an": "Arlanda",
      "han": "आर्लांदा",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Stockholm",
      "hct": "स्टोखोल्म"
    }, {
      "ac": "BMA",
      "an": "Bromma",
      "han": "ब्रोम्मा",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Stockholm",
      "hct": "स्टोखोल्म"
    }, {
      "ac": "NYO",
      "an": "Skavsta",
      "han": "स्कव्स्टा",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Stockholm",
      "hct": "स्टोखोल्म"
    }, {
      "ac": "VST",
      "an": "Hasslo",
      "han": "हस्स्लो",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Stockholm",
      "hct": "स्टोखोल्म"
    }, {
      "ac": "CPH",
      "an": "Copenhagen",
      "han": "कोपेन्हाजन",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Copenhagen",
      "hct": "कोपेन्हेगेन"
    }, {
      "ac": "HOU",
      "an": "Houston Hobby",
      "han": "हौस्टन हॉबी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "HOUSTON",
      "hct": "हौस्टन"
    }, {
      "ac": "IAH",
      "an": "George Bush Intercontinental",
      "han": "जॉर्ज बुश इंटर्कोन्टिनेन्टल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Houston",
      "hct": "हौस्टन"
    }, {
      "ac": "BNE",
      "an": "Brisbane",
      "han": "ब्रिस्बेन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Brisbane",
      "hct": "ब्रिसेबेन"
    }, {
      "ac": "DHM",
      "an": "Gaggal",
      "han": "गागगल",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Dharamsala",
      "hct": "धरमसाला"
    }, {
      "ac": "CPT",
      "an": "Cape Town",
      "han": "केप टाउन",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Cape Town",
      "hct": "केप टाउन"
    }, {
      "ac": "HJR",
      "an": "Khajuraho",
      "han": "खजुरहो",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Khajuraho",
      "hct": "खजुराहो"
    }, {
      "ac": "KBP",
      "an": "Borispol",
      "han": "बोरिसपोल",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Kiev",
      "hct": "कीव"
    }, {
      "ac": "IEV",
      "an": "Zhulhany",
      "han": "जूलहानी",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Kiev",
      "hct": "कीव"
    }, {
      "ac": "LGK",
      "an": "Langkawi",
      "han": "लंग्कौई",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Langkawi",
      "hct": "लंग्कौई"
    }, {
      "ac": "GWL",
      "an": "Gwalior",
      "han": "ग्वालियर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Gwalior",
      "hct": "ग्वालियर"
    }, {
      "ac": "PER",
      "an": "Perth",
      "han": "पर्थ",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Perth",
      "hct": "पर्थ"
    }, {
      "ac": "PRG",
      "an": "Ruzyne",
      "han": "रुज़्य्न",
      "cn": "Czech Republic",
      "hcn": "क्ज़ेच रीपबलीक",
      "cc": "CZ",
      "ct": "Prague",
      "hct": "प्रगुए"
    }, {
      "ac": "DUB",
      "an": "Dublin",
      "han": "दुब्लीन",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Dublin",
      "hct": "दुब्लीन"
    }, {
      "ac": "BHX",
      "an": "Birmingham",
      "han": "बीर्मिंघम",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Birmingham",
      "hct": "बीर्मिंघम"
    }, {
      "ac": "BHM",
      "an": "Birmingham",
      "han": "बीर्मिंघम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Birmingham",
      "hct": "बीर्मिंघम"
    }, {
      "ac": "PBH",
      "an": "",
      "han": "",
      "cn": "Bhutan",
      "hcn": "�ूटान",
      "cc": "BT",
      "ct": "Paro",
      "hct": "पारो"
    }, {
      "ac": "LAS",
      "an": "McCarran",
      "han": "म्क्कार्रन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Las Vegas",
      "hct": "लस वेगस"
    }, {
      "ac": "PHL",
      "an": "Philadelphia",
      "han": "फीलाडेल्फिआ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Philadelphia",
      "hct": "फीलाडेल्फिआ"
    }, {
      "ac": "TTN",
      "an": "Trenton Mercer",
      "han": "ट्रेन्टन मर्सर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Philadelphia",
      "hct": "फीलाडेल्फिआ"
    }, {
      "ac": "PHX",
      "an": "Sky Harbor",
      "han": "स्काई हरबोर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Phoenix",
      "hct": "फोयनिक्स"
    }, {
      "ac": "MSP",
      "an": "Minneapolis St Paul",
      "han": "मिन्नीपोलिस स्त्रीट पॉल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Minneapolis St. Paul",
      "hct": "मिन्नीपोलिस स्त्रीट. पॉल"
    }, {
      "ac": "YYC",
      "an": "Calgary",
      "han": "कलगारी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Calgary",
      "hct": "कलगारी"
    }, {
      "ac": "VCE",
      "an": "Marco Polo",
      "han": "मार्को पोलो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Venice",
      "hct": "वेनिस"
    }, {
      "ac": "TSF",
      "an": "Treviso",
      "han": "त्रेविसो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Venice",
      "hct": "वेनिस"
    }, {
      "ac": "HGH",
      "an": "Hangzhou",
      "han": "हेंग्ऴौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Hangzhou",
      "hct": "हैंग्झै"
    }, {
      "ac": "SDV",
      "an": "Dov",
      "han": "डोव",
      "cn": "Israel",
      "hcn": "इसरायल",
      "cc": "IL",
      "ct": "Tel Aviv",
      "hct": "टेल अविव"
    }, {
      "ac": "TLV",
      "an": "Ben Gurion",
      "han": "बेन गूरिओन",
      "cn": "Israel",
      "hcn": "इसरायल",
      "cc": "IL",
      "ct": "Tel Aviv Yafo",
      "hct": "टेल अविव याफ़ो"
    }, {
      "ac": "KBV",
      "an": "Krabi",
      "han": "करबी",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Krabi",
      "hct": "करबी"
    }, {
      "ac": "RDU",
      "an": "Raleigh Durham",
      "han": "रालीघ दर्हम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Raleigh",
      "hct": "रालीघ"
    }, {
      "ac": "MIA",
      "an": "Miami",
      "han": "मियामी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Miami",
      "hct": "मियामी"
    }, {
      "ac": "ORL",
      "an": "Executive",
      "han": "एक्सीक्युटीव",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Orlando",
      "hct": "ओर्लांदो"
    }, {
      "ac": "MCO",
      "an": "Orlando",
      "han": "ओर्लांडो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Orlando",
      "hct": "ओर्लांदो"
    }, {
      "ac": "TPE",
      "an": "Taiwan Taoyuan",
      "han": "ताईवान ताव्युआन",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Taipei",
      "hct": "ताईपी"
    }, {
      "ac": "TSA",
      "an": "Sungshan",
      "han": "संगशान",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Taipei",
      "hct": "ताईपी"
    }, {
      "ac": "HEL",
      "an": "Helsinki",
      "han": "हेल्सीन्की",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Helsinki",
      "hct": "हेल्सीन्की"
    }, {
      "ac": "DUS",
      "an": "Dusseldorf",
      "han": "दुस्सेल्डोर्फ़",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Dusseldorf",
      "hct": "दुस्सेल्डोर्फ़"
    }, {
      "ac": "YMQ",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Montreal",
      "hct": "मॉन्ट्रिअल"
    }, {
      "ac": "YUL",
      "an": "Pierre Elliott Trudeau",
      "han": "पिएर एल्लिओट्ट त्रुडीऊ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Montreal",
      "hct": "मॉन्ट्रिअल"
    }, {
      "ac": "DAR",
      "an": "Es Salaam",
      "han": "एस सेलाम",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Dar Es Salaam",
      "hct": "दार एस सेलाम"
    }, {
      "ac": "RIO",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Rio de Janeiro",
      "hct": "रिओ दे जणेरो"
    }, {
      "ac": "GIG",
      "an": "Rio Internacional",
      "han": "रिओ इंटर्नेसियोनल",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Rio de Janeiro",
      "hct": "रिओ दे जणेरो"
    }, {
      "ac": "SDU",
      "an": "Santos Dumont",
      "han": "संतोष दूमोन्ट",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Rio de Janeiro",
      "hct": "रिओ दे जणेरो"
    }, {
      "ac": "PBD",
      "an": "Porbandar",
      "han": "पोरबन्दर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Porbandar",
      "hct": "पोरबन्दर"
    }, {
      "ac": "LOS",
      "an": "Murtala Muhammed",
      "han": "मूरतअला मुहमद",
      "cn": "Nigeria",
      "hcn": "निगरिया",
      "cc": "NG",
      "ct": "Lagos",
      "hct": "लागोस"
    }, {
      "ac": "OSL",
      "an": "Oslo",
      "han": "ओस्लो",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Oslo",
      "hct": "ओस्लो"
    }, {
      "ac": "SGN",
      "an": "Tan Son Nhut",
      "han": "तन सन न्हुत",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Ho Chi Minh City",
      "hct": "हो ची मिन्ह सिटी"
    }, {
      "ac": "TRF",
      "an": "Sandefjord",
      "han": "संडेफ़्जोर्ड",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Oslo",
      "hct": "ओस्लो"
    }, {
      "ac": "TAS",
      "an": "Vostochny",
      "han": "वोस्टोच्नी",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Tashkent",
      "hct": "ताश्केंत"
    }, {
      "ac": "BUD",
      "an": "Ferihegy",
      "han": "फ़ेरिहेगी",
      "cn": "Hungary",
      "hcn": "हुन्गारी",
      "cc": "HU",
      "ct": "Budapest",
      "hct": "बडपेस्ट"
    }, {
      "ac": "SAO",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sao Paulo",
      "hct": "साव पौलो"
    }, {
      "ac": "GRU",
      "an": "Guarulhos",
      "han": "गुआरुल्होस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sao Paulo",
      "hct": "साव पौलो"
    }, {
      "ac": "CGH",
      "an": "Cangonhas",
      "han": "कोन्गोन्हास",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sao Paulo",
      "hct": "साव पौलो"
    }, {
      "ac": "VCP",
      "an": "Viracopos",
      "han": "विरकोपोस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sao Paulo",
      "hct": "साव पौलो"
    }, {
      "ac": "YEA",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Edmonton",
      "hct": "एद्मोन्टन"
    }, {
      "ac": "YEG",
      "an": "Edmonton",
      "han": "एद्मोन्टन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Edmonton",
      "hct": "एद्मोन्टन"
    }, {
      "ac": "SEZ",
      "an": "Seychelles",
      "han": "सीचेल्स",
      "cn": "Seychelles",
      "hcn": "सीचेल्स",
      "cc": "SC",
      "ct": "Mahe Island",
      "hct": "माहे आयलॅन्ड"
    }, {
      "ac": "CLT",
      "an": "Charlotte Douglas",
      "han": "चार्लोटे डग्लस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Charlotte",
      "hct": "चार्लोटे"
    }, {
      "ac": "OSA",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Osaka",
      "hct": "ओसाका"
    }, {
      "ac": "KIX",
      "an": "Kansai",
      "han": "कन्साई",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Osaka",
      "hct": "ओसाका"
    }, {
      "ac": "ITM",
      "an": "Itami",
      "han": "इतमी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Osaka",
      "hct": "ओसाका"
    }, {
      "ac": "KBL",
      "an": "Kabul",
      "han": "काबुल",
      "cn": "Afghanistan",
      "hcn": "अफ़ग़ानिस्तान‌",
      "cc": "AF",
      "ct": "Kabul",
      "hct": "काबुल"
    }, {
      "ac": "LIS",
      "an": "Lisboa",
      "han": "लिस्बोआ",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Lisbon",
      "hct": "लिस्बोन"
    }, {
      "ac": "LUH",
      "an": "Ludhiana",
      "han": "लुधियाणा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Ludhiana",
      "hct": "लुधियाणा"
    }, {
      "ac": "HAN",
      "an": "Noibai",
      "han": "नोईबाई",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Hanoi",
      "hct": "हानोई"
    }, {
      "ac": "ADL",
      "an": "Adelaide",
      "han": "आडेलाईडे",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Adelaide",
      "hct": "आडेलाईडे"
    }, {
      "ac": "PIT",
      "an": "Pittsburgh",
      "han": "पित्स्बूर्ग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pittsburgh",
      "hct": "पित्स्बूरघ"
    }, {
      "ac": "MFM",
      "an": "Macau",
      "han": "मेकाऊ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Macau",
      "hct": "मेकाऊ"
    }, {
      "ac": "XZM",
      "an": "",
      "han": "",
      "cn": "Macao",
      "hcn": "मेकाव",
      "cc": "MO",
      "ct": "Macau",
      "hct": "मेकाऊ"
    }, {
      "ac": "WAW",
      "an": "Warsaw",
      "han": "वार्सौ",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Warsaw",
      "hct": "वार्सौ"
    }, {
      "ac": "ADJ",
      "an": "",
      "han": "",
      "cn": "Jordan",
      "hcn": "जॉर्डन",
      "cc": "JO",
      "ct": "Amman",
      "hct": "अम्मन"
    }, {
      "ac": "AMM",
      "an": "Queen Alia",
      "han": "क्वीन अलिआ",
      "cn": "Jordan",
      "hcn": "जॉर्डन",
      "cc": "JO",
      "ct": "Amman",
      "hct": "अम्मन"
    }, {
      "ac": "GAY",
      "an": "Gaya",
      "han": "गया",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bodhgaya",
      "hct": "बोधगया"
    }, {
      "ac": "GLA",
      "an": "Glasgow",
      "han": "ग्लासगो",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Glasgow",
      "hct": "ग्लासगो"
    }, {
      "ac": "PIK",
      "an": "Prestwick",
      "han": "प्रेस्ट्विक",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Glasgow",
      "hct": "ग्लासगो"
    }, {
      "ac": "GGW",
      "an": "International Glasgow",
      "han": "इंटरनेशनल ग्लासगो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Glasgow",
      "hct": "ग्लासगो"
    }, {
      "ac": "SAN",
      "an": "Lindbergh",
      "han": "लीन्द्बर्घ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Diego",
      "hct": "सेन दीएगो"
    }, {
      "ac": "HAM",
      "an": "Fuhlsbuettel",
      "han": "फ़ुह्ल्स्बुएटेल",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Hamburg",
      "hct": "हम्बूरग"
    }, {
      "ac": "TAB",
      "an": "Crown Point",
      "han": "क्राउन पोईंट",
      "cn": "Trinidad and Tobago",
      "hcn": "ट्रिनिडाद एण्द टोबगो",
      "cc": "TT",
      "ct": "Tobago",
      "hct": "टोबगो"
    }, {
      "ac": "PNH",
      "an": "Pochentong",
      "han": "पोचेन्टोन्ग",
      "cn": "Cambodia",
      "hcn": "कम्बोडिआ",
      "cc": "KH",
      "ct": "Phnom Penh",
      "hct": "फ्नोम पेन्ह"
    }, {
      "ac": "MEX",
      "an": "Benito Juarez",
      "han": "बेनिटो जुआरेज़",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Mexico City",
      "hct": "मेक्सिको सिटी"
    }, {
      "ac": "EDI",
      "an": "Edinburgh",
      "han": "एडीन्बूर्ग",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Edinburgh Airport",
      "hct": "एडीन्बूरघ एअरप़ॉर्ट"
    }, {
      "ac": "DEN",
      "an": "Denver",
      "han": "डेन्वर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Denver",
      "hct": "डेन्वर"
    }, {
      "ac": "SSE",
      "an": "Sholapur ",
      "han": "सोलापुर ",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Sholapur",
      "hct": "सोलापुर"
    }, {
      "ac": "SJO",
      "an": "Juan Santa Maria",
      "han": "जुआन शांता मारिआ",
      "cn": "Costa Rica",
      "hcn": "कोस्टा रिका",
      "cc": "CR",
      "ct": "San Jose",
      "hct": "सेन जोस"
    }, {
      "ac": "SJI",
      "an": "Antique",
      "han": "आंटिक़",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "San Jose",
      "hct": "सेन जोस"
    }, {
      "ac": "SJC",
      "an": "San Jose",
      "han": "सेन जोस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Jose",
      "hct": "सेन जोस"
    }, {
      "ac": "ACC",
      "an": "Kotoka",
      "han": "कोटोका",
      "cn": "Ghana",
      "hcn": "घन",
      "cc": "GH",
      "ct": "Accra",
      "hct": "आक्रा"
    }, {
      "ac": "WLG",
      "an": "Wellington",
      "han": "वेलींग्टन",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Wellington",
      "hct": "वेलींग्टन"
    }, {
      "ac": "ADD",
      "an": "Bole",
      "han": "बोले",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Addis Ababa",
      "hct": "अडिस आबाबा"
    }, {
      "ac": "CHC",
      "an": "Christchurch",
      "han": "क्राइस्टचर्च",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Christchurch",
      "hct": "क्राइस्टचर्च"
    }, {
      "ac": "NAN",
      "an": "Nadi",
      "han": "नदी",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Nadi",
      "hct": "नदी"
    }, {
      "ac": "BPN",
      "an": "Sepinggan",
      "han": "सेपींगगन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Balikpapan",
      "hct": "बालिक्पापन"
    }, {
      "ac": "STR",
      "an": "Stuttgart",
      "han": "स्तुत्गर्ट",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Stuttgart",
      "hct": "स्तुत्गर्ट"
    }, {
      "ac": "HAJ",
      "an": "Hanover",
      "han": "हानोवर",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Hannover",
      "hct": "हेन्नोवर"
    }, {
      "ac": "LED",
      "an": "Pulkovo",
      "han": "पूलकोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "St Petersburg",
      "hct": "स्त्रीट पेटर्स्बूरग"
    }, {
      "ac": "PIE",
      "an": "St Petersburg Clearwater",
      "han": "स्त्रीट पेटर्स्बूरग क्लिअरवाटर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "St Petersburg",
      "hct": "स्त्रीट पेटर्स्बूरग"
    }, {
      "ac": "NAS",
      "an": "Nassau",
      "han": "नास्सऊ",
      "cn": "Bahamas",
      "hcn": "बहमेस",
      "cc": "BS",
      "ct": "Nassau",
      "hct": "नास्सऊ"
    }, {
      "ac": "SLV",
      "an": "Simla",
      "han": "शिमला",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Shimla",
      "hct": "शिमला"
    }, {
      "ac": "YWG",
      "an": "Winnipeg",
      "han": "विन्निपेग",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Winnipeg",
      "hct": "विन्निपेग"
    }, {
      "ac": "ALA",
      "an": "Almaty",
      "han": "अल्मती",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Almaty",
      "hct": "अल्मती"
    }, {
      "ac": "LAD",
      "an": "Four De Fevereiro",
      "han": "फोर दे फ़ेवेरेरो",
      "cn": "Angola",
      "hcn": "अंगोला",
      "cc": "AO",
      "ct": "Luanda",
      "hct": "लुआंडा"
    }, {
      "ac": "LOP",
      "an": "",
      "han": "",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "",
      "ct": "Lombok",
      "hct": "लोंबोक"
    }, {
      "ac": "KMG",
      "an": "Kunming",
      "han": "कुनमिंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Kunming",
      "hct": "कुनमिंग"
    }, {
      "ac": "LTU",
      "an": "Latur",
      "han": "लातूर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Latur",
      "hct": "लातूर"
    }, {
      "ac": "ODS",
      "an": "Central",
      "han": "सेन्ट्रल",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Odessa",
      "hct": "ओडेस्सा"
    }, {
      "ac": "KLH",
      "an": "Kolhapur",
      "han": "कोल्हापुर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kolhapur",
      "hct": "कोल्हापुर"
    }, {
      "ac": "KGL",
      "an": "Kigali",
      "han": "कीगली",
      "cn": "Rwanda",
      "hcn": "र्वान्डा",
      "cc": "RW",
      "ct": "Kigali",
      "hct": "कीगली"
    }, {
      "ac": "HNL",
      "an": "Honolulu",
      "han": "होनोलुलू",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Honolulu",
      "hct": "होनोलुलू"
    }, {
      "ac": "EBB",
      "an": "Entebbe",
      "han": "एंटेबबे",
      "cn": "Uganda",
      "hcn": "युगांडा",
      "cc": "UG",
      "ct": "Entebbe",
      "hct": "एंटेबबे"
    }, {
      "ac": "DUR",
      "an": "Durban",
      "han": "डरबन",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Durban ",
      "hct": "दूरअबन "
    }, {
      "ac": "IND",
      "an": "India‌napolis",
      "han": "ईंड्यानापोलिस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Indianapolis",
      "hct": "ईंड्यानापोलिस"
    }, {
      "ac": "YOW",
      "an": "Ottawa",
      "han": "ओट्टव",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Ottawa",
      "hct": "ओट्टव"
    }, {
      "ac": "NCE",
      "an": "Cote D Azur",
      "han": "कोटे डी अज़ुर",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Nice",
      "hct": "निस"
    }, {
      "ac": "BUH",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Bucharest",
      "hct": "बूचारेस्ट"
    }, {
      "ac": "OTP",
      "an": "Otopeni",
      "han": "ओटोपेनी",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Bucharest",
      "hct": "बूचारेस्ट"
    }, {
      "ac": "BBU",
      "an": "Aurel Vlaicu",
      "han": "औरेल व्लाईकू",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Bucharest",
      "hct": "बूचारेस्ट"
    }, {
      "ac": "BUF",
      "an": "Greater Buffalo",
      "han": "ग्रेटर बुफलो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Buffalo",
      "hct": "बुफलो"
    }, {
      "ac": "PUS",
      "an": "Kimhae",
      "han": "किम्हए",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Busan",
      "hct": "बूसन"
    }, {
      "ac": "TBS",
      "an": "Tbilisi",
      "han": "ट्बिलिसी",
      "cn": "Georgia",
      "hcn": "जियोरगिआ",
      "cc": "GE",
      "ct": "Tbilisi",
      "hct": "ट्बिलिसी"
    }, {
      "ac": "TPA",
      "an": "Tampa",
      "han": "तंपा",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tampa",
      "hct": "तंपा"
    }, {
      "ac": "SZX",
      "an": "Baoan",
      "han": "बावन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Shenzhen",
      "hct": "शेन्झ़ेन"
    }, {
      "ac": "LUN",
      "an": "Lusaka",
      "han": "लुसाका",
      "cn": "Zambia",
      "hcn": "ज़ाम्बीया",
      "cc": "ZM",
      "ct": "Lusaka",
      "hct": "लुसाका"
    }, {
      "ac": "CLE",
      "an": "Hopkins",
      "han": "होपकिंस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cleveland",
      "hct": "क्लेवेलांद"
    }, {
      "ac": "NTL",
      "an": "Williamtown",
      "han": "विलियमटाउन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Newcastle",
      "hct": "न्यूकासल"
    }, {
      "ac": "NCL",
      "an": "Newcastle",
      "han": "न्यूकासल",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Newcastle",
      "hct": "न्यूकासल"
    }, {
      "ac": "PDX",
      "an": "Portland",
      "han": "पोर्टलैंड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Portland",
      "hct": "प़ॉर्टलॅंद"
    }, {
      "ac": "PWM",
      "an": "Portland Jetport",
      "han": "पोर्टलैंड जेटपोर्ट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Portland",
      "hct": "प़ॉर्टलॅंद"
    }, {
      "ac": "AUS",
      "an": "Bergstrom",
      "han": "बर्ग्स्त्रोम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Austin",
      "hct": "ऑस्टिन"
    }, {
      "ac": "CVG",
      "an": "Cincinnati No Kentucky",
      "han": "सिन्सिन्नती नं केन्टुकी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cincinnati",
      "hct": "सिन्सिन्नती"
    }, {
      "ac": "MED",
      "an": "Prince Mohammad Bin Abdulaziz",
      "han": "प्रिंस मोहम्मद बीन अब्दूलाज़ीज़",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Madinah",
      "hct": "मादिनाह"
    }, {
      "ac": "BUE",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Buenos Aires",
      "hct": "बुएनोस एर्स"
    }, {
      "ac": "AEP",
      "an": "Jorge Newbery",
      "han": "जोर्गे न्यूबेरी",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Buenos Aires",
      "hct": "बुएनोस एर्स"
    }, {
      "ac": "EZE",
      "an": "Ministro Pistarini",
      "han": "मिनीस्त्रो पिस्तारिनी",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Buenos Aires",
      "hct": "बुएनोस एर्स"
    }, {
      "ac": "DLA",
      "an": "Douala",
      "han": "डौअला",
      "cn": "Cameroon",
      "hcn": "केमेरून",
      "cc": "CM",
      "ct": "Douala",
      "hct": "डौअला"
    }, {
      "ac": "CKY",
      "an": "Conakry",
      "han": "कोनक्री",
      "cn": "Guinea",
      "hcn": "गुइनी",
      "cc": "GN",
      "ct": "Conakry",
      "hct": "कोनक्री"
    }, {
      "ac": "DMK",
      "an": "Bangkok ",
      "han": "बैंगकोक",
      "cn": " Don Mueang",
      "hcn": "डॉन मुईंग",
      "cc": "Thailand",
      "ct": "TH",
      "hct": "ठा"
    }, {
      "ac": "ANU",
      "an": "V C Bird",
      "han": "वी सी बर्ड",
      "cn": "Antigua and Barbuda",
      "hcn": "आंटिगुआ एण्द बार्बदा",
      "cc": "AG",
      "ct": "Antigua",
      "hct": "आंटिगुआ"
    }, {
      "ac": "NJF",
      "an": "Al Ashraf International",
      "han": "अल अशरफ इंटरनेशनल",
      "cn": "Iraq",
      "hcn": "इराक़",
      "cc": "IQ",
      "ct": "Al Najaf",
      "hct": "अल नजाफ़"
    }, {
      "ac": "ABV",
      "an": "Nnamdi Azikiwe",
      "han": "न्नाम्डी अज़िकिवे",
      "cn": "Nigeria",
      "hcn": "निगरिया",
      "cc": "NG",
      "ct": "Abuja",
      "hct": "अबुजा"
    }, {
      "ac": "BEP",
      "an": "Bellary Airport",
      "han": "बेलारी एअरप़ॉर्ट",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bellary",
      "hct": "बेलारी"
    }, {
      "ac": "DIU",
      "an": "Diu",
      "han": "दीऊ",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Diu",
      "hct": "दीऊ"
    }, {
      "ac": "IXW",
      "an": "Sonari Airport",
      "han": "सोणारी एअरप़ॉर्ट",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Jamshedpur",
      "hct": "जमशेदपुर"
    }, {
      "ac": "IXY",
      "an": "Kandla",
      "han": "कंड्ला",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kandla",
      "hct": "कांडला"
    }, {
      "ac": "IXL",
      "an": "Leh Kushok Bakula Rimpochee",
      "han": "लेह कुशोक बकुला रिम्पोची",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Leh",
      "hct": "लेह"
    }, {
      "ac": "IXI",
      "an": "Lilabari",
      "han": "लीलाबारी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Lilabari",
      "hct": "लीलाबारी"
    }, {
      "ac": "ISK",
      "an": "Gandhinagar",
      "han": "गांधीनगर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Nasik",
      "hct": "नाशीक"
    }, {
      "ac": "PGH",
      "an": "Pantnagar",
      "han": "पंतनगर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Nainital",
      "hct": "नैनिताल"
    }, {
      "ac": "IXP",
      "an": "Pathankot",
      "han": "पठानकोट",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Pathankot",
      "hct": "पठानकोट"
    }, {
      "ac": "SXV",
      "an": "Salem",
      "han": "सालम",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Salem",
      "hct": "सालम"
    }, {
      "ac": "SHL",
      "an": "Barapani",
      "han": "बरपाणी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Shillong",
      "hct": "षिल्लांग"
    }, {
      "ac": "TEZ",
      "an": "Tezpur",
      "han": "तेज़पूर",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Tezpur",
      "hct": "तेज़पूर"
    }, {
      "ac": "TRZ",
      "an": "Tiruchirapally Civil",
      "han": "तिरुचीरपल्ली सिवील",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Trichy",
      "hct": "त्रिच्ची"
    }, {
      "ac": "DEL",
      "an": "(nearest airport New Delhi)",
      "han": "(निकटतम हवाई अड्डा नई दिल्ली)",
      "cn": "India‌",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Agra",
      "hct": "आगरा",
      "nb": "DEL"
    }, {
      "ac": "JAI",
      "an": "(nearest airport Jaipur)",
      "han": "(निकटम हवाई अड्डा जयपुर)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Ajmer",
      "hct": "अजमेर",
      "nb": "JAI"
    }, {
      "ac": "COK",
      "an": "(nearest airport Kochi)",
      "han": "(निकटतम हवाई अड्डा कोच्चि)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Alleppey",
      "hct": "अल्लेप्पी",
      "nb": "COK"
    }, {
      "ac": "MYQ",
      "an": "(nearest airport Mysore)",
      "han": "(निकटतम हवाई अड्डा मैसूर)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Coorg",
      "hct": "कूर्ग",
      "nb": "MYQ"
    }, {
      "ac": "DHM",
      "an": "(nearest airport Dharamsala)",
      "han": "(निकटतम हवाई अड्डा धर्मशाला)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Dalhousie",
      "hct": "डलहौजी",
      "nb": "DHM"
    }, {
      "ac": "IXB",
      "an": "(nearest airport Bagdogra)",
      "han": "(नजदीकी हवाई अड्डा बागडोगरा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Darjeeling",
      "hct": "दार्जिलिंग",
      "nb": "IXB"
    }, {
      "ac": "DEL",
      "an": "(nearest airport New Delhi)",
      "han": "(निकटतम हवाई अड्डा नई दिल्ली)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Faridabad",
      "hct": "फरीदाबाद",
      "nb": "DEL"
    }, {
      "ac": "IXB",
      "an": "(nearest airport Bagdogra)",
      "han": "(नजदीकी हवाई अड्डा बागडोगरा)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Gangtok",
      "hct": "गंगटोक",
      "nb": "IXB"
    }, {
      "ac": "DEL",
      "an": "(nearest airport New Delhi)",
      "han": "(निकटतम हवाई अड्डा नई दिल्ली)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Gurgaon",
      "hct": "गुडगाँव",
      "nb": "DEL"
    }, {
      "ac": "TRV",
      "an": "(nearest airport Trivandrum)",
      "han": "(निकटतम हवाई अड्डा त्रिवेंद्रम)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kanyakumari",
      "hct": "कन्याकुमारी",
      "nb": "TRV"
    }, {
      "ac": "CCJ",
      "an": "(nearest airport Calicut)",
      "han": "(निकटतम हवाई अड्डा कालीकट)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Kerela",
      "hct": "केरल",
      "nb": "CCJ"
    }, {
      "ac": "IXL",
      "an": "(nearest airport Leh)",
      "han": "(निकटतम हवाई अड्डा लेह)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Ladakh",
      "hct": "लद्दाख",
      "nb": "IXL"
    }, {
      "ac": "CJB",
      "an": "(nearest airport Coimbatore)",
      "han": "(निकटतम हवाई अड्डा कोयम्बटूर)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Ooty",
      "hct": "ऊटी",
      "nb": "CJB"
    }, {
      "ac": "ISK",
      "an": "(nearest airport Nasik)",
      "han": "(निकटतम हवाई अड्डा नासिक)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Shirdi",
      "hct": "शिरडी",
      "nb": "ISK"
    }, {
      "ac": "IXB",
      "an": "(nearest airport Bagdogra)",
      "han": "(नजदीकी हवाई अड्डा बागडोगरा)",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Sikkim",
      "hct": "सिक्किम",
      "nb": "IXB"
    }, {
      "ac": "VNS",
      "an": "Varanasi",
      "han": "वाराणसी",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Banaras",
      "hct": "बनारस",
      "nb": "VNS"
    }, {
      "ac": "BDQ",
      "an": "Vadodara",
      "han": "वडोदरा",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Baroda",
      "hct": "बड़ौदा",
      "nb": "BDQ"
    }, {
      "ac": "IXG",
      "an": "Belgaum",
      "han": "बेलगाम",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Belgavi",
      "hct": "बेळगावी",
      "nb": "IXG"
    }, {
      "ac": "BOM",
      "an": "Mumbai",
      "han": "मुंबई ",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Bombay",
      "hct": "बॉम्बे",
      "nb": "BOM"
    }, {
      "ac": "CCU",
      "an": "Kolkata",
      "han": "कोलकाता",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Calcutta",
      "hct": "कलकत्ता",
      "nb": "CCU"
    }, {
      "ac": "AGX",
      "an": "Agatti Island",
      "han": "अगत्ती आइलैंड",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Lakshadweep",
      "hct": "लक्षद्वीप",
      "nb": "AGX"
    }, {
      "ac": "MAA",
      "an": "Chennai",
      "han": "चेन्नई",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Madras",
      "hct": "मद्रास",
      "nb": "MAA"
    }, {
      "ac": "AAL",
      "an": "Aalborg",
      "han": "आल्बोर्ग",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Aalborg",
      "hct": "आल्बोर्ग"
    }, {
      "ac": "AES",
      "an": "Vigra",
      "han": "विगरा",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Alesund",
      "hct": "आलेसुन्ड"
    }, {
      "ac": "AAR",
      "an": "Tirstrup",
      "han": "तिर्स्त्रुप",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Aarhus",
      "hct": "आर्हूस"
    }, {
      "ac": "JEG",
      "an": "Aasiaat",
      "han": "आसियाट",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Aasiaat",
      "hct": "आसियाट"
    }, {
      "ac": "ABD",
      "an": "Abadan",
      "han": "आबदन",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Abadan",
      "hct": "आबदन"
    }, {
      "ac": "ABA",
      "an": "Abakan",
      "han": "अबकन",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Abakan",
      "hct": "अबकन"
    }, {
      "ac": "YXX",
      "an": "Abbotsford",
      "han": "अब्बोट्स्फ़ोर्ड",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Abbotsford",
      "hct": "अब्बोट्स्फ़र्ड"
    }, {
      "ac": "AEH",
      "an": "Abeche",
      "han": "आबेचे",
      "cn": "Chad",
      "hcn": "चड",
      "cc": "TD",
      "ct": "Abeche",
      "hct": "आबेचे"
    }, {
      "ac": "ABZ",
      "an": "Dyce",
      "han": "ड्य्क",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Aberdeen",
      "hct": "आबेर्डीन"
    }, {
      "ac": "ABR",
      "an": "Aberdeen",
      "han": "आबेर्डीन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "ABERDEEN",
      "hct": "आबेर्डीन"
    }, {
      "ac": "AHB",
      "an": "Abha",
      "han": "आ�ा",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Abha",
      "hct": "आ�ा"
    }, {
      "ac": "ABI",
      "an": "Abilene Municipal",
      "han": "अबिलेन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Abilene",
      "hct": "अबिलेन"
    }, {
      "ac": "ABS",
      "an": "Abu Simbel",
      "han": "अबू सिमबेल",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Abu Simbel",
      "hct": "अबू सिमबेल"
    }, {
      "ac": "ACA",
      "an": "Alvarez",
      "han": "अल्वरेज़",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Acapulco",
      "hct": "आकेपूलको"
    }, {
      "ac": "AGV",
      "an": "Oswaldo Guevara Mujica",
      "han": "ओस्वल्डो गुएवरा मुजिका",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Acarigua",
      "hct": "आकरिगुआ"
    }, {
      "ac": "ADK",
      "an": "Adak",
      "han": "अदक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Adak Island",
      "hct": "अदक आयलॅन्ड"
    }, {
      "ac": "ADA",
      "an": "Adana",
      "han": "अदना",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Adana",
      "hct": "अदना"
    }, {
      "ac": "ADE",
      "an": "Amman",
      "han": "येमेन",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Aden",
      "hct": "आद्न"
    }, {
      "ac": "ADF",
      "an": "",
      "han": "",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Adiyaman",
      "hct": "अडियामॅन"
    }, {
      "ac": "AER",
      "an": "Sochi",
      "han": "सोची",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Sochi",
      "hct": "सोची"
    }, {
      "ac": "AFT",
      "an": "",
      "han": "",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Afutara",
      "hct": "अफ़ुटरा"
    }, {
      "ac": "AGA",
      "an": "Inezgane",
      "han": "इनेज़गणे",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Agadir",
      "hct": "आगदिर"
    }, {
      "ac": "AGF",
      "an": "La Garenne",
      "han": "ला गरेनने",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Agen",
      "hct": "अजन"
    }, {
      "ac": "AGU",
      "an": "Aguascalientes Municipal",
      "han": "अगुआस्कालींट्स म्यूनिसिपल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Aguascalientes",
      "hct": "अगुआस्कालींट्स"
    }, {
      "ac": "AWZ",
      "an": "Ahwaz",
      "han": "अह्वाज़",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Ahwaz",
      "hct": "अह्वाज़"
    }, {
      "ac": "TAJ",
      "an": "",
      "han": "",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Aitape",
      "hct": "आईतापे"
    }, {
      "ac": "AIT",
      "an": "Aitutaki",
      "han": "आईटुटकी",
      "cn": "Cook Islands",
      "hcn": "कूक आयलॅन्ड्स",
      "cc": "CK",
      "ct": "Aitutaki",
      "hct": "आईटुटकी"
    }, {
      "ac": "AJA",
      "an": "Campo Dell Oro",
      "han": "केंपो देल ओरो",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Ajaccio",
      "hct": "अजेक्कियो"
    }, {
      "ac": "AKK",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Akhiok",
      "hct": "अखियोक"
    }, {
      "ac": "KKI",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Akiachak",
      "hct": "अकियाचक"
    }, {
      "ac": "AKI",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Akiak",
      "hct": "अकियाक"
    }, {
      "ac": "AXT",
      "an": "Akita",
      "han": "अकिता",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Akita",
      "hct": "अकिता"
    }, {
      "ac": "LAK",
      "an": "Aklavik",
      "han": "अक्लवीक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Aklavik",
      "hct": "अक्लवीक"
    }, {
      "ac": "CAK",
      "an": "Akron Canton Regional",
      "han": "अक्रों कॅंटॉन रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Akron",
      "hct": "अक्रोन"
    }, {
      "ac": "AKU",
      "an": "Aksu",
      "han": "अक्सू",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Aksu",
      "hct": "अक्सू"
    }, {
      "ac": "SCO",
      "an": "Aktau",
      "han": "अक्तऊ",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Aktau",
      "hct": "अक्तऊ"
    }, {
      "ac": "AKX",
      "an": "Aktyubinsk",
      "han": "अक्ट्युबीन्स्क",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Aktyubinsk",
      "hct": "अक्ट्युबीन्स्क"
    }, {
      "ac": "AKV",
      "an": "Akulivik",
      "han": "अकुलिवीक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Akulivik",
      "hct": "अकुलिवीक"
    }, {
      "ac": "AEY",
      "an": "Akureyri",
      "han": "अकुरीरि",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Akureyri",
      "hct": "अकुरीरि"
    }, {
      "ac": "KQA",
      "an": "Akutan Seaplane Base",
      "han": "अकूटन सीपलाने बेस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Akutan",
      "hct": "अकूटन"
    }, {
      "ac": "AAN",
      "an": "Al Ain",
      "han": "अल आईन",
      "cn": "United Arab Emirates",
      "hcn": "संयुक्त‌ अर‌ब‌ अमीरात‌",
      "cc": "AE",
      "ct": "Al Ain",
      "hct": "अल आईन"
    }, {
      "ac": "AAC",
      "an": "El Arish",
      "han": "एल आरिश",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "El Arish",
      "hct": "एल आरिश"
    }, {
      "ac": "AAY",
      "an": "Al Ghaidah",
      "han": "अल घैदाह",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Al Ghaidah Intl",
      "hct": "अल घैदाह इंट्ल"
    }, {
      "ac": "AHU",
      "an": "Cherif El Idrissi",
      "han": "चेरिफ़ एल इद्रीस्सी",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Al Hociema",
      "hct": "अल होकीमा"
    }, {
      "ac": "HOF",
      "an": "Al Ahsa",
      "han": "अल अह्सा",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Al",
      "hct": "अल"
    }, {
      "ac": "AUK",
      "an": "Alakanuk",
      "han": "अलकनुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Alakanuk",
      "hct": "अलकनुक"
    }, {
      "ac": "ALM",
      "an": "Alamogordo Municipal",
      "han": "आलामोगोर्डो म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Alamogordo",
      "hct": "आलामोगोर्डो"
    }, {
      "ac": "ALS",
      "an": "Alamosa Municipal",
      "han": "आलामोसा म्यूनिसिपल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Alamosa",
      "hct": "आलामोसा"
    }, {
      "ac": "ABC",
      "an": "",
      "han": "",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Albacete",
      "hct": "आल्बसेटे"
    }, {
      "ac": "ABT",
      "an": "Al Baha",
      "han": "अल बहा",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "El",
      "hct": "एल"
    }, {
      "ac": "ALH",
      "an": "Albany",
      "han": "अल्बनी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Albany",
      "hct": "अल्बनी"
    }, {
      "ac": "ABY",
      "an": "Dougherty Cty",
      "han": "डौघेर्टी क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Albany",
      "hct": "अल्बनी"
    }, {
      "ac": "ALB",
      "an": "Albany Cty",
      "han": "अल्बनी क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Albany",
      "hct": "अल्बनी"
    }, {
      "ac": "ABQ",
      "an": "Albuquerque",
      "han": "आल्बुकर्क़",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Albuquerque",
      "hct": "आल्बुकर्क़"
    }, {
      "ac": "ABX",
      "an": "Albury",
      "han": "आल्बूरई",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Albury",
      "hct": "आल्बूरई"
    }, {
      "ac": "ACI",
      "an": "Alderney",
      "han": "अल्डेर्नी",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Alderney",
      "hct": "अल्डेर्नी"
    }, {
      "ac": "WKK",
      "an": "Aleknagik",
      "han": "आलोक्नागिक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Aleknagik",
      "hct": "आलेक्नागिक"
    }, {
      "ac": "ALP",
      "an": "Aleppo",
      "han": "आलेप्पो",
      "cn": "Syria",
      "hcn": "सिरिआ",
      "cc": "SY",
      "ct": "Aleppo",
      "hct": "आलेप्पो"
    }, {
      "ac": "ALY",
      "an": "El Nouzha",
      "han": "एल नौऴा",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Alexandria",
      "hct": "अलेक्जेंड्रिय"
    }, {
      "ac": "HBE",
      "an": "Borg El Arab",
      "han": "बोर्ग एल अरब",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Alexandria",
      "hct": "अलेक्जेंड्रिय"
    }, {
      "ac": "AEX",
      "an": "Alexandria",
      "han": "अलेक्जेंड्रिय",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Alexandria",
      "hct": "अलेक्जेंड्रिय"
    }, {
      "ac": "AXD",
      "an": "Dimokritos",
      "han": "डिमोकृटोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Alexandroupolis",
      "hct": "अलेक्सान्द्रौपोलिस"
    }, {
      "ac": "AHO",
      "an": "Alghero",
      "han": "अल्घेरो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Alghero",
      "hct": "अल्घेरो"
    }, {
      "ac": "ALG",
      "an": "Houari Boumedienne",
      "han": "हौआरी बौमेदीनने",
      "cn": "Algeria",
      "hcn": "अल्गेरिआ",
      "cc": "DZ",
      "ct": "Algiers",
      "hct": "अल्गिएर्ज़"
    }, {
      "ac": "ALC",
      "an": "Alicante",
      "han": "अलिकांटे",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Alicante",
      "hct": "अलिकांटे"
    }, {
      "ac": "ASP",
      "an": "Alice Springs",
      "han": "एलिस स्प्रिंग्स",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Alice Springs",
      "hct": "एलिस स्प्रिंग्स"
    }, {
      "ac": "ALZ",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Alitak",
      "hct": "अलितक"
    }, {
      "ac": "AET",
      "an": "Allakaket",
      "han": "अलकाकेट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Allakaket",
      "hct": "अलकाकेट"
    }, {
      "ac": "ABE",
      "an": "Lehigh Valley",
      "han": "लेहिघ वैली",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Allentown Bethlehem Easton",
      "hct": "अलेनटाउन बेठ्लेहेम ईस्टन"
    }, {
      "ac": "AIA",
      "an": "Alliance",
      "han": "अलिआन्स",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Alliance",
      "hct": "अलिआन्स"
    }, {
      "ac": "LLU",
      "an": "Alluitsup Paa Heliport",
      "han": "अलुइत्सुप पा हेलिपोर्ट",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Alluitsup Paa",
      "hct": "अलुइत्सुप पा"
    }, {
      "ac": "YTF",
      "an": "Alma",
      "han": "अल्मा",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Alma",
      "hct": "अल्मा"
    }, {
      "ac": "LEI",
      "an": "Almeria",
      "han": "अल्मेरिआ",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Almeria",
      "hct": "अल्मेरिआ"
    }, {
      "ac": "AOR",
      "an": "Sultan Abdul Halim",
      "han": "सुल्तान अब्दुल हलीम",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Alor Setar",
      "hct": "आलोर सेतर"
    }, {
      "ac": "GUR",
      "an": "Gurney",
      "han": "गुरने",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Alotau",
      "hct": "आलोटऊ"
    }, {
      "ac": "APN",
      "an": "Phelps Collins",
      "han": "फेल्प्स कौलिंस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Alpena",
      "hct": "आलपना"
    }, {
      "ac": "AFL",
      "an": "Alta Floresta",
      "han": "अलता फ़्लोरेस्टा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Alta Floresta",
      "hct": "अलता फ़्लोरेस्टा"
    }, {
      "ac": "ALF",
      "an": "Alta",
      "han": "अलता",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Alta",
      "hct": "अलता"
    }, {
      "ac": "ATM",
      "an": "Altamira",
      "han": "अलतामीरा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Altamira",
      "hct": "अलतामीरा"
    }, {
      "ac": "AAT",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Altay",
      "hct": "आल्ते"
    }, {
      "ac": "ACH",
      "an": "St Gallen Altenrhein",
      "han": "स्त्रीट गलेन आल्टेन्र्हेइन",
      "cn": "Switzerland",
      "hcn": "स्विट्ज़र‌लैंड‌",
      "cc": "CH",
      "ct": "Altenrhein",
      "hct": "आल्टेन्र्हेइन"
    }, {
      "ac": "AOO",
      "an": "Altoona",
      "han": "आल्टूना",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Altoona   Martinsburg",
      "hct": "आल्टूना   मार्तिन्स्बूरग"
    }, {
      "ac": "LTS",
      "an": "Altus Afb",
      "han": "आल्टस अफ़्ब",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Altus",
      "hct": "आल्टस"
    }, {
      "ac": "XXA",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Alvesta",
      "hct": "आल्वेस्टा"
    }, {
      "ac": "ASJ",
      "an": "Amami",
      "han": "अममी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Amami",
      "hct": "अममी"
    }, {
      "ac": "AMA",
      "an": "Amarillo",
      "han": "अमारिलो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Amarillo",
      "hct": "अमारिलो"
    }, {
      "ac": "ABL",
      "an": "Ambler",
      "han": "आम्बलर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ambler",
      "hct": "आम्बलर"
    }, {
      "ac": "AMQ",
      "an": "Pattimura",
      "han": "पट्टीमुरा",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Ambon",
      "hct": "अम्बोन"
    }, {
      "ac": "ASV",
      "an": "Amboseli",
      "han": "अंबोसेली",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Amboseli National Park",
      "hct": "अंबोसेली नेशनल पार्क"
    }, {
      "ac": "AMV",
      "an": "Amderma",
      "han": "आम्डेर्मा",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Amderma",
      "hct": "आम्डेर्मा"
    }, {
      "ac": "AOS",
      "an": "Amook",
      "han": "अमूक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Amook",
      "hct": "अमूक"
    }, {
      "ac": "DYR",
      "an": "Ugolny",
      "han": "उगोल्नी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Anadyr",
      "hct": "अनाड्य्र"
    }, {
      "ac": "YAA",
      "an": "Anahim Lake",
      "han": "अनहिम लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Anahim Lake",
      "hct": "अनहिम लेक"
    }, {
      "ac": "AKP",
      "an": "Anaktuvuk Pass",
      "han": "अनक्टुवुक पास्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Anaktuvuk Pass",
      "hct": "अनक्टुवुक पास्स"
    }, {
      "ac": "HVA",
      "an": "Analalava",
      "han": "आनालालावा",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Analalava",
      "hct": "आनालालावा"
    }, {
      "ac": "AAQ",
      "an": "Vityazevo",
      "han": "वित्याज़ेवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Anapa",
      "hct": "अनपा"
    }, {
      "ac": "ANC",
      "an": "Anchorage",
      "han": "अंकरागे",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Anchorage",
      "hct": "अंकरागे"
    }, {
      "ac": "AOI",
      "an": "Falconara",
      "han": "फ़ल्कोनार",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Ancona",
      "hct": "अन्कोना"
    }, {
      "ac": "ANS",
      "an": "Andahuaylas",
      "han": "अंदाहुआयलास",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Andahuaylas",
      "hct": "अंदाहुआयलास"
    }, {
      "ac": "ANX",
      "an": "Andoya",
      "han": "अण्दोया",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Andoya",
      "hct": "अण्दोया"
    }, {
      "ac": "AZN",
      "an": "Andizhan",
      "han": "अंडिजेन",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Andizhan",
      "hct": "अंडिझ़ेन"
    }, {
      "ac": "AUY",
      "an": "Anelghowhat",
      "han": "अनेल्घौहट",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Anelghowhat",
      "hct": "अनेल्घौहट"
    }, {
      "ac": "AGH",
      "an": "Angelholm Helsingborg",
      "han": "अंजेल्होल्म हेल्सिंग्बोर्ग",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Angelholm Helsingborg",
      "hct": "अंजेल्होल्म हेल्सिंग्बोर्ग"
    }, {
      "ac": "ANE",
      "an": "Angers",
      "han": "अंगर्ज़",
      "cn": "Loire",
      "hcn": "लोईरे",
      "cc": "France",
      "ct": "FR",
      "hct": "फ़्र"
    }, {
      "ac": "YAX",
      "an": "Wapekeka",
      "han": "वपेकेका",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Angling Lake",
      "hct": "आंग्लींग लेक"
    }, {
      "ac": "AGN",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Angoon",
      "hct": "अंगून"
    }, {
      "ac": "ANI",
      "an": "Aniak",
      "han": "आनिआक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Aniak",
      "hct": "आनिआक"
    }, {
      "ac": "AWD",
      "an": "Aniwa",
      "han": "आणीवा",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Aniwa",
      "hct": "आणीवा"
    }, {
      "ac": "AJN",
      "an": "Ouani",
      "han": "औआणी",
      "cn": "Comoros",
      "hcn": "कोमोरोस",
      "cc": "KM",
      "ct": "Anjouan",
      "hct": "अन्जौआन"
    }, {
      "ac": "AKA",
      "an": "Ankang",
      "han": "अण्कंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Ankang",
      "hct": "अण्कंग"
    }, {
      "ac": "ESB",
      "an": "Esenboga",
      "han": "एसेन्बोगा",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Ankara",
      "hct": "अणकरा"
    }, {
      "ac": "JVA",
      "an": "Ankavandra",
      "han": "अण्कवान्दरा",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Ankavandra",
      "hct": "अण्कवान्दरा"
    }, {
      "ac": "NCY",
      "an": "Meythet",
      "han": "मीठेत",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Annecy",
      "hct": "अन्नेकी"
    }, {
      "ac": "AQG",
      "an": "Anqing",
      "han": "अन्क़िंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Anqing",
      "hct": "अन्क़िंग"
    }, {
      "ac": "ANM",
      "an": "Antsirabato",
      "han": "अंट्सिराबतो",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Antalaha",
      "hct": "अंतलहा"
    }, {
      "ac": "AYT",
      "an": "Antalya",
      "han": "अंतल्या",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Antalya",
      "hct": "अंतल्या"
    }, {
      "ac": "TNR",
      "an": "Ivato",
      "han": "इवतो",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Antananarivo",
      "hct": "अंटनानारिवो"
    }, {
      "ac": "ANF",
      "an": "Cerro Moreno",
      "han": "सेर्रो मोरेनो",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Antofagasta",
      "hct": "अंटोफ़गस्टा"
    }, {
      "ac": "WAQ",
      "an": "Antsalova",
      "han": "अंट्सलोवा",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Antsalova",
      "hct": "अंट्सलोवा"
    }, {
      "ac": "ANR",
      "an": "Antwerp Brussels North",
      "han": "अंट्वेर्प ब्रस्सेल्स नोर्थ",
      "cn": "Belgium",
      "hcn": "बेल्जिय‌म‌",
      "cc": "BE",
      "ct": "Antwerp",
      "hct": "अंट्वेर्प"
    }, {
      "ac": "ANV",
      "an": "Anvik",
      "han": "अन्वीक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Anvik",
      "hct": "अन्वीक"
    }, {
      "ac": "AOJ",
      "an": "Aomori",
      "han": "आव्मोरी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Aomori",
      "hct": "आव्मोरी"
    }, {
      "ac": "AOT",
      "an": "Aosta",
      "han": "आव्स्टा",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Aosta",
      "hct": "आव्स्टा"
    }, {
      "ac": "APO",
      "an": "Apartado",
      "han": "अपर्टाडो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Apartado",
      "hct": "अपर्टाडो"
    }, {
      "ac": "APW",
      "an": "Apia",
      "han": "अपिया",
      "cn": "Western Samoa",
      "hcn": "प‌श्चिमी स‌मोआ",
      "cc": "WS",
      "ct": "Apia",
      "hct": "अपिया"
    }, {
      "ac": "ATW",
      "an": "Outagamie Cty",
      "han": "औतगामी क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Appleton",
      "hct": "अप्प्लेटन"
    }, {
      "ac": "AQJ",
      "an": "Aqaba King Hussein",
      "han": "अक़ब किंग हुसैन",
      "cn": "Jordan",
      "hcn": "जॉर्डन",
      "cc": "JO",
      "ct": "Aqaba",
      "hct": "अक़ब"
    }, {
      "ac": "AJU",
      "an": "Santa Maria",
      "han": "शांता मारिआ",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Aracaju",
      "hct": "अरकजू"
    }, {
      "ac": "ARU",
      "an": "Aracatuba",
      "han": "अरकतुबा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Aracatuba",
      "hct": "अरकतुबा"
    }, {
      "ac": "ARW",
      "an": "Arad",
      "han": "आरेद",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Arad",
      "hct": "आरेद"
    }, {
      "ac": "AUX",
      "an": "Araguaina",
      "han": "आरगुआइना",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Araguaina",
      "hct": "आरगुआइना"
    }, {
      "ac": "RAE",
      "an": "Arar",
      "han": "आरार",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Arar",
      "hct": "आरार"
    }, {
      "ac": "AUC",
      "an": "Santiago Perez",
      "han": "सॅन्टियागो परेज़",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Arauca",
      "hct": "आरऊका"
    }, {
      "ac": "AAX",
      "an": "",
      "han": "",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Araxa",
      "hct": "आरक्ष"
    }, {
      "ac": "AMH",
      "an": "Arba Minch",
      "han": "आर्बा मिन्च",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Arba Minch",
      "hct": "आर्बा मिन्च"
    }, {
      "ac": "EBL",
      "an": "Erbil",
      "han": "एर्बिल",
      "cn": "Iraq",
      "hcn": "इराक़",
      "cc": "IQ",
      "ct": "Erbil",
      "hct": "एर्बिल"
    }, {
      "ac": "ACV",
      "an": "Arcata Eureka",
      "han": "आर्कता युरेका",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Arcata",
      "hct": "आर्कता"
    }, {
      "ac": "ARC",
      "an": "Arctic Village",
      "han": "आर्क्टिक विलेज",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Arctic Village",
      "hct": "आर्क्टिक विलेज"
    }, {
      "ac": "ADU",
      "an": "Ardabil",
      "han": "अर्डाबिल",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Ardabil",
      "hct": "अर्डाबिल"
    }, {
      "ac": "AQP",
      "an": "Rodriguez Ballon",
      "han": "रोड्रिग्ज़ बाल्लोन",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Arequipa",
      "hct": "आरेक़ुइपा"
    }, {
      "ac": "GYL",
      "an": "Argyle",
      "han": "आर्गिल",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Argyle ",
      "hct": "आर्गिल "
    }, {
      "ac": "ARI",
      "an": "Chacalluta",
      "han": "चकालुता",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Arica",
      "hct": "एरिका"
    }, {
      "ac": "ARH",
      "an": "Talagi",
      "han": "तलगी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Arkhangelsk",
      "hct": "आर्खंजेल्स्क"
    }, {
      "ac": "AXM",
      "an": "El Eden",
      "han": "एल एड्न",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Armenia",
      "hct": "अर्मेनिआ"
    }, {
      "ac": "ARM",
      "an": "Armidale",
      "han": "अर्मिदाले",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Armidale",
      "hct": "अर्मिदाले"
    }, {
      "ac": "RNA",
      "an": "",
      "han": "",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Arona",
      "hct": "अरोना"
    }, {
      "ac": "ARK",
      "an": "Arusha",
      "han": "अरुशा",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Arusha",
      "hct": "अरुशा"
    }, {
      "ac": "YEK",
      "an": "Arviat",
      "han": "अर्वियाट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Eskimo Point",
      "hct": "एस्किमो पोईंत"
    }, {
      "ac": "AJR",
      "an": "Arvidsjaur",
      "han": "अर्वीड्स्जौर",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Arvidsjaur",
      "hct": "अर्वीड्स्जौर"
    }, {
      "ac": "AKJ",
      "an": "Asahikawa",
      "han": "असाहीकावा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Asahikawa",
      "hct": "असाहीकावा"
    }, {
      "ac": "AVL",
      "an": "Asheville Municipal",
      "han": "एशेविले म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Asheville   Hendersonville",
      "hct": "एशेविले   हेंडर्सनविले"
    }, {
      "ac": "ASB",
      "an": "Ashgabat",
      "han": "अश्गबत",
      "cn": "Turkmenistan",
      "hcn": "तूर्कमेनीस्तान",
      "cc": "TM",
      "ct": "Ashkhabad",
      "hct": "अश्खाबाद"
    }, {
      "ac": "ASM",
      "an": "Asmara",
      "han": "अस्मार",
      "cn": "Eritrea",
      "hcn": "एरित्रे",
      "cc": "ER",
      "ct": "Asmara",
      "hct": "अस्मार"
    }, {
      "ac": "ASE",
      "an": "Aspen",
      "han": "असपन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Aspen",
      "hct": "असपन"
    }, {
      "ac": "ATZ",
      "an": "Asyut",
      "han": "अस्युत",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Asyut",
      "hct": "अस्युत"
    }, {
      "ac": "TSE",
      "an": "Astana",
      "han": "अस्ताना",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Tselinograd",
      "hct": "ट्सेलीनोग्रेद"
    }, {
      "ac": "ASF",
      "an": "Astrakhan",
      "han": "आस्ट्राखान",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Astrakhan",
      "hct": "आस्ट्राखान"
    }, {
      "ac": "OVD",
      "an": "Asturias",
      "han": "आस्तूर्यास",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Asturias",
      "hct": "आस्तूर्यास"
    }, {
      "ac": "JTY",
      "an": "Astypalaia",
      "han": "आस्त्य्पालया",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Astypalaia",
      "hct": "आस्त्य्पालया"
    }, {
      "ac": "ASU",
      "an": "Salvio Pettirosse",
      "han": "साल्वियो पेट्टिरोस्स",
      "cn": "Paraguay",
      "hcn": "परगुआय",
      "cc": "PY",
      "ct": "Asuncion",
      "hct": "असन्सियोन"
    }, {
      "ac": "ASW",
      "an": "Aswan",
      "han": "आस्वान",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Aswan",
      "hct": "आस्वान"
    }, {
      "ac": "AIU",
      "an": "Atiu Island",
      "han": "अतीऊ आयलॅन्ड",
      "cn": "Cook Islands",
      "hcn": "कूक आयलॅन्ड्स",
      "cc": "CK",
      "ct": "Atiu Island",
      "hct": "अतीऊ आयलॅन्ड"
    }, {
      "ac": "AKB",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Atka",
      "hct": "अट्का"
    }, {
      "ac": "ACY",
      "an": "Pomona Field",
      "han": "पोमोना फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Atlantic City",
      "hct": "ॲटलांटिक सिटी"
    }, {
      "ac": "ATT",
      "an": "Camp Mabry Austin City",
      "han": "कॅंप माब्री ऑस्टिन सिटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Austin TX",
      "hct": "ऑस्टिन ट्क्स"
    }, {
      "ac": "ATD",
      "an": "Uru Harbour",
      "han": "उरू हर्बौर",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Atoifi",
      "hct": "अतोईफ़ी"
    }, {
      "ac": "ATK",
      "an": "Atqasuk Edward Burnell Sr Memorial",
      "han": "अट्क़सुक एड्वर्ड बूरनेल श्र मेमोरिल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Atqasuk",
      "hct": "अट्क़सुक"
    }, {
      "ac": "YAT",
      "an": "Attawapiskat",
      "han": "आट्टवपिसकट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Attawapiskat",
      "hct": "आट्टवपिसकट"
    }, {
      "ac": "GUW",
      "an": "Atyrau",
      "han": "अत्य्राऊ",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Atyrau",
      "hct": "अत्य्राऊ"
    }, {
      "ac": "AGS",
      "an": "Bush Field",
      "han": "बुश फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Augusta",
      "hct": "औगुस्टा"
    }, {
      "ac": "AUG",
      "an": "Maine State",
      "han": "मायन स्टेट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Augusta",
      "hct": "औगुस्टा"
    }, {
      "ac": "AKS",
      "an": "Auki",
      "han": "अऊकी",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Auki",
      "hct": "अऊकी"
    }, {
      "ac": "YPJ",
      "an": "Aupaluk",
      "han": "अऊपलुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Aupaluk",
      "hct": "अऊपलुक"
    }, {
      "ac": "AUR",
      "an": "Aurillac",
      "han": "औरिलाक",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Aurillac",
      "hct": "औरिलाक"
    }, {
      "ac": "AUU",
      "an": "Aurukun",
      "han": "औरुकून",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Aurukun Mission ",
      "hct": "औरुकून मिशन "
    }, {
      "ac": "AVV",
      "an": "Avalon",
      "han": "अवालोन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Avalon",
      "hct": "अवालोन"
    }, {
      "ac": "XYP",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Avesta Krylbo",
      "hct": "अवेस्टा क्रील्बो"
    }, {
      "ac": "AVN",
      "an": "Caumont",
      "han": "चौमोन्त",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Avignon",
      "hct": "अविग्नोन"
    }, {
      "ac": "AVU",
      "an": "Avu Avu",
      "han": "अवू अवू",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Avu Avu",
      "hct": "अवू अवू"
    }, {
      "ac": "AWB",
      "an": "",
      "han": "",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Awaba",
      "hct": "आवाब"
    }, {
      "ac": "AXU",
      "an": "Axum",
      "han": "अक्सुम",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Axum",
      "hct": "अक्सुम"
    }, {
      "ac": "AYP",
      "an": "Coronel Fap Alfredo Mendivil Duarte",
      "han": "कोरोनेल फ़प आल्फ़्रेदो मेंदिवील ड्युआरटे",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Ayacucho",
      "hct": "आयकुचो"
    }, {
      "ac": "AYQ",
      "an": "Ayers Rock",
      "han": "अयेर्ज़ रॉक",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Ayers Rock",
      "hct": "अयेर्ज़ रॉक"
    }, {
      "ac": "BCM",
      "an": "Bacau",
      "han": "बकाऊ",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Bacau",
      "hct": "बकाऊ"
    }, {
      "ac": "BCD",
      "an": "Bacolod",
      "han": "बकोलोड",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Bacolod",
      "hct": "बकोलोड"
    }, {
      "ac": "BJZ",
      "an": "Talavera La Real",
      "han": "तालवेरा ला रियल",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Badajoz",
      "hct": "बदाजोज़"
    }, {
      "ac": "BDD",
      "an": "Badu Island",
      "han": "बडू आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Badu Island ",
      "hct": "बडू आयलॅन्ड "
    }, {
      "ac": "BGW",
      "an": "Baghdad",
      "han": "बघ्दाद",
      "cn": "Iraq",
      "hcn": "इराक़",
      "cc": "IQ",
      "ct": "Baghdad",
      "hct": "बघ्दाद"
    }, {
      "ac": "YBG",
      "an": "Bagotville",
      "han": "बगोट्वील",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Bagotville",
      "hct": "बगोट्वील"
    }, {
      "ac": "BAG",
      "an": "Baguio",
      "han": "बागुइओ",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Baguio",
      "hct": "बागुइओ"
    }, {
      "ac": "BJR",
      "an": "Bahir Dar",
      "han": "बहिर दार",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Bahar Dar",
      "hct": "बहार दार"
    }, {
      "ac": "BHI",
      "an": "Commandante",
      "han": "कम्मांदंटे",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Bahia Blanca",
      "hct": "बाहिया ब्लान्का"
    }, {
      "ac": "BSC",
      "an": "Jose Celestino Mutis",
      "han": "जोस सेलेस्टिनो मतिस",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Bahia Solano",
      "hct": "बाहिया सोलानो"
    }, {
      "ac": "BAY",
      "an": "Tautii Magheraus",
      "han": "तऊटिई मघेरऊस",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Baia Mare",
      "hct": "बया मारे"
    }, {
      "ac": "YBC",
      "an": "Baie Comeau",
      "han": "बाई कमीऊ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "BAIE",
      "hct": "बाई"
    }, {
      "ac": "BKM",
      "an": "Bakalalan",
      "han": "बकलालन",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Bakalalan",
      "hct": "बकलालन"
    }, {
      "ac": "YBK",
      "an": "Baker Lake",
      "han": "बकर लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Baker Lake",
      "hct": "बकर लेक"
    }, {
      "ac": "BFL",
      "an": "Meadows Field",
      "han": "मेडौस फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bakersfield",
      "hct": "बेकर्ज़फ़ील्ड"
    }, {
      "ac": "GYD",
      "an": "Heydar Aliyev",
      "han": "हेय्दार अलियेव",
      "cn": "Azerbaijan",
      "hcn": "अज़ेर्बैजान",
      "cc": "AZ",
      "ct": "Baku",
      "hct": "बकु"
    }, {
      "ac": "BAS",
      "an": "Ballalae",
      "han": "बाल्ललेए",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Ballalae",
      "hct": "बाल्ललेए"
    }, {
      "ac": "OPU",
      "an": "Balimo",
      "han": "बालीमो",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Balimo",
      "hct": "बालीमो"
    }, {
      "ac": "BNK",
      "an": "Ballina Byron Gateway",
      "han": "बाल्लीना बिरों गेटवे",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Ballina Byron Bay",
      "hct": "बाल्लीना बिरों बे"
    }, {
      "ac": "BBA",
      "an": "Teniente Vidal",
      "han": "तेनींटे विदल",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Balmaceda",
      "hct": "बाल्मेसेदा"
    }, {
      "ac": "BWI",
      "an": "Baltimore Washington",
      "han": "बाल्टीमोरे वाशींग्टन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Baltimore",
      "hct": "बाल्टीमोरे"
    }, {
      "ac": "BXR",
      "an": "Bam",
      "han": "बाम",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Bam",
      "hct": "बाम"
    }, {
      "ac": "ABM",
      "an": "Bamaga Injinoo",
      "han": "बमेगा इन्जिनू",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Amberley",
      "hct": "अम्बेरले"
    }, {
      "ac": "BKO",
      "an": "Senou",
      "han": "सेनौ",
      "cn": "Mali",
      "hcn": "माली",
      "cc": "ML",
      "ct": "Bamako",
      "hct": "बामाको"
    }, {
      "ac": "BTJ",
      "an": "Sultan Iskandarmuda",
      "han": "सुल्तान इस्कंदर्मुद",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Banda Aceh",
      "hct": "बंदा आसेह"
    }, {
      "ac": "BND",
      "an": "Bandar Abbass",
      "han": "बंदर अब्बास",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Bandar Abbas",
      "hct": "बंदर अबास"
    }, {
      "ac": "TKG",
      "an": "Radin Inten II (Branti)",
      "han": "रेडिन इंटेन 2 (ब्रंटी)",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Bandar Lampung",
      "hct": "बंदर लांपन्ग"
    }, {
      "ac": "BDH",
      "an": "Bandar Lengeh",
      "han": "बंदर लेन्जेह",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Bandar Lengeh",
      "hct": "बंदर लेन्जेह"
    }, {
      "ac": "BWN",
      "an": "Brunei",
      "han": "ब्रुणी",
      "cn": "Brunei",
      "hcn": "ब्रुणी",
      "cc": "BN",
      "ct": "Bandar Seri Begawan",
      "hct": "बंदर सेरी बेगावान"
    }, {
      "ac": "BDO",
      "an": "Husein Sastranegara",
      "han": "हुसैन सास्त्राणेगरा",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Bandung",
      "hct": "बंदून्ग"
    }, {
      "ac": "BPX",
      "an": "Qamdo Bangda",
      "han": "क़म्डो बांग्डा",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Bangda",
      "hct": "बांग्डा"
    }, {
      "ac": "BGR",
      "an": "Bangor",
      "han": "बंगोर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bangor",
      "hct": "बंगोर"
    }, {
      "ac": "BGF",
      "an": "Bangui",
      "han": "बंगुइ",
      "cn": "Central African Republic",
      "hcn": "सेन्ट्रल आफ़्रिकन रीपबलीक",
      "cc": "CF",
      "ct": "Bangui",
      "hct": "बंगुइ"
    }, {
      "ac": "BNX",
      "an": "Banja Luka",
      "han": "बन्जा लुका",
      "cn": "Bosnia and Herzegovina",
      "hcn": "बोसनिआ एण्द हेर्ज़ेगोवीना",
      "cc": "BA",
      "ct": "Banja Luka",
      "hct": "बन्जा लुका"
    }, {
      "ac": "BDJ",
      "an": "Syamsudin Noor",
      "han": "स्याम्सुदीन नूर",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Banjarmasin",
      "hct": "बन्जर्मासीन"
    }, {
      "ac": "BJL",
      "an": "Yundum",
      "han": "यून्दूम",
      "cn": "Gambia",
      "hcn": "गम्बिआ",
      "cc": "GM",
      "ct": "Banjul",
      "hct": "बन्जुल"
    }, {
      "ac": "BMV",
      "an": "Buon Ma Thuot",
      "han": "बुओन मा थुओट",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Buonmethuot",
      "hct": "बुओन्मेठुओट"
    }, {
      "ac": "BSD",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Baoshan",
      "hct": "बाव्शन"
    }, {
      "ac": "BAV",
      "an": "Baotou",
      "han": "बाव्टौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Baotou",
      "hct": "बाव्टौ"
    }, {
      "ac": "BHB",
      "an": "Hancock County",
      "han": "हन्कोक कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bar Harbor",
      "hct": "बार हरबोर"
    }, {
      "ac": "BCI",
      "an": "Barcaldine",
      "han": "बार्कालदीन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Barcaldine ",
      "hct": "बार्कालदीन "
    }, {
      "ac": "BDU",
      "an": "Bardufoss",
      "han": "बर्डुफ़ोस",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Bardufoss",
      "hct": "बर्डुफ़ोस"
    }, {
      "ac": "BRI",
      "an": "Bari",
      "han": "बारी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Bari",
      "hct": "बारी"
    }, {
      "ac": "BNS",
      "an": "Barinas",
      "han": "बरीनस",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Barinas",
      "hct": "बरीनस"
    }, {
      "ac": "BBN",
      "an": "Bario",
      "han": "बरिओ",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Bario",
      "hct": "बरिओ"
    }, {
      "ac": "BAX",
      "an": "Barnaul",
      "han": "बेर्नौल",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Barnaul",
      "hct": "बेर्नौल"
    }, {
      "ac": "BRM",
      "an": "Barquisimeto",
      "han": "बेर्क़ुइसिमेटो",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Barquisimeto",
      "hct": "बेर्क़ुइसिमेटो"
    }, {
      "ac": "BRR",
      "an": "Barra",
      "han": "बेररा",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Barra",
      "hct": "बेररा"
    }, {
      "ac": "EJA",
      "an": "Yariguies",
      "han": "यारिगुइस",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Barrancabermeja",
      "hct": "बेर्रान्काबेर्मेजा"
    }, {
      "ac": "BAQ",
      "an": "E Cortissoz",
      "han": "इ कोर्टिस्सोज़",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Barranquilla",
      "hct": "बेर्रन्क़ुइला"
    }, {
      "ac": "BRA",
      "an": "Barreiras",
      "han": "बार्रेरेस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Barreiras",
      "hct": "बार्रेरेस"
    }, {
      "ac": "BWF",
      "an": "Walney Island",
      "han": "वाल्नी आयलॅन्ड",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Barrow Island",
      "hct": "बेर्रोव आयलॅन्ड"
    }, {
      "ac": "BRW",
      "an": "Barrow WBAS",
      "han": "बेर्रोव व्बेस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Barrow",
      "hct": "बेर्रोव"
    }, {
      "ac": "BTI",
      "an": "Barter Island Lrrs",
      "han": "बरतूर आयलॅन्ड ल्र्र्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Barter Island",
      "hct": "बरतूर आयलॅन्ड"
    }, {
      "ac": "BSO",
      "an": "Basco",
      "han": "बास्को",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Basco",
      "hct": "बास्को"
    }, {
      "ac": "BSL",
      "an": "Basel EuroAirport Swiss",
      "han": "बसेल युरोएअरप़ॉर्ट स्वीस़",
      "cn": "Switzerland",
      "hcn": "स्विट्ज़र‌लैंड‌",
      "cc": "CH",
      "ct": "Basel Mulhouse",
      "hct": "बसेल मलहाऊस"
    }, {
      "ac": "BSX",
      "an": "Pathein",
      "han": "पठैन",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Pathein",
      "hct": "पठैन"
    }, {
      "ac": "BIA",
      "an": "Poretta",
      "han": "पोरेता",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Bastia",
      "hct": "बास्तिआ"
    }, {
      "ac": "BTH",
      "an": "Hang Nadim",
      "han": "हंग नादिम",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Batam",
      "hct": "बाटम"
    }, {
      "ac": "BHS",
      "an": "Bathurst",
      "han": "बथूरस्त",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Bathurst ",
      "hct": "बथूरस्त "
    }, {
      "ac": "ZBF",
      "an": "Bathurst",
      "han": "बथूरस्त",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Bathurst",
      "hct": "बथूरस्त"
    }, {
      "ac": "BAL",
      "an": "Batman",
      "han": "बॅटमन",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Batman",
      "hct": "बॅटमन"
    }, {
      "ac": "BTR",
      "an": "Ryan",
      "han": "र्यान",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Baton Rouge",
      "hct": "बतन रौगे"
    }, {
      "ac": "BUS",
      "an": "Batumi",
      "han": "बतुमी",
      "cn": "Georgia",
      "hcn": "जियोरगिआ",
      "cc": "GE",
      "ct": "Batumi",
      "hct": "बतुमी"
    }, {
      "ac": "BAU",
      "an": "Bauru",
      "han": "बौरू",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Bauru",
      "hct": "बौरू"
    }, {
      "ac": "MBS",
      "an": "Saginaw",
      "han": "सागिनव",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bay City Midland Saginaw",
      "hct": "बे सिटी मिडलॅंद सागिनव"
    }, {
      "ac": "BZB",
      "an": "",
      "han": "",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Bazaruto Island",
      "hct": "बज़ारुटो आयलॅन्ड"
    }, {
      "ac": "XBE",
      "an": "Bearskin Lake",
      "han": "बीर्स्किन लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Bearskin Lake",
      "hct": "बीर्स्किन लेक"
    }, {
      "ac": "BPT",
      "an": "Jefferson Cty",
      "han": "जेफरसन क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Beaumont",
      "hct": "बीउमोन्त"
    }, {
      "ac": "WBQ",
      "an": "Beaver",
      "han": "बीवर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Beaver",
      "hct": "बीवर"
    }, {
      "ac": "BKW",
      "an": "Raleigh Cty Memorial",
      "han": "रालीघ क्टी मेमोरिल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Beckley",
      "hct": "बेक्ली"
    }, {
      "ac": "BED",
      "an": "Bedford",
      "han": "बेदफोर्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bedford Hanscom",
      "hct": "बेदफोर्ड हन्स्कोम"
    }, {
      "ac": "BEU",
      "an": "Bedourie",
      "han": "बेदौरि",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Bedourie ",
      "hct": "बेदौरि "
    }, {
      "ac": "LAQ",
      "an": "La Abraq",
      "han": "ला अब्रक़",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Al Bayda'",
      "hct": "अल बैडा"
    }, {
      "ac": "BHY",
      "an": "Beihai",
      "han": "बीहाई",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Beihai",
      "hct": "बीहाई"
    }, {
      "ac": "BEW",
      "an": "Beira",
      "han": "बेरा",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Beira",
      "hct": "बेरा"
    }, {
      "ac": "BEY",
      "an": "Beirut",
      "han": "बेरुत",
      "cn": "Lebanon",
      "hcn": "लेबानोन",
      "cc": "LB",
      "ct": "Beirut",
      "hct": "बेरुत"
    }, {
      "ac": "BEL",
      "an": "Val De Cans",
      "han": "वाल दे कन्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Belem",
      "hct": "बेलेम"
    }, {
      "ac": "BMY",
      "an": "Belep Islands",
      "han": "बेलेप आयलॅन्ड्स",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Waala",
      "hct": "वाला"
    }, {
      "ac": "BFS",
      "an": "Belfast",
      "han": "बेल्फ़ास्ट",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Belfast",
      "hct": "बेल्फ़ास्ट"
    }, {
      "ac": "BHD",
      "an": "Belfast City",
      "han": "बेल्फ़ास्ट सिटी",
      "cn": "Northern Ireland",
      "hcn": "नोर्ठर्न आय‌र‌लैंड‌",
      "cc": "XB",
      "ct": "Belfast",
      "hct": "बेल्फ़ास्ट"
    }, {
      "ac": "EGO",
      "an": "Belgorod",
      "han": "बेल्गोरोड",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Belgorod",
      "hct": "बेल्गोरोड"
    }, {
      "ac": "BEG",
      "an": "Belgrade Beograd",
      "han": "बेलग्रेड बियोग्रेद",
      "cn": "Serbia",
      "hcn": "सेरबिया",
      "cc": "CS",
      "ct": "Belgrade",
      "hct": "बेलग्रेड"
    }, {
      "ac": "ZEL",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Bella Bella",
      "hct": "बेला बेला"
    }, {
      "ac": "QBC",
      "an": "Bella Coola",
      "han": "बेला कूला",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Bella Coola",
      "hct": "बेला कूला"
    }, {
      "ac": "BLV",
      "an": "Scott Afb Midamerica",
      "han": "स्कॉट अफ़्ब मिदमेरिका",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Belleville",
      "hct": "बॅलविले"
    }, {
      "ac": "BLI",
      "an": "Bellingham",
      "han": "बेलींघम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bellingham",
      "hct": "बेलींघम"
    }, {
      "ac": "BNY",
      "an": "Bellona",
      "han": "बेलोना",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Bellona",
      "hct": "बेलोना"
    }, {
      "ac": "BHZ",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Belo Horizonte      Belo Horizon",
      "hct": "बेलो होरिज़ोनटे      बेलो होरिज़ोन"
    }, {
      "ac": "CNF",
      "an": "Tancredo Neves",
      "han": "तनक्रेदो नेव्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Belo Horizonte      Belo Horizon",
      "hct": "बेलो होरिज़ोनटे      बेलो होरिज़ोन"
    }, {
      "ac": "PLU",
      "an": "Pampulha",
      "han": "पंपूलहा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Belo Horizonte      Belo Horizon",
      "hct": "बेलो होरिज़ोनटे      बेलो होरिज़ोन"
    }, {
      "ac": "BMD",
      "an": "Belo sur Tsiribihina",
      "han": "बेलो सुर ट्सिरिबिहीना",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Belo sur Tsiribihina",
      "hct": "बेलो सुर ट्सिरिबिहीना"
    }, {
      "ac": "BJI",
      "an": "Bemidji Municipal",
      "han": "बेमिडजी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bemidji",
      "hct": "बेमिडजी"
    }, {
      "ac": "BEB",
      "an": "Benbecula",
      "han": "बेन्बेकुला",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Benbecula",
      "hct": "बेन्बेकुला"
    }, {
      "ac": "RDM",
      "an": "Roberts Field",
      "han": "रॉबर्ट्स फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bend Redmond",
      "hct": "बेंद रेद्मोन्द"
    }, {
      "ac": "BEN",
      "an": "Benina",
      "han": "बेनीना",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Benghazi",
      "hct": "बेनघज़ी"
    }, {
      "ac": "BKS",
      "an": "Fatmawati Soekarno",
      "han": "फातमावती सोयकर्नो",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Bengkulu",
      "hct": "बेन्ग्कुलू"
    }, {
      "ac": "XNA",
      "an": "Northwest Arkansas Regional",
      "han": "नोर्थवॅस्ट आर्कन्सास रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Benton",
      "hct": "बेंटोन"
    }, {
      "ac": "YBV",
      "an": "Berens River",
      "han": "बेरेन्स रिवर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Berens River",
      "hct": "बेरेन्स रिवर"
    }, {
      "ac": "BGO",
      "an": "Flesland",
      "han": "फ़्लेस्लांद",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Bergen",
      "hct": "बॅर्गन"
    }, {
      "ac": "EGC",
      "an": "Roumaniere",
      "han": "रौमाणीएरे",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Bergerac",
      "hct": "बर्गेरक"
    }, {
      "ac": "BJF",
      "an": "Batsfjord",
      "han": "बट्स्फ़्जोर्ड",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Batsfjord",
      "hct": "बट्स्फ़्जोर्ड"
    }, {
      "ac": "BVG",
      "an": "Berlevag",
      "han": "बेर्लेवाग",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Berlevag",
      "hct": "बेर्लेवाग"
    }, {
      "ac": "BRN",
      "an": "Belp",
      "han": "बेल्प",
      "cn": "Switzerland",
      "hcn": "स्विट्ज़र‌लैंड‌",
      "cc": "CH",
      "ct": "Berne",
      "hct": "बेर्णे"
    }, {
      "ac": "BPY",
      "an": "Besalampy",
      "han": "बेसलांपी",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Besalampy",
      "hct": "बेसलांपी"
    }, {
      "ac": "BET",
      "an": "Bethel",
      "han": "बेथेल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bethel",
      "hct": "बेथेल"
    }, {
      "ac": "BTT",
      "an": "Bettles",
      "han": "बेत्ल्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bettles",
      "hct": "बेत्ल्स"
    }, {
      "ac": "BZR",
      "an": "Vias",
      "han": "विआस",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Beziers",
      "hct": "बेज़िएर्ज़"
    }, {
      "ac": "BDP",
      "an": "",
      "han": "",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Bhadrapur",
      "hct": "�द्रपुर"
    }, {
      "ac": "BWA",
      "an": "Bhairahawa",
      "han": "�ैरहवा",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Bhairawa",
      "hct": "�ैरवा"
    }, {
      "ac": "BMO",
      "an": "Banmaw",
      "han": "बन्मौ",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Banmaw",
      "hct": "बन्मौ"
    }, {
      "ac": "BHR",
      "an": "",
      "han": "",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Bharatpur",
      "hct": "�ारतपुर"
    }, {
      "ac": "BIK",
      "an": "Frans Kaisiepo",
      "han": "फ़्रान्स कैसीपो",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Biak",
      "hct": "बियाक"
    }, {
      "ac": "BIQ",
      "an": "Anglet",
      "han": "आंगलेट",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Biarritz",
      "hct": "बिआरीट्ज़"
    }, {
      "ac": "YTL",
      "an": "Big Trout Lake",
      "han": "बिग ट्रूत लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Big Trout Lake",
      "hct": "बिग ट्रूत लेक"
    }, {
      "ac": "BII",
      "an": "Enyu Airfield",
      "han": "एन्यू एअरफ़ील्ड",
      "cn": "Marshall Islands",
      "hcn": "मार्शल आयलॅन्ड्स",
      "cc": "MH",
      "ct": "Bikini Atoll",
      "hct": "बिकिनी अटोल"
    }, {
      "ac": "BIO",
      "an": "Bilbao",
      "han": "बिल्बाव",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Bilbao",
      "hct": "बिल्बाव"
    }, {
      "ac": "BIL",
      "an": "Logan Field",
      "han": "लोगन फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Billings",
      "hct": "बिलींग्स"
    }, {
      "ac": "BLL",
      "an": "Billund",
      "han": "बिलून्ड",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Billund",
      "hct": "बिलून्ड"
    }, {
      "ac": "ZBL",
      "an": "Biloela",
      "han": "बिलोयअला",
      "cn": "Australia ",
      "hcn": "औस्ट्रालिया ",
      "cc": "AU",
      "ct": "Biloela ",
      "hct": "बिलोयअला "
    }, {
      "ac": "BMU",
      "an": "Muhammad Salahuddin",
      "han": "महमद सलाहुद्दीन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Bima",
      "hct": "बीमा"
    }, {
      "ac": "BGM",
      "an": "Edwin A Link Field",
      "han": "एडविन ए लीन्क फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Binghamton Endicott Johnson City",
      "hct": "बींघम्टन एंडिकोत जॉनसन सिटी"
    }, {
      "ac": "BTU",
      "an": "Bintulu",
      "han": "बींटुलू",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Bintulu",
      "hct": "बींटुलू"
    }, {
      "ac": "BIR",
      "an": "Biratnagar",
      "han": "बीरतनगर",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Biratnagar",
      "hct": "बीरतनगर"
    }, {
      "ac": "KBC",
      "an": "Birch Creek",
      "han": "बर्च क्रीक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Birch Creek",
      "hct": "बर्च क्रीक"
    }, {
      "ac": "BVI",
      "an": "Birdsville",
      "han": "बीर्ड्स्वील",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Birdsville",
      "hct": "बीर्ड्स्वील"
    }, {
      "ac": "XBJ",
      "an": "Birjand",
      "han": "बीर्जण्द",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Birjand",
      "hct": "बीर्जण्द"
    }, {
      "ac": "BHH",
      "an": "Bisha",
      "han": "बिशा",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Bisha",
      "hct": "बिशा"
    }, {
      "ac": "FRU",
      "an": "Bishkek",
      "han": "बिश्केक",
      "cn": "Kyrgyzstan",
      "hcn": "क्य्र्ग्य्ज़्स्तान",
      "cc": "KG",
      "ct": "Bishkek",
      "hct": "बिश्केक"
    }, {
      "ac": "BIS",
      "an": "Bismarck Municipal",
      "han": "बिस्मार्क म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bismarck",
      "hct": "बिस्मार्क"
    }, {
      "ac": "OXB",
      "an": "Osvaldo Vieira",
      "han": "ओस्वल्डो वीइरा",
      "cn": "Guinea",
      "hcn": "गुइनी",
      "cc": "Bissau",
      "ct": "GW",
      "hct": "ग्व"
    }, {
      "ac": "YBI",
      "an": "Black Tickle",
      "han": "ब्लैक टिक्ले",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Black Tickle",
      "hct": "ब्लॅक टिक्ले"
    }, {
      "ac": "BKQ",
      "an": "Blackall",
      "han": "ब्लैकऑल",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Blackall",
      "hct": "ब्लॅकऑल"
    }, {
      "ac": "BLK",
      "an": "Blackpool",
      "han": "ब्लाक्पूल",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Blackpool",
      "hct": "ब्लाक्पूल"
    }, {
      "ac": "BLT",
      "an": "Blackwater",
      "han": "ब्लैकवाटर",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Blackwater ",
      "hct": "ब्लॅकवाटर "
    }, {
      "ac": "BQS",
      "an": "Ignatyevo",
      "han": "इग्नात्येवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Blagoveschensk",
      "hct": "ब्लागोवेस्चेन्स्क"
    }, {
      "ac": "YBX",
      "an": "Lourdes De Blanc Sablon",
      "han": "लूर्ड्स दे ब्लान्क सब्लोन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Lourdes",
      "hct": "लूर्ड्स"
    }, {
      "ac": "BLZ",
      "an": "Chileka",
      "han": "चीलेका",
      "cn": "Malawi",
      "hcn": "मालौई",
      "cc": "MW",
      "ct": "Blantyre",
      "hct": "ब्लंत्य्रे"
    }, {
      "ac": "BHE",
      "an": "Woodbourne",
      "han": "वूड्बौर्णे",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Woodbourne",
      "hct": "वूड्बौर्णे"
    }, {
      "ac": "BID",
      "an": "Block Island",
      "han": "ब्लॉक आयलॅन्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Block Island",
      "hct": "ब्लॉक आयलॅन्ड"
    }, {
      "ac": "BFN",
      "an": "Bloemfontein",
      "han": "ब्लोयम्फ़ोन्टैन",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Bloemfontein",
      "hct": "ब्लोयम्फ़ोन्टैन"
    }, {
      "ac": "BMI",
      "an": "Normal",
      "han": "नोरमल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bloomington",
      "hct": "ब्लूमिंग्टन"
    }, {
      "ac": "TRI",
      "an": "Tri City Regional",
      "han": "त्रि सिटी रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Blountville",
      "hct": "ब्लौन्ट्वील"
    }, {
      "ac": "BYH",
      "an": "Arkansas",
      "han": "आर्कन्सास",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Blytheville",
      "hct": "ब्ल्य्ठेवील"
    }, {
      "ac": "BVB",
      "an": "Boa Vista",
      "han": "बोआ विस्ता",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Boa Vista",
      "hct": "बोआ विस्ता"
    }, {
      "ac": "BVC",
      "an": "Rabil",
      "han": "रबिल",
      "cn": "Cape Verde",
      "hcn": "केप‌ व‌र्द",
      "cc": "CV",
      "ct": "Boa Vista",
      "hct": "बोआ विस्ता"
    }, {
      "ac": "BOY",
      "an": "Bobo Dioulasso",
      "han": "बॉबो डियोउलस्सो",
      "cn": "Burkina Faso",
      "hcn": "बूरकिना फ़सो",
      "cc": "BF",
      "ct": "Bobo",
      "hct": "बॉबो"
    }, {
      "ac": "BOO",
      "an": "Bodo",
      "han": "बोडो",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Bodo",
      "hct": "बोडो"
    }, {
      "ac": "BOG",
      "an": "Eldorado",
      "han": "एलडोरादो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Bogota",
      "hct": "बोगोटा"
    }, {
      "ac": "GIC",
      "an": "Boigu",
      "han": "बोईगू",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Boigu Island ",
      "hct": "बोईगू आयलॅन्ड "
    }, {
      "ac": "BOI",
      "an": "Boise Municipal Gowen Field",
      "han": "बोईसे म्यूनिसिपल गौएन फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Boise",
      "hct": "बोईसे"
    }, {
      "ac": "BJB",
      "an": "Bojnourd",
      "han": "बोज्नौर्ड",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Bojnourd",
      "hct": "बोज्नौर्ड"
    }, {
      "ac": "BWK",
      "an": "Brac",
      "han": "ब्रक",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Brac",
      "hct": "ब्रक"
    }, {
      "ac": "BLQ",
      "an": "Guglielmo Marconi",
      "han": "गुग्लील्मो मार्कोनी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Bologna",
      "hct": "बोलोग्ना"
    }, {
      "ac": "BZO",
      "an": "Bolzano",
      "han": "बोल्ज़ानो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Bolzano",
      "hct": "बोल्ज़ानो"
    }, {
      "ac": "YVB",
      "an": "Bonaventure",
      "han": "बोनवन्टूर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Bonaventure",
      "hct": "बोनवन्टूर"
    }, {
      "ac": "BOB",
      "an": "Motu Mute",
      "han": "मोटू मुटे",
      "cn": "French Polynesia",
      "hcn": "फ़्रेन्च पॉलीनेसिआ",
      "cc": "PF",
      "ct": "Bora Bora",
      "hct": "बोरा बोरा"
    }, {
      "ac": "BOD",
      "an": "Bordeaux",
      "han": "बोर्डीउक्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Bordeaux",
      "hct": "बोर्डीउक्स"
    }, {
      "ac": "BLE",
      "an": "Borlange",
      "han": "बोर्लंगे",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Borlange",
      "hct": "बोर्लंगे"
    }, {
      "ac": "RNN",
      "an": "Bornholm Ronne",
      "han": "बोर्न्होल्म रोनने",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Ronne",
      "hct": "रोनने"
    }, {
      "ac": "BLD",
      "an": "Boulder City Municipal",
      "han": "बौल्डर सिटी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Boulder City",
      "hct": "बौल्डर सिटी"
    }, {
      "ac": "BQL",
      "an": "Boulia",
      "han": "बौलिआ",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Boulia ",
      "hct": "बौलिआ "
    }, {
      "ac": "BOJ",
      "an": "Bourgas",
      "han": "बौर्गस",
      "cn": "Bulgaria",
      "hcn": "बूलगारिआ",
      "cc": "BG",
      "ct": "Bourgas",
      "hct": "बौर्गस"
    }, {
      "ac": "BOH",
      "an": "Bournemouth",
      "han": "बौर्णेमौठ",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Bournemouth",
      "hct": "बौर्णेमौठ"
    }, {
      "ac": "BZN",
      "an": "Gallatin Field",
      "han": "गल्लातीन फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bozeman",
      "hct": "बोज़ेमॅन"
    }, {
      "ac": "SRQ",
      "an": "Sarasota Bradenton",
      "han": "सारसोटा ब्रेदेन्टन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bradenton Sarasota",
      "hct": "ब्रेदेन्टन सारसोटा"
    }, {
      "ac": "BRF",
      "an": "Bradford",
      "han": "ब्रेड्फ़ोर्ड",
      "cn": "United Kingdom ",
      "hcn": "युनाईटेड किंग्डोम ",
      "cc": "GB",
      "ct": "Bradford ",
      "hct": "ब्रेड्फ़र्ड "
    }, {
      "ac": "BFD",
      "an": "Bradford Regional",
      "han": "ब्रेड्फ़ोर्ड रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bradford",
      "hct": "ब्रेड्फ़र्ड"
    }, {
      "ac": "BRD",
      "an": "Crow Wing County",
      "han": "क्रो विंग कौंट्री",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Brainerd",
      "hct": "ब्रायनर्ड"
    }, {
      "ac": "YBR",
      "an": "Brandon Muni",
      "han": "ब्रॅंदन मुनि",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Brandon",
      "hct": "ब्रॅंदन"
    }, {
      "ac": "BSB",
      "an": "Brasilia",
      "han": "ब्रसिलिआ",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Brasilia",
      "hct": "ब्रसिलिआ"
    }, {
      "ac": "BTS",
      "an": "Ivanka",
      "han": "इवान्का",
      "cn": "Slovakia",
      "hcn": "स्लोवकिआ",
      "cc": "SK",
      "ct": "Bratislava",
      "hct": "ब्रतिस्लावा"
    }, {
      "ac": "BTK",
      "an": "Bratsk",
      "han": "ब्राट्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Bratsk",
      "hct": "ब्राट्स्क"
    }, {
      "ac": "BZV",
      "an": "Maya Maya",
      "han": "माया माया",
      "cn": "Congo",
      "hcn": "कांगो",
      "cc": "CG",
      "ct": "Brazzaville",
      "hct": "ब्रज़्ज़वील"
    }, {
      "ac": "BRE",
      "an": "Bremen",
      "han": "ब्रेमेन",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Bremen",
      "hct": "ब्रेमेन"
    }, {
      "ac": "BES",
      "an": "Guipavas",
      "han": "गुइपावस",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Brest",
      "hct": "ब्रेस्ट"
    }, {
      "ac": "BDR",
      "an": "Igor I Sikorsky Mem",
      "han": "इगोर आई सिकोर्स्की मेम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Stratford",
      "hct": "स्ट्रट्फ़र्ड"
    }, {
      "ac": "BGI",
      "an": "Grantley Adams",
      "han": "ग्रांटले एडम्स",
      "cn": "Barbados",
      "hcn": "बार्बडोस",
      "cc": "BB",
      "ct": "Bridge Town",
      "hct": "ब्रीज टाउन"
    }, {
      "ac": "BSH",
      "an": "Brighton",
      "han": "ब्राईटन",
      "cn": "United Kingdom ",
      "hcn": "युनाईटेड किंग्डोम ",
      "cc": "GB",
      "ct": "Brighton ",
      "hct": "ब्राईटन "
    }, {
      "ac": "BDS",
      "an": "Casale",
      "han": "कासाले",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Brindisi",
      "hct": "ब्रिंदिसी"
    }, {
      "ac": "BRS",
      "an": "Bristol",
      "han": "ब्रिस्टोल",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Bristol",
      "hct": "ब्रिस्टोल"
    }, {
      "ac": "BVE",
      "an": "La Roche",
      "han": "ला रोश",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Brive",
      "hct": "ब्रिवे"
    }, {
      "ac": "BZZ",
      "an": "Brize Norton",
      "han": "ब्रिज़े नोर्टन",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Brize Norton",
      "hct": "ब्रिज़े नोर्टन"
    }, {
      "ac": "BRQ",
      "an": "Turany",
      "han": "तूराणी",
      "cn": "Czech Republic",
      "hcn": "क्ज़ेच रीपबलीक",
      "cc": "CZ",
      "ct": "Turany",
      "hct": "तूराणी"
    }, {
      "ac": "YBT",
      "an": "Brochet",
      "han": "ब्रोचेत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Brochet",
      "hct": "ब्रोचेत"
    }, {
      "ac": "XBR",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Brockville",
      "hct": "ब्रोक्वील"
    }, {
      "ac": "BHQ",
      "an": "Broken Hill",
      "han": "ब्रोकेन हील",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Broken Hill ",
      "hct": "ब्रोकेन हील "
    }, {
      "ac": "BNN",
      "an": "Bronnoy",
      "han": "ब्रोन्नोय",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Bronnoysund",
      "hct": "ब्रोन्नोय्सुन्ड"
    }, {
      "ac": "RBH",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Brooks Lodge",
      "hct": "ब्रूक्स लॉज"
    }, {
      "ac": "BME",
      "an": "Broome",
      "han": "ब्रूम",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Broome",
      "hct": "ब्रूम"
    }, {
      "ac": "BRO",
      "an": "South Padre Island",
      "han": "साउथ पद्रे आयलॅन्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Brownsville",
      "hct": "ब्रौन्स्वील"
    }, {
      "ac": "BQK",
      "an": "Gylnco Jet Port",
      "han": "ग्य्ल्न्को जेट पोर्ट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Brunswick",
      "hct": "ब्रुन्स्विक"
    }, {
      "ac": "BGA",
      "an": "Palo Negro",
      "han": "पालो नेग्रो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Bucaramanga",
      "hct": "बकारमंगा"
    }, {
      "ac": "BKC",
      "an": "Buckland",
      "han": "बक्लैंड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Buckland",
      "hct": "बक्लांद"
    }, {
      "ac": "BUN",
      "an": "Gerardo Tobar Lopez",
      "han": "गेरार्डो टोबर लोपेज़",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Buenaventura",
      "hct": "बुएनवन्टूरा"
    }, {
      "ac": "BFO",
      "an": "Buffalo Range",
      "han": "बुफालो रेंज",
      "cn": "Zimbabwe",
      "hcn": "ज़िम्बाब्वे",
      "cc": "ZW",
      "ct": "Chiredzi",
      "hct": "चीरेड्ज़ी"
    }, {
      "ac": "BJM",
      "an": "Bujumbura",
      "han": "बुजुम्बुरा",
      "cn": "Burundi",
      "hcn": "बुरुन्डी",
      "cc": "BI",
      "ct": "Bujumbura",
      "hct": "बुजुम्बुरा"
    }, {
      "ac": "BUA",
      "an": "Buka",
      "han": "बुका",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Buka Island",
      "hct": "बुका आयलॅन्ड"
    }, {
      "ac": "BKY",
      "an": "Bukavu Kavumu",
      "han": "बुकवू कवुमू",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Bukavu kavumu",
      "hct": "बुकवू कवुमू"
    }, {
      "ac": "BHK",
      "an": "Bukhara",
      "han": "बुखारा",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Bukhara",
      "hct": "बुखारा"
    }, {
      "ac": "BKZ",
      "an": "Bukoba",
      "han": "बुकोबा",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Bukoba",
      "hct": "बुकोबा"
    }, {
      "ac": "BUQ",
      "an": "Bulawayo",
      "han": "बुलावायो",
      "cn": "Zimbabwe",
      "hcn": "ज़िम्बाब्वे",
      "cc": "ZW",
      "ct": "Bulawayo",
      "hct": "बुलावायो"
    }, {
      "ac": "IFP",
      "an": "Laughlin Bullhead",
      "han": "लौघ्लीन बूलहेड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Bullhead City",
      "hct": "बूलहेड सिटी"
    }, {
      "ac": "BDB",
      "an": "Bundaberg",
      "han": "बुंदाबर्ग",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Bundaberg",
      "hct": "बुंदाबर्ग"
    }, {
      "ac": "BUX",
      "an": "Bunia",
      "han": "बुनिआ",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Bunia",
      "hct": "बुनिआ"
    }, {
      "ac": "BUR",
      "an": "Burbank Glendale Pasadena",
      "han": "बूरबैंक ग्लेंडाळे पासादेना",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Burbank",
      "hct": "बूरबैंक"
    }, {
      "ac": "LEV",
      "an": "Levuka Airfield",
      "han": "लेवुका एअरफ़ील्ड",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Levuka",
      "hct": "लेवुका"
    }, {
      "ac": "BFV",
      "an": "Buri Ram",
      "han": "बरी राम",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Buri Ram",
      "hct": "बरी राम"
    }, {
      "ac": "BUC",
      "an": "Burketown",
      "han": "बूरकेटौन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Burketown ",
      "hct": "बूरकेटौन "
    }, {
      "ac": "BRL",
      "an": "Burlington Municipal",
      "han": "बरलींग्टन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Burlington",
      "hct": "बरलींग्टन"
    }, {
      "ac": "BTV",
      "an": "Burlington",
      "han": "बरलींग्टन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Burlington",
      "hct": "बरलींग्टन"
    }, {
      "ac": "BWT",
      "an": "Wynyard",
      "han": "वीन्यार्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Burnie",
      "hct": "बूरनी"
    }, {
      "ac": "YPZ",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Burns Lake",
      "hct": "बूरन्स लेक"
    }, {
      "ac": "YEI",
      "an": "Yenisehir",
      "han": "येनीसेहीर",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Yenisehir",
      "hct": "येनीसेहीर"
    }, {
      "ac": "BUZ",
      "an": "Bushehr",
      "han": "बुशेह्र",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Bushehr",
      "hct": "बुशेह्र"
    }, {
      "ac": "USU",
      "an": "Busuanga",
      "han": "बसुआंगा",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Busuanga",
      "hct": "बसुआंगा"
    }, {
      "ac": "BTM",
      "an": "Bert Mooney",
      "han": "बेर्ट मूनी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Butte",
      "hct": "बटे"
    }, {
      "ac": "BXU",
      "an": "Butuan",
      "han": "बुटुआन",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Butuan",
      "hct": "बुटुआन"
    }, {
      "ac": "BZG",
      "an": "Bydgoszcz Ignacy Jan Paderewski",
      "han": "ब्य्ड्गोस्ज़्क्ज़ इग्नेकी जन पादेर्यूज्की",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Bydgoszcz",
      "hct": "ब्य्ड्गोस्ज़्क्ज़"
    }, {
      "ac": "CAH",
      "an": "Ca Mau",
      "han": "का मऊ",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Ca Mau",
      "hct": "का मऊ"
    }, {
      "ac": "CFR",
      "an": "Carpiquet",
      "han": "कर्पिक़्त",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Caen",
      "hct": "कएन"
    }, {
      "ac": "CGY",
      "an": "Lumbia",
      "han": "लुम्बिआ",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Cagayan De Oro",
      "hct": "कगायान दे ओरो"
    }, {
      "ac": "CAG",
      "an": "Elmas",
      "han": "एल्मास",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Cagliari",
      "hct": "कग्लिरी"
    }, {
      "ac": "CNS",
      "an": "Cairns",
      "han": "काईर्न्स",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Cairns",
      "hct": "काईर्न्स"
    }, {
      "ac": "CJA",
      "an": "Mayor General FAP Armando Revoredo Iglesias",
      "han": "मयोर जनरल फ़प आर्मंदो रेवोरेदो इग्लेसिआस",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Cajamarca",
      "hct": "कजमर्का"
    }, {
      "ac": "CJC",
      "an": "El Loa",
      "han": "एल लोआ",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Calama",
      "hct": "कलमा"
    }, {
      "ac": "CYP",
      "an": "Calbayog",
      "han": "कल्बायोग",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Calbayog City",
      "hct": "कल्बायोग सिटी"
    }, {
      "ac": "CDW",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Caldwell",
      "hct": "काल्ड्वेल"
    }, {
      "ac": "CLO",
      "an": "Alfonso Bonilla Aragon",
      "han": "आल्फान्सो बोनीला आरगोन",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Cali",
      "hct": "काली"
    }, {
      "ac": "CLY",
      "an": "Saint Catherine",
      "han": "सेंट कॅथेरिन",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Calvi",
      "hct": "कल्वी"
    }, {
      "ac": "CXR",
      "an": "Cam Ranh",
      "han": "केम रान्ह",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Nha Trang",
      "hct": "न्हा टरंग"
    }, {
      "ac": "YCB",
      "an": "Cambridge Bay",
      "han": "केम्ब्रीज बे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Cambridge Bay",
      "hct": "केम्ब्रीज बे"
    }, {
      "ac": "CBG",
      "an": "Cambridge",
      "han": "केम्ब्रीज",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Cambridge",
      "hct": "केम्ब्रीज"
    }, {
      "ac": "CGM",
      "an": "Camiguin",
      "han": "केमिगुइन",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Camiguin ",
      "hct": "केमिगुइन "
    }, {
      "ac": "YBL",
      "an": "Campbell River Municipal",
      "han": "कैम्पबेल रिवर म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Campbell River",
      "hct": "कैम्पबेल रिवर"
    }, {
      "ac": "CAL",
      "an": "Campbeltown",
      "han": "केंप्बेल्टाउन",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Campbeltown",
      "hct": "केंप्बेल्टौन"
    }, {
      "ac": "CPE",
      "an": "Ingeniero Alberto Acuna Ongay",
      "han": "इंगेनिएरो अलबर्टो आकुना ओनगाय",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Campeche",
      "hct": "केम्पेचे"
    }, {
      "ac": "CPV",
      "an": "Presidente Joao Suassuna",
      "han": "प्रेसिडेंटे जोआओ सुआस्सुना",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Campina Grande",
      "hct": "केंपिना ग्रांडे"
    }, {
      "ac": "CPQ",
      "an": "Campinas Int'l",
      "han": "केंपिनस इंतएल",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Campinas",
      "hct": "केंपिनस"
    }, {
      "ac": "CGR",
      "an": "Campo Grande",
      "han": "केंपो ग्रांडे",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Campo Grande",
      "hct": "केंपो ग्रांडे"
    }, {
      "ac": "CAW",
      "an": "Bartolomeu Lisandro",
      "han": "बार्टोलोमेउ लिसन्द्रो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Campos",
      "hct": "कम्पोस"
    }, {
      "ac": "CKZ",
      "an": "Canakkale",
      "han": "कानककाले",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Canakkale",
      "hct": "कानककाले"
    }, {
      "ac": "CBR",
      "an": "Canberra",
      "han": "कन्बेररा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Canberra",
      "hct": "कन्बेररा"
    }, {
      "ac": "CUN",
      "an": "Cancun Aeropuerto Internacional",
      "han": "कन्कुन एरोपुएर्टो इंटर्नेसियोनल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Cancun",
      "hct": "कन्कुन"
    }, {
      "ac": "CSK",
      "an": "Cap Skiring",
      "han": "कॅप स्किरिंग",
      "cn": "Senegal",
      "hcn": "सेनेगल",
      "cc": "SN",
      "ct": "Cap Skiring",
      "hct": "कॅप स्किरिंग"
    }, {
      "ac": "YTE",
      "an": "Cape Dorset",
      "han": "केपे दोर्सेत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Cape Dorset",
      "hct": "केपे दोर्सेत"
    }, {
      "ac": "CGI",
      "an": "Cape Girardeau Municipal",
      "han": "केपे गीरेर्डीऊ म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cape Girardeau",
      "hct": "केपे गीरेर्डीऊ"
    }, {
      "ac": "LUR",
      "an": "Cape Lisburne Lrrs",
      "han": "केपे लिस्बूरणे ल्र्र्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cape Lisburne",
      "hct": "केपे लिस्बूरणे"
    }, {
      "ac": "EHM",
      "an": "Cape Newenham Lrrs",
      "han": "केपे नेवेन्हेम ल्र्र्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cape Newenham",
      "hct": "केपे नेवेन्हेम"
    }, {
      "ac": "CAP",
      "an": "Cap Haitien",
      "han": "कॅप हैटीन",
      "cn": "Haiti",
      "hcn": "हैटी",
      "cc": "HT",
      "ct": "Cap Haitien",
      "hct": "कॅप हैटीन"
    }, {
      "ac": "CCS",
      "an": "Simon Bolivar",
      "han": "साइमन बोलीवार",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Caracas",
      "hct": "कारकास"
    }, {
      "ac": "CKS",
      "an": "Carajas",
      "han": "काराजेस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Carajas",
      "hct": "काराजेस"
    }, {
      "ac": "CWL",
      "an": "Cardiff Wales",
      "han": "कार्डिफ वालेस",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Cardiff",
      "hct": "कार्डिफ"
    }, {
      "ac": "CAX",
      "an": "Carlisle",
      "han": "कार्लिसले",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Carlisle",
      "hct": "कार्लिसले"
    }, {
      "ac": "CLD",
      "an": "Carlsbad",
      "han": "कार्ल्सबाद",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Diego",
      "hct": "सेन दीएगो"
    }, {
      "ac": "CNM",
      "an": "Cavern City Air Terminal",
      "han": "कवर्न सिटी एअर टर्मिनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Carlsbad",
      "hct": "कार्ल्सबाद"
    }, {
      "ac": "MRY",
      "an": "Monterey Peninsula",
      "han": "मॉन्टेरे पेनिन्सूला",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Carmel Monterey",
      "hct": "कार्मल मॉन्टेरे"
    }, {
      "ac": "CVQ",
      "an": "Carnarvon",
      "han": "कार्नर्वोन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Carnarvon ",
      "hct": "कार्नर्वोन "
    }, {
      "ac": "CTG",
      "an": "Rafael Nunez",
      "han": "रफ़एल नुणेज़",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Cartagena",
      "hct": "कर्तजना"
    }, {
      "ac": "YRF",
      "an": "Cartwright",
      "han": "कर्ट्व्रिघ्ट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Cartwright",
      "hct": "कर्ट्व्रिघ्ट"
    }, {
      "ac": "CUP",
      "an": "General Jose Francisco Bermudez",
      "han": "जनरल जोस फ़्रान्सिस्को बेर्मुडेज़",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Carupano",
      "hct": "करुपनो"
    }, {
      "ac": "CMN",
      "an": "Mohamed V",
      "han": "मोहम्मद वी",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Casablanca",
      "hct": "कासाब्लान्का"
    }, {
      "ac": "CAC",
      "an": "Cascavel",
      "han": "कास्कवेल",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Cascavel",
      "hct": "कास्कवेल"
    }, {
      "ac": "CPR",
      "an": "Natrona Cty",
      "han": "नात्रोणा क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Casper",
      "hct": "कॅस्पर"
    }, {
      "ac": "YCG",
      "an": "Ralph West",
      "han": "राल्फ वॅस्ट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "CASTLEGAR",
      "hct": "कासलगर"
    }, {
      "ac": "DCM",
      "an": "Mazamet",
      "han": "मज़मेत",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Castres",
      "hct": "कास्टरेस"
    }, {
      "ac": "YAC",
      "an": "Cat Lake",
      "han": "कॅट लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Cat Lake",
      "hct": "कॅट लेक"
    }, {
      "ac": "CTC",
      "an": "Catamarca",
      "han": "कतामर्का",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Catamarca",
      "hct": "कतामर्का"
    }, {
      "ac": "CTA",
      "an": "Fontanarossa",
      "han": "फ़ोन्तनरोस्सा",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Catania",
      "hct": "कताणिआ"
    }, {
      "ac": "CRM",
      "an": "Catarman National",
      "han": "कतार्मॅन नेशनल",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Catarman",
      "hct": "कतार्मॅन"
    }, {
      "ac": "MPH",
      "an": "Godofredo P",
      "han": "गोडोफ़्रेदो पी",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Caticlan",
      "hct": "कतिक्लन"
    }, {
      "ac": "CXJ",
      "an": "Campo Dos Bugres",
      "han": "केंपो दोस बुग्र्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Caxias Do Sul",
      "hct": "कक्षिआस दो सूल"
    }, {
      "ac": "CAY",
      "an": "Rochambeau",
      "han": "रोचम्बीऊ",
      "cn": "French Guiana",
      "hcn": "फ़्रेन्च गुइना",
      "cc": "GF",
      "ct": "Cayenne",
      "hct": "कयेनने"
    }, {
      "ac": "CEB",
      "an": "Cebu",
      "han": "सेबू",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Cebu",
      "hct": "सेबू"
    }, {
      "ac": "CDC",
      "an": "Cedar City Municipal",
      "han": "सेदार सिटी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cedar City",
      "hct": "सेदार सिटी"
    }, {
      "ac": "CID",
      "an": "Cedar Rapids Municipal",
      "han": "सेदार रेपिड्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cedar Rapids",
      "hct": "सेदार रेपिड्स"
    }, {
      "ac": "CED",
      "an": "Ceduna",
      "han": "सेदुना",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Ceduna ",
      "hct": "सेदुना "
    }, {
      "ac": "CEM",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Central",
      "hct": "सेन्ट्रल"
    }, {
      "ac": "CDR",
      "an": "Chadron",
      "han": "चद्रों",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chadron",
      "hct": "चद्रोन"
    }, {
      "ac": "ZBR",
      "an": "Chah Bahar",
      "han": "चाह बहार",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Chah Bahar",
      "hct": "चाह बहार"
    }, {
      "ac": "CIK",
      "an": "Chalkyitsik",
      "han": "चळ्क्यित्सिक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chalkyitsik",
      "hct": "चलिक्यित्सिक"
    }, {
      "ac": "CMF",
      "an": "Chambery Aix Les Bains",
      "han": "चंबेरी आईक्स ल्स बैन्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Chambery",
      "hct": "चंबेरी"
    }, {
      "ac": "CMI",
      "an": "Univ Of Illinois Willard",
      "han": "ऊनिव ऑफ़ इलीनोईस विलार्द",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Champaign",
      "hct": "चंपैग्न"
    }, {
      "ac": "CGQ",
      "an": "Changchun",
      "han": "चंगचुन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Changchun",
      "hct": "चंगचुन"
    }, {
      "ac": "CGD",
      "an": "Changde",
      "han": "चंगदे",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Changde",
      "hct": "चंगदे"
    }, {
      "ac": "CSX",
      "an": "Huanghua",
      "han": "हुआंघुआ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Changcha",
      "hct": "चंग्चा"
    }, {
      "ac": "CIH",
      "an": "Changzhi",
      "han": "चंग्ऴी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Changzhi",
      "hct": "चंग्ऴी"
    }, {
      "ac": "CZX",
      "an": "Changzhou",
      "han": "चंग्ऴौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Changzhou",
      "hct": "चंग्ऴौ"
    }, {
      "ac": "CHQ",
      "an": "Souda",
      "han": "सौदा",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Chania",
      "hct": "चाणिआ"
    }, {
      "ac": "XAP",
      "an": "Chapeco",
      "han": "चापेको",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Chapeco",
      "hct": "चापेको"
    }, {
      "ac": "CHS",
      "an": "Charleston",
      "han": "चार्लेस्तन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Charleston",
      "hct": "चार्लेस्तन"
    }, {
      "ac": "CRW",
      "an": "Yeager",
      "han": "येगर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Charleston",
      "hct": "चार्लेस्तन"
    }, {
      "ac": "CTL",
      "an": "Charleville",
      "han": "चार्लेवील",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Charleville",
      "hct": "चार्लेवील"
    }, {
      "ac": "CHO",
      "an": "Charlottesville Albemarle",
      "han": "चार्लोटेस्वील आल्बेमरले",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Charlottesville",
      "hct": "चार्लोटेस्वील"
    }, {
      "ac": "YHG",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Charlottetown",
      "hct": "चार्लोटेटाउन"
    }, {
      "ac": "YYG",
      "an": "Charlottetown Municipal",
      "han": "चार्लोटेटाउन म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Charlottetown",
      "hct": "चार्लोटेटाउन"
    }, {
      "ac": "CHT",
      "an": "Chatham Islands",
      "han": "च्थम आयलॅन्ड्स",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Chatham Island",
      "hct": "च्थम आयलॅन्ड"
    }, {
      "ac": "XCM",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Chatham",
      "hct": "च्थम"
    }, {
      "ac": "CHA",
      "an": "Chattanooga Lovell Fld",
      "han": "चटनूगा लोवेल फ़्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chattanooga",
      "hct": "चटनूगा"
    }, {
      "ac": "CSY",
      "an": "Cheboksary",
      "han": "चेबोकसारी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Cheboksary",
      "hct": "चेबोकसारी"
    }, {
      "ac": "CYF",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chefornak",
      "hct": "चेफ़ोर्नाक"
    }, {
      "ac": "CEK",
      "an": "Balandino",
      "han": "बालांदीनो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Chelyabinsk",
      "hct": "चेल्याबीन्स्क"
    }, {
      "ac": "CTU",
      "an": "Shuangliu",
      "han": "शुआंग्लीऊ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Chengdu",
      "hct": "चेन्ग्डू"
    }, {
      "ac": "CJJ",
      "an": "Cheongju",
      "han": "चियोन्ग्जू",
      "cn": "South Korea",
      "hcn": "द‌क्षिण‌ कोरिया",
      "cc": "KR",
      "ct": "Chongju",
      "hct": "चोंग्जू"
    }, {
      "ac": "CER",
      "an": "Maupertus",
      "han": "मऊपेर्तस",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Cherbourg",
      "hct": "चेर्बौर्ग"
    }, {
      "ac": "CEE",
      "an": "Cherepovets",
      "han": "चेरेपोवेट्स",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Cherepovets",
      "hct": "चेरेपोवेट्स"
    }, {
      "ac": "CWC",
      "an": "Chernivtsi",
      "han": "चर्निव्ट्सी",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Chernovtsk",
      "hct": "चर्नोव्ट्स्क"
    }, {
      "ac": "CYX",
      "an": "Cherskiy",
      "han": "चर्स्किय",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Cherskiy",
      "hct": "चर्स्किय"
    }, {
      "ac": "CEG",
      "an": "Hawarden",
      "han": "हवर्ड्न",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Hawarden",
      "hct": "हवर्ड्न"
    }, {
      "ac": "YCS",
      "an": "Chesterfield Inlet",
      "han": "चेस्टरफ़ील्ड इनलेट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Chesterfield Inlet",
      "hct": "चेस्टरफ़ील्ड इनलेट"
    }, {
      "ac": "CTM",
      "an": "Chetumal",
      "han": "चेतूमल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Chetumal",
      "hct": "चेतूमल"
    }, {
      "ac": "VAK",
      "an": "Chevak",
      "han": "चेवक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chevak",
      "hct": "चेवक"
    }, {
      "ac": "YHR",
      "an": "Chevery",
      "han": "चेवेरी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Chevery",
      "hct": "चेवेरी"
    }, {
      "ac": "CYS",
      "an": "Cheyenne",
      "han": "चीएनने",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cheyenne",
      "hct": "चीएनने"
    }, {
      "ac": "CNX",
      "an": "Chiang Mai",
      "han": "चियांग मै",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Chiang Mai",
      "hct": "चियांग मै"
    }, {
      "ac": "CEI",
      "an": "Chaing Rai",
      "han": "चायन्ग राय",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Chiang Rai",
      "hct": "चियांग राय"
    }, {
      "ac": "CYI",
      "an": "Chiayi",
      "han": "चियायी",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Chiayi",
      "hct": "चियायी"
    }, {
      "ac": "YMT",
      "an": "Chibougamau",
      "han": "चिबौगमऊ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Chibougamau",
      "hct": "चिबौगमऊ"
    }, {
      "ac": "CIX",
      "an": "Cornel Ruiz",
      "han": "कोर्नेल रुइज़",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Chiclayo",
      "hct": "चिक्लायो"
    }, {
      "ac": "CIC",
      "an": "Chico Municipal",
      "han": "चिको म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chico",
      "hct": "चिको"
    }, {
      "ac": "CIF",
      "an": "Chifeng",
      "han": "चिफ़ेन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Chifeng",
      "hct": "चिफ़ेन्ग"
    }, {
      "ac": "KCG",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chignik",
      "hct": "चिग्निक"
    }, {
      "ac": "KCL",
      "an": "Chignik",
      "han": "चिग्निक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chignik",
      "hct": "चिग्निक"
    }, {
      "ac": "KCQ",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chignik",
      "hct": "चिग्निक"
    }, {
      "ac": "CUU",
      "an": "Chihuahua",
      "han": "चिहुआहुआ",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Chihuahua",
      "hct": "चिहुआहुआ"
    }, {
      "ac": "VPY",
      "an": "Chimoio",
      "han": "चिमोईओ",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Chimoio",
      "hct": "चिमोईओ"
    }, {
      "ac": "JKH",
      "an": "Chios",
      "han": "चियोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Chios",
      "hct": "चियोस"
    }, {
      "ac": "CIP",
      "an": "Chipata",
      "han": "चिपता",
      "cn": "Zambia",
      "hcn": "ज़ाम्बीया",
      "cc": "ZM",
      "ct": "Chipata",
      "hct": "चिपता"
    }, {
      "ac": "CZN",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chisana",
      "hct": "चीसना"
    }, {
      "ac": "YKU",
      "an": "Chisasibi",
      "han": "चीससिबी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Chisasibi",
      "hct": "चीससिबी"
    }, {
      "ac": "HIB",
      "an": "Hibbing Chisolm",
      "han": "हिब्बींग चीसोल्म",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chisholm Hibbing",
      "hct": "चीशोल्म हिब्बींग"
    }, {
      "ac": "KIV",
      "an": "Chisinau",
      "han": "चीसीनऊ",
      "cn": "Moldova",
      "hcn": "मोल्डोवा",
      "cc": "MD",
      "ct": "Chisinau",
      "hct": "चीसीनऊ"
    }, {
      "ac": "HTA",
      "an": "Kadala",
      "han": "कडअला",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Chita",
      "hct": "चिता"
    }, {
      "ac": "CJL",
      "an": "Chitral",
      "han": "चित्र्ल",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Chitral",
      "hct": "चित्र्ल"
    }, {
      "ac": "CGP",
      "an": "Patenga",
      "han": "पटेन्गा",
      "cn": "Bangladesh",
      "hcn": "बांग्लादेश",
      "cc": "BD",
      "ct": "Chittagong",
      "hct": "चितगोन्ग"
    }, {
      "ac": "CHY",
      "an": "Choiseul Bay",
      "han": "चोईसेउल बे",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Choiseul Bay",
      "hct": "चोईसेउल बे"
    }, {
      "ac": "CKH",
      "an": "",
      "han": "",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Chokurdah",
      "hct": "चोकूरडाह"
    }, {
      "ac": "CKG",
      "an": "Jiangbei",
      "han": "जिआंग्बी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Chongqing",
      "hct": "चोंग्क़िंग"
    }, {
      "ac": "XCH",
      "an": "Christmas Island",
      "han": "क्राइस्टमेस आयलॅन्ड",
      "cn": "Christmas Island",
      "hcn": "क्राइस्टमेस आयलॅन्ड",
      "cc": "CX",
      "ct": "Christmas Island",
      "hct": "क्राइस्टमेस आयलॅन्ड"
    }, {
      "ac": "CXI",
      "an": "Christmas Island",
      "han": "क्राइस्टमेस आयलॅन्ड",
      "cn": "Kiribati",
      "hcn": "किरिबती",
      "cc": "KI",
      "ct": "Kiritimati",
      "hct": "किरितीमती"
    }, {
      "ac": "CHU",
      "an": "Chuathbaluk",
      "han": "चुआठ्बालुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Chuathbaluk",
      "hct": "चुआठ्बालुक"
    }, {
      "ac": "ZUM",
      "an": "Churchill Falls",
      "han": "चर्चिल फॉल्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Churchill Falls",
      "hct": "चर्चिल फॉल्स"
    }, {
      "ac": "YYQ",
      "an": "Churchill",
      "han": "चर्चिल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Churchill",
      "hct": "चर्चिल"
    }, {
      "ac": "ICI",
      "an": "Cicia",
      "han": "सिसिया",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Cicia",
      "hct": "सिसिया"
    }, {
      "ac": "IRC",
      "an": "Circle City",
      "han": "सर्कल सिटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Circle",
      "hct": "सर्कल"
    }, {
      "ac": "CBL",
      "an": "Ciudad Bolivar",
      "han": "सीऊदाद बोलीवार",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Ciudad Bolivar",
      "hct": "सीऊदाद बोलीवार"
    }, {
      "ac": "CME",
      "an": "Ciudad Del Carmen",
      "han": "सीऊदाद देल कारमेन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Ciudad Del Carmen",
      "hct": "सीऊदाद देल कारमेन"
    }, {
      "ac": "AGT",
      "an": "Ciudad del Este",
      "han": "सीऊदाद देल एस्ते",
      "cn": "Paraguay",
      "hcn": "परगुआय",
      "cc": "PY",
      "ct": "Ciudad del Este",
      "hct": "सीऊदाद देल एस्ते"
    }, {
      "ac": "CJS",
      "an": "Intl Abraham Gonzalez",
      "han": "इंट्ल अब्राहम गोन्ज़ालेज़",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Ciudad Juarez",
      "hct": "सीऊदाद जुआरेज़"
    }, {
      "ac": "CEN",
      "an": "Ciudad Obregon",
      "han": "सीऊदाद ओब्रेगोन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Ciudad Obregon",
      "hct": "सीऊदाद ओब्रेगोन"
    }, {
      "ac": "CVM",
      "an": "General Pedro Jose Mendez",
      "han": "जनरल पेद्रो जोस मेंडेज़",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Ciudad Victoria",
      "hct": "सीऊदाद विक्टोरिआ"
    }, {
      "ac": "CLP",
      "an": "Clarks Point",
      "han": "क्लार्क्स पोईंत",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Clarks Point",
      "hct": "क्लार्क्स पोईंत"
    }, {
      "ac": "CKB",
      "an": "Clarksburg Benedum",
      "han": "क्लार्क्स्बूरग बेन्दूम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Clarksburg",
      "hct": "क्लार्क्स्बूरग"
    }, {
      "ac": "CFE",
      "an": "Auvergne",
      "han": "अऊवर्गने",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Clermont Ferrand",
      "hct": "क्लर्मोन्त फ़ेर्रंड"
    }, {
      "ac": "CNJ",
      "an": "Cloncurry",
      "han": "क्लोन्कूररी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Cloncurry ",
      "hct": "क्लोन्कूररी "
    }, {
      "ac": "CVN",
      "an": "Clovis",
      "han": "क्लोविस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Clovis",
      "hct": "क्लोविस"
    }, {
      "ac": "CMK",
      "an": "Club Makokola",
      "han": "क्लब मकोकोला",
      "cn": "Malawi",
      "hcn": "मालौई",
      "cc": "MW",
      "ct": "Club Makokola",
      "hct": "क्लब मकोकोला"
    }, {
      "ac": "CLJ",
      "an": "Napoca",
      "han": "नापोका",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Cluj",
      "hct": "क्लुज"
    }, {
      "ac": "YCY",
      "an": "Clyde River",
      "han": "क्ल्य्डे रिवर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Clyde River",
      "hct": "क्ल्य्डे रिवर"
    }, {
      "ac": "CIJ",
      "an": "Heroes Del Acre",
      "han": "हेरोयस देल एकर",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Cobija",
      "hct": "कोबीजा"
    }, {
      "ac": "OCC",
      "an": "Francisco De Orellana",
      "han": "फ़्रान्सिस्को दे ओरेलना",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Coca",
      "hct": "कोका"
    }, {
      "ac": "CBB",
      "an": "J Wilsterman",
      "han": "जे विल्स्टर्मॅन",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Cochabamba",
      "hct": "कोचाबाम्बा"
    }, {
      "ac": "CNC",
      "an": "Coconut Island",
      "han": "कोकोनट आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Coconut Island ",
      "hct": "कोकोनट आयलॅन्ड "
    }, {
      "ac": "CCK",
      "an": "",
      "han": "",
      "cn": "Cocos Keeling Islands",
      "hcn": "कोकोस कीलींग आयलॅन्ड्स",
      "cc": "CC",
      "ct": "Cocos Islands",
      "hct": "कोकोस आयलॅन्ड्स"
    }, {
      "ac": "COD",
      "an": "Yellowstone Regional",
      "han": "येलौस्टोन रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cody Yellowstone",
      "hct": "कोडी येलौस्टोन"
    }, {
      "ac": "CUQ",
      "an": "Coen",
      "han": "कोयन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Coen ",
      "hct": "कोयन "
    }, {
      "ac": "KCC",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Coffman Cove",
      "hct": "कोफ्मॅन कोव"
    }, {
      "ac": "CFS",
      "an": "Coffs Harbour",
      "han": "कोफ्स हर्बौर",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Coff's Harbour",
      "hct": "कोफएस हर्बौर"
    }, {
      "ac": "CDB",
      "an": "Cold Bay",
      "han": "कोल्ड बे",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cold Bay",
      "hct": "कोल्ड बे"
    }, {
      "ac": "CLQ",
      "an": "Colima",
      "han": "कोलीमा",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Colima",
      "hct": "कोलीमा"
    }, {
      "ac": "CLL",
      "an": "Easterwood Field",
      "han": "ईस्टरवुड फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "College Station",
      "hct": "कॉलेज स्टेशन"
    }, {
      "ac": "CGN",
      "an": "Koeln Bonn",
      "han": "कोयल्न बोन्न",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Cologne",
      "hct": "कोलोगने"
    }, {
      "ac": "COS",
      "an": "Colorado Springs Municipal",
      "han": "कोलोरादो स्प्रिंग्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Colorado Springs",
      "hct": "कोलोरादो स्प्रिंग्स"
    }, {
      "ac": "CAE",
      "an": "Columbia Metro",
      "han": "कोलंम्बिआ मेट्रो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Columbia",
      "hct": "कोलंम्बिआ"
    }, {
      "ac": "COU",
      "an": "Columbia Regional",
      "han": "कोलंम्बिआ रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Columbia",
      "hct": "कोलंम्बिआ"
    }, {
      "ac": "CMH",
      "an": "Port Columbus",
      "han": "प़ॉर्ट कोलंबस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Columbus",
      "hct": "कोलंबस"
    }, {
      "ac": "CSG",
      "an": "Columbus Metro Ft Benning",
      "han": "कोलंबस मेट्रो फ़्ट बेन्निन्ग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Columbus",
      "hct": "कोलंबस"
    }, {
      "ac": "GTR",
      "an": "Golden",
      "han": "गोल्डन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Columbus",
      "hct": "कोलंबस"
    }, {
      "ac": "YCK",
      "an": "Colville Lake",
      "han": "कोल्वील लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Colville Lake",
      "hct": "कोल्वील लेक"
    }, {
      "ac": "CRD",
      "an": "Comodoro Rivadavia",
      "han": "कोमोडोरो रिवडविआ",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Comodoro Rivadavia",
      "hct": "कोमोडोरो रिवडविआ"
    }, {
      "ac": "YQQ",
      "an": "Royal Canadian Air Force Station",
      "han": "रॉयल कानडिन एअर फ़ोर्स स्टेशन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Comox",
      "hct": "कोमोक्स"
    }, {
      "ac": "VCS",
      "an": "Co Ong",
      "han": "को ओन्ग",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Conson",
      "hct": "कॉनसन"
    }, {
      "ac": "CCP",
      "an": "Carriel Sur",
      "han": "कारीएल सुर",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Concepcion",
      "hct": "कोन्सेप्कियोन"
    }, {
      "ac": "CND",
      "an": "Kogalniceanu",
      "han": "कोगल्निसीनू",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Constanta",
      "hct": "कन्स्टंता"
    }, {
      "ac": "CPD",
      "an": "Coober Pedy",
      "han": "कोओबेर पेडी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Coober Pedy ",
      "hct": "कोओबेर पेडी "
    }, {
      "ac": "CTN",
      "an": "Cooktown",
      "han": "कूकटाउन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Cooktown ",
      "hct": "कूकटाउन "
    }, {
      "ac": "CPO",
      "an": "Chamonate",
      "han": "चमोनाटे",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Copiapo",
      "hct": "कोपियापो"
    }, {
      "ac": "YZS",
      "an": "Fairmont Hot Springs",
      "han": "फ़ैर्मोन्ट हॉट स्प्रिंग्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Coral Harbour",
      "hct": "कोरल हर्बौर"
    }, {
      "ac": "COR",
      "an": "Pajas Blanco",
      "han": "पजेस ब्लान्को",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Cordoba",
      "hct": "कोर्डोबा"
    }, {
      "ac": "CDV",
      "an": "Mudhole Smith",
      "han": "मुधोल स्मिथ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cordova",
      "hct": "कोर्डोवा"
    }, {
      "ac": "CFU",
      "an": "I Kapodistrias",
      "han": "आई कपोडिस्ट्रिआस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kerkyra",
      "hct": "केर्क्यरा"
    }, {
      "ac": "ORK",
      "an": "Cork",
      "han": "कोर्क",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Cork",
      "hct": "कोर्क"
    }, {
      "ac": "ELM",
      "an": "Elmira Corning Regional",
      "han": "एल्मीरा कॉर्निन्ग रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Corning Elmira",
      "hct": "कॉर्निन्ग एल्मीरा"
    }, {
      "ac": "YCC",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Cornwall",
      "hct": "कोर्नवाल"
    }, {
      "ac": "CZE",
      "an": "Jose Leonardo Chirinos",
      "han": "जोस लियोनार्डो चीरिनोस",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Coro",
      "hct": "कोरो"
    }, {
      "ac": "CZU",
      "an": "Las Brujas",
      "han": "लस बृजेस",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Corozal",
      "hct": "कोरोज़ाल"
    }, {
      "ac": "CRP",
      "an": "Corpus Christi",
      "han": "कोर्पुस क्रिस्ती",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Corpus Christi",
      "hct": "कोर्पुस क्रिस्ती"
    }, {
      "ac": "CNQ",
      "an": "Corrientes",
      "han": "कोरींट्स",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Corrientes",
      "hct": "कोरींट्स"
    }, {
      "ac": "CEZ",
      "an": "Montezuma County",
      "han": "मोन्तेज़ुमा कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Cortez",
      "hct": "कोर्टेज़"
    }, {
      "ac": "CMG",
      "an": "Corumba",
      "han": "कोरअम्बा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Corumba",
      "hct": "कोरअम्बा"
    }, {
      "ac": "CVU",
      "an": "Corvo",
      "han": "कोर्वो",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Corvo",
      "hct": "कोर्वो"
    }, {
      "ac": "CBO",
      "an": "Cotabato",
      "han": "कोटाबतो",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Cotabato",
      "hct": "कोटाबतो"
    }, {
      "ac": "COO",
      "an": "Cotonou",
      "han": "कोटोनौ",
      "cn": "Benin",
      "hcn": "बेनिन",
      "cc": "BJ",
      "ct": "Cotonou",
      "hct": "कोटोनौ"
    }, {
      "ac": "YCA",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Courtenay",
      "hct": "कौर्टेनय"
    }, {
      "ac": "CVT",
      "an": "Coventry",
      "han": "कोवन्त्री",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Coventry",
      "hct": "कोवन्त्री"
    }, {
      "ac": "CXB",
      "an": "Coxs Bazar",
      "han": "कोक्स बाज़ार",
      "cn": "Bangladesh",
      "hcn": "बांग्लादेश",
      "cc": "BD",
      "ct": "Cox's Bazar",
      "hct": "कोक्सएस बज़ार"
    }, {
      "ac": "CZM",
      "an": "Aeropuerto De Cozumel",
      "han": "एरोपुएर्टो दे कोज़ुमेल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Cozumel",
      "hct": "कोज़ुमेल"
    }, {
      "ac": "CCV",
      "an": "Craig Cove",
      "han": "क्रैग कोव",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Craig Cove",
      "hct": "क्रैग कोव"
    }, {
      "ac": "CGA",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Craig",
      "hct": "क्रैग"
    }, {
      "ac": "CRA",
      "an": "Craiova",
      "han": "क्राईओवा",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Craiova",
      "hct": "क्राईओवा"
    }, {
      "ac": "YXC",
      "an": "Canadian Rockies",
      "han": "कानडिन रोकीस",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Cranbrook",
      "hct": "क्रन्ब्रूक"
    }, {
      "ac": "CEC",
      "an": "Crescent City Municipal",
      "han": "क्रिस्सेंट सिटी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Crescent City",
      "hct": "क्रिस्सेंट सिटी"
    }, {
      "ac": "CKD",
      "an": "Crooked Creek",
      "han": "क्रूकेद क्रीक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Crooked Creek",
      "hct": "क्रूकेद क्रीक"
    }, {
      "ac": "YCR",
      "an": "Cross Lake",
      "han": "क्रॉस लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Cross Lake",
      "hct": "क्रॉस लेक"
    }, {
      "ac": "CRV",
      "an": "Crotone",
      "han": "क्रोटोन",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Crotone",
      "hct": "क्रोटोन"
    }, {
      "ac": "CZS",
      "an": "Cruzeiro Do Sul",
      "han": "क्रुज़ेरो दो सूल",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Cruiziro Do Sul",
      "hct": "क्रुइज़िरो दो सूल"
    }, {
      "ac": "CUC",
      "an": "Camilo Dazo",
      "han": "केमीलो दज़ो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Cucuta",
      "hct": "कुकता"
    }, {
      "ac": "CUE",
      "an": "Cuenca",
      "han": "कुएन्का",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Cuenca",
      "hct": "कुएन्का"
    }, {
      "ac": "CGB",
      "an": "Marechal Rondon",
      "han": "मारेचाल रोनदान",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Cuiaba",
      "hct": "कुइब"
    }, {
      "ac": "CUL",
      "an": "Fedl De Bachigualato",
      "han": "फ़ेड्ल दे बचिगुआलातो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Culiacan",
      "hct": "कुलियाकन"
    }, {
      "ac": "CUM",
      "an": "Antonio Jose De Sucre",
      "han": "एंटोनियो जोस दे सक्रे",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Cumana",
      "hct": "कमना"
    }, {
      "ac": "CUF",
      "an": "Levaldigi",
      "han": "लेवल्दिगी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Levaldigi",
      "hct": "लेवल्दिगी"
    }, {
      "ac": "CMA",
      "an": "Cunnamulla",
      "han": "कुन्नामूल्ला",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Cunnamulla ",
      "hct": "कुन्नामूल्ला "
    }, {
      "ac": "CUR",
      "an": "Areopuerto Hato",
      "han": "आरियोपुएर्टो हतो",
      "cn": "Netherlands Antilles",
      "hcn": "नेद‌र‌लैंड‌ एंटिलेस",
      "cc": "AN",
      "ct": "Curacao",
      "hct": "करकाव"
    }, {
      "ac": "CWB",
      "an": "Afonso Pena",
      "han": "अफ़ोन्सो पेना",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Curitiba",
      "hct": "करितिबा"
    }, {
      "ac": "CUZ",
      "an": "Tte Velazco Astete",
      "han": "ते वेलाज़्को आस्टेटे",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Cuzco",
      "hct": "कुज़्को"
    }, {
      "ac": "DAD",
      "an": "Danang",
      "han": "दनंग",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Danang",
      "hct": "दनंग"
    }, {
      "ac": "TAE",
      "an": "Daegu Ab",
      "han": "डाएगू अब",
      "cn": "South Korea",
      "hcn": "द‌क्षिण‌ कोरिया",
      "cc": "KR",
      "ct": "Taegu",
      "hct": "तएगू"
    }, {
      "ac": "DKR",
      "an": "Leopold Sedar Senghor",
      "han": "लियोपोल्ड सेदार सेनघोर",
      "cn": "Senegal",
      "hcn": "सेनेगल",
      "cc": "SN",
      "ct": "Dakar",
      "hct": "दकर"
    }, {
      "ac": "VIL",
      "an": "Dakhla",
      "han": "दख्ला",
      "cn": "Western Sahara",
      "hcn": "प‌श्चिमी स‌हारा",
      "cc": "EH",
      "ct": "Dakhla",
      "hct": "दख्ला"
    }, {
      "ac": "DLM",
      "an": "Dalman",
      "han": "डाल्मॅन",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Dalaman",
      "hct": "दलामॅन"
    }, {
      "ac": "DLI",
      "an": "Dalat",
      "han": "दलत",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Dalat",
      "hct": "दलत"
    }, {
      "ac": "DBA",
      "an": "Dalbandin",
      "han": "दल्बंदिन",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Dalbandin",
      "hct": "दल्बंदिन"
    }, {
      "ac": "DLU",
      "an": "Dali",
      "han": "डाली",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dali",
      "hct": "डाली"
    }, {
      "ac": "DLC",
      "an": "Zhoushuizi",
      "han": "जौशुइज़ी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dalian",
      "hct": "दलिआन"
    }, {
      "ac": "DFW",
      "an": "Dallas Ft Worth",
      "han": "दालास फ़्ट वर्थ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dallas Fort Worth",
      "hct": "दालास फ़ोर्ट वर्थ"
    }, {
      "ac": "DAL",
      "an": "Love Field",
      "han": "लव फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "DALLAS",
      "hct": "दालास"
    }, {
      "ac": "DAM",
      "an": "Damascus",
      "han": "दमास्कस",
      "cn": "Syria",
      "hcn": "सिरिआ",
      "cc": "SY",
      "ct": "Damascus",
      "hct": "दमास्कस"
    }, {
      "ac": "DDG",
      "an": "Dandong",
      "han": "डंडोंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dandong",
      "hct": "डंडोंग"
    }, {
      "ac": "NLF",
      "an": "Darnley Island",
      "han": "दर्नले आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Darnley Island",
      "hct": "दर्नले आयलॅन्ड"
    }, {
      "ac": "DAU",
      "an": "Daru",
      "han": "दारू",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Daru",
      "hct": "दारू"
    }, {
      "ac": "DRW",
      "an": "Darwin",
      "han": "डार्विन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Darwin",
      "hct": "डार्विन"
    }, {
      "ac": "DTD",
      "an": "Datadawai",
      "han": "दातादवाई",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Datadawai",
      "hct": "दातादवाई"
    }, {
      "ac": "DAT",
      "an": "Datong",
      "han": "दतोन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Datong",
      "hct": "दतोन्ग"
    }, {
      "ac": "YDN",
      "an": "Dauphin Barker",
      "han": "दऊफीन बारकर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Dauphin",
      "hct": "दऊफीन"
    }, {
      "ac": "DVO",
      "an": "Mati",
      "han": "मती",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Davao",
      "hct": "दवाव"
    }, {
      "ac": "DWD",
      "an": "Dawadmi Domestic",
      "han": "दवाद्मी डोमेस्टिक",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Dawadmi",
      "hct": "दवाद्मी"
    }, {
      "ac": "TVY",
      "an": "Dawei",
      "han": "दवी",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Dawei",
      "hct": "दवी"
    }, {
      "ac": "YDA",
      "an": "Dawson City",
      "han": "डौसन सिटी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Dawson",
      "hct": "डौसन"
    }, {
      "ac": "YDQ",
      "an": "Dawson Creek",
      "han": "डौसन क्रीक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Dawson Creek",
      "hct": "डौसन क्रीक"
    }, {
      "ac": "DAX",
      "an": "Dachuan",
      "han": "दचुआन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dazhou",
      "hct": "दऴौ"
    }, {
      "ac": "DYG",
      "an": "Dayong",
      "han": "दायोन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dayong",
      "hct": "दायोन्ग"
    }, {
      "ac": "DAY",
      "an": "Dayton",
      "han": "दैटन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dayton",
      "hct": "दैटन"
    }, {
      "ac": "DAB",
      "an": "Daytona Beach Regional",
      "han": "दैटोणा बीच रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Daytona Beach",
      "hct": "दैटोणा बीच"
    }, {
      "ac": "DEC",
      "an": "Decatur Municipal",
      "han": "डेकतर म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Decatur",
      "hct": "डेकतर"
    }, {
      "ac": "YDF",
      "an": "Deer Lake",
      "han": "दीर लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Deer Lake",
      "hct": "दीर लेक"
    }, {
      "ac": "YVZ",
      "an": "Deer Lake",
      "han": "दीर लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Deer Lake",
      "hct": "दीर लेक"
    }, {
      "ac": "DRG",
      "an": "Deering",
      "han": "दीरिंग",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Deering",
      "hct": "दीरिंग"
    }, {
      "ac": "YWJ",
      "an": "Deline",
      "han": "देलाईन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Deline",
      "hct": "देलाईन"
    }, {
      "ac": "DNZ",
      "an": "Cardak",
      "han": "कार्डक",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Denizli",
      "hct": "देनिज़्ली"
    }, {
      "ac": "DEA",
      "an": "Dera Ghazi Khan",
      "han": "डेरा घज़ी खान",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Dera Ghazi Khan",
      "hct": "डेरा घज़ी खान"
    }, {
      "ac": "DSM",
      "an": "Des Moines Municipal",
      "han": "देस मोईन्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Des Moines",
      "hct": "देस मोईन्स"
    }, {
      "ac": "DVL",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Devils Lake",
      "hct": "देवील्स लेक"
    }, {
      "ac": "DPO",
      "an": "Devonport",
      "han": "देवोन्पोर्ट",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Devonport ",
      "hct": "देवोन्पोर्ट "
    }, {
      "ac": "DIK",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dickinson",
      "hct": "डिकिनसन"
    }, {
      "ac": "DIN",
      "an": "Dien Bien Phu",
      "han": "दीन बीन फू",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Dienbienphu",
      "hct": "दीन्बीन्फू"
    }, {
      "ac": "DIJ",
      "an": "Longvic",
      "han": "लोन्ग्विक",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Dijon",
      "hct": "दिजोन"
    }, {
      "ac": "DKS",
      "an": "",
      "han": "",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Dikson",
      "hct": "दिकसन"
    }, {
      "ac": "DIL",
      "an": "Presidente Nicolau Lobato",
      "han": "प्रेसिडेंटे निकोलऊ लोबतो",
      "cn": "East Timor",
      "hcn": "ईस्ट टिमोर",
      "cc": "TP",
      "ct": "Dili",
      "hct": "दिली"
    }, {
      "ac": "DLG",
      "an": "Dillingham Municipal",
      "han": "दिलींघम म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dillingham",
      "hct": "दिलींघम"
    }, {
      "ac": "DLY",
      "an": "Dillon's Bay",
      "han": "दिल्लोनएस बे",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Dillon's Bay",
      "hct": "दिल्लोनएस बे"
    }, {
      "ac": "DNR",
      "an": "Pleurtuit",
      "han": "प्लेउर्तुइत",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Dinard",
      "hct": "दीनार्ड"
    }, {
      "ac": "DPL",
      "an": "Dipolog",
      "han": "दिपोलोग",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Dipolog",
      "hct": "दिपोलोग"
    }, {
      "ac": "DIG",
      "an": "Diqing",
      "han": "दिक़िंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Shangri",
      "hct": "शांगरी"
    }, {
      "ac": "DIR",
      "an": "Dire Dawa",
      "han": "डिरे दवा",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Dire Dawa",
      "hct": "डिरे दवा"
    }, {
      "ac": "DIY",
      "an": "Diyarbakir",
      "han": "दियर्बकिर",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Diyabakir",
      "hct": "दियाबकिर"
    }, {
      "ac": "DJE",
      "an": "Zarzis",
      "han": "ज़र्ज़िस",
      "cn": "Tunisia",
      "hcn": "ट्यूनीशिया",
      "cc": "TN",
      "ct": "Djerba",
      "hct": "ड्जेर्बा"
    }, {
      "ac": "JIB",
      "an": "Ambouli",
      "han": "अम्बौली",
      "cn": "Djibouti",
      "hcn": "ड्जिबौती",
      "cc": "DJ",
      "ct": "Djibouti",
      "hct": "ड्जिबौती"
    }, {
      "ac": "DNK",
      "an": "Dnipropetrovsk",
      "han": "ड्निप्रोपेट्रोव्स्क",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Dnepropetrovsk",
      "hct": "ड्नेप्रोपेट्रोव्स्क"
    }, {
      "ac": "DDC",
      "an": "Dodge City Municipal",
      "han": "डोडगे सिटी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dodge City",
      "hct": "दोद्गे सिटी"
    }, {
      "ac": "DOP",
      "an": "Dolpa",
      "han": "डोल्पा",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Dolpa",
      "hct": "डोल्पा"
    }, {
      "ac": "DSA",
      "an": "Robin Hood Doncaster Sheffield",
      "han": "रॉबिन हूड डोन्केस्टर शेफील्ड",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Doncaster",
      "hct": "डोन्केस्टर"
    }, {
      "ac": "CFN",
      "an": "Donegal",
      "han": "डोनेगल",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Dongloe",
      "hct": "डोंग्लोय"
    }, {
      "ac": "DOK",
      "an": "Donetsk",
      "han": "डोनेट्स्क",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Donetsk",
      "hct": "डोनेट्स्क"
    }, {
      "ac": "DSN",
      "an": "Dongsheng",
      "han": "डोंग्शेन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dongsheng",
      "hct": "डोंग्शेन्ग"
    }, {
      "ac": "DOY",
      "an": "Dongying",
      "han": "डोंग्यिंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dongying",
      "hct": "डोंग्यिंग"
    }, {
      "ac": "DMD",
      "an": "Doomadgee",
      "han": "दूमद्गेए",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Doomadgee ",
      "hct": "दूमद्गेए "
    }, {
      "ac": "DTM",
      "an": "Wickede Dortmund",
      "han": "विकेडे दोर्टमून्ड",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Dortmund",
      "hct": "दोर्टमून्ड"
    }, {
      "ac": "DHN",
      "an": "Dothan Municipal",
      "han": "दोथान म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dothan",
      "hct": "दोथान"
    }, {
      "ac": "DOU",
      "an": "Dourados",
      "han": "डौरेदोस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Dourados",
      "hct": "डौरेदोस"
    }, {
      "ac": "DRS",
      "an": "Dresden",
      "han": "ड्रेस्ड्न",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Dresden",
      "hct": "ड्रेस्ड्न"
    }, {
      "ac": "YHD",
      "an": "Dryden Municipal",
      "han": "ड्र्य्ड्न म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Dryden",
      "hct": "ड्र्य्ड्न"
    }, {
      "ac": "DBO",
      "an": "Dubbo",
      "han": "डुब्बो",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Dubbo",
      "hct": "डुब्बो"
    }, {
      "ac": "DUJ",
      "an": "Dubois Jefferson Cty",
      "han": "दुबोईस जेफरसन क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Du Bois",
      "hct": "दू बोईस"
    }, {
      "ac": "DBV",
      "an": "Dubrovnik",
      "han": "दुब्रोव्निक",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Dubrovnik",
      "hct": "दुब्रोव्निक"
    }, {
      "ac": "DBQ",
      "an": "Dubuque Municipal",
      "han": "दुबुक़ म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dubuque",
      "hct": "दुबुक़"
    }, {
      "ac": "DLH",
      "an": "Duluth",
      "han": "दूलुठ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Duluth",
      "hct": "दूलुठ"
    }, {
      "ac": "DGT",
      "an": "Dumaguete",
      "han": "दुमागुएटे",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Dumaguete",
      "hct": "दुमागुएटे"
    }, {
      "ac": "DUQ",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Duncan/Quam",
      "hct": "डंकन/क़म"
    }, {
      "ac": "DND",
      "an": "Dundee",
      "han": "दुन्दी",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Dundee",
      "hct": "दुन्दी"
    }, {
      "ac": "DUD",
      "an": "Momona",
      "han": "मोमोना",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Dunedin",
      "hct": "दूणेडीन"
    }, {
      "ac": "DNH",
      "an": "Dunhuang",
      "han": "दन्हुआंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Dunhuang",
      "hct": "दन्हुआंग"
    }, {
      "ac": "DGO",
      "an": "Durango",
      "han": "दुरन्गो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Durango",
      "hct": "डुरन्गो"
    }, {
      "ac": "DRO",
      "an": "Durango La Plata Cty",
      "han": "दुरन्गो ला पलता क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Durango",
      "hct": "डुरन्गो"
    }, {
      "ac": "DYU",
      "an": "Dushanbe",
      "han": "दुशनबे",
      "cn": "Tajikistan",
      "hcn": "ताजिकिस्तान",
      "cc": "TJ",
      "ct": "Dushanbe",
      "hct": "दुशनबे"
    }, {
      "ac": "DUT",
      "an": "Emergency Field",
      "han": "इमर्जन्सी फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Dutch Harbor",
      "hct": "डच हरबोर"
    }, {
      "ac": "DZA",
      "an": "Dzaoudzi",
      "han": "ड्ज़ौड्ज़ी",
      "cn": "Mayotte",
      "hcn": "मयोत",
      "cc": "YT",
      "ct": "Dzaoudzi",
      "hct": "ड्ज़ौड्ज़ी"
    }, {
      "ac": "EAA",
      "an": "Eagle",
      "han": "ईगल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Eagle",
      "hct": "ईगल"
    }, {
      "ac": "ELS",
      "an": "East London",
      "han": "ईस्ट लंडन",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "East London",
      "hct": "ईस्ट लंडन"
    }, {
      "ac": "ZEM",
      "an": "Eastmain River",
      "han": "ईस्ट्मैन रिवर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Eastmain River",
      "hct": "ईस्ट्मैन रिवर"
    }, {
      "ac": "IPC",
      "an": "Mataveri",
      "han": "मटवेरी",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Easter Island",
      "hct": "ईस्टर आयलॅन्ड"
    }, {
      "ac": "ESD",
      "an": "Orcas Island",
      "han": "ओर्कास आयलॅन्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Eastsound",
      "hct": "ईस्टसौन्ड"
    }, {
      "ac": "EAU",
      "an": "Claire Municipal",
      "han": "क्लाईरे म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Eau Claire",
      "hct": "ईऊ क्लाईरे"
    }, {
      "ac": "EDA",
      "an": "Edna Bay",
      "han": "एड्ना बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Edna Bay",
      "hct": "एड्ना बे"
    }, {
      "ac": "EDO",
      "an": "EDREMIT KORFEZ",
      "han": "एद्रेमीत कोर्फ़ेज़",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "EDREMIT KORFEZ",
      "hct": "एद्रेमीत कोर्फ़ेज़"
    }, {
      "ac": "EDR",
      "an": "Pormpuraaw",
      "han": "पोर्म्पूराव",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Edward River ",
      "hct": "एड्वर्ड रिवर "
    }, {
      "ac": "EEK",
      "an": "Eek",
      "han": "ईक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Eek",
      "hct": "ईक"
    }, {
      "ac": "EGX",
      "an": "Egegik",
      "han": "एगेगिक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Egegik",
      "hct": "एगेगिक"
    }, {
      "ac": "EGS",
      "an": "Egilsstadir",
      "han": "एगिल्स्स्तादिर",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Egilsstadir",
      "hct": "एगिल्स्स्तादिर"
    }, {
      "ac": "ETH",
      "an": "Elat",
      "han": "एलाट",
      "cn": "Israel",
      "hcn": "इसरायल",
      "cc": "IL",
      "ct": "Elat",
      "hct": "एलाट"
    }, {
      "ac": "EIN",
      "an": "Welschap",
      "han": "वेल्स्काप",
      "cn": "Netherlands",
      "hcn": "नेद‌र‌लैंड‌",
      "cc": "NL",
      "ct": "Eindhoven",
      "hct": "ऐन्ढोवेन"
    }, {
      "ac": "EIB",
      "an": "Eisenach",
      "han": "ईसेनाच",
      "cn": "Germany ",
      "hcn": "ज‌र्म‌नी ",
      "cc": "DE",
      "ct": "Eisenach ",
      "hct": "ईसेनाच "
    }, {
      "ac": "SVX",
      "an": "Koltsovo",
      "han": "कोल्ट्सोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Ekaterinburg",
      "hct": "एकटेरिन्बूरग"
    }, {
      "ac": "KKU",
      "an": "Ekuk",
      "han": "एकुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ekuk",
      "hct": "एकुक"
    }, {
      "ac": "KEK",
      "an": "Ekwok",
      "han": "एक्वोक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ekwok",
      "hct": "एक्वोक"
    }, {
      "ac": "FTE",
      "an": "El Calafate",
      "han": "एल कलाफ़टे",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "El Calafate",
      "hct": "एल कलाफ़टे"
    }, {
      "ac": "IPL",
      "an": "Imperial County",
      "han": "इम्पीरियल कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "El Centro Imperial",
      "hct": "एल सेन्त्रो इम्पीरियल"
    }, {
      "ac": "ELD",
      "an": "Goodwin Field",
      "han": "गुडविन फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "El Dorado",
      "hct": "एल दोराडो"
    }, {
      "ac": "ENI",
      "an": "El Nido",
      "han": "एल निडो",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "El Nido",
      "hct": "एल निडो"
    }, {
      "ac": "EBD",
      "an": "El Obeid",
      "han": "एल ओबीद",
      "cn": "Sudan",
      "hcn": "सुदान",
      "cc": "SD",
      "ct": "El Obeid",
      "hct": "एल ओबीद"
    }, {
      "ac": "ELP",
      "an": "El Paso",
      "han": "एल पासो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "El Paso",
      "hct": "एल पासो"
    }, {
      "ac": "ESR",
      "an": "El Salvador",
      "han": "एल साल्वेदोर",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "El Salvador",
      "hct": "एल साल्वेदोर"
    }, {
      "ac": "VIG",
      "an": "",
      "han": "",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "El Vigia",
      "hct": "एल विगिआ"
    }, {
      "ac": "EYP",
      "an": "El Alcarava",
      "han": "एल आल्कारवा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Yopal",
      "hct": "योपाल"
    }, {
      "ac": "EZS",
      "an": "Elazig",
      "han": "एलाज़िग",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Elazig",
      "hct": "एलाज़िग"
    }, {
      "ac": "ELC",
      "an": "Elcho Island",
      "han": "एल्चो आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Elcho Island ",
      "hct": "एल्चो आयलॅन्ड "
    }, {
      "ac": "EDL",
      "an": "Eldoret",
      "han": "एल्डोरेत",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Eldoret",
      "hct": "एल्डोरेत"
    }, {
      "ac": "ELV",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Elfin Cove",
      "hct": "एल्फ़िन कोव"
    }, {
      "ac": "ELI",
      "an": "Elim",
      "han": "एलीम",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Elim",
      "hct": "एलीम"
    }, {
      "ac": "EKO",
      "an": "J C Harris Field",
      "han": "जे सी हेरिस फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Elko",
      "hct": "एळ्को"
    }, {
      "ac": "EOZ",
      "an": "",
      "han": "",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Elorza",
      "hct": "एलोर्ज़ा"
    }, {
      "ac": "ELY",
      "an": "Yelland",
      "han": "येलंद",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ely",
      "hct": "एली"
    }, {
      "ac": "EAE",
      "an": "Sangafa",
      "han": "संगफ़ा",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Sangafa",
      "hct": "संगफ़ा"
    }, {
      "ac": "EMD",
      "an": "Emerald",
      "han": "एमेराल्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Emerald",
      "hct": "एमेराल्ड"
    }, {
      "ac": "EMK",
      "an": "Emmonak",
      "han": "एम्मोनक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Emmonak",
      "hct": "एम्मोनक"
    }, {
      "ac": "ENE",
      "an": "H Hasan Aroeboesman",
      "han": "एच हसन आरोयबोयस्मॅन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Ende",
      "hct": "एंडे"
    }, {
      "ac": "EIE",
      "an": "Yeniseysk",
      "han": "येनीसीस्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Yeniseysk",
      "hct": "येनीसीस्क"
    }, {
      "ac": "XWQ",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Enkoping",
      "hct": "एन्कोपींग"
    }, {
      "ac": "ENF",
      "an": "Enontekio",
      "han": "एनोन्टेकियो",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Enontekio",
      "hct": "एनोन्टेकियो"
    }, {
      "ac": "ENH",
      "an": "Enshi",
      "han": "एन्शी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Enshi",
      "hct": "एन्शी"
    }, {
      "ac": "ECN",
      "an": "Ercan",
      "han": "एरकन",
      "cn": "Cyprus",
      "hcn": "सीप्रुस",
      "cc": "CY",
      "ct": "Nicosia",
      "hct": "निकोसिआ"
    }, {
      "ac": "ERF",
      "an": "Erfurt",
      "han": "एर्फ़ूरट",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Erfurt",
      "hct": "एर्फ़ूरट"
    }, {
      "ac": "ERI",
      "an": "Erie",
      "han": "एरि",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Erie",
      "hct": "एरि"
    }, {
      "ac": "ERH",
      "an": "Moulay Ali Cherif",
      "han": "मौले अली चेरिफ़",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Er",
      "hct": "एर"
    }, {
      "ac": "ERC",
      "an": "Erzincan",
      "han": "एर्ज़िनकन",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Erzincan",
      "hct": "एर्ज़िनकन"
    }, {
      "ac": "ERZ",
      "an": "Erzurum",
      "han": "एर्ज़ुरुम",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Erzurum",
      "hct": "एर्ज़ुरुम"
    }, {
      "ac": "EBJ",
      "an": "Esbjerg",
      "han": "एस्ब्जेर्ग",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Esbjerg",
      "hct": "एस्ब्जेर्ग"
    }, {
      "ac": "ESC",
      "an": "Delta County",
      "han": "डेल्टा कौंटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Escanaba",
      "hct": "एस्कानाब"
    }, {
      "ac": "AOE",
      "an": "Anadolu",
      "han": "अनडोलू",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Eskissehir",
      "hct": "एस्किस्सेहीर"
    }, {
      "ac": "ESM",
      "an": "General Rivadeneira",
      "han": "जनरल रिवाद्नेरा",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Tachina",
      "hct": "ताचीना"
    }, {
      "ac": "EPR",
      "an": "Esperance",
      "han": "एस्पेरान्स",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Esperance ",
      "hct": "एस्पेरान्स "
    }, {
      "ac": "SON",
      "an": "Santo Pekoa",
      "han": "संतो पेकोआ",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Santo",
      "hct": "संतो"
    }, {
      "ac": "EQS",
      "an": "Esquel",
      "han": "एस्क़्ल",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Esquel",
      "hct": "एस्क़्ल"
    }, {
      "ac": "ESU",
      "an": "Mogador",
      "han": "मोगादोर",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Essadouira",
      "hct": "एस्सदौइरा"
    }, {
      "ac": "EUA",
      "an": "Kaufana",
      "han": "कऊफ़ना",
      "cn": "Tonga",
      "hcn": "टोंगा",
      "cc": "TO",
      "ct": "Eua Island",
      "hct": "एउआ आयलॅन्ड"
    }, {
      "ac": "EUG",
      "an": "Eugene",
      "han": "युगेने",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Eugene",
      "hct": "युगेने"
    }, {
      "ac": "EVV",
      "an": "Evansville Regional",
      "han": "इवन्सविले रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Evansville",
      "hct": "इवन्सविले"
    }, {
      "ac": "EXI",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Excursion Inlet",
      "hct": "एक्स्कूरशन इनलेट"
    }, {
      "ac": "EXT",
      "an": "Exeter",
      "han": "एक्सेटर",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Exeter",
      "hct": "एक्सेटर"
    }, {
      "ac": "VDB",
      "an": "Leirin",
      "han": "लीरिन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Fagernes",
      "hct": "फ़गर्न्स"
    }, {
      "ac": "FAI",
      "an": "Fairbanks",
      "han": "फेअरबैंक्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fairbanks",
      "hct": "फेअरबैंक्स"
    }, {
      "ac": "LYP",
      "an": "Faisalabad",
      "han": "फ़ैसलाबाद",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Faisalabad",
      "hct": "फ़ैसलाबाद"
    }, {
      "ac": "FKQ",
      "an": "Fak Fak",
      "han": "फ़क फ़क",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Fak Fak",
      "hct": "फ़क फ़क"
    }, {
      "ac": "EWB",
      "an": "New Bedford Municipal",
      "han": "न्यू बेदफोर्ड म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fall River New Bedford",
      "hct": "फ़ल रिवर न्यू बेदफोर्ड"
    }, {
      "ac": "KFP",
      "an": "False Pass",
      "han": "फ़ल्से पास्स",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "False Pass",
      "hct": "फ़ल्से पास्स"
    }, {
      "ac": "RVA",
      "an": "Farafangana",
      "han": "फ़रफ़ंगना",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Farafangana",
      "hct": "फ़रफ़ंगना"
    }, {
      "ac": "FAR",
      "an": "Hector",
      "han": "हेक्टर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fargo",
      "hct": "फ़ार्गो"
    }, {
      "ac": "FMN",
      "an": "Four Corners Regional",
      "han": "फोर कोर्नर्ज़ रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Farmington",
      "hct": "फ़ार्मिंग्टन"
    }, {
      "ac": "FAB",
      "an": "Farnborough",
      "han": "फ़र्न्बोरौघ",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Farnborough",
      "hct": "फ़र्न्बोरौघ"
    }, {
      "ac": "FAO",
      "an": "Faro",
      "han": "फ़रो",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Faro",
      "hct": "फ़रो"
    }, {
      "ac": "FAE",
      "an": "Vagar",
      "han": "वागर",
      "cn": "Faroe Islands",
      "hcn": "फ़रोय आयलॅन्ड्स",
      "cc": "FO",
      "ct": "Vagar",
      "hct": "वागर"
    }, {
      "ac": "FYT",
      "an": "Faya Largeau",
      "han": "फ़य लार्गीऊ",
      "cn": "Chad",
      "hcn": "चड",
      "cc": "TD",
      "ct": "Faya",
      "hct": "फ़य"
    }, {
      "ac": "FAY",
      "an": "Fayetteville Municipal",
      "han": "फ़येटेवील म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fayetteville",
      "hct": "फ़येटेवील"
    }, {
      "ac": "FRE",
      "an": "Fera/Maringe",
      "han": "फ़ेरा/मरिंगे",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Fera Island",
      "hct": "फ़ेरा आयलॅन्ड"
    }, {
      "ac": "FEG",
      "an": "Fergana",
      "han": "फ़र्गना",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Fergana",
      "hct": "फ़र्गना"
    }, {
      "ac": "FEN",
      "an": "Fernando De Noronha",
      "han": "फर्नांडो दे नोरोन्हा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Fernando Do Noronha",
      "hct": "फर्नांडो दो नोरोन्हा"
    }, {
      "ac": "FEZ",
      "an": "Fez",
      "han": "फ़ेज़",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Fez",
      "hct": "फ़ेज़"
    }, {
      "ac": "FSC",
      "an": "Sud Corse",
      "han": "सुद कोर्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Figari",
      "hct": "फ़िगारी"
    }, {
      "ac": "FZO",
      "an": "Bristol Filton",
      "han": "ब्रिस्टोल फ़िल्टन",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Bristol",
      "hct": "ब्रिस्टोल"
    }, {
      "ac": "FLG",
      "an": "Flagstaff",
      "han": "फ़्लाग्स्ताफ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Flagstaff",
      "hct": "फ़्लाग्स्ताफ"
    }, {
      "ac": "XYI",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Flen",
      "hct": "फ़्लेन"
    }, {
      "ac": "YFO",
      "an": "Flin Flon",
      "han": "फ़्लीन फ़्लोन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Flin Flon",
      "hct": "फ़्लीन फ़्लोन"
    }, {
      "ac": "FNT",
      "an": "Bishop",
      "han": "बिशप",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Flint",
      "hct": "फ़्लींत"
    }, {
      "ac": "FLR",
      "an": "Amerigo Vespucci",
      "han": "अमेरिगो वेस्पुक्की",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Florence",
      "hct": "फ्लोरेंस"
    }, {
      "ac": "FLO",
      "an": "Gilbert Field",
      "han": "गिलबर्ट फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Florence",
      "hct": "फ्लोरेंस"
    }, {
      "ac": "MSL",
      "an": "Muscle Shoals",
      "han": "मुस्क्ल शोआल्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Muscle Shoals",
      "hct": "मुस्क्ल शोआल्स"
    }, {
      "ac": "FLA",
      "an": "Gustavo Artunduaga Paredes",
      "han": "गुस्टवो आर्टउन्डूआगा पेरेडेस",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Florencia",
      "hct": "फ़्लोरेनसिया"
    }, {
      "ac": "FLW",
      "an": "Flores",
      "han": "फ़्लोर्स",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Flores Island",
      "hct": "फ़्लोर्स आयलॅन्ड"
    }, {
      "ac": "FLN",
      "an": "Hercilio Luz",
      "han": "हेर्किलियो लुज़",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Florianopolis",
      "hct": "फ़्लोरिआनोपोलिस"
    }, {
      "ac": "FRO",
      "an": "Flora",
      "han": "फ्लोरा",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Floro",
      "hct": "फ़्लोरो"
    }, {
      "ac": "FOG",
      "an": "Gino Lisa",
      "han": "गिनो लिसा",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Foggia",
      "hct": "फ़ोगगिआ"
    }, {
      "ac": "FDE",
      "an": "Forde Bringeland",
      "han": "फ़र्डे ब्रिंगेलांद",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Forde",
      "hct": "फ़र्डे"
    }, {
      "ac": "FRL",
      "an": "Forli",
      "han": "फ़ोर्ली",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Forli",
      "hct": "फ़ोर्ळी"
    }, {
      "ac": "FMA",
      "an": "El Pucu",
      "han": "एल पुकू",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Formosa",
      "hct": "फ़ोर्मोसा"
    }, {
      "ac": "YFA",
      "an": "Fort Albany",
      "han": "फ़ोर्ट अल्बनी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Albany",
      "hct": "फ़ोर्ट अल्बनी"
    }, {
      "ac": "FNL",
      "an": "Fort Collins Loveland Muni",
      "han": "फ़ोर्ट कौलिंस लवलॅंद मुनि",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "F",
      "hct": "एफ़"
    }, {
      "ac": "FTU",
      "an": "Tolagnaro",
      "han": "तॉलग्नरो",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Tolagnaro",
      "hct": "तॉलग्नरो"
    }, {
      "ac": "FOD",
      "an": "Ft Dodge Municipal",
      "han": "फ़्ट दोद्गे म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fort Dodge",
      "hct": "फ़ोर्ट दोद्गे"
    }, {
      "ac": "YAG",
      "an": "Fort फ्रांसs Municipal",
      "han": "फ़ोर्ट फ्रांसिस म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Frances",
      "hct": "फ़ोर्ट फ्रांसिस"
    }, {
      "ac": "YGH",
      "an": "Fort Good Hope",
      "han": "फ़ोर्ट गूड होप",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Good Hope",
      "hct": "फ़ोर्ट गूड होप"
    }, {
      "ac": "YFH",
      "an": "Fort Hope",
      "han": "फ़ोर्ट होप",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Hope",
      "hct": "फ़ोर्ट होप"
    }, {
      "ac": "FLL",
      "an": "Ft Lauderdale Hollywood",
      "han": "फ़्ट लाउदर्डाले होलिवुड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fort Lauderdale",
      "hct": "फ़ोर्ट लाउदर्डाळे"
    }, {
      "ac": "TBN",
      "an": "Forney Field",
      "han": "फ़ोर्नी फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fort Leonard Wood",
      "hct": "फ़ोर्ट लियोनार्ड वुड"
    }, {
      "ac": "YMM",
      "an": "Fort Mcmurray",
      "han": "फ़ोर्ट म्क्मूरअराय",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Mcmurray",
      "hct": "फ़ोर्ट म्क्मूरअराय"
    }, {
      "ac": "ZFM",
      "an": "Fort Mcpherson",
      "han": "फ़ोर्ट म्क्फेरसन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Mcpherson",
      "hct": "फ़ोर्ट म्क्फेरसन"
    }, {
      "ac": "RSW",
      "an": "Regional Southwest",
      "han": "रिजनल साउथवेस्ट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fort Myers",
      "hct": "फ़ोर्ट मायर्स"
    }, {
      "ac": "YYE",
      "an": "Fort Nelson",
      "han": "फ़ोर्ट नेल्सन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Nelson",
      "hct": "फ़ोर्ट नेल्सन"
    }, {
      "ac": "YER",
      "an": "Fort Severn",
      "han": "फ़ोर्ट सेवर्न",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Severn",
      "hct": "फ़ोर्ट सेवर्न"
    }, {
      "ac": "YFS",
      "an": "Fort Simpson",
      "han": "फ़ोर्ट सिम्पसन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Simpson",
      "hct": "फ़ोर्ट सिम्पसन"
    }, {
      "ac": "YSM",
      "an": "Ft Smith Municipal",
      "han": "फ़्ट स्मिथ म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Fort Smith",
      "hct": "फ़ोर्ट स्मिथ"
    }, {
      "ac": "FSM",
      "an": "Ft Smith Municipal",
      "han": "फ़्ट स्मिथ म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fort Smith",
      "hct": "फ़ोर्ट स्मिथ"
    }, {
      "ac": "YXJ",
      "an": "Ft St John Municipal",
      "han": "फ़्ट स्त्रीट जॉन म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "FORT ST JOHN",
      "hct": "फ़ोर्ट स्त्रीट जॉन"
    }, {
      "ac": "FWA",
      "an": "Baer Field",
      "han": "बर फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fort Wayne",
      "hct": "फ़ोर्ट वेन"
    }, {
      "ac": "FYU",
      "an": "Fort Yukon",
      "han": "फ़ोर्ट युकोन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fort Yukon",
      "hct": "फ़ोर्ट युकोन"
    }, {
      "ac": "FOR",
      "an": "Pinto Martines",
      "han": "पिंटो मार्तिन्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Fortaleza",
      "hct": "फ़ोर्तलेज़ा"
    }, {
      "ac": "YFX",
      "an": "St. Lewis (Fox Harbour)",
      "han": "स्त्रीट. ल्युइस (फ़ोक्स हर्बौर)",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "St. Lewis",
      "hct": "स्त्रीट. ल्युइस"
    }, {
      "ac": "FRC",
      "an": "Franca",
      "han": "फ़्रान्का",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Franca",
      "hct": "फ़्रान्का"
    }, {
      "ac": "MVB",
      "an": "Mvengue",
      "han": "म्वेन्गुए",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Franceville",
      "hct": "फ्रांसविले"
    }, {
      "ac": "FKL",
      "an": "Chess Lamberton",
      "han": "चेस लाम्बेर्टन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Franklin",
      "hct": "फ्रॅंक्लीन"
    }, {
      "ac": "YFC",
      "an": "Fredericton Municipal",
      "han": "फ़्रेडेरिक्टन म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "FREDERICTON",
      "hct": "फ़्रेडेरिक्टन"
    }, {
      "ac": "FNA",
      "an": "Lungi",
      "han": "लुंगी",
      "cn": "Sierra Leone",
      "hcn": "सिएररा लियोने",
      "cc": "SL",
      "ct": "Freetown",
      "hct": "फ़्रीटौन"
    }, {
      "ac": "FAT",
      "an": "Fresno Air Terminal",
      "han": "फ़्रेस्नो एअर टर्मिनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Fresno",
      "hct": "फ़्रेस्नो"
    }, {
      "ac": "FRD",
      "an": "Friday Harbor",
      "han": "फ़्रिदय हरबोर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Friday Harbor",
      "hct": "फ्राईडे हरबोर"
    }, {
      "ac": "FDH",
      "an": "Friedrichshafen Lowenthal",
      "han": "फ़्रिएद्रिच्शफ़ेन लौएंठल",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Friedrichshafen",
      "hct": "फ़्रिएद्रिच्शफ़ेन"
    }, {
      "ac": "FUE",
      "an": "Fuerteventura",
      "han": "फ़ुएर्टेवन्टूरा",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Fuerteventura   Puerto Del Rosario",
      "hct": "फ़ुएर्टेवन्टूरा   पुएर्टो देल रोसारिओ"
    }, {
      "ac": "FUJ",
      "an": "Fukue",
      "han": "फ़ुकुए",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Fukue",
      "hct": "फ़ुकुए"
    }, {
      "ac": "FUK",
      "an": "Itazuke",
      "han": "इताज़ुके",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Fukuoka",
      "hct": "फ़ुकुओका"
    }, {
      "ac": "FKS",
      "an": "",
      "han": "",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Fukushima",
      "hct": "फ़ुकुशिमा"
    }, {
      "ac": "FUN",
      "an": "Funafuti",
      "han": "फ़ुनाफ़ुती",
      "cn": "Tuvalu",
      "hcn": "तुवालू",
      "cc": "TV",
      "ct": "Funafuti Atol",
      "hct": "फ़ुनाफ़ुती अटोल"
    }, {
      "ac": "FTA",
      "an": "Futuna",
      "han": "फ़ुटुना",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Futuna Island",
      "hct": "फ़ुटुना आयलॅन्ड"
    }, {
      "ac": "FUT",
      "an": "Pointe Vele",
      "han": "पोईंटे वेले",
      "cn": "Wallis and Futuna",
      "hcn": "वालिस‌ और‌ फुतुना द्वीप‌स‌मूह‌",
      "cc": "WF",
      "ct": "Futuna Island",
      "hct": "फ़ुटुना आयलॅन्ड"
    }, {
      "ac": "FUG",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Fuyang",
      "hct": "फ़ुयांग"
    }, {
      "ac": "FOC",
      "an": "Fuzhou",
      "han": "फ़ुजौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Fuzhou",
      "hct": "फु़झ़ै"
    }, {
      "ac": "GBE",
      "an": "Gaborone",
      "han": "गाबोरोन",
      "cn": "Botswana",
      "hcn": "बोट्सवना",
      "cc": "BW",
      "ct": "Gaborone",
      "hct": "गाबोरोन"
    }, {
      "ac": "GAF",
      "an": "Gafsa",
      "han": "गाफ़्सा",
      "cn": "Tunisia",
      "hcn": "ट्यूनीशिया",
      "cc": "TN",
      "ct": "Gafsa",
      "hct": "गाफ़्सा"
    }, {
      "ac": "GNV",
      "an": "Gainesville Regional",
      "han": "गायनेस्वील रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gainesville",
      "hct": "गायनेस्वील"
    }, {
      "ac": "GPS",
      "an": "Baltra",
      "han": "बाल्टरा",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Galapagos Is",
      "hct": "गलपागोस इस"
    }, {
      "ac": "GAL",
      "an": "Galena",
      "han": "गालेना",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Galena",
      "hct": "गाळेना"
    }, {
      "ac": "GEV",
      "an": "Gallivare",
      "han": "गलिवारे",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Gallivare",
      "hct": "गलिवारे"
    }, {
      "ac": "GUP",
      "an": "Gallup Municipal",
      "han": "गलुप म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gallup",
      "hct": "गलुप"
    }, {
      "ac": "GWY",
      "an": "Carnmore",
      "han": "कार्नमोरे",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Galway",
      "hct": "गालवै"
    }, {
      "ac": "GAX",
      "an": "Gamba",
      "han": "गम्बा",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Gamba",
      "hct": "गम्बा"
    }, {
      "ac": "GMB",
      "an": "Gambella",
      "han": "गम्बेला",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Gambella",
      "hct": "गम्बेला"
    }, {
      "ac": "GAM",
      "an": "Gambell",
      "han": "गम्बेल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gambell",
      "hct": "गम्बेल"
    }, {
      "ac": "GAN",
      "an": "",
      "han": "",
      "cn": "Maldives",
      "hcn": "माल‌दीव‌",
      "cc": "MV",
      "ct": "Gan Island",
      "hct": "गण आयलॅन्ड"
    }, {
      "ac": "YQX",
      "an": "Gander",
      "han": "गानदर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Gander",
      "hct": "गानदर"
    }, {
      "ac": "KOW",
      "an": "Ganzhou",
      "han": "गान्जौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Ganzhou",
      "hct": "गान्झ़ौ"
    }, {
      "ac": "GCK",
      "an": "Garden City Municipal",
      "han": "गार्डन सिटी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Garden City",
      "hct": "गार्डन सिटी"
    }, {
      "ac": "GMI",
      "an": "Gasmata Island",
      "han": "गॅसमाता आयलॅन्ड",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Gasmata Island",
      "hct": "गॅसमाता आयलॅन्ड"
    }, {
      "ac": "ELQ",
      "an": "Gassim",
      "han": "गॅससिम",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Gassim",
      "hct": "गॅससिम"
    }, {
      "ac": "GTA",
      "an": "Gatokae",
      "han": "गतोकए",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Gatokae",
      "hct": "गतोकए"
    }, {
      "ac": "ZGU",
      "an": "Gaua Island",
      "han": "गौआ आयलॅन्ड",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Gaua Island",
      "hct": "गौआ आयलॅन्ड"
    }, {
      "ac": "GZT",
      "an": "Oguzeli",
      "han": "ओगुज़ेली",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Gaziantep",
      "hct": "गज़ियांटेप"
    }, {
      "ac": "GDN",
      "an": "Rebiechowo",
      "han": "रेबीचौओ",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Gdansk",
      "hct": "ग्डन्स्क"
    }, {
      "ac": "EGN",
      "an": "Geneina",
      "han": "जेनैना",
      "cn": "Sudan",
      "hcn": "सुदान",
      "cc": "SD",
      "ct": "Geneina",
      "hct": "जेनैना"
    }, {
      "ac": "GES",
      "an": "General Santos",
      "han": "जनरल संतोष",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "General Santos City",
      "hct": "जनरल संतोष सिटी"
    }, {
      "ac": "GOA",
      "an": "Christoforo Colombo",
      "han": "क्रिस्टोफ़ोरो कोलोंबो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Genoa",
      "hct": "गेनोआ"
    }, {
      "ac": "GRJ",
      "an": "George",
      "han": "जॉर्ज",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "George",
      "hct": "जॉर्ज"
    }, {
      "ac": "GEO",
      "an": "Cheddi Jagan",
      "han": "चेद्दी जगन",
      "cn": "Guyana",
      "hcn": "गुयाना",
      "cc": "GY",
      "ct": "Georgetown",
      "hct": "जॉर्जटाउन"
    }, {
      "ac": "ASI",
      "an": "",
      "han": "",
      "cn": "Saint Helena",
      "hcn": "सेंट हेलना",
      "cc": "SH",
      "ct": "Georgetown",
      "hct": "जॉर्जटाउन"
    }, {
      "ac": "GET",
      "an": "Geraldton",
      "han": "गेराल्ड्टन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Geraldton ",
      "hct": "गेराल्ड्टन "
    }, {
      "ac": "GRO",
      "an": "Costa Brava",
      "han": "कोस्टा ब्रवा",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Gerona",
      "hct": "गरोणा"
    }, {
      "ac": "ZGS",
      "an": "Gethsemani",
      "han": "गेठ्सेमानी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Gethsemani",
      "hct": "गेठ्सेमानी"
    }, {
      "ac": "LTD",
      "an": "Ghadames East",
      "han": "घदम्स ईस्ट",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Ghadames",
      "hct": "घदम्स"
    }, {
      "ac": "GHT",
      "an": "Ghat",
      "han": "घाट",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Ghat",
      "hct": "घाट"
    }, {
      "ac": "GSM",
      "an": "Gheshm",
      "han": "घेश्म",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Gheshm",
      "hct": "घेश्म"
    }, {
      "ac": "GIB",
      "an": "North Front",
      "han": "नोर्थ फ़्रोन्त",
      "cn": "Gibraltar",
      "hcn": "गिब्राल्टर",
      "cc": "GI",
      "ct": "Gibraltar",
      "hct": "गिब्राल्टर"
    }, {
      "ac": "GIL",
      "an": "Gilgit",
      "han": "गिलगित",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Gilgit",
      "hct": "गिलगित"
    }, {
      "ac": "YGX",
      "an": "Gillam",
      "han": "गिलेम",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Gillam",
      "hct": "गिलेम"
    }, {
      "ac": "GCC",
      "an": "Campbell Cty",
      "han": "कैम्पबेल क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gillette",
      "hct": "गिलेटे"
    }, {
      "ac": "YGB",
      "an": "Gillies Bay",
      "han": "गिलीस बे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Gillies Bay",
      "hct": "गिलीस बे"
    }, {
      "ac": "GIS",
      "an": "Gisborne",
      "han": "गिस्बोर्णे",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Gisborne",
      "hct": "गिस्बोर्णे"
    }, {
      "ac": "GZO",
      "an": "Nusatupe",
      "han": "नुसतुपे",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Gizo",
      "hct": "गिज़ो"
    }, {
      "ac": "YHK",
      "an": "Gjoa Haven",
      "han": "ग्जोआ हवेन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Gjoa Haven",
      "hct": "ग्जोआ हवेन"
    }, {
      "ac": "GGG",
      "an": "Greg County",
      "han": "ग्रेग कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gladewater Kilgore Longview",
      "hct": "ग्लाडेवाटर कीलगोरे लोन्ग्वीव"
    }, {
      "ac": "GLT",
      "an": "Gladstone",
      "han": "ग्लाडस्टोन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Gladstone ",
      "hct": "ग्लाडस्टोन "
    }, {
      "ac": "GDV",
      "an": "Dawson Community",
      "han": "डौसन कम्युनिटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Glendive",
      "hct": "ग्लेंदिवे"
    }, {
      "ac": "GLO",
      "an": "Gloucestershire",
      "han": "ग्लौकेस्टर्शीरे",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Golouchestershire",
      "hct": "गोलौचेस्टर्शीरे"
    }, {
      "ac": "GDE",
      "an": "Gode",
      "han": "गोडे",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Gode",
      "hct": "गोडे"
    }, {
      "ac": "YGO",
      "an": "Gods Lake Narrows",
      "han": "गोड्स लेक नररौस",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Gods Lake Narrows",
      "hct": "गोड्स लेक नररौस"
    }, {
      "ac": "ZGI",
      "an": "Gods River",
      "han": "गोड्स रिवर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Gods River",
      "hct": "गोड्स रिवर"
    }, {
      "ac": "GYN",
      "an": "Santa Genoveva",
      "han": "शांता गेनोवेवा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Goiania",
      "hct": "गोईआनिआ"
    }, {
      "ac": "OOL",
      "an": "Gold Coast",
      "han": "गोल्ड कोस्ट",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Gold Coast",
      "hct": "गोल्ड कोस्ट"
    }, {
      "ac": "GOQ",
      "an": "Golmud",
      "han": "गोल्मुद",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Golmud",
      "hct": "गोल्मुद"
    }, {
      "ac": "GLV",
      "an": "Golovin",
      "han": "गोलोविन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Golovin",
      "hct": "गोलोविन"
    }, {
      "ac": "GOM",
      "an": "Goma",
      "han": "गोमा",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Goma",
      "hct": "गोमा"
    }, {
      "ac": "GME",
      "an": "Gomel",
      "han": "गोमेल",
      "cn": "Belarus",
      "hcn": "बेलारुस",
      "cc": "BY",
      "ct": "Gomel",
      "hct": "गोमेल"
    }, {
      "ac": "GDQ",
      "an": "Gondar",
      "han": "गोन्दार",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Gondar",
      "hct": "गोन्दार"
    }, {
      "ac": "GNU",
      "an": "Goodnews Mumtrak",
      "han": "गूडन्युझ मुम्ट्रक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Goodnews Mumtrak",
      "hct": "गूडन्युझ मुम्ट्रक"
    }, {
      "ac": "YYR",
      "an": "Municipal Goose Bay",
      "han": "म्यूनिसिपल गूसे बे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Goose Bay",
      "hct": "गूसे बे"
    }, {
      "ac": "GBT",
      "an": "Gorgan",
      "han": "गोरगण",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Gorgan",
      "hct": "गोरगण"
    }, {
      "ac": "GKA",
      "an": "Goroka",
      "han": "गोरओक",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Goroka",
      "hct": "गोरओक"
    }, {
      "ac": "GTO",
      "an": "Jalaluddin",
      "han": "जलालुद्दीन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Gorontalo",
      "hct": "गोरोन्तलो"
    }, {
      "ac": "GOT",
      "an": "Landvetter",
      "han": "लांड्वेटेर",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Gothenburg",
      "hct": "गोठेन्बूरग"
    }, {
      "ac": "GSE",
      "an": "Save",
      "han": "सवे",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Gothenborg",
      "hct": "गोठेन्बोर्ग"
    }, {
      "ac": "GLN",
      "an": "",
      "han": "",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Goulimime",
      "hct": "गौलीमिमे"
    }, {
      "ac": "GOV",
      "an": "Gove",
      "han": "गोवे",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Gove ",
      "hct": "गोवे "
    }, {
      "ac": "GVR",
      "an": "Governador Valadares",
      "han": "गोवर्नडोर वलादारेस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Governador Valadares",
      "hct": "गोवर्नडोर वलादारेस"
    }, {
      "ac": "GRW",
      "an": "Graciosa",
      "han": "ग्रासियोसा",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Graciosa Island",
      "hct": "ग्रासियोसा आयलॅन्ड"
    }, {
      "ac": "GFN",
      "an": "Grafton",
      "han": "ग्राफ़्टन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Grafton ",
      "hct": "ग्राफ़्टन "
    }, {
      "ac": "GRX",
      "an": "Granada",
      "han": "ग्राणडा",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Granada",
      "hct": "ग्राणडा"
    }, {
      "ac": "GCN",
      "an": "Grand Canyon Natl Park",
      "han": "ग्रॅंड कन्योन नट्ल पार्क",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Grand Canyon",
      "hct": "ग्रॅंड कन्योन"
    }, {
      "ac": "GFK",
      "an": "Grand Forks Mark Andrews",
      "han": "ग्रॅंड फ़ोर्क्स मार्क एंड्रज",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Grand Forks",
      "hct": "ग्रॅंड फ़ोर्क्स"
    }, {
      "ac": "GRI",
      "an": "Hall Cty Regional",
      "han": "हॉल क्टी रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Grand Island",
      "hct": "ग्रॅंड आयलॅन्ड"
    }, {
      "ac": "GJT",
      "an": "Walker Field",
      "han": "वाकर फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Grand Junction",
      "hct": "ग्रॅंड जन्क्शन"
    }, {
      "ac": "GRR",
      "an": "Kent County",
      "han": "केंट कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Grand Rapids",
      "hct": "ग्रॅंड रेपिड्स"
    }, {
      "ac": "YQU",
      "an": "Grande Prairie",
      "han": "ग्रांडे प्रैरि",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Grande Prairie",
      "hct": "ग्रांडे प्रैरि"
    }, {
      "ac": "GTS",
      "an": "",
      "han": "",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Granites",
      "hct": "ग्रॅणाईट्स"
    }, {
      "ac": "KGX",
      "an": "Grayling",
      "han": "ग्राय्लींग",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Grayling",
      "hct": "ग्राय्लींग"
    }, {
      "ac": "GRZ",
      "an": "Thalerhof",
      "han": "थलर्होफ़",
      "cn": "Austria",
      "hcn": "ऑस्ट्रिया",
      "cc": "AT",
      "ct": "Graz",
      "hct": "गराज़"
    }, {
      "ac": "GBD",
      "an": "Great Bend",
      "han": "ग्रेट बेंद",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Great Bend",
      "hct": "ग्रेट बेंद"
    }, {
      "ac": "GTF",
      "an": "Great Falls",
      "han": "ग्रेट फॉल्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Great Falls",
      "hct": "ग्रेट फॉल्स"
    }, {
      "ac": "GRB",
      "an": "Austin Straubel Fld",
      "han": "ऑस्टिन स्ट्रऊबेल फ़्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Green Bay",
      "hct": "ग्रीन बे"
    }, {
      "ac": "GSO",
      "an": "Piedmont Triad",
      "han": "पीद्मोन्त त्रीआद",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Greensboro High Point High Point",
      "hct": "ग्रीन्स्बोरो हाई पोईंत हाई पोईंत"
    }, {
      "ac": "GLH",
      "an": "Greenville Municipal",
      "han": "ग्रीनविले म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Greenville",
      "hct": "ग्रीनविले"
    }, {
      "ac": "PGV",
      "an": "Pitt",
      "han": "पित्त",
      "cn": "Greenville",
      "hcn": "ग्रीनविले",
      "cc": "USA",
      "ct": "US",
      "hct": "उस"
    }, {
      "ac": "GSP",
      "an": "Greenville Spartanburg",
      "han": "ग्रीनविले स्पर्तन्बूरग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Greenville Greer",
      "hct": "ग्रीनविले ग्रीर"
    }, {
      "ac": "GNB",
      "an": "Saint Geoirs",
      "han": "सेंट गियोइर्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Grenoble",
      "hct": "ग्रेनोबळे"
    }, {
      "ac": "GFF",
      "an": "Griffith",
      "han": "ग्रिफिठ",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Griffith ",
      "hct": "ग्रिफिठ "
    }, {
      "ac": "GSY",
      "an": "Binbrook",
      "han": "बीन्ब्रूक",
      "cn": "United Kingdom ",
      "hcn": "युनाईटेड किंग्डोम ",
      "cc": "GB",
      "ct": "Grimsby ",
      "hct": "ग्रिम्स्बी "
    }, {
      "ac": "GRY",
      "an": "Grimsey",
      "han": "ग्रिम्से",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Grimsey",
      "hct": "ग्रिम्से"
    }, {
      "ac": "YGZ",
      "an": "Grise Fiord",
      "han": "ग्रिसे फ़ियोर्ड",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Grise Fiord",
      "hct": "ग्रिसे फ़ियोर्ड"
    }, {
      "ac": "GRQ",
      "an": "Eelde",
      "han": "ईल्डे",
      "cn": "Netherlands",
      "hcn": "नेद‌र‌लैंड‌",
      "cc": "NL",
      "ct": "Groningen",
      "hct": "ग्रोनिंगेन"
    }, {
      "ac": "GTE",
      "an": "Groote Eylandt",
      "han": "ग्रूटे ईलांड्ट",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Groote Eylandt ",
      "hct": "ग्रूटे ईलांड्ट "
    }, {
      "ac": "GRV",
      "an": "Grozny",
      "han": "ग्रोज़्नी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Grozny",
      "hct": "ग्रोज़्नी"
    }, {
      "ac": "GDL",
      "an": "Miguel Hidalgo",
      "han": "मिगुएल हीदाल्गो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Guadalajara",
      "hct": "गुआदलजरा"
    }, {
      "ac": "GUM",
      "an": "Antonio B Won Pat",
      "han": "एंटोनियो बी वोन पत",
      "cn": "Guam",
      "hcn": "गुआम",
      "cc": "GU",
      "ct": "Tamning",
      "hct": "तम्निन्ग"
    }, {
      "ac": "GPI",
      "an": "Juan Casiano",
      "han": "जुआन कास्यनो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Guapi",
      "hct": "गुआपी"
    }, {
      "ac": "GDO",
      "an": "",
      "han": "",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Guasdualito",
      "hct": "गुआस्डुआलिटो"
    }, {
      "ac": "GYE",
      "an": "Simon Bolivar",
      "han": "साइमन बोलीवार",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Guayaquil",
      "hct": "गुआयाक़ील"
    }, {
      "ac": "GYA",
      "an": "Guayaramerin",
      "han": "गुआयारमेरिन",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Guayaramerin",
      "hct": "गुआयारमेरिन"
    }, {
      "ac": "GYM",
      "an": "Gen Jose M Yanez",
      "han": "जन जोस एम यानेज़",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Guaymas",
      "hct": "गुआय्मेस"
    }, {
      "ac": "GCI",
      "an": "Guernsey",
      "han": "गुएर्न्से",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Guernsey",
      "hct": "गुएर्न्से"
    }, {
      "ac": "KWL",
      "an": "Guilin",
      "han": "गुइलीन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Guilin",
      "hct": "गुइलीन"
    }, {
      "ac": "KWE",
      "an": "Longdongbao",
      "han": "लोन्ग्डोंग्बाव",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Guiyang",
      "hct": "गुइयंग"
    }, {
      "ac": "GPT",
      "an": "Biloxi Regional",
      "han": "बिलोक्सी रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gulfport",
      "hct": "गुल्फ़प़ॉर्ट"
    }, {
      "ac": "ULU",
      "an": "Gulu",
      "han": "गुलु",
      "cn": "Uganda",
      "hcn": "युगांडा",
      "cc": "UG",
      "ct": "Gulu",
      "hct": "गुलु"
    }, {
      "ac": "GUC",
      "an": "Gunnison",
      "han": "गन्नीसन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gunnison",
      "hct": "गन्नीसन"
    }, {
      "ac": "KUV",
      "an": "Kunsan",
      "han": "कून्सान",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Gunsan",
      "hct": "गन्सान"
    }, {
      "ac": "URY",
      "an": "Guriat",
      "han": "गूरिट",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Guriat",
      "hct": "गूरिट"
    }, {
      "ac": "GST",
      "an": "Gustavus",
      "han": "गुस्टवुस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Gustavus",
      "hct": "गुस्टवुस"
    }, {
      "ac": "GWD",
      "an": "Gwadar",
      "han": "ग्वादार",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Gwadar",
      "hct": "ग्वादार"
    }, {
      "ac": "KWJ",
      "an": "Gwangju",
      "han": "ग्वांग्जू",
      "cn": "South Korea",
      "hcn": "द‌क्षिण‌ कोरिया",
      "cc": "KR",
      "ct": "Kwangju",
      "hct": "क्वांग्जू"
    }, {
      "ac": "KVD",
      "an": "Ganja",
      "han": "गन्ज",
      "cn": "Azerbaijan",
      "hcn": "अज़ेर्बैजान",
      "cc": "AZ",
      "ct": "Ganja",
      "hct": "गन्ज"
    }, {
      "ac": "LWN",
      "an": "Gyumri",
      "han": "ग्युमृ",
      "cn": "Armenia",
      "hcn": "अर्मेनिआ",
      "cc": "AM",
      "ct": "Gyumri",
      "hct": "ग्युमृ"
    }, {
      "ac": "HPA",
      "an": "Lifuka Island",
      "han": "लिफ़ुका आयलॅन्ड",
      "cn": "Tonga",
      "hcn": "टोंगा",
      "cc": "TO",
      "ct": "Lifuka",
      "hct": "लिफ़ुका"
    }, {
      "ac": "HAC",
      "an": "Hachijojima",
      "han": "हचिजोजिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Hachijojima",
      "hct": "हचिजोजिमा"
    }, {
      "ac": "HBT",
      "an": "King Khaled Military City",
      "han": "किंग खालिद मीलिटरी सिटी",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "King Khalid Mil.city",
      "hct": "किंग खालिद मील.सिटी"
    }, {
      "ac": "HFS",
      "an": "Hagfors",
      "han": "हाग्फ़ोर्स",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Hagfors",
      "hct": "हाग्फ़ोर्स"
    }, {
      "ac": "HFA",
      "an": "Haifa",
      "han": "हैफ़ा",
      "cn": "Israel",
      "hcn": "इसरायल",
      "cc": "IL",
      "ct": "Haifa",
      "hct": "हैफ़ा"
    }, {
      "ac": "HAK",
      "an": "Haikou",
      "han": "हेकौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Haikou",
      "hct": "हेकौ"
    }, {
      "ac": "HAS",
      "an": "Hail",
      "han": "हैल",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Hail",
      "hct": "हैल"
    }, {
      "ac": "HLD",
      "an": "Dongshan",
      "han": "डोंगशान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Hailar",
      "hct": "हैलर"
    }, {
      "ac": "SUN",
      "an": "Sun Valley",
      "han": "सन वॅली",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hailey",
      "hct": "हैली"
    }, {
      "ac": "HNS",
      "an": "Haines Municipal",
      "han": "हेन्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Haines",
      "hct": "हेन्स"
    }, {
      "ac": "HPH",
      "an": "Cat Bi",
      "han": "कॅट बी",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Haiphong",
      "hct": "हाईफोंग"
    }, {
      "ac": "HKD",
      "an": "Hakodate",
      "han": "हकोडाते",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Hakodate",
      "hct": "हकोडाते"
    }, {
      "ac": "YHZ",
      "an": "Halifax",
      "han": "हलिफ़ाक्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Halifax",
      "hct": "हलिफ़ाक्स"
    }, {
      "ac": "YUX",
      "an": "Hall Beach",
      "han": "हॉल बीच",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Hall Beach",
      "hct": "हॉल बीच"
    }, {
      "ac": "HAD",
      "an": "Halmstad",
      "han": "हल्म्स्ताद",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Halmstad",
      "hct": "हल्म्स्ताद"
    }, {
      "ac": "HDM",
      "an": "Hamadan",
      "han": "हमदन",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Hamadan",
      "hct": "हमदन"
    }, {
      "ac": "HTI",
      "an": "Hamilton Island",
      "han": "हेमिल्टन आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Hamilton Island",
      "hct": "हेमिल्टन आयलॅन्ड"
    }, {
      "ac": "YHM",
      "an": "Hamilton",
      "han": "हेमिल्टन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Hamilton",
      "hct": "हेमिल्टन"
    }, {
      "ac": "HLZ",
      "an": "Hamilton",
      "han": "हेमिल्टन",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Hamilton",
      "hct": "हेमिल्टन"
    }, {
      "ac": "HFT",
      "an": "Hammerfest",
      "han": "हम्मेर्फ़ेस्ट",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Hammerfest",
      "hct": "हम्मेर्फ़ेस्ट"
    }, {
      "ac": "PHF",
      "an": "Williamsburg",
      "han": "विलियाम्स्बूरग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hampton Newport News Williamsburg",
      "hct": "हंप्टन न्यूप़ॉर्ट न्युझ विलियाम्स्बूरग"
    }, {
      "ac": "HNM",
      "an": "Hana Municipal",
      "han": "हना म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hana",
      "hct": "हना"
    }, {
      "ac": "HNA",
      "an": "Hanamaki",
      "han": "हनमकी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Hanamaki",
      "hct": "हनमकी"
    }, {
      "ac": "CMX",
      "an": "Houghton Cty Memorial",
      "han": "हौघ्टन क्टी मेमोरिल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hancock",
      "hct": "हन्कोक"
    }, {
      "ac": "HAQ",
      "an": "Hanimaadhoo",
      "han": "हाणीमाढू",
      "cn": "Maldives",
      "hcn": "माल‌दीव‌",
      "cc": "MV",
      "ct": "Haa Dhaalu Atoll",
      "hct": "हा धालू अटोल"
    }, {
      "ac": "LEB",
      "an": "Lebanon Regional",
      "han": "लेबानोन रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hanover Lebanon White River",
      "hct": "हानोवर लेबानोन वाइट रिवर"
    }, {
      "ac": "HZG",
      "an": "Hanzhong",
      "han": "हान्जोंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Hanzhong",
      "hct": "हान्झ़ोंग"
    }, {
      "ac": "HRE",
      "an": "Harare",
      "han": "हरारे",
      "cn": "Zimbabwe",
      "hcn": "ज़िम्बाब्वे",
      "cc": "ZW",
      "ct": "Harare",
      "hct": "हरारे"
    }, {
      "ac": "HRB",
      "an": "Taiping",
      "han": "ताईपींग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Harbin",
      "hct": "हर्बीन"
    }, {
      "ac": "HRL",
      "an": "Rio Grande Valley",
      "han": "रिओ ग्रांडे वॅली",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Harlingen",
      "hct": "हार्लींगेन"
    }, {
      "ac": "MDT",
      "an": "Harrisburg",
      "han": "हरीस्बूरग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Harrisburg",
      "hct": "हरीस्बूरग"
    }, {
      "ac": "HRO",
      "an": "Boone County",
      "han": "बूने कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Harrison",
      "hct": "हरीसन"
    }, {
      "ac": "HRT",
      "an": "Hurlburt Fld",
      "han": "हूरल्बूरट फ़्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mary Esther",
      "hct": "मॅरी एस्थर"
    }, {
      "ac": "EVE",
      "an": "Evenes",
      "han": "एवन्स",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Harstad",
      "hct": "हर्स्ताद"
    }, {
      "ac": "XWP",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Hassleholm",
      "hct": "हस्स्लेहोल्म"
    }, {
      "ac": "HAA",
      "an": "Hasvik",
      "han": "हस्वीक",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Hasvik",
      "hct": "हस्वीक"
    }, {
      "ac": "HDY",
      "an": "Hat Yai",
      "han": "हट याई",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Hat Yai",
      "hct": "हट याई"
    }, {
      "ac": "HTG",
      "an": "Khatanga",
      "han": "खटंगा",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Khatanga",
      "hct": "खटंगा"
    }, {
      "ac": "HAU",
      "an": "Karmoy",
      "han": "कर्मोय",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Haugesund",
      "hct": "हौगेसुन्ड"
    }, {
      "ac": "HAV",
      "an": "Jose Marti",
      "han": "जोस मार्ती",
      "cn": "Cuba",
      "hcn": "कुबा",
      "cc": "CU",
      "ct": "Havana",
      "hct": "हावना"
    }, {
      "ac": "HAE",
      "an": "Havasupai",
      "han": "हवसुपई",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Havasupai",
      "hct": "हवसुपई"
    }, {
      "ac": "YGV",
      "an": "Havre St Pierre",
      "han": "हव्रे स्त्रीट पिएर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Havre St Pierre",
      "hct": "हव्रे स्त्रीट पिएर"
    }, {
      "ac": "HVR",
      "an": "City County",
      "han": "सिटी कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Havre",
      "hct": "हव्रे"
    }, {
      "ac": "YHY",
      "an": "Hay River Municipal",
      "han": "हाय रिवर म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Hay River",
      "hct": "हाय रिवर"
    }, {
      "ac": "HDN",
      "an": "Hayden",
      "han": "हैद्न",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hayden",
      "hct": "हैद्न"
    }, {
      "ac": "HIS",
      "an": "Hayman Island",
      "han": "हैमॅन आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Hayman Island",
      "hct": "हैमॅन आयलॅन्ड"
    }, {
      "ac": "HYS",
      "an": "Hays Municipal",
      "han": "हैस म्यूनिसिपल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hays",
      "hct": "हैस"
    }, {
      "ac": "HKB",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Healy Lake",
      "hct": "हेली लेक"
    }, {
      "ac": "XXU",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Hedemora",
      "hct": "हेडेमोरा"
    }, {
      "ac": "HFE",
      "an": "Luogang",
      "han": "लुओगंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Hefei",
      "hct": "हेफ़ी"
    }, {
      "ac": "HEH",
      "an": "Heho",
      "han": "हेहो",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Heho",
      "hct": "हेहो"
    }, {
      "ac": "HDB",
      "an": "Heidelberg",
      "han": "हेइडेल्बर्ग",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Heidelberg",
      "hct": "हेइडेल्बर्ग"
    }, {
      "ac": "HEK",
      "an": "Heihe",
      "han": "हेइहे",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Heihe",
      "hct": "हेइहे"
    }, {
      "ac": "HLN",
      "an": "Helena Municipal",
      "han": "हेलना म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Helena",
      "hct": "हेलना"
    }, {
      "ac": "HGL",
      "an": "Helgoland",
      "han": "हेल्गोलंद",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Helgoland",
      "hct": "हेल्गोलंद"
    }, {
      "ac": "HMV",
      "an": "Hemavan",
      "han": "हेमावन",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Hemavan",
      "hct": "हेमावन"
    }, {
      "ac": "HCN",
      "an": "Hengchun",
      "han": "हेन्ग्चन",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Hengchun",
      "hct": "हेन्ग्चन"
    }, {
      "ac": "HER",
      "an": "N Kazantzakis",
      "han": "एन कज़ांट्ज़किस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Heraklion",
      "hct": "हेरक्लियोन"
    }, {
      "ac": "HEA",
      "an": "Herat",
      "han": "हेरट",
      "cn": "Afghanistan",
      "hcn": "अफ़ग़ानिस्तान‌",
      "cc": "AF",
      "ct": "Herat",
      "hct": "हेरट"
    }, {
      "ac": "HDF",
      "an": "Heringsdorf",
      "han": "हेरिंग्स्डोर्फ़",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Heringsdorf",
      "hct": "हेरिंग्स्डोर्फ़"
    }, {
      "ac": "HMO",
      "an": "Gen Ignacio Pesqueira Garcia",
      "han": "जन इग्नेसियो पेस्क़िरा गार्सिया",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Hermosillo",
      "hct": "हेर्मोसिलो"
    }, {
      "ac": "XYC",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Herrljunga",
      "hct": "हेर्र्ल्जुन्गा"
    }, {
      "ac": "HVB",
      "an": "Hervey Bay",
      "han": "हेर्वी बे",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Hervey Bay",
      "hct": "हेर्वी बे"
    }, {
      "ac": "YOJ",
      "an": "Footner Lake",
      "han": "फ़ूट्नेर लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "HIGH LEVEL",
      "hct": "हाई लेवेल"
    }, {
      "ac": "ITO",
      "an": "Hilo Hawaii",
      "han": "हीलो हवाई",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hilo",
      "hct": "हीलो"
    }, {
      "ac": "HHH",
      "an": "Hilton Head",
      "han": "हिल्टन हेड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hilton Head",
      "hct": "हिल्टन हेड"
    }, {
      "ac": "HIJ",
      "an": "Hiroshima",
      "han": "हीरोशीमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Hiroshima",
      "hct": "हीरोशीमा"
    }, {
      "ac": "HBA",
      "an": "Hobart",
      "han": "होबर्ट",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Hobart",
      "hct": "होबर्ट"
    }, {
      "ac": "HOB",
      "an": "Lea Co Rgnl",
      "han": "ली को र्ग्न्ल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hobbs",
      "hct": "होब्ब्स"
    }, {
      "ac": "HOD",
      "an": "Hodeidah",
      "han": "होडीदाह",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Hodeidah",
      "hct": "होडीदाह"
    }, {
      "ac": "HDS",
      "an": "Hoedspruit Afb",
      "han": "होयड्स्प्रुइत अफ़्ब",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Hoedspruit",
      "hct": "होयड्स्प्रुइत"
    }, {
      "ac": "HOQ",
      "an": "Hof Plauen",
      "han": "होफ़ प्लऊएन",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Hof",
      "hct": "होफ़"
    }, {
      "ac": "HET",
      "an": "Hohhot",
      "han": "होह्होट",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Hohhot",
      "hct": "होह्होट"
    }, {
      "ac": "HKK",
      "an": "Hokitika",
      "han": "होकितिका",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Hokitika",
      "hct": "होकितिका"
    }, {
      "ac": "HYL",
      "an": "Hollis",
      "han": "होलिस",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hollis",
      "hct": "होलिस"
    }, {
      "ac": "YHI",
      "an": "Holman",
      "han": "होलमन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Holman",
      "hct": "होलमन"
    }, {
      "ac": "HCR",
      "an": "Holy Cross",
      "han": "होली क्रॉस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Holy Cross",
      "hct": "होली क्रॉस"
    }, {
      "ac": "HLY",
      "an": "Anglesey",
      "han": "अंगलसे",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Angelsey",
      "hct": "अंजेल्से"
    }, {
      "ac": "HOM",
      "an": "Homer Municipal",
      "han": "होमेर म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Homer",
      "hct": "होमेर"
    }, {
      "ac": "HIR",
      "an": "Henderson",
      "han": "हेंडर्सन",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Honiara",
      "hct": "होनियारा"
    }, {
      "ac": "HVG",
      "an": "Valan",
      "han": "वालेन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Honningsvag",
      "hct": "होन्निंग्स्वाग"
    }, {
      "ac": "MKK",
      "an": "Molokai",
      "han": "मोलोकाई",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hoolehua",
      "hct": "हूलेहुआ"
    }, {
      "ac": "HNH",
      "an": "Hoonah",
      "han": "हूनाह",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hoonah",
      "hct": "हूनाह"
    }, {
      "ac": "HPB",
      "an": "Hooper Bay",
      "han": "हूपर बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hooper Bay",
      "hct": "हूपर बे"
    }, {
      "ac": "YHO",
      "an": "Hopedale",
      "han": "होपदेल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Hopedale",
      "hct": "होपदेल"
    }, {
      "ac": "HID",
      "an": "Horn Island",
      "han": "होर्न आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Horn Island ",
      "hct": "होर्न आयलॅन्ड "
    }, {
      "ac": "HOR",
      "an": "Horta",
      "han": "होर्ता",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Horta",
      "hct": "होर्ता"
    }, {
      "ac": "HKN",
      "an": "Hoskins",
      "han": "होस्किन्स",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Hoskins",
      "hct": "होस्किन्स"
    }, {
      "ac": "HOT",
      "an": "Memorial Field",
      "han": "मेमोरिल फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hot Springs",
      "hct": "हॉट स्प्रिंग्स"
    }, {
      "ac": "HTN",
      "an": "Hotan",
      "han": "होटन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Hotan",
      "hct": "होटन"
    }, {
      "ac": "HOE",
      "an": "Houeisay",
      "han": "हौईसे",
      "cn": "Lao People's Democratic Republic",
      "hcn": "लाव पीपलएस डेमोक्रतिक रीपबलीक",
      "cc": "LA",
      "ct": "Houeisay",
      "hct": "हौईसे"
    }, {
      "ac": "HHQ",
      "an": "Hua Hin",
      "han": "हुआ हीन",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Prachuap Khiri Khan",
      "hct": "प्रचुआप खीरी खान"
    }, {
      "ac": "HUN",
      "an": "Hualien",
      "han": "हुआलीन",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Hualien",
      "hct": "हुआलीन"
    }, {
      "ac": "HYN",
      "an": "Huangyan Luqiao",
      "han": "हुआनज्ञान लुक़ियाओ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Huangyan",
      "hct": "हुआनज्ञान"
    }, {
      "ac": "HUX",
      "an": "Bahias De Huatulco",
      "han": "बहिआस दे हुआतूलको",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Bahias Dehuatulco",
      "hct": "बहिआस देहुआतूलको"
    }, {
      "ac": "HUV",
      "an": "Hudiksvall",
      "han": "हुडिक्सवाल",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Hudiksvall",
      "hct": "हुडिक्सवाल"
    }, {
      "ac": "YHB",
      "an": "Hudson Bay",
      "han": "हूदसन बे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Hudson Bay",
      "hct": "हूदसन बे"
    }, {
      "ac": "HUI",
      "an": "Phu Bai",
      "han": "फू बाई",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Hue",
      "hct": "हुए"
    }, {
      "ac": "HGD",
      "an": "Hughenden",
      "han": "हुघेंड्न",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Hughenden ",
      "hct": "हुघेंड्न "
    }, {
      "ac": "HUS",
      "an": "Hughes Municipal",
      "han": "ह्यूग्स म्यूनिसिपल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hughes",
      "hct": "ह्यूग्स"
    }, {
      "ac": "HUY",
      "an": "Humberside",
      "han": "हुम्बर्सिडे",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Humberside",
      "hct": "हुम्बर्सिडे"
    }, {
      "ac": "HTS",
      "an": "Tri State Milton",
      "han": "त्रि स्टेट मिल्टन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Huntington   Ashland",
      "hct": "हुन्टींग्टन   अश्लांद"
    }, {
      "ac": "HSV",
      "an": "Huntsville",
      "han": "हुन्ट्स्वील",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Huntsville",
      "hct": "हुन्ट्स्वील"
    }, {
      "ac": "HRG",
      "an": "Hurghada",
      "han": "हरघदा",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Hurghada",
      "hct": "हरघदा"
    }, {
      "ac": "HON",
      "an": "Huron Municipal",
      "han": "हरों म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Huron",
      "hct": "हरों"
    }, {
      "ac": "HSL",
      "an": "Huslia",
      "han": "हूसलिआ",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Huslia",
      "hct": "हूसलिआ"
    }, {
      "ac": "HYA",
      "an": "Barnstable Cty",
      "han": "बेर्न्स्टाबल क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hyannis",
      "hct": "ह्यान्नीस"
    }, {
      "ac": "HYG",
      "an": "Hydaburg",
      "han": "हैदाबूरग",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Hydaburg",
      "hct": "हैदाबूरग"
    }, {
      "ac": "IAS",
      "an": "Iasi",
      "han": "इआसी",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Iasi",
      "hct": "इआसी"
    }, {
      "ac": "IBE",
      "an": "Perales",
      "han": "पेरल्स",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Ibague",
      "hct": "इबागुए"
    }, {
      "ac": "IBZ",
      "an": "Ibiza",
      "han": "इबिज़ा",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Ibiza",
      "hct": "इबिज़ा"
    }, {
      "ac": "IDA",
      "an": "Fanning Field",
      "han": "फ़न्निन्ग फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Idaho Falls",
      "hct": "इदाहो फॉल्स"
    }, {
      "ac": "IAA",
      "an": "Igarka",
      "han": "इगर्का",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Igarka",
      "hct": "इगर्का"
    }, {
      "ac": "IGG",
      "an": "Igiugig",
      "han": "इगीऊगिग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Igiugig",
      "hct": "इगीऊगिग"
    }, {
      "ac": "YGT",
      "an": "Igloolik",
      "han": "इग्लूलिक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Igloolik",
      "hct": "इग्लूलिक"
    }, {
      "ac": "IGU",
      "an": "Cataratas",
      "han": "कतरातास",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Iguassu Falls",
      "hct": "इगुआस्सू फॉल्स"
    }, {
      "ac": "IGR",
      "an": "Iguazu",
      "han": "इगुआज़ू",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Iguazu",
      "hct": "इगुआज़ू"
    }, {
      "ac": "JIK",
      "an": "Ikaria",
      "han": "इकारिआ",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "IKARIA ISLAND",
      "hct": "इकारिआ आयलॅन्ड"
    }, {
      "ac": "IIL",
      "an": "Ilam",
      "han": "इलेम",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Ilam",
      "hct": "इलेम"
    }, {
      "ac": "ILP",
      "an": "Ile Des Pins",
      "han": "इले देस पिन्स",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Ile Des Pins",
      "hct": "इले देस पिन्स"
    }, {
      "ac": "YGR",
      "an": "House Harbour",
      "han": "हाऊस हर्बौर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Iles De La Madeleine",
      "hct": "इल्स दे ला मेडेलैन"
    }, {
      "ac": "ILF",
      "an": "Ilford",
      "han": "इल्फ़र्ड",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Ilford",
      "hct": "इल्फ़र्ड"
    }, {
      "ac": "IOS",
      "an": "Eduardo Gomes",
      "han": "एडुआर्डो गोम्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Ilheus",
      "hct": "इल्हेउस"
    }, {
      "ac": "ILI",
      "an": "Iliamna",
      "han": "इलियाम्ना",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Iliamna",
      "hct": "इलियाम्ना"
    }, {
      "ac": "ILO",
      "an": "Mandurriao",
      "han": "मंडूररिआव",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Iloilo",
      "hct": "इलोईलो"
    }, {
      "ac": "JAV",
      "an": "Ilulissat",
      "han": "इलुलिस्सट",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Ilulissat",
      "hct": "इलुलिस्सट"
    }, {
      "ac": "IKA",
      "an": "Imam Khomeini",
      "han": "इमाम खोमैनी",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Imam Khomeini",
      "hct": "इमाम खोमैनी"
    }, {
      "ac": "IMP",
      "an": "Imperatriz",
      "han": "इम्पेराट्रिज़",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Imperatriz",
      "hct": "इम्पेराट्रिज़"
    }, {
      "ac": "SHC",
      "an": "Indaselassie",
      "han": "ईंदासेलस्सी",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Indaselassie",
      "hct": "ईंदासेलस्सी"
    }, {
      "ac": "INH",
      "an": "Inhambane",
      "han": "इन्हम्बाणे",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Inhambane",
      "hct": "इन्हम्बाणे"
    }, {
      "ac": "INN",
      "an": "Kranebitten",
      "han": "क्राणेबित्तेन",
      "cn": "Austria",
      "hcn": "ऑस्ट्रिया",
      "cc": "AT",
      "ct": "Innsbruck",
      "hct": "इन्स्ब्रुक"
    }, {
      "ac": "INL",
      "an": "Intl Falls",
      "han": "इंट्ल फॉल्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "International Falls",
      "hct": "इंटरनेशनल फॉल्स"
    }, {
      "ac": "YPH",
      "an": "Inukjuak",
      "han": "इनुक्जुआक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Inukjuak",
      "hct": "इनुक्जुआक"
    }, {
      "ac": "YEV",
      "an": "Inuvik Mike Zubko",
      "han": "इनुवीक माईक ज़ुब्को",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Inuvik",
      "hct": "इनुवीक"
    }, {
      "ac": "IVC",
      "an": "Invercargill",
      "han": "इन्वेर्कारगिल",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Invercargill",
      "hct": "इन्वेर्कारगिल"
    }, {
      "ac": "IVR",
      "an": "Inverell",
      "han": "इन्वेरेल",
      "cn": "Australia ",
      "hcn": "औस्ट्रालिया ",
      "cc": "AU",
      "ct": "Inverell ",
      "hct": "इन्वेरेल "
    }, {
      "ac": "INV",
      "an": "Inverness",
      "han": "इन्वर्नेस",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Inverness",
      "hct": "इन्वर्नेस"
    }, {
      "ac": "IYK",
      "an": "Kern County",
      "han": "कर्न कौंटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Inyokern",
      "hct": "इन्योकर्न"
    }, {
      "ac": "IOA",
      "an": "Ioannina",
      "han": "इयोन्निना",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Ioannina",
      "hct": "इयोन्निना"
    }, {
      "ac": "IPN",
      "an": "Usiminas",
      "han": "उसिमिनस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Ipatinga",
      "hct": "इपातिंगा"
    }, {
      "ac": "IPI",
      "an": "San Luis",
      "han": "सेन लुईस",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Ipiales",
      "hct": "इपियाल्स"
    }, {
      "ac": "IPH",
      "an": "Ipoh",
      "han": "इपोह",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Ipoh",
      "hct": "इपोह"
    }, {
      "ac": "IPA",
      "an": "Ipota",
      "han": "इपोटा",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Ipota",
      "hct": "इपोटा"
    }, {
      "ac": "IPW",
      "an": "Ipswich",
      "han": "इप्स्विच",
      "cn": "United Kingdom ",
      "hcn": "युनाईटेड किंग्डोम ",
      "cc": "GB",
      "ct": "Ipswich ",
      "hct": "इप्स्विच "
    }, {
      "ac": "YFB",
      "an": "Iqaluit",
      "han": "इक़लुइत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Iqaluit",
      "hct": "इक़लुइत"
    }, {
      "ac": "IQQ",
      "an": "Cavancha Chucumata",
      "han": "कावंचा चुकमाता",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Iquique",
      "hct": "इक़िक़"
    }, {
      "ac": "IQT",
      "an": "C F Secada",
      "han": "सी एफ़ सेकेदा",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Iquitos",
      "hct": "इक़िटोस"
    }, {
      "ac": "IKT",
      "an": "Irkutsk",
      "han": "इर्कूत्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Irkutsk",
      "hct": "इर्कूत्स्क"
    }, {
      "ac": "IMT",
      "an": "Ford",
      "han": "फोर्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Iron Mountain",
      "hct": "आइरन मौंतैन"
    }, {
      "ac": "IWD",
      "an": "Gogebic County",
      "han": "गोगेबिक कौंटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ironwood",
      "hct": "इरोन्वूड"
    }, {
      "ac": "IFJ",
      "an": "Isafjordur",
      "han": "इसाफ़्जोर्डर",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Isafjordur",
      "hct": "इसाफ़्जोर्डर"
    }, {
      "ac": "IFN",
      "an": "Esfahan Shahid Beheshti",
      "han": "एस्फ़हेन शहीद बेहेश्टी",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Isfahan",
      "hct": "इस्फ़हेन"
    }, {
      "ac": "ISG",
      "an": "Ishigaki",
      "han": "इशिगकी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Ishigaki",
      "hct": "इशिगकी"
    }, {
      "ac": "ISB",
      "an": "Islamabad",
      "han": "इस्लामाबाद",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Islamabad",
      "hct": "इस्लामाबाद"
    }, {
      "ac": "YIV",
      "an": "Island Lake",
      "han": "आयलॅन्ड लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Island Lake",
      "hct": "आयलॅन्ड लेक"
    }, {
      "ac": "ILY",
      "an": "Islay",
      "han": "इसलै",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Islay",
      "hct": "इसलै"
    }, {
      "ac": "IOM",
      "an": "Ronaldsway",
      "han": "रोणाल्ड्सवै",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Isle Of Man",
      "hct": "इसले ऑफ़ मन"
    }, {
      "ac": "ISP",
      "an": "Long Island Macarthur",
      "han": "लोन्ग आयलॅन्ड मेकर्ठूर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Islip",
      "hct": "इस्लिप"
    }, {
      "ac": "ITH",
      "an": "Tomkins County",
      "han": "टोम्किन्स कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ithaca",
      "hct": "इठका"
    }, {
      "ac": "IVL",
      "an": "Ivalo",
      "han": "इवालो",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Ivalo",
      "hct": "इवालो"
    }, {
      "ac": "IFO",
      "an": "Ivano Frankivsk",
      "han": "इवानो फ़्रान्किव्स्क",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Ivano",
      "hct": "इवानो"
    }, {
      "ac": "YIK",
      "an": "Ivujivik",
      "han": "इवुजिवीक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Ivujivik",
      "hct": "इवुजिवीक"
    }, {
      "ac": "IWJ",
      "an": "",
      "han": "",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Iwami",
      "hct": "इवामी"
    }, {
      "ac": "ZIH",
      "an": "Zihuatanejo",
      "han": "ज़िहुआतनेजो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Ixtapa Zihuatanejo",
      "hct": "इक्स्टपा ज़िहुआतनेजो"
    }, {
      "ac": "IZM",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Izmir",
      "hct": "इज़्मीर"
    }, {
      "ac": "ADB",
      "an": "Adnan Menderes",
      "han": "अदनान मेंदेर्स",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Izmir",
      "hct": "इज़्मीर"
    }, {
      "ac": "IZO",
      "an": "Izumo",
      "han": "इज़ुमो",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Izumo",
      "hct": "इज़ुमो"
    }, {
      "ac": "JAC",
      "an": "Jackson Hole",
      "han": "जैकसन होले",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jackson",
      "hct": "जैकसन"
    }, {
      "ac": "JAN",
      "an": "Jackson Evers",
      "han": "जैकसन एवर्ज़",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jackson",
      "hct": "जैकसन"
    }, {
      "ac": "MKL",
      "an": "McKellar Fld",
      "han": "म्केलर फ़्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jackson",
      "hct": "जैकसन"
    }, {
      "ac": "OAJ",
      "an": "Albert J Ellis",
      "han": "आल्बर्ट जे एलिस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jacksonville",
      "hct": "जैकसनविले"
    }, {
      "ac": "JAX",
      "an": "Jacksonville",
      "han": "जैकसनविले",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jacksonville",
      "hct": "जैकसनविले"
    }, {
      "ac": "JAQ",
      "an": "Jacquinot Bay",
      "han": "जेक्क़ुइनोट बे",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Jacquinot Bay",
      "hct": "जेक्क़ुइनोट बे"
    }, {
      "ac": "JAL",
      "an": "Lencero",
      "han": "लेन्सेरो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Jalapa",
      "hct": "जलपा"
    }, {
      "ac": "DJB",
      "an": "Sultan Thaha",
      "han": "सुल्तान ठहा",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Jambi",
      "hct": "जेम्बी"
    }, {
      "ac": "JHW",
      "an": "Jamestown",
      "han": "जेम्सटाउन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jamestown",
      "hct": "जेम्सटाउन"
    }, {
      "ac": "JMS",
      "an": "Jamestown",
      "han": "जेम्सटाउन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jamestown",
      "hct": "जेम्सटाउन"
    }, {
      "ac": "DJJ",
      "an": "Sentani",
      "han": "सेनताणी",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Jayapura",
      "hct": "जयपूरा"
    }, {
      "ac": "GIZ",
      "an": "King Abdullah Bin Abdulaziz",
      "han": "किंग अब्दुल्लाह बीन अब्दूलाज़ीज़",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Gizan",
      "hct": "गिज़ान"
    }, {
      "ac": "JEJ",
      "an": "Jeh",
      "han": "जेह",
      "cn": "Marshall Islands",
      "hcn": "मार्शल आयलॅन्ड्स",
      "cc": "MH",
      "ct": "Ailinglapalap Atoll",
      "hct": "ऐलींग्लपलप अटोल"
    }, {
      "ac": "CJU",
      "an": "Jeju",
      "han": "जेजू",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Jeju",
      "hct": "जेजू"
    }, {
      "ac": "XRY",
      "an": "La Parra",
      "han": "ला पररा",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Jerez De La Frontera",
      "hct": "जेरेज़ दे ला फ़्रोन्टेरा"
    }, {
      "ac": "JER",
      "an": "States",
      "han": "स्टेट्स",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Jersey",
      "hct": "जेरसे"
    }, {
      "ac": "JSR",
      "an": "Jessore",
      "han": "जेस्सोरे",
      "cn": "Bangladesh",
      "hcn": "बांग्लादेश",
      "cc": "BD",
      "ct": "Jessore",
      "hct": "जेस्सोरे"
    }, {
      "ac": "JGS",
      "an": "Jing Gang Shan",
      "han": "जिंग गंग शान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Ji An",
      "hct": "जी अन"
    }, {
      "ac": "JMU",
      "an": "Jiamusi",
      "han": "जियामुसी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jiamusi",
      "hct": "जियामुसी"
    }, {
      "ac": "JIJ",
      "an": "",
      "han": "",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Jijiga",
      "hct": "जिजिगा"
    }, {
      "ac": "TNA",
      "an": "Jinan",
      "han": "जिनान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jinan",
      "hct": "जिनान"
    }, {
      "ac": "JDZ",
      "an": "Jingdezhen",
      "han": "जिंग्डेऴेन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jingdezhen",
      "hct": "जिंग्डेझ़ेन"
    }, {
      "ac": "JHG",
      "an": "Jinghong",
      "han": "जिंघोंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jinghong",
      "hct": "जिंघोंग"
    }, {
      "ac": "HIN",
      "an": "Sacheon Air Base",
      "han": "सचियोन एअर बेस",
      "cn": "South Korea",
      "hcn": "द‌क्षिण‌ कोरिया",
      "cc": "KR",
      "ct": "Sacheon",
      "hct": "सचियोन"
    }, {
      "ac": "JNZ",
      "an": "Jinzhou",
      "han": "जिन्झौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jinzhou",
      "hct": "जिन्झ़ै"
    }, {
      "ac": "JPR",
      "an": "",
      "han": "",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Ji-Parana",
      "hct": "जी-परना"
    }, {
      "ac": "JYR",
      "an": "",
      "han": "",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Jiroft",
      "hct": "जिरोफ़्ट"
    }, {
      "ac": "JIU",
      "an": "Jiujiang Lushan",
      "han": "जीऊजिआंग लुशन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jiujiang",
      "hct": "जीऊजिआंग"
    }, {
      "ac": "JGN",
      "an": "Jiayuguan",
      "han": "जियायुगुआन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jiayuguan",
      "hct": "जियायुगुआन"
    }, {
      "ac": "JPA",
      "an": "Presidente Castro Pinto",
      "han": "प्रेसिडेंटे कास्त्रो पिंटो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Joao Pessoa",
      "hct": "जोआओ पेस्सोआ"
    }, {
      "ac": "JOE",
      "an": "Joensuu",
      "han": "जोयन्सू",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Joensuu",
      "hct": "जोयन्सू"
    }, {
      "ac": "JST",
      "an": "Johnstown Cambria",
      "han": "जोह्न्स्टौन कम्ब्रिआ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Johnstown",
      "hct": "जोह्न्स्टौन"
    }, {
      "ac": "JHB",
      "an": "Sultan Ismail",
      "han": "सुल्तान इस्माईल",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Johor Bahru",
      "hct": "जोहोर बह्रू"
    }, {
      "ac": "JOI",
      "an": "Cubatao",
      "han": "कुबाटाव",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Joinville",
      "hct": "जोईन्वील"
    }, {
      "ac": "JOL",
      "an": "Jolo",
      "han": "जोलो",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Jolo ",
      "hct": "जोलो "
    }, {
      "ac": "JBR",
      "an": "Jonesboro Muni",
      "han": "जोन्सबोरो मुनि",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Jonesboro",
      "hct": "जोन्सबोरो"
    }, {
      "ac": "JKG",
      "an": "Axamo",
      "han": "अक्षमो",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Jonkoping",
      "hct": "जोन्कोपींग"
    }, {
      "ac": "JLN",
      "an": "Joplin Municipal",
      "han": "जोप्लीन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Joplin",
      "hct": "जोप्लीन"
    }, {
      "ac": "AJF",
      "an": "Al",
      "han": "अल",
      "cn": "Jawf Domestic",
      "hcn": "जौफ़ डोमेस्टिक",
      "cc": "Saudi Arabia",
      "ct": "SA",
      "hct": "सा"
    }, {
      "ac": "JDO",
      "an": "Orlando Bezerra de Menezes",
      "han": "ओर्लांदो बेज़ेररा दे मिनेज़िस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Juazeiro Do Norte",
      "hct": "जुआज़ेरो दो नोरटे"
    }, {
      "ac": "JDF",
      "an": "Francisco De Assis",
      "han": "फ़्रान्सिस्को दे अस्सिस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Juiz De Fora",
      "hct": "जुइज़ दे फ़ोरा"
    }, {
      "ac": "JUJ",
      "an": "El Cadillal",
      "han": "एल केदिलल",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Jujuy",
      "hct": "जुजुय"
    }, {
      "ac": "JCK",
      "an": "Julia Creek",
      "han": "जुलिया क्रीक",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Julia Creek ",
      "hct": "जुलिया क्रीक "
    }, {
      "ac": "JUL",
      "an": "Juliaca",
      "han": "जुलियाका",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Juliaca",
      "hct": "जुलियाका"
    }, {
      "ac": "JUM",
      "an": "Jumla",
      "han": "जुमल",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Jumla",
      "hct": "जुमल"
    }, {
      "ac": "JNU",
      "an": "Juneau",
      "han": "जुणीऊ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Juneau",
      "hct": "जुणीऊ"
    }, {
      "ac": "JUZ",
      "an": "Quzhou",
      "han": "क़ुझौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Quzhou",
      "hct": "क़ुझ़ै"
    }, {
      "ac": "JYV",
      "an": "Jyvaskyla",
      "han": "ज्य्वस्क्य्ला",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Jyvaskyla",
      "hct": "ज्य्वस्क्य्ला"
    }, {
      "ac": "KDM",
      "an": "Kaadedhdhoo",
      "han": "काडेढ्ढू",
      "cn": "Maldives",
      "hcn": "माल‌दीव‌",
      "cc": "MV",
      "ct": "Kaadedhdhoo",
      "hct": "काडेढ्ढू"
    }, {
      "ac": "ABK",
      "an": "Kabri Dehar",
      "han": "काब्री देहार",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Kabri Dehar",
      "hct": "काब्री देहार"
    }, {
      "ac": "KDO",
      "an": "Kadhdhoo",
      "han": "काढ्ढू",
      "cn": "Maldives",
      "hcn": "माल‌दीव‌",
      "cc": "MV",
      "ct": "Laamu Atoll",
      "hct": "लामू अटोल"
    }, {
      "ac": "KGE",
      "an": "Kagau Island",
      "han": "कागऊ आयलॅन्ड",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Kagau Island",
      "hct": "कागऊ आयलॅन्ड"
    }, {
      "ac": "KOJ",
      "an": "Kagoshima",
      "han": "कागोसहिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kagoshima",
      "hct": "कागोसहिमा"
    }, {
      "ac": "KCM",
      "an": "KAHRAMANMARAS",
      "han": "कह्रमन्मरेस",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "KAHRAMANMARAS",
      "hct": "कह्रमन्मरेस"
    }, {
      "ac": "OGG",
      "an": "Kahului",
      "han": "कहूलुइ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kahului",
      "hct": "कहूलुइ"
    }, {
      "ac": "KNG",
      "an": "Kaimana",
      "han": "काईमना",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Kaimana",
      "hct": "काईमना"
    }, {
      "ac": "KAT",
      "an": "Kaitaia",
      "han": "कैतया",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Kaitaia",
      "hct": "कैतया"
    }, {
      "ac": "KAJ",
      "an": "Kajaani",
      "han": "कजानी",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Kajaani",
      "hct": "कजानी"
    }, {
      "ac": "KAE",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kake",
      "hct": "काके"
    }, {
      "ac": "KNK",
      "an": "Kakhonak",
      "han": "काखोनक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kakhonak",
      "hct": "काखोनक"
    }, {
      "ac": "KLX",
      "an": "Kalamata",
      "han": "कलमाता",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kalamata",
      "hct": "कलमाता"
    }, {
      "ac": "AZO",
      "an": "Kalamazoo Cty",
      "han": "कलामजू क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kalamazoo",
      "hct": "कलामजू"
    }, {
      "ac": "LUP",
      "an": "Kalaupapa",
      "han": "कलऊपपा",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Molokai",
      "hct": "मोलोकाई"
    }, {
      "ac": "KAX",
      "an": "Kalbarri",
      "han": "कलबारी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Kalbarri ",
      "hct": "कलबारी "
    }, {
      "ac": "KMV",
      "an": "Kalay",
      "han": "कलै",
      "cn": "Myanmar",
      "hcn": "म्यानमर",
      "cc": "MM",
      "ct": "Kalemyo",
      "hct": "कालेम्यो"
    }, {
      "ac": "KGI",
      "an": "Kalgoorlie Boulder",
      "han": "कलगूर्ली बौल्डर",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Kalgoorlie",
      "hct": "कलगूर्ली"
    }, {
      "ac": "KLO",
      "an": "Kalibo",
      "han": "कालिबो",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Kalibo",
      "hct": "कालिबो"
    }, {
      "ac": "KGD",
      "an": "Kaliningrad",
      "han": "कलीनिंग्रेद",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kaliningrad",
      "hct": "कलीनिंग्रेद"
    }, {
      "ac": "FCA",
      "an": "Glacier Park",
      "han": "ग्लेसिएर पार्क",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kalispell",
      "hct": "कालिस्पेल"
    }, {
      "ac": "KLR",
      "an": "Kalmar",
      "han": "कल्मर",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Kalmar",
      "hct": "कल्मर"
    }, {
      "ac": "KLG",
      "an": "Kalskag",
      "han": "कलस्काग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kalskag",
      "hct": "कलस्काग"
    }, {
      "ac": "KAL",
      "an": "Kaltag",
      "han": "कलतेग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kaltag",
      "hct": "कलतेग"
    }, {
      "ac": "JKL",
      "an": "Kalymnos Island",
      "han": "कलय्म्नोस आयलॅन्ड",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kalymnos",
      "hct": "कलय्म्नोस"
    }, {
      "ac": "KAC",
      "an": "Kamishly",
      "han": "कामीश्ली",
      "cn": "Syria",
      "hcn": "सिरिआ",
      "cc": "SY",
      "ct": "Kamishly",
      "hct": "कामीश्ली"
    }, {
      "ac": "YKA",
      "an": "Davie Fulton",
      "han": "दवी फ़ूलटन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "KAMLOOPS",
      "hct": "कम्लूप्स"
    }, {
      "ac": "KLA",
      "an": "",
      "han": "",
      "cn": "Uganda",
      "hcn": "युगांडा",
      "cc": "",
      "ct": "Kampala",
      "hct": "कंपाला"
    }, {
      "ac": "MUE",
      "an": "Waimea Kohala",
      "han": "वाईमी कोहअला",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kamuela",
      "hct": "कामुएला"
    }, {
      "ac": "KGA",
      "an": "Kananga",
      "han": "कनांगा",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Kananga",
      "hct": "कनांगा"
    }, {
      "ac": "KDV",
      "an": "Vunisea",
      "han": "वुनीसी",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Vunisea",
      "hct": "वुनीसी"
    }, {
      "ac": "KDR",
      "an": "Kandrian",
      "han": "कन्द्रिआन",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Kandrian",
      "hct": "कन्द्रिआन"
    }, {
      "ac": "SFJ",
      "an": "Sondre Stromfjord",
      "han": "सोन्ड्रे स्त्रोम्फ़्जोर्ड",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Sondrestrom",
      "hct": "सोन्ड्रेस्त्रोम"
    }, {
      "ac": "XGR",
      "an": "Kangiqsualujjuaq",
      "han": "कंगिक़्सुआलुज्जुआक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kangiqsualujjuaq",
      "hct": "कंगिक़्सुआलुज्जुआक़"
    }, {
      "ac": "YWB",
      "an": "Kangiqsujuaq",
      "han": "कंगिक़्सुजुआक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kangiqsujuaq",
      "hct": "कंगिक़्सुजुआक़"
    }, {
      "ac": "YKG",
      "an": "Kangirsuk",
      "han": "कंगीर्सुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kangirsuk",
      "hct": "कंगीर्सुक"
    }, {
      "ac": "MCI",
      "an": "Kansas City",
      "han": "कन्सास सिटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kansas City",
      "hct": "कन्सास सिटी"
    }, {
      "ac": "KHH",
      "an": "Kaohsiung",
      "han": "काव्ह्सीऊन्ग",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Kaohsiung",
      "hct": "काव्ह्सीऊन्ग"
    }, {
      "ac": "JHM",
      "an": "Kapalua",
      "han": "कपलुआ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kapalua",
      "hct": "कपलुआ"
    }, {
      "ac": "YYU",
      "an": "Japuskasing Municipal",
      "han": "जापुस्कासिंह म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kapuskasing",
      "hct": "कपुस्कासिंह"
    }, {
      "ac": "KHI",
      "an": "Quaid E Azam",
      "han": "क़ाईद इ आज़म",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Karachi",
      "hct": "कराची"
    }, {
      "ac": "KGF",
      "an": "Sary",
      "han": "सारी",
      "cn": "Arka",
      "hcn": "आर्का",
      "cc": "Kazakhstan",
      "ct": "KZ",
      "hct": "क्ज़"
    }, {
      "ac": "KRY",
      "an": "Karamay",
      "han": "करमाय",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Karamay",
      "hct": "करमाय"
    }, {
      "ac": "KDL",
      "an": "Amari",
      "han": "अमारी",
      "cn": "Estonia",
      "hcn": "एस्तोनिआ",
      "cc": "EE",
      "ct": "Armari Air Force Base",
      "hct": "अरमारी एअर फ़ोर्स बेस"
    }, {
      "ac": "KAB",
      "an": "Kariba",
      "han": "कारीबा",
      "cn": "Zimbabwe",
      "hcn": "ज़िम्बाब्वे",
      "cc": "ZW",
      "ct": "Kariba",
      "hct": "कारीबा"
    }, {
      "ac": "KLV",
      "an": "Karlovy Vary",
      "han": "कर्लोवी वारी",
      "cn": "Czech Republic",
      "hcn": "क्ज़ेच रीपबलीक",
      "cc": "CZ",
      "ct": "Karlovy Vary",
      "hct": "कर्लोवी वारी"
    }, {
      "ac": "FKB",
      "an": "Soellingen",
      "han": "सोयलींगेन",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Karlsruhe Baden Baden",
      "hct": "कर्ल्स्रुहे बद्न बद्न"
    }, {
      "ac": "KSD",
      "an": "Karlstad",
      "han": "कर्ल्स्ताद",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Karlstad",
      "hct": "कर्ल्स्ताद"
    }, {
      "ac": "KYK",
      "an": "Karluk",
      "han": "कर्लुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Karluk",
      "hct": "कर्लुक"
    }, {
      "ac": "AOK",
      "an": "Karpathos",
      "han": "कर्पाथोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Karpathos",
      "hct": "कर्पाथोस"
    }, {
      "ac": "KTA",
      "an": "Karratha",
      "han": "कर्राथा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Karratha",
      "hct": "कर्राथा"
    }, {
      "ac": "KSY",
      "an": "Kars",
      "han": "कर्स",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Kars",
      "hct": "कर्स"
    }, {
      "ac": "KSQ",
      "an": "Karshi Khanabad",
      "han": "कर्शी खानबाद",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Khanabad",
      "hct": "खानबाद"
    }, {
      "ac": "KRB",
      "an": "Karumba",
      "han": "कारुम्बा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Karumba ",
      "hct": "कारुम्बा "
    }, {
      "ac": "KRP",
      "an": "Karup",
      "han": "करुप",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Karup",
      "hct": "करुप"
    }, {
      "ac": "KXA",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kasaan",
      "hct": "कासान"
    }, {
      "ac": "XKS",
      "an": "Kasabonika",
      "han": "कसाबोनिका",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kasabonika",
      "hct": "कसाबोनिका"
    }, {
      "ac": "ZKE",
      "an": "Kashechewan",
      "han": "काशेचेवन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kashechewan",
      "hct": "काशेचेवन"
    }, {
      "ac": "KHG",
      "an": "Kashi",
      "han": "काशी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Kashi",
      "hct": "काशी"
    }, {
      "ac": "KUK",
      "an": "Kasigluk",
      "han": "कासिग्लुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kasigluk",
      "hct": "कासिग्लुक"
    }, {
      "ac": "KSJ",
      "an": "Kasos Island",
      "han": "कसोस आयलॅन्ड",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kasos Island",
      "hct": "कसोस आयलॅन्ड"
    }, {
      "ac": "KSF",
      "an": "Kassel Calden",
      "han": "काससेल काल्द्न",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Kassel",
      "hct": "काससेल"
    }, {
      "ac": "KZS",
      "an": "Kastelorizo",
      "han": "कस्टेलोरिज़ो",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kastelorizo",
      "hct": "कस्टेलोरिज़ो"
    }, {
      "ac": "KSO",
      "an": "Aristotelis",
      "han": "आरिस्टोटेलिस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kastoria",
      "hct": "कस्टोरिआ"
    }, {
      "ac": "KTR",
      "an": "Tindal",
      "han": "तीनदल",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Katherine",
      "hct": "काथेरिन"
    }, {
      "ac": "KTW",
      "an": "Pyrzowice",
      "han": "पिर्ज़ौइस",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Katowice",
      "hct": "कतौइस"
    }, {
      "ac": "XXK",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Katrineholm",
      "hct": "कट्रिनेहोल्म"
    }, {
      "ac": "LIH",
      "an": "Lihue Municipal",
      "han": "लिहुए म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kauai",
      "hct": "कौआई"
    }, {
      "ac": "KVA",
      "an": "Megas Alexandros",
      "han": "मेगस अलेक्सान्द्रोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kavala",
      "hct": "कावाला"
    }, {
      "ac": "KVG",
      "an": "Kavieng",
      "han": "कवीन्ग",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Kavieng",
      "hct": "कवीन्ग"
    }, {
      "ac": "KAW",
      "an": "Kawthoung",
      "han": "कव्ठौन्ग",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Kawthoung",
      "hct": "कव्ठौन्ग"
    }, {
      "ac": "KYS",
      "an": "Kayes Dag Dag",
      "han": "कयेस दाग दाग",
      "cn": "Mali",
      "hcn": "माली",
      "cc": "ML",
      "ct": "Kayes",
      "hct": "कयेस"
    }, {
      "ac": "ASR",
      "an": "Erkilet",
      "han": "एर्कीलेट",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Kayseri",
      "hct": "कैसेरी"
    }, {
      "ac": "KZN",
      "an": "Kazan",
      "han": "कज़ान",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kazan",
      "hct": "कज़ान"
    }, {
      "ac": "EAR",
      "an": "Kearney Municipal Arrpt",
      "han": "कीरने म्यूनिसिपल आर्र्प्ट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kearney",
      "hct": "कीरने"
    }, {
      "ac": "KEW",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Keewaywin",
      "hct": "कीवेविन"
    }, {
      "ac": "EFL",
      "an": "Argostoli",
      "han": "आर्गोस्टोली",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kefallinia",
      "hct": "केफ़लीनिआ"
    }, {
      "ac": "ZKG",
      "an": "Kegaska",
      "han": "केगास्का",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kegaska",
      "hct": "केगास्का"
    }, {
      "ac": "YLW",
      "an": "Ellison Field",
      "han": "एलिसन फ़ील्ड",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kelowna",
      "hct": "केलौना"
    }, {
      "ac": "KEJ",
      "an": "Kemerovo",
      "han": "केमेरोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kemorovo",
      "hct": "केमोरोवो"
    }, {
      "ac": "KEM",
      "an": "Kemi Tornio",
      "han": "केमी टर्नियो",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Kemi Tornio",
      "hct": "केमी टर्नियो"
    }, {
      "ac": "ENA",
      "an": "Kenai Municipal",
      "han": "केनै म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kenai",
      "hct": "केनै"
    }, {
      "ac": "KDI",
      "an": "Wolter Monginsidi",
      "han": "वोल्टर मोंजिन्सिडी",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Kendari",
      "hct": "केंडारी"
    }, {
      "ac": "KET",
      "an": "Kengtung",
      "han": "केन्ग्टुन्ग",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Kengtung",
      "hct": "केन्ग्टुन्ग"
    }, {
      "ac": "YQK",
      "an": "Kenora",
      "han": "केनोरा",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kenora",
      "hct": "केनोरा"
    }, {
      "ac": "KMA",
      "an": "",
      "han": "",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Kerema",
      "hct": "केरेमा"
    }, {
      "ac": "KKE",
      "an": "Kerikeri",
      "han": "केरीकेरी",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Kerikeri",
      "hct": "केरीकेरी"
    }, {
      "ac": "KER",
      "an": "Kerman",
      "han": "केरमान",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Kerman",
      "hct": "केरमान"
    }, {
      "ac": "KSH",
      "an": "Kermanshah",
      "han": "केरमानशाह",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Kermanshah",
      "hct": "केरमानशाह"
    }, {
      "ac": "KIR",
      "an": "Kerry County",
      "han": "केरी कौंटी",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Kerry County",
      "hct": "केरी कौंटी"
    }, {
      "ac": "KTN",
      "an": "Ketchikan",
      "han": "केत्चिकन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ketchikan",
      "hct": "केत्चिकन"
    }, {
      "ac": "EYW",
      "an": "Key West",
      "han": "के वॅस्ट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Key West",
      "hct": "के वॅस्ट"
    }, {
      "ac": "KHV",
      "an": "Novyy",
      "han": "नोव्य",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Khabarovsk",
      "hct": "खबरोव्स्क"
    }, {
      "ac": "KHM",
      "an": "Khamti",
      "han": "खम्टी",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Khamti",
      "hct": "खम्टी"
    }, {
      "ac": "HMA",
      "an": "Khanty Mansiysk",
      "han": "खान्ती मान्सिय्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Khanty",
      "hct": "खान्ती"
    }, {
      "ac": "HRK",
      "an": "Kharkov",
      "han": "खार्कोव",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Kharkov",
      "hct": "खार्कोव"
    }, {
      "ac": "KRT",
      "an": "Civil",
      "han": "सिवील",
      "cn": "Sudan",
      "hcn": "सुदान",
      "cc": "SD",
      "ct": "Khartoum",
      "hct": "खर्टौम"
    }, {
      "ac": "KHS",
      "an": "Khasab",
      "han": "खासब",
      "cn": "Oman",
      "hcn": "ओमान",
      "cc": "OM",
      "ct": "Khasab",
      "hct": "खासब"
    }, {
      "ac": "KHE",
      "an": "Kherson",
      "han": "खेरसन",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Kherson",
      "hct": "खेरसन"
    }, {
      "ac": "KKC",
      "an": "Khon Kaen",
      "han": "खोन कएन",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Khon Kaen",
      "hct": "खोन कएन"
    }, {
      "ac": "LBD",
      "an": "Khudzhand",
      "han": "खूदजेंद",
      "cn": "Tajikistan",
      "hcn": "ताजिकिस्तान",
      "cc": "TJ",
      "ct": "Khudzhand",
      "hct": "खूदझ़ेंद"
    }, {
      "ac": "IAN",
      "an": "Bob Barker Memorial",
      "han": "बॉब बारकर मेमोरिल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kiana",
      "hct": "कियाना"
    }, {
      "ac": "KRI",
      "an": "Kikori",
      "han": "किकोरी",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Kikori",
      "hct": "किकोरी"
    }, {
      "ac": "JRO",
      "an": "Kilimanjaro",
      "han": "कीलीमन्जारो",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Kilimanjaro",
      "hct": "कीलीमन्जारो"
    }, {
      "ac": "GRK",
      "an": "Robert Gray Aaf",
      "han": "रॉबर्ट गराय आफ़",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Killeen",
      "hct": "कीलीन"
    }, {
      "ac": "KIM",
      "an": "Kimberley",
      "han": "किम्बरले",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Kimberley",
      "hct": "किम्बरले"
    }, {
      "ac": "YLC",
      "an": "Kimmirut",
      "han": "किम्मीरुत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kimmirut",
      "hct": "किम्मीरुत"
    }, {
      "ac": "KND",
      "an": "Kindu",
      "han": "किन्दू",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Kindu",
      "hct": "किन्दू"
    }, {
      "ac": "KVC",
      "an": "King Cove",
      "han": "किंग कोव",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "King Cove",
      "hct": "किंग कोव"
    }, {
      "ac": "KNS",
      "an": "King Island",
      "han": "किंग आयलॅन्ड",
      "cn": " Tasmania ,10",
      "hcn": " तासमणीया ,10",
      "cc": "Australia",
      "ct": "King Island",
      "hct": "किंग आयलॅन्ड"
    }, {
      "ac": "KMC",
      "an": "",
      "han": "",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "King Khalid Mil. City",
      "hct": "किंग खालिद मील. सिटी"
    }, {
      "ac": "AKN",
      "an": "King Salmon",
      "han": "किंग शलमोन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "King Salmon",
      "hct": "किंग शलमोन"
    }, {
      "ac": "KIF",
      "an": "Kingfisher Lake",
      "han": "किंग्फ़िशेर लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kingfisher Lake",
      "hct": "किंग्फ़िशेर लेक"
    }, {
      "ac": "IGM",
      "an": "Mohave County",
      "han": "मोहवे कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kingman",
      "hct": "किंगमन"
    }, {
      "ac": "KGC",
      "an": "Kingscote",
      "han": "किंग्स्कोटे",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Kingscote",
      "hct": "किंग्स्कोटे"
    }, {
      "ac": "YGK",
      "an": "Norman Rodgers",
      "han": "नोर्मन रोद्गेर्ज़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kingston",
      "hct": "किंगस्टन"
    }, {
      "ac": "KNH",
      "an": "Shang Yi",
      "han": "शंग यी",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Chinmen",
      "hct": "चीनमान"
    }, {
      "ac": "FIH",
      "an": "Kinshasa",
      "han": "किन्शसा",
      "cn": "Democratic Republic of Congo",
      "hcn": "कांगो, लोक‌तांत्रिक‌ ग‌ण‌राज्य‌",
      "cc": "CD",
      "ct": "Kinshasa",
      "hct": "किन्शसा"
    }, {
      "ac": "ISO",
      "an": "Stalliings Field",
      "han": "स्टालिइंग्स फ़ील्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kinston",
      "hct": "किन्स्टन"
    }, {
      "ac": "KPN",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kipnuk",
      "hct": "किप्नुक"
    }, {
      "ac": "IRA",
      "an": "Ngorangora",
      "han": "न्गोरन्गोरा",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Kirakira",
      "hct": "किरकिरा"
    }, {
      "ac": "KKN",
      "an": "Hoeybuktmoen",
      "han": "होयय्बुक्ट्मोयन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Kirkenes",
      "hct": "किर्केन्स"
    }, {
      "ac": "IRK",
      "an": "Kirksville Municipal",
      "han": "किर्क्स्वील म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kirksville",
      "hct": "किर्क्स्वील"
    }, {
      "ac": "KOI",
      "an": "Kirkwall",
      "han": "किर्कवाल",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Kirkwall   Orkney Island",
      "hct": "किर्कवाल   ओर्क्णी आयलॅन्ड"
    }, {
      "ac": "KRN",
      "an": "Kiruna",
      "han": "कीरुना",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Kiruna",
      "hct": "कीरुना"
    }, {
      "ac": "FKI",
      "an": "Kisangani Simisini",
      "han": "किसनगणी सिमीसीनी",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Kisangani",
      "hct": "किसनगणी"
    }, {
      "ac": "KIH",
      "an": "Kish Island",
      "han": "किश आयलॅन्ड",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Kish Island",
      "hct": "किश आयलॅन्ड"
    }, {
      "ac": "KIS",
      "an": "Kisumu",
      "han": "किसूमू",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Kisumu",
      "hct": "किसूमू"
    }, {
      "ac": "KKJ",
      "an": "New Kitakyushu",
      "han": "न्यू कितक्युशू",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kitakyushu",
      "hct": "कितक्युशू"
    }, {
      "ac": "YKF",
      "an": "Waterloo",
      "han": "वाटर्लू",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Waterloo",
      "hct": "वाटर्लू"
    }, {
      "ac": "KIT",
      "an": "Kithira",
      "han": "किथीरा",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kithira",
      "hct": "किथीरा"
    }, {
      "ac": "KKB",
      "an": "Kitoi Bay",
      "han": "किटोई बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kitoi Bay",
      "hct": "किटोई बे"
    }, {
      "ac": "KTT",
      "an": "Kittila",
      "han": "किट्टिला",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Kittila",
      "hct": "किट्टिला"
    }, {
      "ac": "UNG",
      "an": "Kiunga",
      "han": "कीऊन्गा",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Kiunga",
      "hct": "कीऊन्गा"
    }, {
      "ac": "KVL",
      "an": "Kivalina",
      "han": "किवलीना",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kivalina",
      "hct": "किवलीना"
    }, {
      "ac": "KLU",
      "an": "Klagenfurt",
      "han": "क्लजन्फ़ूरट",
      "cn": "Austria",
      "hcn": "ऑस्ट्रिया",
      "cc": "AT",
      "ct": "Klagenfurt",
      "hct": "क्लजन्फ़ूरट"
    }, {
      "ac": "PLQ",
      "an": "Palanga",
      "han": "पालंगा",
      "cn": "Lithuania",
      "hcn": "लिठुआणिआ",
      "cc": "LT",
      "ct": "Palanga",
      "hct": "पालंगा"
    }, {
      "ac": "LMT",
      "an": "Kingsley Field",
      "han": "किंग्सले फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Klamath Falls",
      "hct": "क्लामथ फॉल्स"
    }, {
      "ac": "KLW",
      "an": "Klawock",
      "han": "कलवोक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Klawock",
      "hct": "कलवोक"
    }, {
      "ac": "NOC",
      "an": "Knock",
      "han": "क्नोक",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Knock",
      "hct": "क्नोक"
    }, {
      "ac": "TYS",
      "an": "McGhee Tyson",
      "han": "म्कघी त्यसन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "KNOXVILLE",
      "hct": "क्नोक्सविले"
    }, {
      "ac": "UKB",
      "an": "Kobe",
      "han": "कोबे",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kobe",
      "hct": "कोबे"
    }, {
      "ac": "OBU",
      "an": "Kobuk,Wien",
      "han": "कोबुक,वीन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kobuk",
      "hct": "कोबुक"
    }, {
      "ac": "ADQ",
      "an": "Kodiak",
      "han": "कोडियाक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kodiak",
      "hct": "कोडियाक"
    }, {
      "ac": "KGP",
      "an": "Kogalym",
      "han": "कोगल्य्म",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kogalym",
      "hct": "कोगल्य्म"
    }, {
      "ac": "KOK",
      "an": "Kokkola",
      "han": "कोक्कोला",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Kokkola Pietarsaari",
      "hct": "कोक्कोला पीतरसारी"
    }, {
      "ac": "KKD",
      "an": "Kokoda",
      "han": "कोकोदा",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Kokoda",
      "hct": "कोकोदा"
    }, {
      "ac": "KMQ",
      "an": "Komatsu",
      "han": "कोमात्सु",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kanazawa",
      "hct": "कनज़ावा"
    }, {
      "ac": "KXK",
      "an": "Komsomolsk",
      "han": "कोम्सोमोल्स्क",
      "cn": "on",
      "hcn": "ओन",
      "cc": "Amur",
      "ct": "Russia",
      "hct": "रुससिआ"
    }, {
      "ac": "KOA",
      "an": "Keahole",
      "han": "कीहोले",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kona",
      "hct": "कोना"
    }, {
      "ac": "KNQ",
      "an": "Kone",
      "han": "कोने",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Kone",
      "hct": "कोने"
    }, {
      "ac": "KKH",
      "an": "Kongiganak",
      "han": "कोन्गिगनक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kongiganak",
      "hct": "कोन्गिगनक"
    }, {
      "ac": "KYA",
      "an": "Konya",
      "han": "कोन्या",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Konya",
      "hct": "कोन्या"
    }, {
      "ac": "KRL",
      "an": "Korla",
      "han": "कोरला",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Korla",
      "hct": "कोरला"
    }, {
      "ac": "KXF",
      "an": "Koro Island",
      "han": "कोरो आयलॅन्ड",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Koro Island",
      "hct": "कोरो आयलॅन्ड"
    }, {
      "ac": "KGS",
      "an": "Kos",
      "han": "कोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kos",
      "hct": "कोस"
    }, {
      "ac": "KSC",
      "an": "Barca",
      "han": "बार्का",
      "cn": "Slovakia",
      "hcn": "स्लोवकिआ",
      "cc": "SK",
      "ct": "Kosice",
      "hct": "कोसिस"
    }, {
      "ac": "KSN",
      "an": "Kostanay West",
      "han": "कोस्टनाई वॅस्ट",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Kostanay",
      "hct": "कोस्टनाई"
    }, {
      "ac": "OSZ",
      "an": "",
      "han": "",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Koszalin",
      "hct": "कोस्ज़ालीन"
    }, {
      "ac": "KBR",
      "an": "Pengkalan Chepa",
      "han": "पेन्ग्कलन चेपा",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kota Bharu",
      "hct": "कोटा �रू"
    }, {
      "ac": "BKI",
      "an": "Kota Kinabalu",
      "han": "कोटा किनबालू",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kota Kinabalu",
      "hct": "कोटा किनबालू"
    }, {
      "ac": "KSZ",
      "an": "Kotlas",
      "han": "कोटलास",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kotlas",
      "hct": "कोटलास"
    }, {
      "ac": "KOT",
      "an": "Kotlik",
      "han": "कोट्लिक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kotlik",
      "hct": "कोट्लिक"
    }, {
      "ac": "OTZ",
      "an": "Ralph Wien Memorial",
      "han": "राल्फ वीन मेमोरिल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kotzebue",
      "hct": "कोट्ज़ेबुए"
    }, {
      "ac": "KOU",
      "an": "Koulamoutou",
      "han": "कौलामौटौ",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Koulamoutou",
      "hct": "कौलामौटौ"
    }, {
      "ac": "KOC",
      "an": "Koumac",
      "han": "कौमेक",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Koumac",
      "hct": "कौमेक"
    }, {
      "ac": "KWM",
      "an": "Kowanyama",
      "han": "कौआण्यामा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Kowanyama ",
      "hct": "कौआण्यामा "
    }, {
      "ac": "KKA",
      "an": "Koyuk",
      "han": "कोयुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Koyuk",
      "hct": "कोयुक"
    }, {
      "ac": "KYU",
      "an": "Koyukuk",
      "han": "कोयुकुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Koyukuk",
      "hct": "कोयुकुक"
    }, {
      "ac": "KZI",
      "an": "Filippos",
      "han": "फ़िलिप्पोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Kozani",
      "hct": "कोज़ाणी"
    }, {
      "ac": "KRK",
      "an": "John Paul II",
      "han": "जॉन पॉल 2",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Krakow",
      "hct": "क्रकौ"
    }, {
      "ac": "KRF",
      "an": "Kramfors Solleftea",
      "han": "क्राम्फ़ोर्स सोलेफ़्टी",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Kramfors",
      "hct": "क्राम्फ़ोर्स"
    }, {
      "ac": "KRR",
      "an": "Krasnodar",
      "han": "क्रस्नोदार",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Krasnodar",
      "hct": "क्रस्नोदार"
    }, {
      "ac": "KJA",
      "an": "Krasnojarsk",
      "han": "क्रस्नोजर्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Krasnojarsk",
      "hct": "क्रस्नोजर्स्क"
    }, {
      "ac": "KRS",
      "an": "Kjevik",
      "han": "क्जेवीक",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Kristiansand",
      "hct": "क्रिस्तिआन्सण्द"
    }, {
      "ac": "KID",
      "an": "Kristianstad",
      "han": "क्रिस्तिआन्स्ताद",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Kristianstad",
      "hct": "क्रिस्तिआन्स्ताद"
    }, {
      "ac": "KSU",
      "an": "Kvernberget",
      "han": "क्वर्न्बर्गेत",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Kristiansund",
      "hct": "क्रिस्तिआन्सुन्ड"
    }, {
      "ac": "KWG",
      "an": "Krivoy Rog",
      "han": "कृवोय रोग",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Krivoy Rog",
      "hct": "कृवोय रोग"
    }, {
      "ac": "TGG",
      "an": "Sultan Mahmood",
      "han": "सुल्तान महमूद",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kuala Terengganu",
      "hct": "कुआला तेरेन्ग्गनू"
    }, {
      "ac": "KUA",
      "an": "Kuantan",
      "han": "कुआनतन",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kuantan",
      "hct": "कुआनतन"
    }, {
      "ac": "KUG",
      "an": "Kubin",
      "han": "कुबीन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Kubin Island ",
      "hct": "कुबीन आयलॅन्ड "
    }, {
      "ac": "KCH",
      "an": "Kuching",
      "han": "कुचींग",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kuching",
      "hct": "कुचींग"
    }, {
      "ac": "KUD",
      "an": "Kudat",
      "han": "कुदत",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Kudat",
      "hct": "कुदत"
    }, {
      "ac": "AKF",
      "an": "Kufra",
      "han": "कुफ़्रा",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Kufra",
      "hct": "कुफ़्रा"
    }, {
      "ac": "YBB",
      "an": "Kugaaruk",
      "han": "कुगारुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Pelly Bay",
      "hct": "पेली बे"
    }, {
      "ac": "YCO",
      "an": "Kugluktuk",
      "han": "कुग्लुक्टुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Coppermine",
      "hct": "कोप्पेर्मिन"
    }, {
      "ac": "KUS",
      "an": "Kulusuk",
      "han": "कुलुसुक",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Kulusuk",
      "hct": "कुलुसुक"
    }, {
      "ac": "KMJ",
      "an": "Kumamoto",
      "han": "कुमामोटो",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kumamoto",
      "hct": "कुमामोटो"
    }, {
      "ac": "UEO",
      "an": "Kumejima",
      "han": "कूमेजिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kumejima",
      "hct": "कूमेजिमा"
    }, {
      "ac": "CMU",
      "an": "Chimbu",
      "han": "चिम्बू",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Kundiawa",
      "hct": "कून्डियावा"
    }, {
      "ac": "KNX",
      "an": "Kununurra",
      "han": "कूनुनूररा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Kununurra",
      "hct": "कूनुनूररा"
    }, {
      "ac": "KUO",
      "an": "Kuopio",
      "han": "कुओपियो",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Kuopio",
      "hct": "कुओपियो"
    }, {
      "ac": "KOE",
      "an": "El Tari",
      "han": "एल टारी",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Kupang",
      "hct": "कुपंग"
    }, {
      "ac": "KCA",
      "an": "Kuqa",
      "han": "कुक़",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Kuqa",
      "hct": "कुक़"
    }, {
      "ac": "URE",
      "an": "Kuressaare",
      "han": "कुरेससारे",
      "cn": "Estonia",
      "hcn": "ईथियोपिया",
      "cc": "EE",
      "ct": "Kuressaare",
      "hct": "कुरेससारे"
    }, {
      "ac": "KRO",
      "an": "Kurgan",
      "han": "कूरगन",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kurgan",
      "hct": "कूरगन"
    }, {
      "ac": "URS",
      "an": "Kursk East",
      "han": "कूरस्क ईस्ट",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kursk",
      "hct": "कूरस्क"
    }, {
      "ac": "KUH",
      "an": "Kushiro",
      "han": "कुशीरो",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Kushiro",
      "hct": "कुशीरो"
    }, {
      "ac": "KUT",
      "an": "Kopitnari",
      "han": "कोपिटनारी",
      "cn": "Georgia",
      "hcn": "जियोरगिआ",
      "cc": "GE",
      "ct": "Kutaisi",
      "hct": "कूतैसी"
    }, {
      "ac": "YVP",
      "an": "Kuujjuaq",
      "han": "कूज्जुआक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kuujjuaq",
      "hct": "कूज्जुआक़"
    }, {
      "ac": "YGW",
      "an": "Kuujjuaraapik",
      "han": "कूज्जुआरापिक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Kuujjuarapik",
      "hct": "कूज्जुआरेपिक"
    }, {
      "ac": "KAO",
      "an": "Kuusamo",
      "han": "कूसमो",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Kuusamo",
      "hct": "कूसमो"
    }, {
      "ac": "EAL",
      "an": "",
      "han": "",
      "cn": "Marshall Islands",
      "hcn": "मार्शल आयलॅन्ड्स",
      "cc": "MH",
      "ct": "Kwajalein Atoll",
      "hct": "क्वाजलैन अटोल"
    }, {
      "ac": "KWA",
      "an": "Kwajalein",
      "han": "क्वाजलैन",
      "cn": "Marshall Islands",
      "hcn": "मार्शल आयलॅन्ड्स",
      "cc": "MH",
      "ct": "Kwajalein",
      "hct": "क्वाजलैन"
    }, {
      "ac": "KWT",
      "an": "Kwethluk",
      "han": "क्वेठ्लुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kwethluk",
      "hct": "क्वेठ्लुक"
    }, {
      "ac": "KWK",
      "an": "Kwigillingok",
      "han": "क्विगिलींगोक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Kwigillingok",
      "hct": "क्विगिलींगोक"
    }, {
      "ac": "KYP",
      "an": "Kyaukpyu",
      "han": "क्याउक्प्यू",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Kyaukpyu",
      "hct": "क्याउक्प्यू"
    }, {
      "ac": "KYZ",
      "an": "Kyzyl",
      "han": "क्य्ज़ील",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Kyzyl",
      "hct": "क्य्ज़ील"
    }, {
      "ac": "KZO",
      "an": "Kzyl",
      "han": "क्ज़ील",
      "cn": "Orda",
      "hcn": "ओर्डा",
      "cc": "Kazakhstan",
      "ct": "KZ",
      "hct": "क्ज़"
    }, {
      "ac": "LCR",
      "an": "La Chorrera",
      "han": "ला चोर्रेरा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "La Chorrera",
      "hct": "ला चोर्रेरा"
    }, {
      "ac": "LCG",
      "an": "La Coruna",
      "han": "ला कोरुना",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "La Coruna",
      "hct": "ला कोरुना"
    }, {
      "ac": "LSE",
      "an": "La Crosse Municipal",
      "han": "ला क्रोस्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "La Crosse",
      "hct": "ला क्रोस्स"
    }, {
      "ac": "LFR",
      "an": "La Fria",
      "han": "ला फ़्रिआ",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "La Fria",
      "hct": "ला फ़्रिआ"
    }, {
      "ac": "YGL",
      "an": "La Grande Riviere",
      "han": "ला ग्रांडे रिविएरे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "La Grande Riviere",
      "hct": "ला ग्रांडे रिविएरे"
    }, {
      "ac": "LPB",
      "an": "El Alto",
      "han": "एल आल्टो",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "La Paz",
      "hct": "ला पज़"
    }, {
      "ac": "LAP",
      "an": "Aeropuerto Gen Marquez De Leon",
      "han": "एरोपुएर्टो जन मार्क़्ज़ दे लियोन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "La Paz",
      "hct": "ला पज़"
    }, {
      "ac": "LPD",
      "an": "La Pedrera",
      "han": "ला पेद्रेरा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "La Pedrera",
      "hct": "ला पेद्रेरा"
    }, {
      "ac": "IRJ",
      "an": "La Rioja",
      "han": "ला रिओजा",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "La Rioja",
      "hct": "ला रिओजा"
    }, {
      "ac": "LRH",
      "an": "Laleu",
      "han": "लालेउ",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "La Rochelle",
      "hct": "ला रोचेल"
    }, {
      "ac": "LSC",
      "an": "La Florida",
      "han": "ला फ़्लोरिदा",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "La Serena",
      "hct": "ला सरेना"
    }, {
      "ac": "ZLT",
      "an": "La Tabatiere",
      "han": "ला ताबतिएरे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "La Tabatiere",
      "hct": "ला ताबतिएरे"
    }, {
      "ac": "YLQ",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "La Tuque",
      "hct": "ला तुक़"
    }, {
      "ac": "EUN",
      "an": "Hassan I",
      "han": "हसन आई",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Laayoune",
      "hct": "लायौणे"
    }, {
      "ac": "LBS",
      "an": "Labasa",
      "han": "लेबसा",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Lambasa",
      "hct": "लम्बसा"
    }, {
      "ac": "LBJ",
      "an": "Mutiara ",
      "han": "मतिआरा ",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Labuhan Bajo",
      "hct": "लेबुहान बजो"
    }, {
      "ac": "LBU",
      "an": "Labuan",
      "han": "लेबुआन",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Labuan",
      "hct": "लेबुआन"
    }, {
      "ac": "XLB",
      "an": "Lac Brochet",
      "han": "लेक ब्रोचेत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Lac Brochet",
      "hct": "लेक ब्रोचेत"
    }, {
      "ac": "LAE",
      "an": "Nadzab",
      "han": "नड्ज़ाब",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Nadzab",
      "hct": "नड्ज़ाब"
    }, {
      "ac": "LFT",
      "an": "Lafayette Municipal",
      "han": "लाफ़येट म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lafayette",
      "hct": "लाफ़येट"
    }, {
      "ac": "LGQ",
      "an": "Nueva Loja",
      "han": "नुएवा लोजा",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Lago Agrio",
      "hct": "लागो अग्रिओ"
    }, {
      "ac": "LDU",
      "an": "Lahad Datu",
      "han": "लाहड़ दतू",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Lahad Datu",
      "hct": "लाहड़ दतू"
    }, {
      "ac": "LHE",
      "an": "Lahore",
      "han": "लाहोर",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Lahore",
      "hct": "लाहोर"
    }, {
      "ac": "LCH",
      "an": "Lake Charles Municipal",
      "han": "लेक चार्ल्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lake Charles",
      "hct": "लेक चार्ल्स"
    }, {
      "ac": "LMA",
      "an": "Lake Minchumina",
      "han": "लेक मिन्चुमिना",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lake Minchumina",
      "hct": "लेक मिन्चुमिना"
    }, {
      "ac": "LMY",
      "an": "Lake Murray",
      "han": "लेक मुररे",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Lake Murray",
      "hct": "लेक मुररे"
    }, {
      "ac": "LKB",
      "an": "Lakeba Island",
      "han": "लेकबा आयलॅन्ड",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Lakeba Island",
      "hct": "लेकबा आयलॅन्ड"
    }, {
      "ac": "LKL",
      "an": "Banak",
      "han": "बनक",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Banak",
      "hct": "बनक"
    }, {
      "ac": "LMC",
      "an": "Lamacarena",
      "han": "लामाकेअरना",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Lamacarena",
      "hct": "लामाकेअरना"
    }, {
      "ac": "LPM",
      "an": "Lamap",
      "han": "लामाप",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Lamap",
      "hct": "लामाप"
    }, {
      "ac": "LNB",
      "an": "Lamen Bay",
      "han": "लेमेन बे",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Lamen Bay",
      "hct": "लेमेन बे"
    }, {
      "ac": "LFM",
      "an": "",
      "han": "",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Lamerd",
      "hct": "लेमेर्ड"
    }, {
      "ac": "SUF",
      "an": "S Eufemia",
      "han": "एस एउफ़ेमिआ",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Lamezia Terme",
      "hct": "लेमेज़िया टेर्मे"
    }, {
      "ac": "LDN",
      "an": "Lamidanda",
      "han": "लामिदंडा",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Lamidanda",
      "hct": "लामिदंडा"
    }, {
      "ac": "LPT",
      "an": "Lampang",
      "han": "लाम्पंग",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Lampang",
      "hct": "लाम्पंग"
    }, {
      "ac": "LMP",
      "an": "Lampedusa",
      "han": "लाम्पेदुसा",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Lampedusa",
      "hct": "लाम्पेदुसा"
    }, {
      "ac": "LAU",
      "an": "Lamu Manda",
      "han": "लेमू मंदा",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Lamu",
      "hct": "लेमू"
    }, {
      "ac": "LNY",
      "an": "Lanai",
      "han": "लानाई",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lanai City",
      "hct": "लानाई सिटी"
    }, {
      "ac": "LUV",
      "an": "Dumatumbun",
      "han": "दुमतुम्बुन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Langgur",
      "hct": "लंगगूर"
    }, {
      "ac": "YLY",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Langley",
      "hct": "लंगले"
    }, {
      "ac": "LAI",
      "an": "Lannion",
      "han": "लन्नियोन",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Lannion",
      "hct": "लन्नियोन"
    }, {
      "ac": "YLH",
      "an": "Lansdowne House",
      "han": "लान्स्डौने हाऊस",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Lansdowne House",
      "hct": "लान्स्डौने हाऊस"
    }, {
      "ac": "HLA",
      "an": "Lanseria",
      "han": "लान्सेरिआ",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Johannesburg",
      "hct": "जोहान्नेस्बूरग"
    }, {
      "ac": "LAN",
      "an": "Lansing",
      "han": "लानसिंह",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lansing",
      "hct": "लानसिंह"
    }, {
      "ac": "ACE",
      "an": "Lanzarote",
      "han": "लन्ज़रोटे",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Lanzarote",
      "hct": "लन्ज़रोटे"
    }, {
      "ac": "LHW",
      "an": "Lanzhou",
      "han": "लन्झौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Lanzhou",
      "hct": "लन्झ़ै"
    }, {
      "ac": "LAO",
      "an": "Laoag",
      "han": "लावग",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Laoag",
      "hct": "लावग"
    }, {
      "ac": "LRR",
      "an": "Lar",
      "han": "लार",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Lar",
      "hct": "लार"
    }, {
      "ac": "LAR",
      "an": "General Brees Fld",
      "han": "जनरल ब्रीस फ़्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Laramie",
      "hct": "लरामी"
    }, {
      "ac": "LRD",
      "an": "Laredo",
      "han": "लारेदो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Laredo",
      "hct": "लारेदो"
    }, {
      "ac": "LCA",
      "an": "Larnaca",
      "han": "लर्नेका",
      "cn": "Cyprus",
      "hcn": "सीप्रुस",
      "cc": "CY",
      "ct": "Larnaca",
      "hct": "लर्नेका"
    }, {
      "ac": "KLN",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Larsen Bay",
      "hct": "लार्सन बे"
    }, {
      "ac": "LPA",
      "an": "Aeropuerto De Gran Canaria",
      "han": "एरोपुएर्टो दे ग्रन कानारिआ",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Las Palmas",
      "hct": "लस पाल्मास"
    }, {
      "ac": "LSP",
      "an": "Josefa Camejo",
      "han": "जोसेफ़ा केमेजो",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Paraguana",
      "hct": "परगुआना"
    }, {
      "ac": "LSH",
      "an": "Lashio",
      "han": "लशियो",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Lashio",
      "hct": "लशियो"
    }, {
      "ac": "LTK",
      "an": "Bassel Al Assad",
      "han": "बस्सेल अल अस्साद",
      "cn": "Syria",
      "hcn": "सिरिआ",
      "cc": "SY",
      "ct": "Latakia",
      "hct": "लटाकिआ"
    }, {
      "ac": "LBE",
      "an": "Westmorland County",
      "han": "वॅस्टमोरलॅंद कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Latrobe",
      "hct": "लत्रोबे"
    }, {
      "ac": "LST",
      "an": "Launceston",
      "han": "लौन्सेस्तन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Launceston",
      "hct": "लौन्सेस्तन"
    }, {
      "ac": "PIB",
      "an": "Hattiesburg Laurel Regional",
      "han": "हटीस्बूरग लौरेल रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Laurel",
      "hct": "लौरेल"
    }, {
      "ac": "LVO",
      "an": "Laverton",
      "han": "लावर्टन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Laverton ",
      "hct": "लावर्टन "
    }, {
      "ac": "LWY",
      "an": "Lawas",
      "han": "लवस",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Lawas",
      "hct": "लवस"
    }, {
      "ac": "LAW",
      "an": "Lawton Municipal",
      "han": "लव्टन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lawton",
      "hct": "लव्टन"
    }, {
      "ac": "LZC",
      "an": "Lazaro Cardenas",
      "han": "लाज़ारो कार्डेनास",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Lazaro Cardenas",
      "hct": "लाज़ारो कार्डेनास"
    }, {
      "ac": "LEH",
      "an": "Octeville",
      "han": "ओक्टेवील",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Le Havre",
      "hct": "ले हव्रे"
    }, {
      "ac": "LPY",
      "an": "Loudes",
      "han": "लौड्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Le Puy",
      "hct": "ले पुय"
    }, {
      "ac": "LEA",
      "an": "Learmonth",
      "han": "लीर्मोन्थ",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Learmonth",
      "hct": "लीर्मोन्थ"
    }, {
      "ac": "LBA",
      "an": "Leeds Bradford",
      "han": "लीड्स ब्रेड्फ़र्ड",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Leeds",
      "hct": "लीड्स"
    }, {
      "ac": "LGP",
      "an": "Legaspi",
      "han": "ल्गस्पी",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Legaspi",
      "hct": "ल्गस्पी"
    }, {
      "ac": "LER",
      "an": "Leinster",
      "han": "लैन्स्टर",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Leinster ",
      "hct": "लैन्स्टर "
    }, {
      "ac": "LEJ",
      "an": "Leipzig",
      "han": "लीप्ज़िग",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Leipzig Halle",
      "hct": "लीप्ज़िग हॉले"
    }, {
      "ac": "LKN",
      "an": "Leknes",
      "han": "लेक्ण्स",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Leknes",
      "hct": "लेक्ण्स"
    }, {
      "ac": "LEN",
      "an": "Leon",
      "han": "लियोन",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Leon",
      "hct": "लियोन"
    }, {
      "ac": "BJX",
      "an": "Del Bajio",
      "han": "देल बाजिओ",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Leon Guanajuato",
      "hct": "लियोन गुआनजुआतो"
    }, {
      "ac": "LNO",
      "an": "Leonora",
      "han": "लियोनोरा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Leonora",
      "hct": "लियोनोरा"
    }, {
      "ac": "LRS",
      "an": "Leros",
      "han": "लरोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Leros",
      "hct": "लरोस"
    }, {
      "ac": "LDG",
      "an": "",
      "han": "",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Leshukonskoye",
      "hct": "लेशुकोन्स्कोये"
    }, {
      "ac": "YQL",
      "an": "Lethbridge",
      "han": "लेठ्ब्रिज",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "LETHBRIDGE",
      "hct": "लेठ्ब्रिज"
    }, {
      "ac": "LET",
      "an": "Alfredo Vasquez Cobo",
      "han": "आल्फ़्रेदो वस्क़्ज़ कोबो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Leticia",
      "hct": "लेटिसिया"
    }, {
      "ac": "KLL",
      "an": "Levelock",
      "han": "लेवेलोक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Levelock",
      "hct": "लेवेलोक"
    }, {
      "ac": "LWB",
      "an": "Greenbrier Valley",
      "han": "ग्रीन्ब्रिएर वॅली",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lewisburg",
      "hct": "लेवीस्बूरग"
    }, {
      "ac": "LWS",
      "an": "Lewiston Nez Pierce",
      "han": "लेवीस्टन नेज़ पिअर्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lewiston",
      "hct": "लेवीस्टन"
    }, {
      "ac": "LWT",
      "an": "Lewistown Municipal",
      "han": "ल्युइसटाउन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lewistown",
      "hct": "ल्युइसटाउन"
    }, {
      "ac": "LEX",
      "an": "Blue Grass Field",
      "han": "ब्लु ग्रास फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lexington",
      "hct": "लेक्सिंग्टन"
    }, {
      "ac": "LXA",
      "an": "Lhasa",
      "han": "ल्हसा",
      "cn": "Gonggar",
      "hcn": "गोन्गगर",
      "cc": "China",
      "ct": "CN",
      "hct": "क्न"
    }, {
      "ac": "LYG",
      "an": "Lianyungang",
      "han": "लियानयूनगंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Lianyungang",
      "hct": "लियानयूनगंग"
    }, {
      "ac": "LBL",
      "an": "Liberal Municipal",
      "han": "लिबेरल म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Liberal",
      "hct": "लिबेरल"
    }, {
      "ac": "LBV",
      "an": "Libreville",
      "han": "लिब्रेवील",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Libreville",
      "hct": "लिब्रेवील"
    }, {
      "ac": "VXC",
      "an": "Lichinga",
      "han": "लिचींगा",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Lichinga",
      "hct": "लिचींगा"
    }, {
      "ac": "LPX",
      "an": "Liepaja",
      "han": "लीपजा",
      "cn": "Latvia",
      "hcn": "लतविआ",
      "cc": "LV",
      "ct": "Liepaja",
      "hct": "लीपजा"
    }, {
      "ac": "LIF",
      "an": "Lifou",
      "han": "लिफ़ौ",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Lifou",
      "hct": "लिफ़ौ"
    }, {
      "ac": "LNV",
      "an": "Lihir Island",
      "han": "लिहीर आयलॅन्ड",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Lihir Island",
      "hct": "लिहीर आयलॅन्ड"
    }, {
      "ac": "LJG",
      "an": "Lijiang",
      "han": "लिजिआंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Lijiang",
      "hct": "लिजिआंग"
    }, {
      "ac": "LIL",
      "an": "Lesquin",
      "han": "लेस्क़ुइन",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Lille",
      "hct": "लिले"
    }, {
      "ac": "LLW",
      "an": "Lilongwe",
      "han": "लिलोन्ग्वे",
      "cn": "Malawi",
      "hcn": "मालौई",
      "cc": "MW",
      "ct": "Lilongwe",
      "hct": "लिलोन्ग्वे"
    }, {
      "ac": "LIM",
      "an": "Jorge Chavez",
      "han": "जोर्गे चवेज़",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Lima",
      "hct": "लीमा"
    }, {
      "ac": "LMN",
      "an": "Limbang",
      "han": "लीम्बंग",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Limbang",
      "hct": "लीम्बंग"
    }, {
      "ac": "LXS",
      "an": "Limnos",
      "han": "लीम्नोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Limnos",
      "hct": "लीम्नोस"
    }, {
      "ac": "LIG",
      "an": "Bellegarde",
      "han": "बेल्गार्डे",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Limoges",
      "hct": "लीमोगेस"
    }, {
      "ac": "LNJ",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Lincang",
      "hct": "लीन्कंग"
    }, {
      "ac": "LNK",
      "an": "Lincoln Municipal",
      "han": "लिंकन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lincoln",
      "hct": "लिंकन"
    }, {
      "ac": "LDI",
      "an": "Kikwetu",
      "han": "किक्वेटू",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Lindi",
      "hct": "लींदी"
    }, {
      "ac": "LPI",
      "an": "Saab",
      "han": "साब",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Linkoping",
      "hct": "लीन्कोपींग"
    }, {
      "ac": "LYI",
      "an": "Shubuling",
      "han": "शुबुलींग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Linyi",
      "hct": "लीन्यी"
    }, {
      "ac": "LNZ",
      "an": "Hoersching",
      "han": "होयर्स्खींग",
      "cn": "Austria",
      "hcn": "ऑस्ट्रिया",
      "cc": "AT",
      "ct": "Linz",
      "hct": "लीन्ज़"
    }, {
      "ac": "LPK",
      "an": "Lipetsk",
      "han": "लिपेट्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Lipetsk",
      "hct": "लिपेट्स्क"
    }, {
      "ac": "LSY",
      "an": "Lismore",
      "han": "लिसमोरे",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Lismore ",
      "hct": "लिसमोरे "
    }, {
      "ac": "LIT",
      "an": "Little Rock Regional",
      "han": "लित्ल रॉक रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Little Rock",
      "hct": "लित्ल रॉक"
    }, {
      "ac": "LZH",
      "an": "Bailian",
      "han": "बैलिआन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Liuzhou",
      "hct": "लीऊऴौ"
    }, {
      "ac": "LPL",
      "an": "Liverpool",
      "han": "लिवर्पूल",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Liverpool",
      "hct": "लिवर्पूल"
    }, {
      "ac": "LVI",
      "an": "Livingstone",
      "han": "लिवींग्स्टोन",
      "cn": "Zambia",
      "hcn": "ज़ाम्बीया",
      "cc": "ZM",
      "ct": "Livingstone",
      "hct": "लिवींग्स्टोन"
    }, {
      "ac": "LVB",
      "an": "Dos Galpoes",
      "han": "दोस गाल्पोयस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Livramento",
      "hct": "लिव्रामेन्टो"
    }, {
      "ac": "LJU",
      "an": "Brnik",
      "han": "ब्र्निक",
      "cn": "Slovenia",
      "hcn": "स्लोवेनिआ",
      "cc": "SI",
      "ct": "Ljubljana",
      "hct": "ल्जुब्लजन"
    }, {
      "ac": "YLL",
      "an": "Lloydminster",
      "han": "लोय्ड्मिन्स्टर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Lloydminster",
      "hct": "लोय्ड्मिन्स्टर"
    }, {
      "ac": "IRG",
      "an": "Lockhart River",
      "han": "लोखर्ट रिवर",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Lockhart River ",
      "hct": "लोखर्ट रिवर "
    }, {
      "ac": "LCJ",
      "an": "Lublinek",
      "han": "लुब्लीनेक",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Lodz",
      "hct": "लोड्ज़"
    }, {
      "ac": "RJL",
      "an": "",
      "han": "",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Logrono",
      "hct": "लोग्रोनो"
    }, {
      "ac": "LIW",
      "an": "Loikaw",
      "han": "लोईकौ",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Loikaw",
      "hct": "लोईकौ"
    }, {
      "ac": "LOH",
      "an": "Camilo Ponce Enriquez",
      "han": "केमीलो पोन्स एन्रिक़्ज़",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "La Toma (Catamayo)",
      "hct": "ला टोमा (कतमयो)"
    }, {
      "ac": "LKG",
      "an": "Lokichoggio",
      "han": "लोकिचोग्गियो",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Lokichoggio",
      "hct": "लोकिचोग्गियो"
    }, {
      "ac": "LFW",
      "an": "Lome",
      "han": "लोम",
      "cn": "Togo",
      "hcn": "टोगो",
      "cc": "TG",
      "ct": "Lome",
      "hct": "लोम"
    }, {
      "ac": "LDY",
      "an": "Eglinton",
      "han": "एग्लीन्टन",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Londonderry",
      "hct": "लोन्दोन्डर्री"
    }, {
      "ac": "LDB",
      "an": "Londrina",
      "han": "लोन्द्रिना",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Londrina",
      "hct": "लोन्द्रिना"
    }, {
      "ac": "LKH",
      "an": "Long Akah",
      "han": "लोन्ग अकाह",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Long Akah",
      "hct": "लोन्ग अकाह"
    }, {
      "ac": "LBP",
      "an": "Long Banga",
      "han": "लोन्ग बंगा",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Long Banga",
      "hct": "लोन्ग बंगा"
    }, {
      "ac": "LGB",
      "an": "Long Beach Municipal",
      "han": "लोन्ग बीच म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "LONG BEACH",
      "hct": "लोन्ग बीच"
    }, {
      "ac": "LGL",
      "an": "Long Lellang",
      "han": "लोन्ग लेलंग",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Long Datih",
      "hct": "लोन्ग दटिह"
    }, {
      "ac": "ODN",
      "an": "Long Seridan",
      "han": "लोन्ग सेरीदान",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Long Seridan",
      "hct": "लोन्ग सेरीदान"
    }, {
      "ac": "LOD",
      "an": "Longana",
      "han": "लोन्गना",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Longana",
      "hct": "लोन्गना"
    }, {
      "ac": "LRE",
      "an": "Longreach",
      "han": "लोन्ग्रीच",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Longreach ",
      "hct": "लोन्ग्रीच "
    }, {
      "ac": "LCX",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Longyan",
      "hct": "लोन्ग्यान"
    }, {
      "ac": "LYR",
      "an": "Svalbard",
      "han": "स्वल्बर्ड",
      "cn": "Svalbard and Jan Mayen Islands",
      "hcn": "स्वल्बर्ड एण्द जन मयेन आयलॅन्ड्स",
      "cc": "SJ",
      "ct": "Longyearbyen",
      "hct": "लोन्ग्येर्ब्येन"
    }, {
      "ac": "LNE",
      "an": "Lonorore",
      "han": "लोनोरोरे",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Lonorore",
      "hct": "लोनोरोरे"
    }, {
      "ac": "LPS",
      "an": "Lopez Island",
      "han": "लोपेज़ आयलॅन्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lopez",
      "hct": "लोपेज़"
    }, {
      "ac": "LDH",
      "an": "Lord Howe Island",
      "han": "लोर्ड होवे आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Lord Howe Island ",
      "hct": "लोर्ड होवे आयलॅन्ड "
    }, {
      "ac": "LTO",
      "an": "Loreto",
      "han": "लोरेटो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Loreto",
      "hct": "लोरेटो"
    }, {
      "ac": "LRT",
      "an": "Lann Bihoue",
      "han": "लन बिहौए",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Lorient",
      "hct": "लोरीएन्ट"
    }, {
      "ac": "LMM",
      "an": "Federal Los Mochis",
      "han": "फ़ेडेरल लोस मोचीस",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Los Mochis",
      "hct": "लोस मोचीस"
    }, {
      "ac": "LSA",
      "an": "Losuia",
      "han": "लोसुइआ",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Losuia",
      "hct": "लोसुइआ"
    }, {
      "ac": "SDF",
      "an": "Louisville",
      "han": "लुइसविले",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "LOUISVILLE",
      "hct": "लुइसविले"
    }, {
      "ac": "LDE",
      "an": "Tarbes",
      "han": "तार्बेस",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Lourdes Tarbes",
      "hct": "लूर्ड्स तार्बेस"
    }, {
      "ac": "LPQ",
      "an": "Luang Phabang",
      "han": "लुआंग फाबंग",
      "cn": "Laos",
      "hcn": "लाव्स",
      "cc": "LA",
      "ct": "Luang Prabang",
      "hct": "लुआंग प्रबंग"
    }, {
      "ac": "SDD",
      "an": "Lubango",
      "han": "लुबंगो",
      "cn": "Angola",
      "hcn": "अंगोला",
      "cc": "AO",
      "ct": "Lubango",
      "hct": "लुबंगो"
    }, {
      "ac": "LBB",
      "an": "Lubbock Preston Smith",
      "han": "लुब्बोक प्रेस्तन स्मिथ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lubbock",
      "hct": "लुब्बोक"
    }, {
      "ac": "FBM",
      "an": "Lubumbashi",
      "han": "लुबुम्बशी",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Lubumashi",
      "hct": "लुबुमशी"
    }, {
      "ac": "LUD",
      "an": "Luderitz",
      "han": "लुदेरिट्ज़",
      "cn": "Namibia",
      "hcn": "नामीबिया",
      "cc": "NA",
      "ct": "Luderitz",
      "hct": "लुदेरिट्ज़"
    }, {
      "ac": "LBC",
      "an": "Lubeck Blankensee",
      "han": "लुबेक ब्लन्केन्सी",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Luebeck",
      "hct": "लुएबेक"
    }, {
      "ac": "LUG",
      "an": "Agno",
      "han": "अग्नो",
      "cn": "Switzerland",
      "hcn": "स्विट्ज़र‌लैंड‌",
      "cc": "CH",
      "ct": "Lugano",
      "hct": "लुगनो"
    }, {
      "ac": "VSG",
      "an": "Lugansk",
      "han": "लुगन्स्क",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Lugansk",
      "hct": "लुगन्स्क"
    }, {
      "ac": "LUA",
      "an": "Lukla",
      "han": "लुक्ला",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Lukla",
      "hct": "लुक्ला"
    }, {
      "ac": "LLA",
      "an": "Kallax",
      "han": "कललक्स",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Lulea",
      "hct": "लुली"
    }, {
      "ac": "LYA",
      "an": "Luoyang",
      "han": "लुओयंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Luoyang",
      "hct": "लुओयंग"
    }, {
      "ac": "YSG",
      "an": "Lutselk'e",
      "han": "लुत्सेळ्कइ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Lutselk'e",
      "hct": "लुत्सेळ्कइ"
    }, {
      "ac": "LUX",
      "an": "Luxembourg",
      "han": "लुक्सेम्बौर्ग",
      "cn": "Luxembourg",
      "hcn": "लुक्सेम्बौर्ग",
      "cc": "LU",
      "ct": "Luxembourg",
      "hct": "लुक्सेम्बौर्ग"
    }, {
      "ac": "LUM",
      "an": "Mangshi",
      "han": "मंगशी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Luxi",
      "hct": "लुक्सी"
    }, {
      "ac": "LXR",
      "an": "Luxor",
      "han": "लक्सर",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Luxor",
      "hct": "लक्सर"
    }, {
      "ac": "LZO",
      "an": "Luzhou",
      "han": "लुऴौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Luzhou",
      "hct": "लुझ़ै"
    }, {
      "ac": "CRK",
      "an": "Diosdado Macapagal",
      "han": "डियोस्डाडो मेकपगल",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Angeles City",
      "hct": "अंजेल्स सिटी"
    }, {
      "ac": "LWO",
      "an": "Snilow",
      "han": "स्नीलौ",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Lviv",
      "hct": "ल्विव"
    }, {
      "ac": "LYC",
      "an": "Lycksele",
      "han": "ल्य्क्सेले",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Lycksele",
      "hct": "ल्य्क्सेले"
    }, {
      "ac": "LYH",
      "an": "Lynchburg Municipal",
      "han": "लीन्च्बूरग म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Lynchburg",
      "hct": "लीन्च्बूरग"
    }, {
      "ac": "YYL",
      "an": "Lynn Lake",
      "han": "लिन लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Lynn Lake",
      "hct": "लिन लेक"
    }, {
      "ac": "LYS",
      "an": "Lyon Saint Exupery",
      "han": "ल्योन सेंट एक्सुपेरी",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Lyon",
      "hct": "ल्योन"
    }, {
      "ac": "MST",
      "an": "Maastricht Aachen",
      "han": "मास्ट्रिच्ट आचेन",
      "cn": "Netherlands",
      "hcn": "नेद‌र‌लैंड‌",
      "cc": "NL",
      "ct": "Maastricht",
      "hct": "मास्ट्रिच्ट"
    }, {
      "ac": "UBB",
      "an": "Mabuiag Island",
      "han": "माबुइग आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mabuiag Island",
      "hct": "माबुइग आयलॅन्ड"
    }, {
      "ac": "MEA",
      "an": "Maca",
      "han": "मेका",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Maca_",
      "hct": "मेका_"
    }, {
      "ac": "MCP",
      "an": "Macapa",
      "han": "मेकपा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Macapa",
      "hct": "मेकपा"
    }, {
      "ac": "XMS",
      "an": "Coronel E Carvajal",
      "han": "कोरोनेल इ कार्वाजल",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Macas",
      "hct": "मेकास"
    }, {
      "ac": "MCZ",
      "an": "Palmeres",
      "han": "पाल्मेर्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Maceio",
      "hct": "मेसीओ"
    }, {
      "ac": "MKY",
      "an": "Mackay",
      "han": "मॅके",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mackay",
      "hct": "मॅके"
    }, {
      "ac": "MCN",
      "an": "Lewis B Wilson",
      "han": "ल्युइस बी विलसन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Macon",
      "hct": "माकॉन"
    }, {
      "ac": "MAG",
      "an": "Madang",
      "han": "मदंग",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Madang",
      "hct": "मदंग"
    }, {
      "ac": "FNC",
      "an": "Madeira",
      "han": "मदरिया",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Madeira",
      "hct": "मदरिया"
    }, {
      "ac": "MSN",
      "an": "Dane County Regional",
      "han": "दाणे कौंटी रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Madison",
      "hct": "मॅदीसन"
    }, {
      "ac": "HGN",
      "an": "Mae Hong Son",
      "han": "मए होंग सन",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Mae Hong Son",
      "hct": "मए होंग सन"
    }, {
      "ac": "MWF",
      "an": "Naone",
      "han": "नांवे",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Maewo Island",
      "hct": "मएवो आयलॅन्ड"
    }, {
      "ac": "GDX",
      "an": "Magadan",
      "han": "मगदन",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Magadan",
      "hct": "मगदन"
    }, {
      "ac": "MQF",
      "an": "Magnitogorsk",
      "han": "माग्निटोगोर्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Magnetiogorsk",
      "hct": "माग्नेटियोगोर्स्क"
    }, {
      "ac": "MWQ",
      "an": "Magwe",
      "han": "मेग्वे",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Magwe",
      "hct": "मेग्वे"
    }, {
      "ac": "MXT",
      "an": "Maintirano",
      "han": "मायन्टिरनो",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Maintirano",
      "hct": "मायन्टिरनो"
    }, {
      "ac": "MMO",
      "an": "Maio",
      "han": "माईओ",
      "cn": "Cape Verde",
      "hcn": "केप‌ व‌र्द",
      "cc": "CV",
      "ct": "Maio",
      "hct": "माईओ"
    }, {
      "ac": "MJN",
      "an": "Philibert tsiranana ",
      "han": "फीलिबेर्ट ट्सिरानना",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Mahajanga",
      "hct": "महाजंग"
    }, {
      "ac": "MAJ",
      "an": "Amata Kabua",
      "han": "अमाता काबुआ",
      "cn": "Marshall Islands",
      "hcn": "मार्शल आयलॅन्ड्स",
      "cc": "MH",
      "ct": "Majuro",
      "hct": "मजुरो"
    }, {
      "ac": "MQX",
      "an": "Makale",
      "han": "माकले",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Makale",
      "hct": "माकले"
    }, {
      "ac": "MCX",
      "an": "Uytash",
      "han": "उय्टाश",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Makhachkala",
      "hct": "मखचकला"
    }, {
      "ac": "YMN",
      "an": "Makkovik",
      "han": "मक्कोवीक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Makkovik",
      "hct": "मक्कोवीक"
    }, {
      "ac": "MKU",
      "an": "Makokou",
      "han": "मकोकौ",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Makokou",
      "hct": "मकोकौ"
    }, {
      "ac": "MZG",
      "an": "Magong",
      "han": "मागोन्ग",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Makung",
      "hct": "माकुंग"
    }, {
      "ac": "AAM",
      "an": "Malamala",
      "han": "मलमअला",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Malamala",
      "hct": "मलमअला"
    }, {
      "ac": "SSG",
      "an": "Santa Isabel",
      "han": "शांता इसाबॅल",
      "cn": "Equatorial Guinea",
      "hcn": "एक़ुटोरिल गुइनी",
      "cc": "GQ",
      "ct": "Malabo",
      "hct": "मलबो"
    }, {
      "ac": "AGP",
      "an": "Malaga",
      "han": "मालगा",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Malaga",
      "hct": "मालगा"
    }, {
      "ac": "MAK",
      "an": "Malakal",
      "han": "मालकल",
      "cn": "Sudan",
      "hcn": "सुदान",
      "cc": "SD",
      "ct": "Malakal",
      "hct": "मालकल"
    }, {
      "ac": "MLG",
      "an": "Abdul Rachman Saleh",
      "han": "अब्दुल रच्मॅन सालेह",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Malang",
      "hct": "मालंग"
    }, {
      "ac": "MLX",
      "an": "Erhac",
      "han": "एर्हक",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Malatya",
      "hct": "मालाट्या"
    }, {
      "ac": "MYD",
      "an": "Malindi",
      "han": "मालींदी",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Malindi",
      "hct": "मालींदी"
    }, {
      "ac": "MMA",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Malmo",
      "hct": "माल्मो"
    }, {
      "ac": "MMX",
      "an": "Sturup",
      "han": "स्तरुप",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Malmo",
      "hct": "माल्मो"
    }, {
      "ac": "PTF",
      "an": "Malololailai",
      "han": "मलोलोलैलै",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Malololailai",
      "hct": "मलोलोलैलै"
    }, {
      "ac": "MLA",
      "an": "Luqa",
      "han": "लुक़",
      "cn": "Malta",
      "hcn": "मल्ता",
      "cc": "MT",
      "ct": "Malta",
      "hct": "मल्ता"
    }, {
      "ac": "MNF",
      "an": "Mana Island",
      "han": "मन आयलॅन्ड",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Mana Island",
      "hct": "मन आयलॅन्ड"
    }, {
      "ac": "MDC",
      "an": "Samratulang",
      "han": "सम्रतुलंग",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Manado",
      "hct": "मनडो"
    }, {
      "ac": "MNJ",
      "an": "Mananjary",
      "han": "मनन्जारी",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Mananjary",
      "hct": "मनन्जारी"
    }, {
      "ac": "MAO",
      "an": "Intl Eduardo Gomes",
      "han": "इंट्ल एडुआर्डो गोम्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Manaus",
      "hct": "मनौस"
    }, {
      "ac": "MDL",
      "an": "Mandalay",
      "han": "मॅन्डले",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Mandalay",
      "hct": "मॅन्डले"
    }, {
      "ac": "MGS",
      "an": "Mangaia Island",
      "han": "मंगया आयलॅन्ड",
      "cn": "Cook Islands",
      "hcn": "कूक आयलॅन्ड्स",
      "cc": "CK",
      "ct": "Mangaia Island",
      "hct": "मंगया आयलॅन्ड"
    }, {
      "ac": "MHK",
      "an": "Manhattan Municipal",
      "han": "मॅनहॅटन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Manhattan",
      "hct": "मॅनहॅटन"
    }, {
      "ac": "MNG",
      "an": "Maningrida",
      "han": "मणींग्रिदा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Maningrida ",
      "hct": "मणींग्रिदा "
    }, {
      "ac": "MBL",
      "an": "Blacker",
      "han": "ब्लॅकेर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Manistee",
      "hct": "मणीस्टी"
    }, {
      "ac": "MZL",
      "an": "Santaguida",
      "han": "संतेगुइदा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Manizales",
      "hct": "मणीज़ाल्स"
    }, {
      "ac": "MJA",
      "an": "Manja",
      "han": "मन्जा",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Manja",
      "hct": "मन्जा"
    }, {
      "ac": "MLY",
      "an": "Manley Hot Spring",
      "han": "मनले हॉट स्प्रिंग",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Manley Hot Spring",
      "hct": "मनले हॉट स्प्रिंग"
    }, {
      "ac": "MHG",
      "an": "Mannheim City",
      "han": "मान्न्हेइम सिटी",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Mannheim",
      "hct": "मान्न्हेइम"
    }, {
      "ac": "KMO",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Manokotak",
      "hct": "मानोकोटक"
    }, {
      "ac": "MKW",
      "an": "Rendani",
      "han": "रेंडाणी",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Manokwari",
      "hct": "मनोकवरी"
    }, {
      "ac": "MEC",
      "an": "Eloy Alfaro",
      "han": "एलोय अल्फ़रो",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Manta",
      "hct": "मंता"
    }, {
      "ac": "MAS",
      "an": "Momote",
      "han": "मोमोटे",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Manus Island",
      "hct": "मानुस आयलॅन्ड"
    }, {
      "ac": "ZLO",
      "an": "Manzanillo",
      "han": "मन्ज़ाणीलो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Manzanillo",
      "hct": "मन्ज़ाणीलो"
    }, {
      "ac": "MTS",
      "an": "Matsapha",
      "han": "मट्सफा",
      "cn": "Swaziland",
      "hcn": "स्वाज़िलांद",
      "cc": "SZ",
      "ct": "Manzini",
      "hct": "मन्ज़िनी"
    }, {
      "ac": "MPM",
      "an": "Maputo",
      "han": "मपुटो",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Maputo",
      "hct": "मपुटो"
    }, {
      "ac": "MDQ",
      "an": "Mar Del Plata",
      "han": "मार देल पलता",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Mar Del Plata",
      "hct": "मार देल पलता"
    }, {
      "ac": "MRE",
      "an": "Mara Lodges",
      "han": "मर लोद्गेस",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Mara Lodges",
      "hct": "मर लोद्गेस"
    }, {
      "ac": "MAB",
      "an": "Maraba",
      "han": "मरब",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Maraba",
      "hct": "मरब"
    }, {
      "ac": "MAR",
      "an": "La Chinita",
      "han": "ला चीनिता",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Maracaibo",
      "hct": "मरकाईबो"
    }, {
      "ac": "RUS",
      "an": "Marau",
      "han": "मराऊ",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Marau",
      "hct": "मराऊ"
    }, {
      "ac": "MQM",
      "an": "",
      "han": "",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Mardin",
      "hct": "मारदीन"
    }, {
      "ac": "MEE",
      "an": "Mare",
      "han": "मारे",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Mare",
      "hct": "मारे"
    }, {
      "ac": "MGH",
      "an": "Margate",
      "han": "मार्गेटे",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Margate",
      "hct": "मार्गेटे"
    }, {
      "ac": "MHQ",
      "an": "Mariehamn",
      "han": "मारीएहेम्न",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Mariehamn",
      "hct": "मारीएहेम्न"
    }, {
      "ac": "MGE",
      "an": "Dobbins Arb",
      "han": "दोब्बीन्स आर्ब",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Marietta",
      "hct": "मारीएता"
    }, {
      "ac": "PKB",
      "an": "Wood County",
      "han": "वुड कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Marietta Parkersburg",
      "hct": "मारीएता पार्केर्स्बूरग"
    }, {
      "ac": "MII",
      "an": "Dr Gasto Vidigal",
      "han": "ड्र गास्टो वीडीगल",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Marilia",
      "hct": "मारिलिआ"
    }, {
      "ac": "MGF",
      "an": "Regional De Maringa Silvio Name Junior",
      "han": "रिजनल दे मरिंगा सिल्वियो नामे जूनियर",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Maringa",
      "hct": "मरिंगा"
    }, {
      "ac": "MWA",
      "an": "Williamson County",
      "han": "विलियमसन कौंटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Marion",
      "hct": "मॅरिअन"
    }, {
      "ac": "MPW",
      "an": "Mariupol",
      "han": "मारिउपोल",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Mariupol",
      "hct": "मारिउपोल"
    }, {
      "ac": "WMN",
      "an": "Maroantsetra",
      "han": "मरोआंट्सेतरा",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Maroantsetra",
      "hct": "मरोआंट्सेतरा"
    }, {
      "ac": "MQT",
      "an": "Marquette County",
      "han": "मार्क़ॅट कौंटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Marquette",
      "hct": "मार्क़ॅट"
    }, {
      "ac": "RAK",
      "an": "Menara",
      "han": "मेनार",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Marrakech",
      "hct": "मेर्राकेच"
    }, {
      "ac": "RMF",
      "an": "Marsa Alam",
      "han": "मार्सा आलम",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Marsa Alam",
      "hct": "मार्सा आलम"
    }, {
      "ac": "MRS",
      "an": "Marseille Provence",
      "han": "मार्सीले प्रोवेन्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Marseille",
      "hct": "मार्सीले"
    }, {
      "ac": "MLL",
      "an": "Marshall Fortuna",
      "han": "मार्शल फ़ोर्तुना",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Marshall Fortuna",
      "hct": "मार्शल फ़ोर्तुना"
    }, {
      "ac": "MVY",
      "an": "Dukes County",
      "han": "दुकेस कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Marthas Vineyard",
      "hct": "मार्थस विनेयर्ड"
    }, {
      "ac": "MUR",
      "an": "Marudi",
      "han": "मारूडी",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Marudi",
      "hct": "मारूडी"
    }, {
      "ac": "YMH",
      "an": "Mary's Harbour",
      "han": "मॅरीएस हर्बौर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Mary's Harbour",
      "hct": "मॅरीएस हर्बौर"
    }, {
      "ac": "MBT",
      "an": "Masbate",
      "han": "मास्बटे",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Masbate",
      "hct": "मास्बटे"
    }, {
      "ac": "MSU",
      "an": "Moshoeshoe",
      "han": "मोशोयशोय",
      "cn": "Lesotho",
      "hcn": "लेसोठो",
      "cc": "LS",
      "ct": "Maseru",
      "hct": "मेसेरू"
    }, {
      "ac": "MHD",
      "an": "Mashhad",
      "han": "मश्हद",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Mashhad",
      "hct": "मश्हद"
    }, {
      "ac": "MCW",
      "an": "Mason City",
      "han": "मेसन सिटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mason City",
      "hct": "मेसन सिटी"
    }, {
      "ac": "MSW",
      "an": "Massawa",
      "han": "मास्सवा",
      "cn": "Eritrea",
      "hcn": "एरित्रे",
      "cc": "ER",
      "ct": "Massawa",
      "hct": "मास्सवा"
    }, {
      "ac": "MSS",
      "an": "Richards Field",
      "han": "रिचर्ड्स फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Massena",
      "hct": "मास्सेना"
    }, {
      "ac": "ZMT",
      "an": "Masset",
      "han": "मास्सेत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Masset",
      "hct": "मास्सेत"
    }, {
      "ac": "MVZ",
      "an": "Masvingo",
      "han": "मेस्वींगो",
      "cn": "Zimbabwe",
      "hcn": "ज़िम्बाब्वे",
      "cc": "ZW",
      "ct": "Masvingo",
      "hct": "मेस्वींगो"
    }, {
      "ac": "MAM",
      "an": "Servando Canales",
      "han": "सर्वंदो कानाल्स",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Matamoros",
      "hct": "मतमोरोस"
    }, {
      "ac": "AMI",
      "an": "Selaparang",
      "han": "सेलापरंग",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Mataram",
      "hct": "माताराम"
    }, {
      "ac": "MFK",
      "an": "Matsu Beigan",
      "han": "मट्सू बेगन",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Matsu Islands",
      "hct": "मट्सू आयलॅन्ड्स"
    }, {
      "ac": "MYJ",
      "an": "Matsuyama",
      "han": "मत्सुयामा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Matsuyama",
      "hct": "मत्सुयामा"
    }, {
      "ac": "MUN",
      "an": "Quiriquire",
      "han": "क़ुइरिक़िरे",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Maturin",
      "hct": "मतूरिन"
    }, {
      "ac": "MUK",
      "an": "Mauke",
      "han": "मऊके",
      "cn": "Cook Islands",
      "hcn": "कूक आयलॅन्ड्स",
      "cc": "CK",
      "ct": "Mauke Island",
      "hct": "मऊके आयलॅन्ड"
    }, {
      "ac": "MOF",
      "an": "Wai Oti",
      "han": "वै ओती",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Maumere",
      "hct": "मौमेरे"
    }, {
      "ac": "MZR",
      "an": "Mazar I Sharif",
      "han": "मज़र आई शरीफ़",
      "cn": "Afghanistan",
      "hcn": "अफ़ग़ानिस्तान‌",
      "cc": "AF",
      "ct": "Mazar",
      "hct": "मज़र"
    }, {
      "ac": "MZT",
      "an": "Buelina",
      "han": "बुएलीना",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Mazatlan",
      "hct": "मज़त्लन"
    }, {
      "ac": "MBU",
      "an": "Babanakira",
      "han": "बबनकिरा",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Mbambanakira",
      "hct": "म्बाम्बनकिरा"
    }, {
      "ac": "MDK",
      "an": "Mbandaka",
      "han": "म्बन्दका",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Mbandaka",
      "hct": "म्बन्दका"
    }, {
      "ac": "MJM",
      "an": "Mbuji Mayi",
      "han": "म्बुजी मायी",
      "cn": "Congo (Kinshasa)",
      "hcn": "कांगो (किंशासा)",
      "cc": "CG",
      "ct": "Mbuji",
      "hct": "म्बुजी"
    }, {
      "ac": "MFE",
      "an": "Miller",
      "han": "मीलर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mc Allen Mission",
      "hct": "म्क अलेन मिशन"
    }, {
      "ac": "MCV",
      "an": "McArthur River Mine",
      "han": "म्कार्ठूर रिवर माईन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mcarthur River ",
      "hct": "म्कार्ठूर रिवर "
    }, {
      "ac": "MYL",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "McCall",
      "hct": "म्क्काल"
    }, {
      "ac": "MCK",
      "an": "Mccook",
      "han": "म्क्कूक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mccook",
      "hct": "म्क्कूक"
    }, {
      "ac": "MCG",
      "an": "McGrath",
      "han": "म्क्गरथ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mcgrath",
      "hct": "म्क्गरथ"
    }, {
      "ac": "MES",
      "an": "Polonia",
      "han": "पोलोनिआ",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Medan",
      "hct": "मेदान"
    }, {
      "ac": "EOH",
      "an": "Enrique Olaya Herrara",
      "han": "एन्रिक़ ओलय हेर्रेरा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Medellin",
      "hct": "मडेलीन"
    }, {
      "ac": "MDE",
      "an": "Jose Marie Cordova",
      "han": "जोस मॅरी कोर्डोवा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Medellin",
      "hct": "मडेलीन"
    }, {
      "ac": "MFR",
      "an": "Medford Jackson Cty",
      "han": "मेडफोर्ड जैकसन क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Medford",
      "hct": "मेडफोर्ड"
    }, {
      "ac": "YXH",
      "an": "Medicine Hat",
      "han": "मेडिसिन हट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "MEDICINE HAT",
      "hct": "मेडिसिन हट"
    }, {
      "ac": "MKR",
      "an": "Meekatharra",
      "han": "मीकठाररा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Meekatharra ",
      "hct": "मीकठाररा "
    }, {
      "ac": "MEY",
      "an": "Meghauli",
      "han": "मेघौली",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Meghauli",
      "hct": "मेघौली"
    }, {
      "ac": "MEH",
      "an": "Mehamn",
      "han": "मेहेम्न",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Mehamn",
      "hct": "मेहेम्न"
    }, {
      "ac": "MXZ",
      "an": "Meixian",
      "han": "मीक्सियान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Meixian",
      "hct": "मीक्सियान"
    }, {
      "ac": "MYU",
      "an": "Mekoryuk",
      "han": "मेकोर्युक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mekoryuk",
      "hct": "मेकोर्युक"
    }, {
      "ac": "MLN",
      "an": "Melilla",
      "han": "मेलिला",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Melilla",
      "hct": "मेलिला"
    }, {
      "ac": "MMB",
      "an": "Memanbetsu",
      "han": "मेमान्बेट्सू",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Memanbetsu",
      "hct": "मेमान्बेट्सू"
    }, {
      "ac": "FMM",
      "an": "Memmingen Allgau",
      "han": "मेम्मिंगेन अल्गऊ",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Memmingen Allgau",
      "hct": "मेम्मिंगेन अल्गऊ"
    }, {
      "ac": "MEM",
      "an": "Memphis",
      "han": "मेंफीस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Memphis",
      "hct": "मेंफीस"
    }, {
      "ac": "MDU",
      "an": "Mendi",
      "han": "मेंदी",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Mendi",
      "hct": "मेंदी"
    }, {
      "ac": "MDZ",
      "an": "El Plumerillo",
      "han": "एल प्लमेरिलो",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Mendoza",
      "hct": "मेंदोज़ा"
    }, {
      "ac": "MAH",
      "an": "Aerop De Menorca",
      "han": "एरोप दे मेनोर्का",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Menorca",
      "hct": "मेनोर्का"
    }, {
      "ac": "MKQ",
      "an": "Mopah",
      "han": "मोपाह",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Merauke",
      "hct": "मेरऊके"
    }, {
      "ac": "MCE",
      "an": "Merced Municipal",
      "han": "मेर्केड म्यूनिसिपल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Merced",
      "hct": "मेर्केड"
    }, {
      "ac": "MID",
      "an": "Merida",
      "han": "मेरिदा",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Merida",
      "hct": "मेरिदा"
    }, {
      "ac": "MRD",
      "an": "Alberto Carnevalli",
      "han": "अलबर्टो कार्णेवाली",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Merida",
      "hct": "मेरिदा"
    }, {
      "ac": "MEI",
      "an": "Key Field",
      "han": "के फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Meridian",
      "hct": "मेरिदिन"
    }, {
      "ac": "MIM",
      "an": "Merimbula",
      "han": "मेरिम्बुला",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Merimbula ",
      "hct": "मेरिम्बुला "
    }, {
      "ac": "AZA",
      "an": "Phoenix",
      "han": "फोयनिक्स",
      "cn": "Mesa Gateway",
      "hcn": "मेसा गेटवे",
      "cc": "United States",
      "ct": "US",
      "hct": "उस"
    }, {
      "ac": "MTM",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Metlakatla",
      "hct": "मेट्लकत्ला"
    }, {
      "ac": "ETZ",
      "an": "Metz Nancy Lorraine",
      "han": "मेट्ज़ नॅन्सी लोरेन",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Metz Nancy",
      "hct": "मेट्ज़ नॅन्सी"
    }, {
      "ac": "MXL",
      "an": "Rodolfg Sachez Taboada",
      "han": "रोडोल्फ़्ग सचेज़ ताबोआदा",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Mexicali",
      "hct": "मेक्सिकालि"
    }, {
      "ac": "WMK",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Meyers Chuck",
      "hct": "मीर्ज़ चुक"
    }, {
      "ac": "MIG",
      "an": "Mianyang",
      "han": "मियांयांग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Mianyang",
      "hct": "मियांयांग"
    }, {
      "ac": "MAF",
      "an": "Midland",
      "han": "मिडलॅंद",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Midland",
      "hct": "मिडलॅंद"
    }, {
      "ac": "JMK",
      "an": "Mykonos ग्रीस",
      "han": "मैकोनोस ग्रीस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Mikonos",
      "hct": "मिकोनोस"
    }, {
      "ac": "MQL",
      "an": "Mildura",
      "han": "मील्डुरा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mildura",
      "hct": "मील्डुरा"
    }, {
      "ac": "MLS",
      "an": "Miles City Municipal",
      "han": "मील्स सिटी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Miles City",
      "hct": "मील्स सिटी"
    }, {
      "ac": "MGT",
      "an": "Milingimbi",
      "han": "मीलीन्गिम्बी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Milingimbi ",
      "hct": "मीलीन्गिम्बी "
    }, {
      "ac": "MLO",
      "an": "Milos",
      "han": "मीलोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Milos",
      "hct": "मीलोस"
    }, {
      "ac": "KYN",
      "an": "Milton Keynes",
      "han": "मिल्टन कीन्स",
      "cn": "United Kingdom ",
      "hcn": "युनाईटेड किंग्डोम ",
      "cc": "GB",
      "ct": "Milton Keynes ",
      "hct": "मिल्टन कीन्स "
    }, {
      "ac": "MKE",
      "an": "General Mitchell",
      "han": "जनरल मीच्चेल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Milwaukee",
      "hct": "मील्वऊकी"
    }, {
      "ac": "MTT",
      "an": "Minatitlan",
      "han": "मिनतिट्लन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Minatitlan",
      "hct": "मिनतिट्लन"
    }, {
      "ac": "MRV",
      "an": "Mineralnyye Vody",
      "han": "मिनरल्न्ये वोडी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Mineralnye Vody",
      "hct": "मिनरल्न्ये वोडी"
    }, {
      "ac": "MOT",
      "an": "Minot",
      "han": "मिनोट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Minot",
      "hct": "मिनोट"
    }, {
      "ac": "MHP",
      "an": "Minsk ",
      "han": "मिन्स्क ",
      "cn": "Belarus",
      "hcn": "बेलारुस",
      "cc": "BY",
      "ct": "Minsk",
      "hct": "मिन्स्क"
    }, {
      "ac": "MSQ",
      "an": "Minsk ",
      "han": "मिन्स्क ",
      "cn": "Belarus",
      "hcn": "बेलारुस",
      "cc": "BY",
      "ct": "Minsk",
      "hct": "मिन्स्क"
    }, {
      "ac": "MNT",
      "an": "Minto",
      "han": "मिन्टो",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Minto",
      "hct": "मिन्टो"
    }, {
      "ac": "MYY",
      "an": "Miri",
      "han": "मीरी",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Miri",
      "hct": "मीरी"
    }, {
      "ac": "MJZ",
      "an": "Mirny",
      "han": "मीर्नी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Mirnyj",
      "hct": "मीर्न्य्ज"
    }, {
      "ac": "MSJ",
      "an": "Misawa",
      "han": "मीसवा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Misawa",
      "hct": "मीसवा"
    }, {
      "ac": "MIS",
      "an": "Misima Island",
      "han": "मीसिमा आयलॅन्ड",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Misima Island",
      "hct": "मीसिमा आयलॅन्ड"
    }, {
      "ac": "MSO",
      "an": "Missoula",
      "han": "मीस्सौला",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Missoula",
      "hct": "मीस्सौला"
    }, {
      "ac": "MRA",
      "an": "Misratah",
      "han": "मिश्राताह",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Misratah",
      "hct": "मिश्राताह"
    }, {
      "ac": "MOI",
      "an": "Mitiaro Island",
      "han": "मितीआरो आयलॅन्ड",
      "cn": "Cook Islands",
      "hcn": "कूक आयलॅन्ड्स",
      "cc": "CK",
      "ct": "Mitiaro Island",
      "hct": "मितीआरो आयलॅन्ड"
    }, {
      "ac": "MVP",
      "an": "Fabio Alberto Leon Bentley",
      "han": "फ़ाबियो अलबर्टो लियोन बेंटले",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Mitu",
      "hct": "मीटू"
    }, {
      "ac": "MYE",
      "an": "Miyake Jima",
      "han": "मियाके जिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Miyake Jima",
      "hct": "मियाके जिमा"
    }, {
      "ac": "MMY",
      "an": "Miyako",
      "han": "मियाको",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Miyako",
      "hct": "मियाको"
    }, {
      "ac": "KMI",
      "an": "Miyazaki",
      "han": "मियाज़की",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Miyazaki",
      "hct": "मियाज़की"
    }, {
      "ac": "MBD",
      "an": "Mmabatho",
      "han": "म्माबथो",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Mafeking",
      "hct": "मफ़ेकिंग"
    }, {
      "ac": "MQN",
      "an": "Mo I Rana",
      "han": "मो आई राणा",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Mo i Rana",
      "hct": "मो आई राणा"
    }, {
      "ac": "CNY",
      "an": "Canyonlands Field",
      "han": "कन्योन्लांड्स फ़ील्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Moab",
      "hct": "मोआब"
    }, {
      "ac": "MFJ",
      "an": "Moala",
      "han": "मोआला",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Moala",
      "hct": "मोआला"
    }, {
      "ac": "MOB",
      "an": "Mobile Municipal",
      "han": "मोबाईल म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mobile",
      "hct": "मोबाईल"
    }, {
      "ac": "MOD",
      "an": "Harry Sham Fld",
      "han": "हॅरी शाम फ़्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Modesto",
      "hct": "मोडेस्टो"
    }, {
      "ac": "NWA",
      "an": "Bandaressalam",
      "han": "बन्दरेस्सआलम",
      "cn": "Comoros",
      "hcn": "कोमोरोस",
      "cc": "KM",
      "ct": "Moheli",
      "hct": "मोहेली"
    }, {
      "ac": "MJD",
      "an": "Moenjodaro",
      "han": "मोयन्जोडारो",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Moenjodaro",
      "hct": "मोयन्जोडारो"
    }, {
      "ac": "MOL",
      "an": "Aro",
      "han": "आरो",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Molde",
      "hct": "मोलदे"
    }, {
      "ac": "MLI",
      "an": "Quad City",
      "han": "क़द सिटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Moline",
      "hct": "मोलीन"
    }, {
      "ac": "MBA",
      "an": "Moi",
      "han": "मोई",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Mombasa",
      "hct": "मोमबसा"
    }, {
      "ac": "MIR",
      "an": "Habib Bourguiba",
      "han": "हबीब बौर्गुइबा",
      "cn": "Tunisia",
      "hcn": "ट्यूनीशिया",
      "cc": "TN",
      "ct": "Monastir",
      "hct": "मोनस्तिर"
    }, {
      "ac": "MBE",
      "an": "Monbetsu",
      "han": "मोन्बेट्सू",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Monbetsu",
      "hct": "मोन्बेट्सू"
    }, {
      "ac": "LOV",
      "an": "Monclova",
      "han": "मोन्क्लोवा",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Monclova",
      "hct": "मोन्क्लोवा"
    }, {
      "ac": "YQM",
      "an": "Moncton Municipal",
      "han": "मोन्क्टन म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Moncton",
      "hct": "मोन्क्टन"
    }, {
      "ac": "MOG",
      "an": "Mong Hsat",
      "han": "मोंग ह्सट",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Mong Hsat",
      "hct": "मोंग ह्सट"
    }, {
      "ac": "MJK",
      "an": "Shark Bay",
      "han": "शर्क बे",
      "cn": "Australia ",
      "hcn": "औस्ट्रालिया ",
      "cc": "AU",
      "ct": "Monkey Mia ",
      "hct": "मोनके मिआ "
    }, {
      "ac": "MNY",
      "an": "Mono",
      "han": "मोनो",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Stirling Island",
      "hct": "स्तिर्लींग आयलॅन्ड"
    }, {
      "ac": "MLU",
      "an": "Monroe Regional",
      "han": "मोनरो रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "MONROE",
      "hct": "मोनरो"
    }, {
      "ac": "MLW",
      "an": "Monrovia Spriggs Payne",
      "han": "मोन्रोविआ स्प्रिग्ग्स पैन",
      "cn": "Liberia",
      "hcn": "लिबेरिआ",
      "cc": "LR",
      "ct": "Monrovia",
      "hct": "मोन्रोविआ"
    }, {
      "ac": "ROB",
      "an": "Roberts",
      "han": "रॉबर्ट्स",
      "cn": "Liberia",
      "hcn": "लिबेरिआ",
      "cc": "LR",
      "ct": "Monrovia",
      "hct": "मोन्रोविआ"
    }, {
      "ac": "YYY",
      "an": "Mont Joli",
      "han": "मोन्त जौली",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Mont Joli",
      "hct": "मोन्त जौली"
    }, {
      "ac": "YTM",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Mont Tremblant",
      "hct": "मोन्त ट्रेम्ब्लंट"
    }, {
      "ac": "MBJ",
      "an": "Sangster",
      "han": "संग्स्टर",
      "cn": "Jamaica",
      "hcn": "जमाईका",
      "cc": "JM",
      "ct": "Montego Bay",
      "hct": "मोन्टेगो बे"
    }, {
      "ac": "MTR",
      "an": "Los Garzones",
      "han": "लोस गर्ज़ोन्स",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Monteria",
      "hct": "मॉन्टेरिआ"
    }, {
      "ac": "MTY",
      "an": "Escobedo",
      "han": "एस्कोबेदो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Monterrey",
      "hct": "मोन्टेर्रे"
    }, {
      "ac": "MOC",
      "an": "Mario Ribeiro",
      "han": "मॅरिओ रिबेरो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Montes Claros",
      "hct": "मोन्ट्स क्लारोस"
    }, {
      "ac": "MVD",
      "an": "Carrasco",
      "han": "कॅर्रेस्को",
      "cn": "Uruguay",
      "hcn": "उरुग्वाय",
      "cc": "UY",
      "ct": "Montevideo",
      "hct": "मॉन्टेविडिओ"
    }, {
      "ac": "MGM",
      "an": "Dannelly Field",
      "han": "दाननेल्ली फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Montgomery",
      "hct": "मॉन्ट्गोमेरी"
    }, {
      "ac": "MPL",
      "an": "Frejorgues",
      "han": "फ़्रेजोर्ग्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Montpellier",
      "hct": "मोन्ट्पेलिएर"
    }, {
      "ac": "MTJ",
      "an": "Montrose County",
      "han": "मोन्ट्रोस कौंटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Montrose",
      "hct": "मोन्ट्रोस"
    }, {
      "ac": "MNI",
      "an": "Geralds",
      "han": "गेराल्ड्स",
      "cn": "Montserrat",
      "hcn": "मोन्ट्स्र्रत",
      "cc": "MS",
      "ct": "Geralds",
      "hct": "गेराल्ड्स"
    }, {
      "ac": "YMO",
      "an": "Moosonee",
      "han": "मूसोनी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Moosonee",
      "hct": "मूसोनी"
    }, {
      "ac": "MZI",
      "an": "Ambodedjo",
      "han": "अम्बोडेड्जो",
      "cn": "Mali",
      "hcn": "माली",
      "cc": "ML",
      "ct": "Mopti",
      "hct": "मोप्टी"
    }, {
      "ac": "MXX",
      "an": "Mora",
      "han": "मोरा",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Mora",
      "hct": "मोरा"
    }, {
      "ac": "TVA",
      "an": "Morafenobe",
      "han": "मोरफ़ेनोबे",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Morafenobe",
      "hct": "मोरफ़ेनोबे"
    }, {
      "ac": "MOV",
      "an": "Moranbah",
      "han": "मोरन्बाह",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Moranbah ",
      "hct": "मोरन्बाह "
    }, {
      "ac": "MRZ",
      "an": "Moree",
      "han": "मोरी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Moree ",
      "hct": "मोरी "
    }, {
      "ac": "MLM",
      "an": "Michoacan Municipal",
      "han": "मिचोआकन म्यूनिसिपल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Morelia",
      "hct": "मोरलिआ"
    }, {
      "ac": "MGW",
      "an": "Morgantown Municipal",
      "han": "मुरगनटाउन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Morgantown",
      "hct": "मुरगनटाउन"
    }, {
      "ac": "ONG",
      "an": "Mornington",
      "han": "मोर्निंग्टन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mornington",
      "hct": "मोर्निंग्टन"
    }, {
      "ac": "MXH",
      "an": "Moro",
      "han": "मोरो",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Moro",
      "hct": "मोरो"
    }, {
      "ac": "MXM",
      "an": "Morombe",
      "han": "मोरोंबे",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Morombe",
      "hct": "मोरोंबे"
    }, {
      "ac": "MOQ",
      "an": "Morondava",
      "han": "मोरोन्डव",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Morondava",
      "hct": "मोरोन्डव"
    }, {
      "ac": "HAH",
      "an": "Prince Said Ibrahim In",
      "han": "प्रिंस सईद ईब्राहीम इन",
      "cn": "Comoros",
      "hcn": "कोमोरोस",
      "cc": "KM",
      "ct": "Moroni",
      "hct": "मोरोनी"
    }, {
      "ac": "YVA",
      "an": "Iconi",
      "han": "इकोनी",
      "cn": "Comoros",
      "hcn": "कोमोरोस",
      "cc": "KM",
      "ct": "Moroni",
      "hct": "मोरोनी"
    }, {
      "ac": "MYA",
      "an": "Moruya",
      "han": "मोरुया",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Moruya ",
      "hct": "मोरुया "
    }, {
      "ac": "KMY",
      "an": "Moser Bay",
      "han": "मोसेर बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Moser Bay",
      "hct": "मोसेर बे"
    }, {
      "ac": "MJF",
      "an": "Kjaerstad",
      "han": "क्जर्स्ताद",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Mosjoen",
      "hct": "मोस्जोयन"
    }, {
      "ac": "OMO",
      "an": "Mostar",
      "han": "मोस्टार",
      "cn": "Bosnia and Herzegovina",
      "hcn": "बोसनिआ एण्द हेर्ज़ेगोवीना",
      "cc": "BA",
      "ct": "Mostar",
      "hct": "मोस्टार"
    }, {
      "ac": "MTV",
      "an": "Mota Lava",
      "han": "मोटा लावा",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Ablow",
      "hct": "अब्लौ"
    }, {
      "ac": "MJL",
      "an": "Mouilla Ville",
      "han": "मौइला विले",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Mouila",
      "hct": "मौइला"
    }, {
      "ac": "MNU",
      "an": "Mawlamyine",
      "han": "माव्लेम्यिन",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Mawlamyine",
      "hct": "माव्लेम्यिन"
    }, {
      "ac": "MGB",
      "an": "Mount Gambier",
      "han": "माऊंट गम्बिएर",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mount Gambier ",
      "hct": "माऊंट गम्बिएर "
    }, {
      "ac": "HGU",
      "an": "Mount Hagen",
      "han": "माऊंट हाजन",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Mount Hagen",
      "hct": "माऊंट हाजन"
    }, {
      "ac": "ISA",
      "an": "Mount Isa",
      "han": "माऊंट इसा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mount Isa",
      "hct": "माऊंट इसा"
    }, {
      "ac": "MMG",
      "an": "Mount Magnet",
      "han": "माऊंट मागनत",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Mount Magnet ",
      "hct": "माऊंट मागनत "
    }, {
      "ac": "MPN",
      "an": "Mount Pleasant",
      "han": "माऊंट प्लेज़ंत",
      "cn": "Falkland Islands",
      "hcn": "फ़ाळ्क्लांद आयलॅन्ड्स",
      "cc": "FK",
      "ct": "Mount Pleasant",
      "hct": "माऊंट प्लेज़ंत"
    }, {
      "ac": "MOU",
      "an": "Mountain Village",
      "han": "मौंतैन विलेज",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Mountain Village",
      "hct": "मौंतैन विलेज"
    }, {
      "ac": "OYG",
      "an": "Moyo",
      "han": "मोयो",
      "cn": "Uganda",
      "hcn": "युगांडा",
      "cc": "UG",
      "ct": "Moyo",
      "hct": "मोयो"
    }, {
      "ac": "MPA",
      "an": "Mpacha",
      "han": "म्पचा",
      "cn": "Namibia",
      "hcn": "नामीबिया",
      "cc": "NA",
      "ct": "Mpacha",
      "hct": "म्पचा"
    }, {
      "ac": "MYW",
      "an": "Mtwara",
      "han": "म्ट्वारा",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Mtwara",
      "hct": "म्ट्वारा"
    }, {
      "ac": "MVS",
      "an": "Mucuri",
      "han": "मुकरी",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Mucuri",
      "hct": "मुकरी"
    }, {
      "ac": "MDG",
      "an": "Mudanjiang",
      "han": "मुदान्जिआंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Mudanjiang",
      "hct": "मुदान्जिआंग"
    }, {
      "ac": "FMO",
      "an": "Muenster",
      "han": "मुएन्स्टर",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Muenster",
      "hct": "मुएन्स्टर"
    }, {
      "ac": "MKM",
      "an": "Mukah",
      "han": "मूकाह",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Mukah",
      "hct": "मूकाह"
    }, {
      "ac": "MLH",
      "an": "Mulhouse Euroairport French",
      "han": "मलहाऊस युरोएअरप़ॉर्ट फ़्रेन्च",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Mulhouse Basel",
      "hct": "मलहाऊस बसेल"
    }, {
      "ac": "MUX",
      "an": "Multan",
      "han": "मुल्तान",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Multan",
      "hct": "मुल्तान"
    }, {
      "ac": "MZV",
      "an": "Mulu",
      "han": "मूलू",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Mulu",
      "hct": "मूलू"
    }, {
      "ac": "MUA",
      "an": "Munda",
      "han": "मुंडा",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Munda",
      "hct": "मुंडा"
    }, {
      "ac": "MJV",
      "an": "San Javier",
      "han": "सेन जेवियर",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Murcia",
      "hct": "मूरकिया"
    }, {
      "ac": "MMK",
      "an": "Murmansk",
      "han": "मूरमन्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Murmansk",
      "hct": "मूरमन्स्क"
    }, {
      "ac": "MYI",
      "an": "Murray Island",
      "han": "मुररे आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Murray Island",
      "hct": "मुररे आयलॅन्ड"
    }, {
      "ac": "MSR",
      "an": "Mus",
      "han": "मुस",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Mus",
      "hct": "मुस"
    }, {
      "ac": "MKG",
      "an": "Muskegon Cty",
      "han": "मुस्केगोन क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Muskegon",
      "hct": "मुस्केगोन"
    }, {
      "ac": "MSA",
      "an": "Muskrat Dam",
      "han": "मुस्क्रट दम",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Muskrat Dam",
      "hct": "मुस्क्रट दम"
    }, {
      "ac": "MUZ",
      "an": "Musoma",
      "han": "मुसोमा",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Musoma",
      "hct": "मुसोमा"
    }, {
      "ac": "MWZ",
      "an": "Mwanza",
      "han": "म्वन्ज़ा",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Mwanza",
      "hct": "म्वन्ज़ा"
    }, {
      "ac": "MGZ",
      "an": "Myeik",
      "han": "म्येइक",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Myeik",
      "hct": "म्येइक"
    }, {
      "ac": "MYT",
      "an": "Myitkyina",
      "han": "म्यिट्क्यिना",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Myitkyina",
      "hct": "म्यिट्क्यिना"
    }, {
      "ac": "MYR",
      "an": "Myrtle Beach Jetway",
      "han": "मर्टले बीच जेटवै",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Myrtle Beach",
      "hct": "मर्टले बीच"
    }, {
      "ac": "MJT",
      "an": "Mytilene",
      "han": "मैटिलेन",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Mytilene",
      "hct": "मैटिलेन"
    }, {
      "ac": "ZZU",
      "an": "Mzuzu",
      "han": "म्ज़ुज़ू",
      "cn": "Malawi",
      "hcn": "मालौई",
      "cc": "MW",
      "ct": "Mzuzu",
      "hct": "म्ज़ुज़ू"
    }, {
      "ac": "NBC",
      "an": "Beaufort Mcas",
      "han": "बोफ़ोर्ट म्कास",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Beaufort",
      "hct": "बोफ़ोर्ट"
    }, {
      "ac": "NDR",
      "an": "",
      "han": "",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Nador",
      "hct": "नडोर"
    }, {
      "ac": "NYM",
      "an": "Nadym",
      "han": "नाड्य्म",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Nadym",
      "hct": "नाड्य्म"
    }, {
      "ac": "WNP",
      "an": "Naga",
      "han": "नाग",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Naga",
      "hct": "नाग"
    }, {
      "ac": "NGS",
      "an": "Nagasaki",
      "han": "नागसाकी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Nagasaki",
      "hct": "नागसाकी"
    }, {
      "ac": "NGO",
      "an": "Chubu Centrair",
      "han": "चुबू सेन्ट्ड़ैर",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Nagoya",
      "hct": "नागोया"
    }, {
      "ac": "YDP",
      "an": "Nain",
      "han": "नैन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Nain",
      "hct": "नैन"
    }, {
      "ac": "SHB",
      "an": "Nakashibetsu",
      "han": "नकशिबेट्सू",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Nakashibetsu",
      "hct": "नकशिबेट्सू"
    }, {
      "ac": "NAJ",
      "an": "Nakhchivan",
      "han": "नाख्चिवन",
      "cn": "Azerbaijan",
      "hcn": "अज़ेर्बैजान",
      "cc": "AZ",
      "ct": "Nakhchivan",
      "hct": "नाख्चिवन"
    }, {
      "ac": "NST",
      "an": "Nakhon Si Thammarat",
      "han": "नाखोन सी तम्मरत",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Nakhon Si Thammarat",
      "hct": "नाखोन सी तम्मरत"
    }, {
      "ac": "YQN",
      "an": "Nakina",
      "han": "नकीना",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Nakina",
      "hct": "नकीना"
    }, {
      "ac": "KOP",
      "an": "Nakhon Phanom",
      "han": "नाखोन फानोम",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Nakhon Phanom",
      "hct": "नाखोन फानोम"
    }, {
      "ac": "NMA",
      "an": "Namangan",
      "han": "नमनगण",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Namangan",
      "hct": "नमनगण"
    }, {
      "ac": "ATN",
      "an": "",
      "han": "",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Namatanai",
      "hct": "नमतानई"
    }, {
      "ac": "APL",
      "an": "Nampula",
      "han": "नामपुला",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Nampula",
      "hct": "नामपुला"
    }, {
      "ac": "OSY",
      "an": "Namsos",
      "han": "नामसोस",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Namsos",
      "hct": "नामसोस"
    }, {
      "ac": "NNT",
      "an": "Nan",
      "han": "नान",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Nan",
      "hct": "नान"
    }, {
      "ac": "YCD",
      "an": "Nanaimo",
      "han": "नानैमो",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "NANAIMO",
      "hct": "नानैमो"
    }, {
      "ac": "ZNA",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Nanaimo",
      "hct": "नानैमो"
    }, {
      "ac": "KHN",
      "an": "Changbei",
      "han": "चंग्बी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Nanchang",
      "hct": "नांचंग"
    }, {
      "ac": "NAO",
      "an": "Nanchong",
      "han": "नांचोंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Nanchong",
      "hct": "नांचोंग"
    }, {
      "ac": "LZN",
      "an": "Matsu Nangan",
      "han": "मट्सू नांगन",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Matsu Islands",
      "hct": "मट्सू आयलॅन्ड्स"
    }, {
      "ac": "YSR",
      "an": "Nanisivik",
      "han": "नाणीसिवीक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Nanisivik",
      "hct": "नाणीसिवीक"
    }, {
      "ac": "NKG",
      "an": "Nanjing",
      "han": "नान्जिंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Nanking Nanjing",
      "hct": "नान्किंग नान्जिंग"
    }, {
      "ac": "NNG",
      "an": "Nanning",
      "han": "नान्निन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Nanning",
      "hct": "नान्निन्ग"
    }, {
      "ac": "JNN",
      "an": "Nanortalik Heliport",
      "han": "नानोर्तालिक हेलिपोर्ट",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Nanortalik",
      "hct": "नानोर्तालिक"
    }, {
      "ac": "NTE",
      "an": "Nantes Atlantique",
      "han": "नंट्स अत्लांटिक़",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Nantes",
      "hct": "नंट्स"
    }, {
      "ac": "NTG",
      "an": "Nantong",
      "han": "नंटोन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Nantong",
      "hct": "नंटोन्ग"
    }, {
      "ac": "ACK",
      "an": "Nantucket Memorial",
      "han": "नंटुकेट मेमोरिल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nantucket",
      "hct": "नंटुकेट"
    }, {
      "ac": "KEB",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nanwalek",
      "hct": "नान्वालेक"
    }, {
      "ac": "NNY",
      "an": "Nanyang",
      "han": "नाण्यांग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Nanyang",
      "hct": "नाण्यांग"
    }, {
      "ac": "NYK",
      "an": "Nanyuki Civil",
      "han": "नाण्युकी सिवील",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Nanyuki",
      "hct": "नाण्युकी"
    }, {
      "ac": "WNA",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Napakiak",
      "hct": "नापाकियाक"
    }, {
      "ac": "PKA",
      "an": "Napaiskakspb",
      "han": "नापइस्काक्स्प्ब",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Napaiskak",
      "hct": "नापइस्काक"
    }, {
      "ac": "NPE",
      "an": "Napier",
      "han": "नेपिअर",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "NAPIER",
      "hct": "नेपिअर"
    }, {
      "ac": "NAP",
      "an": "Capodichino",
      "han": "कपोडिचिनो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Naples",
      "hct": "नाप्ल्स"
    }, {
      "ac": "APF",
      "an": "Naples Municipal",
      "han": "नाप्ल्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Naples",
      "hct": "नाप्ल्स"
    }, {
      "ac": "NAW",
      "an": "Narathiwat",
      "han": "नराथिवाट",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Narathiwat",
      "hct": "नराथिवाट"
    }, {
      "ac": "NAA",
      "an": "Narrabri",
      "han": "नररब्री",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Narrabri ",
      "hct": "नररब्री "
    }, {
      "ac": "NRA",
      "an": "Narrandera",
      "han": "नररंदरा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Narrandera ",
      "hct": "नररंदरा "
    }, {
      "ac": "UAK",
      "an": "Narsarsuaq",
      "han": "नरसर्सुआक़",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Narsarsuaq",
      "hct": "नरसर्सुआक़"
    }, {
      "ac": "NVK",
      "an": "Framnes",
      "han": "फ़्राम्न्स",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Narvik",
      "hct": "नरवीक"
    }, {
      "ac": "NNM",
      "an": "Naryan",
      "han": "नारायण",
      "cn": "Mar",
      "hcn": "मार",
      "cc": "Russia",
      "ct": "RU",
      "hct": "रू"
    }, {
      "ac": "BNA",
      "an": "Nashville",
      "han": "नशविले",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nashville",
      "hct": "नशविले"
    }, {
      "ac": "NAT",
      "an": "Augusto Severo",
      "han": "औगुस्टो सेवेरो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Natal",
      "hct": "नाटल"
    }, {
      "ac": "YNA",
      "an": "Natashquan",
      "han": "नातश्क़ुआन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Natashquan",
      "hct": "नातश्क़ुआन"
    }, {
      "ac": "YNP",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Natuashish",
      "hct": "नातुआशीष"
    }, {
      "ac": "NKI",
      "an": "Naukiti",
      "han": "नौकिती",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Naukiti",
      "hct": "नौकिती"
    }, {
      "ac": "INU",
      "an": "Nauru",
      "han": "नौरू",
      "cn": "Nauru",
      "hcn": "नौरू",
      "cc": "NR",
      "ct": "Nauru",
      "hct": "नौरू"
    }, {
      "ac": "NVT",
      "an": "Navegantes",
      "han": "नवेगंट्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Navegantes",
      "hct": "नवेगंट्स"
    }, {
      "ac": "JNX",
      "an": "Naxos",
      "han": "नाक्सोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Naxos",
      "hct": "नाक्सोस"
    }, {
      "ac": "NDJ",
      "an": "N Djamena",
      "han": "एन ड्जमेना",
      "cn": "Chad",
      "hcn": "चड",
      "cc": "TD",
      "ct": "N Djamena",
      "hct": "एन ड्जमेना"
    }, {
      "ac": "NLA",
      "an": "Ndola",
      "han": "न्डोला",
      "cn": "Zambia",
      "hcn": "ज़ाम्बीया",
      "cc": "ZM",
      "ct": "Ndola",
      "hct": "न्डोला"
    }, {
      "ac": "CNP",
      "an": "Neerlerit Inaat",
      "han": "नीर्लेरित इनाट",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Neerlerit Inaat",
      "hct": "नीर्लेरित इनाट"
    }, {
      "ac": "NVA",
      "an": "Benito Salas",
      "han": "बेनिटो सलस",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Neiva",
      "hct": "नीवा"
    }, {
      "ac": "EAM",
      "an": "Nejran",
      "han": "नेज्रन",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Nejran",
      "hct": "नेज्रन"
    }, {
      "ac": "NLG",
      "an": "Nelson Lagoon",
      "han": "नेल्सन लागून",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nelson Lagoon",
      "hct": "नेल्सन लागून"
    }, {
      "ac": "NSN",
      "an": "Nelson",
      "han": "नेल्सन",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Nelson",
      "hct": "नेल्सन"
    }, {
      "ac": "MQP",
      "an": "Kruger Mpumalanga",
      "han": "क्रुगर म्पूमालंगा",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Mpumalanga",
      "hct": "म्पूमालंगा"
    }, {
      "ac": "YNS",
      "an": "Nemiscau",
      "han": "नेमीस्काऊ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Nemiscau",
      "hct": "नेमीस्काऊ"
    }, {
      "ac": "KEP",
      "an": "",
      "han": "",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Nepalganj",
      "hct": "नेपालगन्ज"
    }, {
      "ac": "NER",
      "an": "Neryungri",
      "han": "नेर्यून्ग्रि",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Neryungri",
      "hct": "नेर्यून्ग्रि"
    }, {
      "ac": "EUM",
      "an": "",
      "han": "",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Neumuenster",
      "hct": "नेउमुएन्स्टर"
    }, {
      "ac": "NQN",
      "an": "Presidente Peron",
      "han": "प्रेसिडेंटे पेरों",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Neuquen",
      "hct": "नेउक़्न"
    }, {
      "ac": "NAV",
      "an": "Kapadokya",
      "han": "कापडोक्या",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Nevsehir",
      "hct": "नेव्सेहीर"
    }, {
      "ac": "EWN",
      "an": "Craven County Regional",
      "han": "क्रावेन कौंटी रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New Bern",
      "hct": "न्यू बर्न"
    }, {
      "ac": "HVN",
      "an": "Tweed New Haven",
      "han": "ट्वीद न्यू हवेन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New Haven",
      "hct": "न्यू हवेन"
    }, {
      "ac": "KGK",
      "an": "New Koliganek",
      "han": "न्यू कोलिगणेक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New Koliganek",
      "hct": "न्यू कोलिगणेक"
    }, {
      "ac": "MSY",
      "an": "Louis Armstrong",
      "han": "लुइस आर्मस्ट्रॉंग",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New Orleans",
      "hct": "न्यू ओर्लीन्स"
    }, {
      "ac": "NPL",
      "an": "New Plymouth",
      "han": "न्यू प्ल्य्मौठ",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "New Plymouth",
      "hct": "न्यू प्ल्य्मौठ"
    }, {
      "ac": "KNW",
      "an": "New Stuyahok",
      "han": "न्यू स्तुयाहोक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "New Stuyahok",
      "hct": "न्यू स्तुयाहोक"
    }, {
      "ac": "SWF",
      "an": "Stewart",
      "han": "स्टुवर्ट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Newburgh",
      "hct": "न्यूबूरघ"
    }, {
      "ac": "ZNE",
      "an": "Newman",
      "han": "न्यूमॅन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Newman ",
      "hct": "न्यूमॅन "
    }, {
      "ac": "NQY",
      "an": "Newquay Civil",
      "han": "न्यूक़ुआय सिवील",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Newquay",
      "hct": "न्यूक़ुआय"
    }, {
      "ac": "WWT",
      "an": "Newtok",
      "han": "न्यूटोक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Newtok",
      "hct": "न्यूटोक"
    }, {
      "ac": "NGI",
      "an": "Ngau",
      "han": "न्गऊ",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Ngau",
      "hct": "न्गऊ"
    }, {
      "ac": "NHA",
      "an": "Nhatrang",
      "han": "न्हातरंग",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Nhatrang",
      "hct": "न्हातरंग"
    }, {
      "ac": "IAG",
      "an": "Niagara Falls",
      "han": "नयागरा फॉल्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Niagara Falls",
      "hct": "नयागरा फॉल्स"
    }, {
      "ac": "NME",
      "an": "Nightmute",
      "han": "निघ्ट्मुटे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nightmute",
      "hct": "निघ्ट्मुटे"
    }, {
      "ac": "KIJ",
      "an": "Niigata",
      "han": "निइगता",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Niigata",
      "hct": "निइगता"
    }, {
      "ac": "NLV",
      "an": "",
      "han": "",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Nikolaev",
      "hct": "निकोलेव"
    }, {
      "ac": "NIB",
      "an": "Nikolai",
      "han": "निकोलै",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nikolai",
      "hct": "निकोलै"
    }, {
      "ac": "IKO",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nikolski",
      "hct": "निकोल्स्की"
    }, {
      "ac": "NGB",
      "an": "Lishe",
      "han": "लिशे",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Ninbo",
      "hct": "निन्बो"
    }, {
      "ac": "INI",
      "an": "Nis",
      "han": "नीस",
      "cn": "Serbia",
      "hcn": "सेरबिया",
      "cc": "CS",
      "ct": "Nis",
      "hct": "नीस"
    }, {
      "ac": "IIS",
      "an": "Nissan Island",
      "han": "नीससन आयलॅन्ड",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Nissan Island",
      "hct": "नीससन आयलॅन्ड"
    }, {
      "ac": "NFO",
      "an": "Mata'aho",
      "han": "माताअहो",
      "cn": " Niuafo'ou Island",
      "hcn": "नीऊफ़ोओऊ आयलॅन्ड",
      "cc": "TO",
      "ct": "Angaha",
      "hct": "अंगहा"
    }, {
      "ac": "NTT",
      "an": "Kuini Lavenia",
      "han": "कुइनी लावेनिआ",
      "cn": "Tonga",
      "hcn": "टोंगा",
      "cc": "TO",
      "ct": "Niuatoputapu",
      "hct": "नीऊटोपुतापू"
    }, {
      "ac": "IUE",
      "an": "Hanan",
      "han": "हनन",
      "cn": "Niue",
      "hcn": "नीऊए",
      "cc": "NU",
      "ct": "Niue Island",
      "hct": "नीऊए आयलॅन्ड"
    }, {
      "ac": "NJC",
      "an": "Nizhnevartovsk",
      "han": "निझवर्टोव्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Nizhnevartovsk",
      "hct": "निझ़वर्टोव्स्क"
    }, {
      "ac": "GOJ",
      "an": "Nizhniy Novgorod",
      "han": "निझिनय नोव्गोरोड",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Nizhniy Novgorod",
      "hct": "निझ़िनय नोव्गोरोड"
    }, {
      "ac": "WTK",
      "an": "Noatak",
      "han": "नोआतक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Noatak",
      "hct": "नोआतक"
    }, {
      "ac": "NOJ",
      "an": "Noyabrsk",
      "han": "नोयब्र्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Noyabrsk",
      "hct": "नोयब्र्स्क"
    }, {
      "ac": "OME",
      "an": "Nome",
      "han": "नोम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nome",
      "hct": "नोम"
    }, {
      "ac": "NNL",
      "an": "Nondalton",
      "han": "नोनडाल्टन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nondalton",
      "hct": "नोनडाल्टन"
    }, {
      "ac": "ORV",
      "an": "Curtis Memorial",
      "han": "कर्टिस मेमोरिल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Noorvik",
      "hct": "नूर्वीक"
    }, {
      "ac": "NLK",
      "an": "Norfolk Island",
      "han": "नोर्फ़ोलक आयलॅन्ड",
      "cn": "Norfolk Island",
      "hcn": "नोर्फ़ोळ्क आयलॅन्ड",
      "cc": "NF",
      "ct": "Norfolk Island",
      "hct": "नोर्फ़ोळ्क आयलॅन्ड"
    }, {
      "ac": "ORF",
      "an": "Norfolk",
      "han": "नोर्फ़ोलक आयलॅन्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Norfolk",
      "hct": "नोर्फ़ोलक"
    }, {
      "ac": "NSK",
      "an": "Alykel",
      "han": "अल्य्केल",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Norilsk",
      "hct": "नोरिल्स्क"
    }, {
      "ac": "YVQ",
      "an": "Norman Wells",
      "han": "नोर्मन वेल्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Norman Wells",
      "hct": "नोर्मन वेल्स"
    }, {
      "ac": "NTN",
      "an": "Normanton",
      "han": "नोर्मंटन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Normanton ",
      "hct": "नोर्मंटन "
    }, {
      "ac": "NRK",
      "an": "Kungsangen",
      "han": "कून्ग्संगेन",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Norrkoeping",
      "hct": "नोर्र्कोयपींग"
    }, {
      "ac": "NUS",
      "an": "Norsup",
      "han": "नोर्सुप",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Norsup",
      "hct": "नोर्सुप"
    }, {
      "ac": "YYB",
      "an": "North Bay Municipal",
      "han": "नोर्थ बे म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "North Bay",
      "hct": "नोर्थ बे"
    }, {
      "ac": "OTH",
      "an": "North Bend Municipal",
      "han": "नोर्थ बेंद म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "North Bend",
      "hct": "नोर्थ बेंद"
    }, {
      "ac": "LBF",
      "an": "Lee Bird Field",
      "han": "ली बर्ड फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "North Platte",
      "hct": "नोर्थ प्लत्ते"
    }, {
      "ac": "YNO",
      "an": "North Spirit Lake",
      "han": "नोर्थ स्पिरिट लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "North Spirit Lake",
      "hct": "नोर्थ स्पिरिट लेक"
    }, {
      "ac": "ORM",
      "an": "Northampton",
      "han": "नोर्ठंप्टन",
      "cn": "United Kingdom ",
      "hcn": "युनाईटेड किंग्डोम ",
      "cc": "GB",
      "ct": "Northampton ",
      "hct": "नोर्ठंप्टन "
    }, {
      "ac": "YNE",
      "an": "Norway House",
      "han": "नोरवै हाऊस",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Norway House",
      "hct": "नोरवै हाऊस"
    }, {
      "ac": "NWI",
      "an": "Norwich",
      "han": "नोर्विच",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Norwich",
      "hct": "नोर्विच"
    }, {
      "ac": "NOS",
      "an": "Fascene",
      "han": "फ़स्केन",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Nosy",
      "hct": "नोसी"
    }, {
      "ac": "EMA",
      "an": "East Midlands",
      "han": "ईस्ट मिडलॅंड्स",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Nottingham",
      "hct": "नोततीनघाम"
    }, {
      "ac": "NDB",
      "an": "Nouadhibou",
      "han": "नौधिबौ",
      "cn": "Mauritania",
      "hcn": "मौरिटानिआ",
      "cc": "MR",
      "ct": "Nouadhibou",
      "hct": "नौधिबौ"
    }, {
      "ac": "NKC",
      "an": "Nouakchott",
      "han": "नौक्चोत",
      "cn": "Mauritania",
      "hcn": "मौरिटानिआ",
      "cc": "MR",
      "ct": "Nouakchott",
      "hct": "नौक्चोत"
    }, {
      "ac": "GEA",
      "an": "Magenta",
      "han": "मजन्ता",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Noumea",
      "hct": "नौमी"
    }, {
      "ac": "NOU",
      "an": "Tontouta",
      "han": "टोन्टौता",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Noumea",
      "hct": "नौमी"
    }, {
      "ac": "NOZ",
      "an": "Spichenkovo",
      "han": "स्पिचेन्कोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Novokuznetsk",
      "hct": "नोवोकुज़्नेट्स्क"
    }, {
      "ac": "OVB",
      "an": "Tolmachevo",
      "han": "तॉल्मेचेवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Novosibirsk",
      "hct": "नोवोसिबीर्स्क"
    }, {
      "ac": "NUX",
      "an": "Novyi Urengoy",
      "han": "नोव्यी उरेन्गोय",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Novyi Urengoy",
      "hct": "नोव्यी उरेन्गोय"
    }, {
      "ac": "NSH",
      "an": "Noshahr",
      "han": "नोशाह्र",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Noshahr",
      "hct": "नोशाह्र"
    }, {
      "ac": "NLD",
      "an": "Quetzalcoatl",
      "han": "क़्ट्ज़ाल्कोट्ल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Nuevo Laredo",
      "hct": "नुएवो लारेदो"
    }, {
      "ac": "NUI",
      "an": "Nuiqsut",
      "han": "नुइक़्सुत",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nuiqsut",
      "hct": "नुइक़्सुत"
    }, {
      "ac": "TBU",
      "an": "Tongatapu",
      "han": "टोन्गतापू",
      "cn": "Tonga",
      "hcn": "टोंगा",
      "cc": "TO",
      "ct": "Tongatapu",
      "hct": "टोन्गतापू"
    }, {
      "ac": "NCU",
      "an": "Nukus",
      "han": "नुकुस",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Nukus",
      "hct": "नुकुस"
    }, {
      "ac": "NUL",
      "an": "Nulato",
      "han": "नलातो",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nulato",
      "hct": "नलातो"
    }, {
      "ac": "NUP",
      "an": "Nunapitchuk",
      "han": "नुनापिच्चुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Nunapitchuk",
      "hct": "नुनापिच्चुक"
    }, {
      "ac": "NQU",
      "an": "Nuqui",
      "han": "नुक़ुई",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Nuqui",
      "hct": "नुक़ुई"
    }, {
      "ac": "NUE",
      "an": "Nuremberg",
      "han": "नुरेम्बर्ग",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Nuremberg",
      "hct": "नुरेम्बर्ग"
    }, {
      "ac": "GOH",
      "an": "Nuuk",
      "han": "नूक",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Nuuk",
      "hct": "नूक"
    }, {
      "ac": "UYL",
      "an": "Nyala",
      "han": "न्याला",
      "cn": "Sudan",
      "hcn": "सुदान",
      "cc": "SD",
      "ct": "Nyala",
      "hct": "न्याला"
    }, {
      "ac": "NYU",
      "an": "Bagan",
      "han": "बागन",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Nyuang U",
      "hct": "न्युआंग यू"
    }, {
      "ac": "XWZ",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Nykoping",
      "hct": "न्य्कोपींग"
    }, {
      "ac": "ODW",
      "an": "Oak Harbor",
      "han": "ओक हरबोर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Oak Harbor",
      "hct": "ओक हरबोर"
    }, {
      "ac": "OKY",
      "an": "Oakey",
      "han": "ओकी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Oakey",
      "hct": "ओकी"
    }, {
      "ac": "ODM",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Oakland",
      "hct": "ओकलॅंद"
    }, {
      "ac": "OAK",
      "an": "Metro Oakland",
      "han": "मेट्रो ओकलॅंद",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Oakland",
      "hct": "ओकलैंड"
    }, {
      "ac": "OAM",
      "an": "Oamaru",
      "han": "ओआमारू",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Oamaru",
      "hct": "ओआमारू"
    }, {
      "ac": "OAX",
      "an": "Xoxocotlan",
      "han": "क्सोक्सोकोट्लन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Oaxaca",
      "hct": "ओआक्षका"
    }, {
      "ac": "OBO",
      "an": "Obihiro",
      "han": "ओबिहीरो",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Obihiro",
      "hct": "ओबिहीरो"
    }, {
      "ac": "OBX",
      "an": "Obo",
      "han": "ओबो",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Obo",
      "hct": "ओबो"
    }, {
      "ac": "ONJ",
      "an": "",
      "han": "",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Odate Noshiro",
      "hct": "ओडाते नोशीरो"
    }, {
      "ac": "OGS",
      "an": "Ogdensburg Municipal",
      "han": "ओग्ड्न्स्बूरग म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ogdensburg",
      "hct": "ओग्ड्न्स्बूरग"
    }, {
      "ac": "YOG",
      "an": "Ogoki Post",
      "han": "ओगोकी पोस्ट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Ogoki Post",
      "hct": "ओगोकी पोस्ट"
    }, {
      "ac": "OHD",
      "an": "Ohrid",
      "han": "ओऋद",
      "cn": "Macedonia",
      "hcn": "मेसेदोनिआ",
      "cc": "MK",
      "ct": "Ohrid",
      "hct": "ओऋद"
    }, {
      "ac": "OIT",
      "an": "Oita",
      "han": "ओईता",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Oita",
      "hct": "ओईता"
    }, {
      "ac": "OKJ",
      "an": "Okayama",
      "han": "ओकयम",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Okayama",
      "hct": "ओकयम"
    }, {
      "ac": "OHO",
      "an": "Okhotsk",
      "han": "ओखोट्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Okhotsk",
      "hct": "ओखोट्स्क"
    }, {
      "ac": "OKA",
      "an": "Naha Field",
      "han": "नहा फ़ील्ड",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Okinawa",
      "hct": "ओकिनवा"
    }, {
      "ac": "OKC",
      "an": "Will Rogers World",
      "han": "विल रोजर्स वर्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Oklahoma City",
      "hct": "ओक्लहोमा सिटी"
    }, {
      "ac": "OLB",
      "an": "Olbia Costa Smeralda",
      "han": "ओल्बिआ कोस्टा स्मेराल्दा",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Olbia",
      "hct": "ओल्बिआ"
    }, {
      "ac": "YOC",
      "an": "Old Crow",
      "han": "ओल्ड क्रौ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Old Crow",
      "hct": "ओल्ड क्रौ"
    }, {
      "ac": "OLH",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Old Harbor",
      "hct": "ओल्ड हरबोर"
    }, {
      "ac": "KOY",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Olga Bay",
      "hct": "ओल्गा बे"
    }, {
      "ac": "OLJ",
      "an": "Olpoi",
      "han": "ओल्पोई",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Olpoi",
      "hct": "ओल्पोई"
    }, {
      "ac": "OLP",
      "an": "Olympic Dam",
      "han": "ओलीम्पिक दम",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Olympic Dam ",
      "hct": "ओलीम्पिक दम "
    }, {
      "ac": "OMA",
      "an": "Eppley Airfield",
      "han": "एप्प्ली एअरफ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Omaha",
      "hct": "ओमहा"
    }, {
      "ac": "OMB",
      "an": "Omboue Hopital",
      "han": "ओंबौए होपइताल",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Omboue Hospial",
      "hct": "ओंबौए होस्पियाल"
    }, {
      "ac": "OMS",
      "an": "Omsk",
      "han": "ओम्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Omsk",
      "hct": "ओम्स्क"
    }, {
      "ac": "OND",
      "an": "Ondangwa",
      "han": "ओन्डांग्वा",
      "cn": "Namibia",
      "hcn": "नामीबिया",
      "cc": "NA",
      "ct": "Ondangwa",
      "hct": "ओन्डांग्वा"
    }, {
      "ac": "ONT",
      "an": "Ontario",
      "han": "ओन्तरिओ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ontario",
      "hct": "ओन्तरिओ"
    }, {
      "ac": "YBS",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Opapamiska Lake",
      "hct": "ओपापामीस्का लेक"
    }, {
      "ac": "OMR",
      "an": "Oradea",
      "han": "ओराडी",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Oradea",
      "hct": "ओराडी"
    }, {
      "ac": "OAG",
      "an": "Springhill",
      "han": "स्प्रिंगहील",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Orange",
      "hct": "ओरन्गे"
    }, {
      "ac": "OMD",
      "an": "Oranjemund",
      "han": "ओरन्जेमून्ड",
      "cn": "Namibia",
      "hcn": "नामीबिया",
      "cc": "NA",
      "ct": "Oranjemund",
      "hct": "ओरन्जेमून्ड"
    }, {
      "ac": "ORB",
      "an": "Orebro Bofors",
      "han": "ओरेब्रो बोफ़ोर्स",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Orebro",
      "hct": "ओरेब्रो"
    }, {
      "ac": "REN",
      "an": "Orenburg",
      "han": "ओरेन्बूरग",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Orenburg",
      "hct": "ओरेन्बूरग"
    }, {
      "ac": "OLA",
      "an": "Orland",
      "han": "ओर्लांद",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Orland",
      "hct": "ओर्लांद"
    }, {
      "ac": "OER",
      "an": "Ornskoldsvik",
      "han": "ओर्न्स्कोल्ड्स्वीक",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Ornskoldsvik",
      "hct": "ओर्न्स्कोल्ड्स्वीक"
    }, {
      "ac": "OSW",
      "an": "Orsk",
      "han": "ओर्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Orsk",
      "hct": "ओर्स्क"
    }, {
      "ac": "HOV",
      "an": "Hovden",
      "han": "होव्ड्न",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Orsta",
      "hct": "ओर्स्टा"
    }, {
      "ac": "OSS",
      "an": "Osh",
      "han": "ओश",
      "cn": "Kyrgyzstan",
      "hcn": "क्य्र्ग्य्ज़्स्तान",
      "cc": "KG",
      "ct": "Osh",
      "hct": "ओश"
    }, {
      "ac": "YOO",
      "an": "Oshawa",
      "han": "ओशवा",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Oshawa",
      "hct": "ओशवा"
    }, {
      "ac": "OIM",
      "an": "Oshima",
      "han": "ओशिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Oshima",
      "hct": "ओशिमा"
    }, {
      "ac": "OSI",
      "an": "Osijek",
      "han": "ओसिजेक",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Osijek",
      "hct": "ओसिजेक"
    }, {
      "ac": "OSK",
      "an": "Oskarshamn",
      "han": "ओस्कर्शाम्न",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Oskarshamn",
      "hct": "ओस्कर्शाम्न"
    }, {
      "ac": "ZOS",
      "an": "Canal Balo",
      "han": "कनाल बालो",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Osorno",
      "hct": "ओसोर्नो"
    }, {
      "ac": "OSD",
      "an": "Froesoe",
      "han": "फ़्रोयसोय",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Ostersund",
      "hct": "ओस्टर्सुन्ड"
    }, {
      "ac": "OSR",
      "an": "Mosnov",
      "han": "मोस्नोव",
      "cn": "Czech Republic",
      "hcn": "क्ज़ेच रीपबलीक",
      "cc": "CZ",
      "ct": "Ostrava",
      "hct": "ओस्ट्रवा"
    }, {
      "ac": "OUA",
      "an": "Ouagadougou",
      "han": "औगदौगौ",
      "cn": "Burkina Faso",
      "hcn": "बूरकिना फ़सो",
      "cc": "BF",
      "ct": "Ouagadougou",
      "hct": "औगदौगौ"
    }, {
      "ac": "OZZ",
      "an": "Ourzazate",
      "han": "और्ज़ाज़टे",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Ouarzazate",
      "hct": "और्ज़ाज़टे"
    }, {
      "ac": "ODY",
      "an": "Oudomxay",
      "han": "औडोम्क्साय",
      "cn": "Laos",
      "hcn": "लाव्स",
      "cc": "LA",
      "ct": "Muang Xay",
      "hct": "मुआंग क्साय"
    }, {
      "ac": "OUD",
      "an": "Angads",
      "han": "अंगड्स",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Oujda",
      "hct": "औज्दा"
    }, {
      "ac": "OUL",
      "an": "Oulu",
      "han": "औलू",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Oulu",
      "hct": "औलू"
    }, {
      "ac": "UVE",
      "an": "Ouvea",
      "han": "औवी",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Ouvea",
      "hct": "औवी"
    }, {
      "ac": "KOZ",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ouzinkie",
      "hct": "औज़िन्की"
    }, {
      "ac": "VDA",
      "an": "Ovda",
      "han": "ओव्दा",
      "cn": "Israel",
      "hcn": "इसरायल",
      "cc": "IL",
      "ct": "Ovda",
      "hct": "ओव्दा"
    }, {
      "ac": "OWB",
      "an": "Daviess County",
      "han": "दवीस कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Owensboro",
      "hct": "ओवेन्सबोरो"
    }, {
      "ac": "YOH",
      "an": "Oxford House",
      "han": "ओक्सफ़ोर्ड हाऊस",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Oxford House",
      "hct": "ओक्सफ़ोर्ड हाऊस"
    }, {
      "ac": "OXF",
      "an": "Kidlington",
      "han": "किड्लींग्टन",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Oxford",
      "hct": "ओक्सफ़ोर्ड"
    }, {
      "ac": "OXR",
      "an": "Oxnard",
      "han": "ओक्स्नार्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Oxnard",
      "hct": "ओक्स्नार्ड"
    }, {
      "ac": "OYE",
      "an": "Oyem",
      "han": "ओयेम",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Oyem",
      "hct": "ओयेम"
    }, {
      "ac": "OZC",
      "an": "Ozamis",
      "han": "ओझामिस",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Ozamis",
      "hct": "ओझामिस"
    }, {
      "ac": "PBJ",
      "an": "Tavie",
      "han": "तवी",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Paama Island",
      "hct": "पामा आयलॅन्ड"
    }, {
      "ac": "JFR",
      "an": "Paamiut Heliport",
      "han": "पामीऊत हेलिपोर्ट",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Paamiut",
      "hct": "पामीऊत"
    }, {
      "ac": "PDG",
      "an": "Tabing",
      "han": "ताबींग",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Padang",
      "hct": "पडंग"
    }, {
      "ac": "PAD",
      "an": "Paderborn",
      "han": "पादर्बोर्न",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Paderborn",
      "hct": "पादर्बोर्न"
    }, {
      "ac": "PAH",
      "an": "Barkley Regional",
      "han": "बर्क्ली रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Paducah",
      "hct": "पदुकाह"
    }, {
      "ac": "PAG",
      "an": "Pagadian",
      "han": "पगडिन",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Pagadian ",
      "hct": "पगडिन "
    }, {
      "ac": "PGA",
      "an": "Page",
      "han": "पागे",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Page",
      "hct": "पागे"
    }, {
      "ac": "PPG",
      "an": "Pago Pago",
      "han": "पागो पागो",
      "cn": "American Samoa",
      "hcn": "अमेरिकन समोआ",
      "cc": "AS",
      "ct": "Pago Pago",
      "hct": "पागो पागो"
    }, {
      "ac": "PJA",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Pajala",
      "hct": "पजअला"
    }, {
      "ac": "PKK",
      "an": "Pakhokku",
      "han": "पखोक्कू",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Pakhokku",
      "hct": "पखोक्कू"
    }, {
      "ac": "PKZ",
      "an": "Pakse",
      "han": "पक्से",
      "cn": "Laos",
      "hcn": "लाव्स",
      "cc": "LA",
      "ct": "Pakse",
      "hct": "पक्से"
    }, {
      "ac": "YIF",
      "an": "St Augustin",
      "han": "स्त्रीट औगस्टिन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "St",
      "hct": "स्त्रीट"
    }, {
      "ac": "PKY",
      "an": "Tjilik Riwut",
      "han": "ट्जिलिक रिवुत",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Palangkaraya",
      "hct": "पालंग्करय"
    }, {
      "ac": "PLM",
      "an": "Mahmud Badaruddin Li",
      "han": "महमूद बदरुद्दीन ली",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Palembang",
      "hct": "पालेमबंग"
    }, {
      "ac": "PMO",
      "an": "Punta Raisi",
      "han": "पंता रैसी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Palermo",
      "hct": "पलर्मो"
    }, {
      "ac": "PMK",
      "an": "Palm Island",
      "han": "पाल्म आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Palm Island ",
      "hct": "पाल्म आयलॅन्ड "
    }, {
      "ac": "PSP",
      "an": "Palm Springs Municipal",
      "han": "पाल्म स्प्रिंग्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Palm Springs",
      "hct": "पाल्म स्प्रिंग्स"
    }, {
      "ac": "PMI",
      "an": "Palma Mallorca",
      "han": "पाल्मा मलोर्का",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Palma Mallorca",
      "hct": "पाल्मा मलोर्का"
    }, {
      "ac": "PMW",
      "an": "Palmas",
      "han": "पाल्मास",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Palmas",
      "hct": "पाल्मास"
    }, {
      "ac": "PMD",
      "an": "Palmdale Rgnl Usaf Plt 42",
      "han": "पाल्म्दाले र्ग्न्ल उसाफ़ प्ल्ट 42",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Palmdale",
      "hct": "पाल्म्दाले"
    }, {
      "ac": "PMR",
      "an": "Palmerston North",
      "han": "पाल्मर्स्टन नोर्थ",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Palmerston North",
      "hct": "पाल्मर्स्टन नोर्थ"
    }, {
      "ac": "PLW",
      "an": "Mutiara",
      "han": "मतिआरा",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Palu",
      "hct": "पलू"
    }, {
      "ac": "PNA",
      "an": "Pamplona Noain",
      "han": "पंप्लोना नोएन",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Pamplona",
      "hct": "पंप्लोना"
    }, {
      "ac": "PZI",
      "an": "Panzhihua",
      "han": "पन्झिहआ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Panzhihua",
      "hct": "पन्ऴिहुआ"
    }, {
      "ac": "PFN",
      "an": "Bay County",
      "han": "बे कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Panama City",
      "hct": "पनामा सिटी"
    }, {
      "ac": "ECP",
      "an": "Northwest Florida Beaches",
      "han": "नोर्थवॅस्ट फ़्लोरिदा बीचेस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Panama City",
      "hct": "पनामा सिटी"
    }, {
      "ac": "PGK",
      "an": "Depati Amir",
      "han": "डेपाती अमीर",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Pangkal Pinang",
      "hct": "पंगकल पिनंग"
    }, {
      "ac": "PKG",
      "an": "Pulau Pangkor",
      "han": "पुलऊ पंग्कोर",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Pangkor Island",
      "hct": "पंग्कोर आयलॅन्ड"
    }, {
      "ac": "YXP",
      "an": "Pangnirtung",
      "han": "पंग्निर्तुन्ग",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Pangnirtung",
      "hct": "पंग्निर्तुन्ग"
    }, {
      "ac": "PJG",
      "an": "Panjgur",
      "han": "पंजगुर",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Panjgur",
      "hct": "पंजगुर"
    }, {
      "ac": "PNL",
      "an": "Pantelleria",
      "han": "पंटेलेरिआ",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Pantelleria",
      "hct": "पंटेलेरिआ"
    }, {
      "ac": "PPT",
      "an": "Intl Tahiti Faaa",
      "han": "इंट्ल ताहिती फ़ाअ",
      "cn": "French Polynesia",
      "hcn": "फ़्रेन्च पॉलीनेसिआ",
      "cc": "PF",
      "ct": "Papeete",
      "hct": "पापीटे"
    }, {
      "ac": "PFO",
      "an": "Paphos",
      "han": "पेफोस",
      "cn": "Cyprus",
      "hcn": "सीप्रुस",
      "cc": "CY",
      "ct": "Paphos",
      "hct": "पेफोस"
    }, {
      "ac": "PBO",
      "an": "Paraburdoo",
      "han": "परबूरडू",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Paraburdoo",
      "hct": "परबूरडू"
    }, {
      "ac": "PBM",
      "an": "Zanderij",
      "han": "ज़न्देरिज",
      "cn": "Suriname",
      "hcn": "सूरिनामे",
      "cc": "SR",
      "ct": "Paramaribo",
      "hct": "परमारिबो"
    }, {
      "ac": "PED",
      "an": "Pardubice",
      "han": "परडबआईस",
      "cn": "Czech Republic",
      "hcn": "क्ज़ेच रीपबलीक",
      "cc": "CZ",
      "ct": "Pardubice",
      "hct": "परडबआईस"
    }, {
      "ac": "PIN",
      "an": "",
      "han": "",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Parintins",
      "hct": "परिंटिन्स"
    }, {
      "ac": "PKE",
      "an": "Parkes",
      "han": "पार्केस",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Parkes ",
      "hct": "पार्केस "
    }, {
      "ac": "PAS",
      "an": "Paros Community",
      "han": "परोस कम्युनिटी",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Paros",
      "hct": "परोस"
    }, {
      "ac": "PFQ",
      "an": "Parsabade Moghan",
      "han": "पर्साबडे मोघेन",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Parsabad",
      "hct": "पर्साबाद"
    }, {
      "ac": "PSC",
      "an": "Tri Cities",
      "han": "त्रि सितीस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pasco",
      "hct": "पास्को"
    }, {
      "ac": "PSI",
      "an": "Pasni",
      "han": "पासनी",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Pasni",
      "hct": "पासनी"
    }, {
      "ac": "PFB",
      "an": "Lauro Kurtz",
      "han": "लौरो कूरट्ज़",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Passo Fundo",
      "hct": "पास्सो फ़ुंदो"
    }, {
      "ac": "PSO",
      "an": "Cano",
      "han": "कानो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Pasto",
      "hct": "पस्टो"
    }, {
      "ac": "POJ",
      "an": "",
      "han": "",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Patos De Minas",
      "hct": "पेतोस दे मिनस"
    }, {
      "ac": "GPA",
      "an": "Araxos",
      "han": "आरक्सोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Patras",
      "hct": "पातरस"
    }, {
      "ac": "PUF",
      "an": "Uzein",
      "han": "उज़ैन",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Pau",
      "hct": "पाऊ"
    }, {
      "ac": "YPC",
      "an": "Paulatuk",
      "han": "पौलतुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Paulatuk",
      "hct": "पौलतुक"
    }, {
      "ac": "PWQ",
      "an": "Pavlodar",
      "han": "पाव्लोदार",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Pavlodar",
      "hct": "पाव्लोदार"
    }, {
      "ac": "YPE",
      "an": "Municipal Peace River",
      "han": "म्यूनिसिपल पीस रिवर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Peace River",
      "hct": "पीस रिवर"
    }, {
      "ac": "YPO",
      "an": "Peawanuck",
      "han": "पीवनुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Peawanuck",
      "hct": "पीवनुक"
    }, {
      "ac": "PEX",
      "an": "Pechora",
      "han": "पेचोरा",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Pechora",
      "hct": "पेचोरा"
    }, {
      "ac": "PDB",
      "an": "Pedro Bay",
      "han": "पेद्रो बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pedro Bay",
      "hct": "पेद्रो बे"
    }, {
      "ac": "PKU",
      "an": "Simpang Tiga",
      "han": "सिम्पंग तिगा",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Pekanbaru",
      "hct": "पेकन्बरू"
    }, {
      "ac": "PEC",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pelican",
      "hct": "पेलिकन"
    }, {
      "ac": "PLN",
      "an": "Emmet Cty",
      "han": "एम्मेत क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pellston",
      "hct": "पेल्स्टन"
    }, {
      "ac": "PET",
      "an": "Pelotas",
      "han": "पेलोटास",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Pelotas",
      "hct": "पेलोटास"
    }, {
      "ac": "POL",
      "an": "Pemba",
      "han": "पेंबा",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Pemba",
      "hct": "पेंबा"
    }, {
      "ac": "PEN",
      "an": "Penang",
      "han": "पेनंग",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Penang",
      "hct": "पेनंग"
    }, {
      "ac": "PDT",
      "an": "Pendleton",
      "han": "पेंड्लेटन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pendleton",
      "hct": "पेंड्लेटन"
    }, {
      "ac": "PNS",
      "an": "Pensacola Regional Municipal",
      "han": "पेन्सकोला रिजनल म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pensacola",
      "hct": "पेन्सकोला"
    }, {
      "ac": "YYF",
      "an": "Penticton Municipal",
      "han": "पेन्टिक्टन म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Penticton",
      "hct": "पेन्टिक्टन"
    }, {
      "ac": "PZE",
      "an": "Penzance Heliport",
      "han": "पेन्ज़ान्स हेलिपोर्ट",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Penzance",
      "hct": "पेन्ज़ान्स"
    }, {
      "ac": "PIA",
      "an": "Greater Peoria",
      "han": "ग्रेटर पियोरिआ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Peoria",
      "hct": "पियोरिआ"
    }, {
      "ac": "PEI",
      "an": "Matecana",
      "han": "माटेकना",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Pereira",
      "hct": "परेरा"
    }, {
      "ac": "PEE",
      "an": "Perm",
      "han": "पेर्म",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Perm",
      "hct": "पेर्म"
    }, {
      "ac": "PGF",
      "an": "Llabanere",
      "han": "लबानेरे",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Perpignan",
      "hct": "पर्पिज्ञान"
    }, {
      "ac": "KPV",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Perryville",
      "hct": "पेरीविले"
    }, {
      "ac": "PEG",
      "an": "Sant Egidio",
      "han": "संत एगिडियो",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Perugia",
      "hct": "पेरुगिआ"
    }, {
      "ac": "PSR",
      "an": "Liberi",
      "han": "लिबेरी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Pescara",
      "hct": "पेस्कारा"
    }, {
      "ac": "PEW",
      "an": "Peshawar",
      "han": "पेशावर",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Peshawar",
      "hct": "पेशावर"
    }, {
      "ac": "PSG",
      "an": "Petersburg Municipal",
      "han": "पेटर्स्बूरग म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Petersburg",
      "hct": "पेटर्स्बूरग"
    }, {
      "ac": "PNZ",
      "an": "Senador Nilo Coelho",
      "han": "सेनडोर नीलो कोएल्हो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Petrolina",
      "hct": "पेट्रोलीना"
    }, {
      "ac": "PPK",
      "an": "Petropavlosk South",
      "han": "पेट्रोपाव्लोस्क साउथ",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Petropavlosk",
      "hct": "पेट्रोपाव्लोस्क"
    }, {
      "ac": "PKC",
      "an": "Petropavlovsk Kamchatskiy",
      "han": "पेट्रोपाव्लोव्स्क कम्च्ट्स्किय",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Petropavlovsk",
      "hct": "पेट्रोपाव्लोव्स्क"
    }, {
      "ac": "PES",
      "an": "Petrozavodsk",
      "han": "पेट्रोज़वोड्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Petrozavodsk",
      "hct": "पेट्रोज़वोड्स्क"
    }, {
      "ac": "PWE",
      "an": "",
      "han": "",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Pevek",
      "hct": "पेवेक"
    }, {
      "ac": "PHW",
      "an": "Phalaborwa",
      "han": "फलबोर्वा",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Phalaborwa",
      "hct": "फलबोर्वा"
    }, {
      "ac": "PPL",
      "an": "Phaplu",
      "han": "फाप्लू",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Phaplu",
      "hct": "फाप्लू"
    }, {
      "ac": "PHS",
      "an": "Phitsanulok",
      "han": "फित्सानुलोक",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Phitsanulok",
      "hct": "फित्सानुलोक"
    }, {
      "ac": "PQC",
      "an": "Duong Dong",
      "han": "दुओन्ग डोंग",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Phu Quoc",
      "hct": "फू क़ुओक"
    }, {
      "ac": "POS",
      "an": "Piarco",
      "han": "प्यारको",
      "cn": "Trinidad and Tobago",
      "hcn": "ट्रिनिडाद एण्द टोबगो",
      "cc": "TT",
      "ct": "Port Of Spain Trinidad",
      "hct": "प़ॉर्ट ऑफ़ स्पैन ट्रिनिडाद"
    }, {
      "ac": "YPL",
      "an": "Pickle Lake",
      "han": "पिक्ले लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Pickle Lake",
      "hct": "पिक्ळे लेक"
    }, {
      "ac": "PIX",
      "an": "Pico",
      "han": "पिको",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Pico",
      "hct": "पिको"
    }, {
      "ac": "PDS",
      "an": "Piedras Negras",
      "han": "पीद्रेस नेग्रस",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Piedras Negras",
      "hct": "पीद्रेस नेग्रस"
    }, {
      "ac": "PIR",
      "an": "Pierre Municipal",
      "han": "पिएर म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pierre",
      "hct": "पिएर"
    }, {
      "ac": "PZB",
      "an": "Pietermaritzburg",
      "han": "पीटर्मारिट्ज़्बूरग",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Pietermaritzburg",
      "hct": "पीटर्मारिट्ज़्बूरग"
    }, {
      "ac": "YPM",
      "an": "Pikangikum",
      "han": "पिकंगिकूम",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Pikangikum",
      "hct": "पिकंगिकूम"
    }, {
      "ac": "PIW",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Pikwitonei",
      "hct": "पिक्विटोनी"
    }, {
      "ac": "PIP",
      "an": "Pilot Point",
      "han": "पायलट पोईंत",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pilot Point",
      "hct": "पायलट पोईंत"
    }, {
      "ac": "UGB",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pilot Point",
      "hct": "पायलट पोईंत"
    }, {
      "ac": "PQS",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pilot Station",
      "hct": "पायलट स्टेशन"
    }, {
      "ac": "PIF",
      "an": "Pingtung South",
      "han": "पींग्टुन्ग साउथ",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Pingtung",
      "hct": "पींग्टुन्ग"
    }, {
      "ac": "PSA",
      "an": "Gal Galilei",
      "han": "गल गलिली",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Pisa",
      "hct": "पिसा"
    }, {
      "ac": "PIO",
      "an": "Pisco",
      "han": "पिस्को",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Pisco",
      "hct": "पिस्को"
    }, {
      "ac": "PIU",
      "an": "Capitan Fap Guillermo Concha Iberico",
      "han": "कापीतान फ़प गुइलर्मो कोन्चा इबेरिको",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Piura",
      "hct": "पीऊरा"
    }, {
      "ac": "PTU",
      "an": "Platinum",
      "han": "प्लटीनम",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Moller",
      "hct": "प़ॉर्ट मोलर"
    }, {
      "ac": "PBG",
      "an": "Plattsburgh",
      "han": "प्लत्स्बूरघ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Plattsburgh",
      "hct": "प्लत्स्बूरघ"
    }, {
      "ac": "PXU",
      "an": "Pleiku",
      "han": "प्लीकू",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Pleiku",
      "hct": "प्लीकू"
    }, {
      "ac": "PLH",
      "an": "Roborough",
      "han": "रोबोरौघ",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Plymouth",
      "hct": "प्ल्य्मौठ"
    }, {
      "ac": "PIH",
      "an": "Pocatello Municipal",
      "han": "पोकटेलो म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pocatello",
      "hct": "पोकटेलो"
    }, {
      "ac": "TGD",
      "an": "Golubovci",
      "han": "गोलुबोव्की",
      "cn": "Montenegro",
      "hcn": "मोन्टनेग्रो",
      "cc": "ME",
      "ct": "PODGORICA",
      "hct": "पोड्गोरिका"
    }, {
      "ac": "KPO",
      "an": "Pohang",
      "han": "पोहंग",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Pohang",
      "hct": "पोहंग"
    }, {
      "ac": "KPB",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Point Baker",
      "hct": "पोईंत बकर"
    }, {
      "ac": "PHO",
      "an": "Point Hope",
      "han": "पोईंत होप",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Point Hope",
      "hct": "पोईंत होप"
    }, {
      "ac": "PIZ",
      "an": "Point Lay Lrrs",
      "han": "पोईंत लै ल्र्र्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Point Lay",
      "hct": "पोईंत लै"
    }, {
      "ac": "PIS",
      "an": "Biard",
      "han": "बिआर्ड",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Poitiers",
      "hct": "पोईतिएर्ज़"
    }, {
      "ac": "PKR",
      "an": "Pokhara",
      "han": "पोखरा",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Pokhara",
      "hct": "पोखरा"
    }, {
      "ac": "PTG",
      "an": "Pietersburg",
      "han": "पीटर्स्बूरग",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Polokwane",
      "hct": "पोलोकवाणे"
    }, {
      "ac": "PYJ",
      "an": "Poliarny",
      "han": "पोलिर्नी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Yakutia",
      "hct": "याकूतिआ"
    }, {
      "ac": "YIO",
      "an": "Pond Inlet",
      "han": "पोन्द इनलेट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Pond Inlet",
      "hct": "पोन्द इनलेट"
    }, {
      "ac": "PDL",
      "an": "Nordela",
      "han": "नोर्डेला",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Ponta Delgada",
      "hct": "पोन्ता देल्गाडा"
    }, {
      "ac": "PNK",
      "an": "Supadio",
      "han": "सुपदिओ",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Pontianak",
      "hct": "पोन्टिआनक"
    }, {
      "ac": "PPN",
      "an": "Guillermo Leon Valencia",
      "han": "गुइलर्मो लियोन वालेनसिया",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Popayan",
      "hct": "पोपायान"
    }, {
      "ac": "YHP",
      "an": "Poplar Hill",
      "han": "पोपलर हील",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Poplar Hill",
      "hct": "पोपलर हील"
    }, {
      "ac": "PNP",
      "an": "Girua",
      "han": "गीरुआ",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Popondetta",
      "hct": "पोपोन्डेता"
    }, {
      "ac": "TAT",
      "an": "Tatry",
      "han": "तत्री",
      "cn": "Slovakia",
      "hcn": "स्लोवकिआ",
      "cc": "SK",
      "ct": "Poprad",
      "hct": "पोप्रेद"
    }, {
      "ac": "POR",
      "an": "Pori",
      "han": "पोरी",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Pori",
      "hct": "पोरी"
    }, {
      "ac": "PMV",
      "an": "Delcaribe Gen S Marino",
      "han": "देल्करिबे जन एस मरीनो",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Porlamar",
      "hct": "पोर्लामर"
    }, {
      "ac": "YPB",
      "an": "Port Alberni",
      "han": "प़ॉर्ट आल्बेरनी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Port Alberni",
      "hct": "प़ॉर्ट आल्बेरनी"
    }, {
      "ac": "PTA",
      "an": "Port Alsworth",
      "han": "प़ॉर्ट अल्स्वर्थ",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Alsworth",
      "hct": "प़ॉर्ट अल्स्वर्थ"
    }, {
      "ac": "CLM",
      "an": "William Fairchild",
      "han": "विलियम फेअरचाइल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Angeles",
      "hct": "प़ॉर्ट अंजेल्स"
    }, {
      "ac": "KPY",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Bailey",
      "hct": "प़ॉर्ट बेली"
    }, {
      "ac": "KPC",
      "an": "Port Clarence Coast Guard Station",
      "han": "प़ॉर्ट क्लारेन्स कोस्ट गार्ड स्टेशन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Clarence",
      "hct": "प़ॉर्ट क्लारेन्स"
    }, {
      "ac": "PLZ",
      "an": "Port Elizabeth",
      "han": "प़ॉर्ट एलिझाबेथ",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Port Elizabeth",
      "hct": "प़ॉर्ट एलिझाबेथ"
    }, {
      "ac": "POG",
      "an": "Port Gentil",
      "han": "प़ॉर्ट गेन्तिल",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Port Gentil",
      "hct": "प़ॉर्ट गेन्तिल"
    }, {
      "ac": "PGM",
      "an": "Port Graham",
      "han": "प़ॉर्ट ग्राहम",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Graham",
      "hct": "प़ॉर्ट ग्राहम"
    }, {
      "ac": "YZT",
      "an": "Port Hardy Municipal",
      "han": "प़ॉर्ट हार्डी म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Port Hardy",
      "hct": "प़ॉर्ट हार्डी"
    }, {
      "ac": "PHE",
      "an": "Port Hedland",
      "han": "प़ॉर्ट हेड्लांद",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Port Hedland",
      "hct": "प़ॉर्ट हेड्लांद"
    }, {
      "ac": "PTH",
      "an": "Port Heiden",
      "han": "प़ॉर्ट हेइड्न",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Heiden",
      "hct": "प़ॉर्ट हेइड्न"
    }, {
      "ac": "YHA",
      "an": "Port Hope Simpson",
      "han": "प़ॉर्ट होप सिम्पसन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Port Hope Simpson",
      "hct": "प़ॉर्ट होप सिम्पसन"
    }, {
      "ac": "PLO",
      "an": "Port Lincoln",
      "han": "प़ॉर्ट लिंकन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Port Lincoln",
      "hct": "प़ॉर्ट लिंकन"
    }, {
      "ac": "ORI",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Lions",
      "hct": "प़ॉर्ट लियोन्स"
    }, {
      "ac": "PQQ",
      "an": "Port Macquarie",
      "han": "प़ॉर्ट मेक्क़ुआरी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Port Macquarie",
      "hct": "प़ॉर्ट मेक्क़ुआरी"
    }, {
      "ac": "PML",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Moller",
      "hct": "प़ॉर्ट मोलर"
    }, {
      "ac": "POM",
      "an": "Jackson Field",
      "han": "जैकसन फ़ील्ड",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Port Moresby",
      "hct": "प़ॉर्ट मोरेस्बी"
    }, {
      "ac": "PPV",
      "an": "Port Protection",
      "han": "प़ॉर्ट प्रोटेक्शन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Protection",
      "hct": "प़ॉर्ट प्रोटेक्शन"
    }, {
      "ac": "VLI",
      "an": "Bauerfield",
      "han": "बौयरफ़ील्ड",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Port Vila",
      "hct": "प़ॉर्ट वीला"
    }, {
      "ac": "KPR",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Port Williams",
      "hct": "प़ॉर्ट विलियम्स"
    }, {
      "ac": "PCA",
      "an": "Ingeniero Juan Guillermo Villasana",
      "han": "इंगेनिएरो जुआन गुइलर्मो वीलासना",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Pachuca",
      "hct": "पचुका"
    }, {
      "ac": "POA",
      "an": "Porto Alegre",
      "han": "पोर्टो आलेग्रे",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Porto Alegre",
      "hct": "पोर्टो आलेग्रे"
    }, {
      "ac": "PXO",
      "an": "Porto Santo",
      "han": "पोर्टो संतो",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Porto Santo",
      "hct": "पोर्टो संतो"
    }, {
      "ac": "BPS",
      "an": "Aeroporto de Porto Seguro",
      "han": "एरोपोर्टो दे पोर्टो सेगूरो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Porto Seguro",
      "hct": "पोर्टो सेगूरो"
    }, {
      "ac": "PVH",
      "an": "Governador Jorge Teixeira De Oliveira",
      "han": "गोवर्नडोर जोर्गे टीक्सेरा दे ऑलिवेयरा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Porto Velho",
      "hct": "पोर्टो वेल्हो"
    }, {
      "ac": "OPO",
      "an": "Porto",
      "han": "पोर्टो",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Porto",
      "hct": "पोर्टो"
    }, {
      "ac": "PME",
      "an": "Portsmouth",
      "han": "पोर्ट्स्मौठ",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Portsmouth",
      "hct": "पोर्ट्स्मौठ"
    }, {
      "ac": "PSM",
      "an": "Pease Tradeport",
      "han": "पीसे ट्रेडप़ॉर्ट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Portsmouth",
      "hct": "पोर्ट्स्मौठ"
    }, {
      "ac": "PSS",
      "an": "Posadas",
      "han": "पोसदास",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Posadas",
      "hct": "पोसदास"
    }, {
      "ac": "YSO",
      "an": "Postville",
      "han": "पोस्टविले",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Postville",
      "hct": "पोस्टविले"
    }, {
      "ac": "YPX",
      "an": "Povungnituk",
      "han": "पोवुन्ग्निटुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Povungnituk",
      "hct": "पोवुन्ग्निटुक"
    }, {
      "ac": "YPW",
      "an": "Westview",
      "han": "वॅस्टव्यु",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Powell River",
      "hct": "पोवेल रिवर"
    }, {
      "ac": "PAZ",
      "an": "Tajin",
      "han": "ताजिन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Poza Rica",
      "hct": "पोज़ा रिका"
    }, {
      "ac": "POZ",
      "an": "Lawica",
      "han": "लविका",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Poznan",
      "hct": "पोज़नान"
    }, {
      "ac": "RAI",
      "an": "Francisco Mendes",
      "han": "फ़्रान्सिस्को मेंडेस",
      "cn": "Cape Verde",
      "hcn": "केप‌ व‌र्द",
      "cc": "CV",
      "ct": "Praia",
      "hct": "प्रया"
    }, {
      "ac": "PRI",
      "an": "Praslin",
      "han": "प्रास्लीन",
      "cn": "Seychelles",
      "hcn": "सीचेल्स",
      "cc": "SC",
      "ct": "Praslin",
      "hct": "प्रास्लीन"
    }, {
      "ac": "PRC",
      "an": "Prescott Municipal",
      "han": "प्रेस्कोत म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Prescott",
      "hct": "प्रेस्कोत"
    }, {
      "ac": "PPB",
      "an": "Presidente Prudente",
      "han": "प्रेसिडेंटे प्रुडेंटे",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "President Prudente",
      "hct": "प्रेसीडेंट प्रुडेंटे"
    }, {
      "ac": "PQI",
      "an": "Northern Maine Regional",
      "han": "नोर्ठर्न मायन रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Presque Isle",
      "hct": "प्रेस्क़ इसले"
    }, {
      "ac": "PVK",
      "an": "Aktio",
      "han": "अक्तियो",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Preveza",
      "hct": "प्रेवेज़ा"
    }, {
      "ac": "YXS",
      "an": "Prince George Municipal",
      "han": "प्रिंस जॉर्ज म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Prince George",
      "hct": "प्रिंस जॉर्ज"
    }, {
      "ac": "YPR",
      "an": "Digby Island",
      "han": "डिग्बी आयलॅन्ड",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Prince Rupert",
      "hct": "प्रिंस रुपेर्ट"
    }, {
      "ac": "PCP",
      "an": "Principe",
      "han": "प्रिन्सिपे",
      "cn": "Sao Tome and Principe",
      "hcn": "साव टोम एण्द प्रिन्सिपे",
      "cc": "ST",
      "ct": "Principe",
      "hct": "प्रिन्सिपे"
    }, {
      "ac": "PRN",
      "an": "Pristina",
      "han": "प्रिस्टिना",
      "cn": "Serbia",
      "hcn": "सेरबिया",
      "cc": "CS",
      "ct": "Pristina",
      "hct": "प्रिस्टिना"
    }, {
      "ac": "PPP",
      "an": "Proserpine Whitsunday Coast",
      "han": "प्रोस्र्पिन व्हित्सुनदय कोस्ट",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Prosserpine",
      "hct": "प्रोस्स्र्पिन"
    }, {
      "ac": "PVD",
      "an": "T F Green St",
      "han": "टी एफ़ ग्रीन स्त्रीट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Providence",
      "hct": "प्रोवीडन्स"
    }, {
      "ac": "PVA",
      "an": "El Embrujo",
      "han": "एल एंब्रुजो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Providencia",
      "hct": "प्रोवीडनसिया"
    }, {
      "ac": "PVC",
      "an": "Provincetown",
      "han": "प्रोविन्सेटौन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Provincetown",
      "hct": "प्रोविन्सेटौन"
    }, {
      "ac": "SCC",
      "an": "Deadhorse",
      "han": "दीढोर्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Deadhorse",
      "hct": "दीढोर्स"
    }, {
      "ac": "PKV",
      "an": "Kresty",
      "han": "क्रेस्टी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Pskov",
      "hct": "प्स्कोव"
    }, {
      "ac": "PCL",
      "an": "Capitan Rolden",
      "han": "कापीतान रोल्द्न",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Pucallpa",
      "hct": "पुकाल्पा"
    }, {
      "ac": "ZPC",
      "an": "Pucan",
      "han": "पुकन",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Pucon",
      "hct": "पुकोन"
    }, {
      "ac": "PBC",
      "an": "Huejostingo",
      "han": "हुएजोस्टिंगो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Puebla",
      "hct": "पुएब्ला"
    }, {
      "ac": "PUB",
      "an": "Pueblo",
      "han": "पुएब्लो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pueblo",
      "hct": "पुएब्लो"
    }, {
      "ac": "PUU",
      "an": "Tres De Mayo",
      "han": "ट्रेस दे मेयो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Puerto Asis",
      "hct": "पुएर्टो आशीष"
    }, {
      "ac": "PYH",
      "an": "Casique Aramare",
      "han": "कासिक़ आरमारे",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Puerto Ayacucho",
      "hct": "पुएर्टो आयकुचो"
    }, {
      "ac": "PCR",
      "an": "Puerto Carreno",
      "han": "पुएर्टो कार्रेनो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Puerto Carreno",
      "hct": "पुएर्टो कार्रेनो"
    }, {
      "ac": "PXM",
      "an": "Puerto Escondido Municipal",
      "han": "पुएर्टो एस्कोंडिडो म्यूनिसिपल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Puerto Escondido",
      "hct": "पुएर्टो एस्कोंडिडो"
    }, {
      "ac": "PDA",
      "an": "Puerto Inirida",
      "han": "पुएर्टो इनिरिदा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Puerto Inirida",
      "hct": "पुएर्टो इनिरिदा"
    }, {
      "ac": "LQM",
      "an": "",
      "han": "",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Puerto Leguizamo",
      "hct": "पुएर्टो लेगुइज़ामो"
    }, {
      "ac": "PMY",
      "an": "El Tehuelche",
      "han": "एल तेहुएल्चे",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Puerto Madryn",
      "hct": "पुएर्टो मद्र्य्न"
    }, {
      "ac": "PEM",
      "an": "Padre Aldamiz",
      "han": "पद्रे अल्दामिज़",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Puerto Maldonado",
      "hct": "पुएर्टो माल्डोनडो"
    }, {
      "ac": "PMC",
      "an": "Tepual",
      "han": "टेपुआल",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Puerto Montt",
      "hct": "पुएर्टो मोन्त"
    }, {
      "ac": "PZO",
      "an": "Puerto Ordaz",
      "han": "पुएर्टो ओर्डज़",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Puerto Ordaz",
      "hct": "पुएर्टो ओर्डज़"
    }, {
      "ac": "PPE",
      "an": "Puerto Penasco",
      "han": "पुएर्टो पेनास्को",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Punta Penasco",
      "hct": "पंता पेनास्को"
    }, {
      "ac": "PPS",
      "an": "Puerto Princesa",
      "han": "पुएर्टो प्रिन्सेसा",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Puerto Princesa",
      "hct": "पुएर्टो प्रिन्सेसा"
    }, {
      "ac": "PSZ",
      "an": "Tte De Av Salvador Ogaya G",
      "han": "ते दे अव साल्वेदोर ओगय जी",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Puerto Suarez",
      "hct": "पुएर्टो सुआरेज़"
    }, {
      "ac": "PVR",
      "an": "Ordaz",
      "han": "ओर्डज़",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Puerto Vallarta",
      "hct": "पुएर्टो वलर्ता"
    }, {
      "ac": "PUY",
      "an": "Pula",
      "han": "पुला",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Pula",
      "hct": "पुला"
    }, {
      "ac": "PUW",
      "an": "Pullman Moscow",
      "han": "पूलल्मॅन मोस्को",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Pullman",
      "hct": "पूलल्मॅन"
    }, {
      "ac": "PUQ",
      "an": "Presidente Ibanez",
      "han": "प्रेसिडेंटे इबनेज़",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Punta Arenas",
      "hct": "पंता आरेनास"
    }, {
      "ac": "PDP",
      "an": "Capitan Corbeta C A Curbelo",
      "han": "कापीतान कोर्बेता सी ए कूरबेलो",
      "cn": "Uruguay",
      "hcn": "उरुग्वाय",
      "cc": "UY",
      "ct": "Punta del Este",
      "hct": "पंता देल एस्ते"
    }, {
      "ac": "PBU",
      "an": "Putao",
      "han": "पुताव",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Putao",
      "hct": "पुताव"
    }, {
      "ac": "FNJ",
      "an": "Pyongyang",
      "han": "प्योन्ग्यांग",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Pyongyang",
      "hct": "प्योन्ग्यांग"
    }, {
      "ac": "NAQ",
      "an": "Qaanaaq",
      "han": "क़ानाक़",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Qaanaaq",
      "hct": "क़ानाक़"
    }, {
      "ac": "JQA",
      "an": "Qaarsut",
      "han": "क़ार्सुत",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Uummannaq",
      "hct": "ऊम्मन्नक़"
    }, {
      "ac": "AQI",
      "an": "Qaisumah",
      "han": "क़ैसूमाह",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Hafr Al",
      "hct": "हाफ़्र अल"
    }, {
      "ac": "JCH",
      "an": "Qasigiannguit",
      "han": "क़ासिग्यान्न्गुइत",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Qasigiannguit",
      "hct": "क़ासिग्यान्न्गुइत"
    }, {
      "ac": "JGO",
      "an": "Qeqertarsuaq Heliport",
      "han": "क़ेक़ेर्तर्सुआक़ हेलिपोर्ट",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Qeqertarsuaq Airport",
      "hct": "क़ेक़ेर्तर्सुआक़ एअरप़ॉर्ट"
    }, {
      "ac": "IQM",
      "an": "Qiemo",
      "han": "क़ीमो",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Qiemo",
      "hct": "क़ीमो"
    }, {
      "ac": "YVM",
      "an": "Qikiqtarjuaq",
      "han": "क़िकिक़्टर्जुआक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Broughton Island",
      "hct": "ब्रौघ्टन आयलॅन्ड"
    }, {
      "ac": "TAO",
      "an": "Liuting",
      "han": "लीऊतिंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Qingdao",
      "hct": "क़िंग्डाव"
    }, {
      "ac": "IQN",
      "an": "Qingyang",
      "han": "क़िंग्यांग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Qingyang",
      "hct": "क़िंग्यांग"
    }, {
      "ac": "SHP",
      "an": "Shanhaiguan",
      "han": "शान्हैगुआन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Qinhuangdao",
      "hct": "क़िन्हुआंग्डाव"
    }, {
      "ac": "NDG",
      "an": "Qiqihar Sanjiazi",
      "han": "क़िक़िहर सन्जियाज़ी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Qiqihar",
      "hct": "क़िक़िहर"
    }, {
      "ac": "XQU",
      "an": "Qualicum",
      "han": "क़ालिकम",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Qualicum",
      "hct": "क़ालिकम"
    }, {
      "ac": "JJN",
      "an": "Quanzhou",
      "han": "क़ान्झौ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Quanzhou",
      "hct": "क़ान्झ़ौ"
    }, {
      "ac": "YQC",
      "an": "Quaqtaq",
      "han": "क़क़्टाक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Quaqtaq",
      "hct": "क़क़्टाक़"
    }, {
      "ac": "XFZ",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Quebec",
      "hct": "क़ेबेक"
    }, {
      "ac": "YQB",
      "an": "Quebec",
      "han": "क़ेबेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Quebec",
      "hct": "क़ेबेक"
    }, {
      "ac": "ZQN",
      "an": "Frankton",
      "han": "फ़्रान्क्टन",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Queenstown",
      "hct": "क़्न्स्टौन"
    }, {
      "ac": "QRO",
      "an": "Queretaro",
      "han": "क़्रेतारो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Queretaro",
      "hct": "क़्रेतारो"
    }, {
      "ac": "YQZ",
      "an": "Quesnel",
      "han": "क़्स्नेल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Quesnel",
      "hct": "क़्स्नेल"
    }, {
      "ac": "UET",
      "an": "Quetta",
      "han": "क़्ता",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Quetta",
      "hct": "क़्ता"
    }, {
      "ac": "UIH",
      "an": "Phu Cat",
      "han": "फू कॅट",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Phucat",
      "hct": "फुकॅट"
    }, {
      "ac": "UIB",
      "an": "Quibdo",
      "han": "क़ुइब्डो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Quibdo",
      "hct": "क़ुइब्डो"
    }, {
      "ac": "ULP",
      "an": "Quilpie",
      "han": "क़ुइल्पी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Quilpie ",
      "hct": "क़ुइल्पी "
    }, {
      "ac": "UIP",
      "an": "Pluguffan",
      "han": "प्लुगुफन",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Quimper",
      "hct": "क़ुइमपाड़"
    }, {
      "ac": "UIN",
      "an": "Baldwin Field",
      "han": "बाल्‌ड्विन फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Quincy",
      "hct": "क़ुइन्सी"
    }, {
      "ac": "KWN",
      "an": "Kwinhagak",
      "han": "क्विन्हगक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Quinhagak",
      "hct": "क़ुइन्हगक"
    }, {
      "ac": "UIO",
      "an": "Mariscal",
      "han": "मारिसकल",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Quito",
      "hct": "क़ुइटो"
    }, {
      "ac": "RBA",
      "an": "Sale",
      "han": "सेल",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Rabat",
      "hct": "रबत"
    }, {
      "ac": "RAB",
      "an": "Lakunai",
      "han": "लकूनै",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Rabaul",
      "hct": "रबौल"
    }, {
      "ac": "VKG",
      "an": "Rach Gia",
      "han": "रच गिआ",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Rach Gia",
      "hct": "रच गिआ"
    }, {
      "ac": "YRA",
      "an": "Rae Lakes",
      "han": "राए लकेस",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Rae Lakes",
      "hct": "राए लकेस"
    }, {
      "ac": "RAH",
      "an": "Rafha",
      "han": "राफ़्हा",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Rafha",
      "hct": "राफ़्हा"
    }, {
      "ac": "RJN",
      "an": "Rafsanjan",
      "han": "रफ़संजन",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Rafsanjan",
      "hct": "रफ़संजन"
    }, {
      "ac": "RYK",
      "an": "Sheikh Zayed",
      "han": "शेख ज़येद",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Rahim Yar Khan",
      "hct": "रहीम यार खान"
    }, {
      "ac": "YOP",
      "an": "Rainbow Lake",
      "han": "रेनबो लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Rainbow Lake",
      "hct": "रेनबो लेक"
    }, {
      "ac": "RBV",
      "an": "Ramata",
      "han": "रमाता",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Ramata",
      "hct": "रमाता"
    }, {
      "ac": "RMP",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rampart",
      "hct": "रांपार्ट"
    }, {
      "ac": "RZR",
      "an": "Ramsar",
      "han": "रामसर",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Ramsar",
      "hct": "रामसर"
    }, {
      "ac": "YRT",
      "an": "Rankin Inlet",
      "han": "रणकिन इनलेट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Rankin Inlet",
      "hct": "रणकिन इनलेट"
    }, {
      "ac": "UNN",
      "an": "Ranong",
      "han": "रनोन्ग",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Ranong",
      "hct": "रनोन्ग"
    }, {
      "ac": "RAP",
      "an": "Rapid City Regional",
      "han": "रेपिड सिटी रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rapid City",
      "hct": "रेपिड सिटी"
    }, {
      "ac": "RAR",
      "an": "Rarotonga",
      "han": "रेरोटोन्गा",
      "cn": "Cook Islands",
      "hcn": "कूक आयलॅन्ड्स",
      "cc": "CK",
      "ct": "Rarotonga",
      "hct": "रेरोटोन्गा"
    }, {
      "ac": "RKT",
      "an": "Ras Al Khaimah",
      "han": "रेस अल खैमाह",
      "cn": "United Arab Emirates",
      "hcn": "संयुक्त‌ अर‌ब‌ अमीरात‌",
      "cc": "AE",
      "ct": "Ras Al Khaimah",
      "hct": "रेस अल खैमाह"
    }, {
      "ac": "RAS",
      "an": "Rasht",
      "han": "राश्ट",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Rasht",
      "hct": "राश्ट"
    }, {
      "ac": "REC",
      "an": "Recife",
      "han": "रेकिफ़े",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Recife",
      "hct": "रेकिफ़े"
    }, {
      "ac": "YQF",
      "an": "Red Deer Regional",
      "han": "रेड दीर रिजनल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Red Deer Industrial",
      "hct": "रेड दीर ईन्दस्ट्रिअल"
    }, {
      "ac": "RDV",
      "an": "Red Devil",
      "han": "रेड देवील",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Red Devil",
      "hct": "रेड देवील"
    }, {
      "ac": "RDB",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Red Dog",
      "hct": "रेड दोग"
    }, {
      "ac": "YRL",
      "an": "Federal Red Lake",
      "han": "फ़ेडेरल रेड लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Red Lake",
      "hct": "रेड लेक"
    }, {
      "ac": "YRS",
      "an": "Red Sucker Lake",
      "han": "रेड सकर लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Red Sucker Lake",
      "hct": "रेड सकर लेक"
    }, {
      "ac": "RDN",
      "an": "Redang",
      "han": "रेदंग",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Redang",
      "hct": "रेदंग"
    }, {
      "ac": "RCL",
      "an": "Redcliffe",
      "han": "रेड्क्लिफ",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Redcliffe",
      "hct": "रेड्क्लिफ"
    }, {
      "ac": "RDD",
      "an": "Redding Municipal",
      "han": "रेड्डींग म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Redding",
      "hct": "रेड्डींग"
    }, {
      "ac": "REG",
      "an": "Tito Menniti",
      "han": "टिटो मेन्नीती",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Reggio Calabria",
      "hct": "रेग्गियो कलब्रिआ"
    }, {
      "ac": "YQR",
      "an": "Regina Municipal",
      "han": "रेजीना म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "REGINA",
      "hct": "रेजीना"
    }, {
      "ac": "RNL",
      "an": "Rennell/Tingoa",
      "han": "रेन्नेल/तीनगोआ",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Rennell Island",
      "hct": "रेन्नेल आयलॅन्ड"
    }, {
      "ac": "RNS",
      "an": "Saint Jacques",
      "han": "सेंट जेक्क़्स",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Rennes",
      "hct": "रेन्न्स"
    }, {
      "ac": "RNO",
      "an": "Reno Tahoe",
      "han": "रेनो ताहोय",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Reno",
      "hct": "रेनो"
    }, {
      "ac": "YUT",
      "an": "Repulse Bay",
      "han": "रेपूलसे बे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Repulse Bay",
      "hct": "रेपूलसे बे"
    }, {
      "ac": "RES",
      "an": "Resistencia",
      "han": "रेसिस्टेनसिया",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Resistencia",
      "hct": "रेसिस्टेनसिया"
    }, {
      "ac": "YRB",
      "an": "Resolute",
      "han": "रेसोलुटे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Resolute",
      "hct": "रेसोलुटे"
    }, {
      "ac": "REU",
      "an": "Reus",
      "han": "रेउस",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Reus",
      "hct": "रेउस"
    }, {
      "ac": "REK",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Reykjavik",
      "hct": "रीक्जावीक"
    }, {
      "ac": "KEF",
      "an": "Reykjavik Keflavik",
      "han": "रीक्जावीक केफ़्लवीक",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Reykjavik",
      "hct": "रीक्जावीक"
    }, {
      "ac": "RKV",
      "an": "Reykjavik Domestic",
      "han": "रीक्जावीक डोमेस्टिक",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Reykjavik",
      "hct": "रीक्जावीक"
    }, {
      "ac": "REX",
      "an": "General Lucio Blanco",
      "han": "जनरल लकियो ब्लान्को",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Reynosa",
      "hct": "रीनोसा"
    }, {
      "ac": "RHI",
      "an": "Oneida County",
      "han": "ओनीडा कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rhinelander",
      "hct": "र्हीनेलांदर"
    }, {
      "ac": "RHO",
      "an": "Diagoras",
      "han": "दिआगोरेस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Rhodes",
      "hct": "र्होड्स"
    }, {
      "ac": "RAO",
      "an": "Leite Lopes",
      "han": "लीटे लोपेस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Ribeirao Preto",
      "hct": "रिबेराव प्रेटो"
    }, {
      "ac": "RIB",
      "an": "Gen Buech",
      "han": "जन बुएच",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Riberalta",
      "hct": "रिबेरल्ता"
    }, {
      "ac": "RCB",
      "an": "Richards Bay",
      "han": "रिचर्ड्स बे",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Richards Bay",
      "hct": "रिचर्ड्स बे"
    }, {
      "ac": "RCM",
      "an": "Richmond",
      "han": "रिच्मोन्द",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Richmond",
      "hct": "रिच्मोन्द"
    }, {
      "ac": "RIC",
      "an": "Richmond",
      "han": "रिच्मोन्द",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Richmond",
      "hct": "रिच्मोन्द"
    }, {
      "ac": "RIX",
      "an": "Riga",
      "han": "रिगा",
      "cn": "Latvia",
      "hcn": "लतविआ",
      "cc": "LV",
      "ct": "Riga",
      "hct": "रिगा"
    }, {
      "ac": "YRG",
      "an": "Rigolet",
      "han": "रिगोलेट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Rigolet",
      "hct": "रिगोलेट"
    }, {
      "ac": "RJK",
      "an": "Rijeka",
      "han": "रिजेका",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Rijeka",
      "hct": "रिजेका"
    }, {
      "ac": "RMI",
      "an": "Rimini",
      "han": "रिमिनी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Rimini",
      "hct": "रिमिनी"
    }, {
      "ac": "RBR",
      "an": "Presidente Medici",
      "han": "प्रेसिडेंटे मेडिसी",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Rio Branco",
      "hct": "रिओ ब्रान्को"
    }, {
      "ac": "RGL",
      "an": "Rio Gallegos Internacional",
      "han": "रिओ गलेगोस इंटर्नेसियोनल",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Rio Gallegos",
      "hct": "रिओ गलेगोस"
    }, {
      "ac": "RGA",
      "an": "Rio Grande",
      "han": "रिओ ग्रांडे",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Rio Grande",
      "hct": "रिओ ग्रांडे"
    }, {
      "ac": "RIG",
      "an": "Rio Grande",
      "han": "रिओ ग्रांडे",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Rio Grande",
      "hct": "रिओ ग्रांडे"
    }, {
      "ac": "RCH",
      "an": "Riohacha",
      "han": "रिओहचा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Riohacha",
      "hct": "रिओहचा"
    }, {
      "ac": "RIS",
      "an": "Rishiri",
      "han": "रिशीरी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Rishiri Island",
      "hct": "रिशीरी आयलॅन्ड"
    }, {
      "ac": "RIW",
      "an": "Riverton",
      "han": "रिवर्टन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Riverton",
      "hct": "रिवर्टन"
    }, {
      "ac": "RIY",
      "an": "Riyan",
      "han": "रियान",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Mukalla",
      "hct": "मूकअला"
    }, {
      "ac": "ROA",
      "an": "Roanoke Regional",
      "han": "रोआनोके रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Roanoke",
      "hct": "रोआनोके"
    }, {
      "ac": "YRJ",
      "an": "Roberval",
      "han": "रोबेरवल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Roberval",
      "hct": "रोबेरवल"
    }, {
      "ac": "RCE",
      "an": "Roche Harbor",
      "han": "रोश हरबोर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Roche Harbor",
      "hct": "रोश हरबोर"
    }, {
      "ac": "ROC",
      "an": "Monroe Cty New York",
      "han": "मोनरो क्टी न्यू यार्क",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rochester",
      "hct": "रोचेस्टर"
    }, {
      "ac": "RST",
      "an": "Rochester Municipal",
      "han": "रोचेस्टर म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rochester (MN)",
      "hct": "रोचेस्टर (म्न)"
    }, {
      "ac": "RKS",
      "an": "Rock Springs Municipal",
      "han": "रॉक स्प्रिंग्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rock Springs",
      "hct": "रॉक स्प्रिंग्स"
    }, {
      "ac": "RFD",
      "an": "Chicago Rockford ",
      "han": "शिकागो रॉकफोर्ड ",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rockford",
      "hct": "रॉकफोर्ड"
    }, {
      "ac": "ROK",
      "an": "Rockhampton",
      "han": "रोखंप्टन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Rockhampton",
      "hct": "रोखंप्टन"
    }, {
      "ac": "RKD",
      "an": "Rockland",
      "han": "रॉकलॅंद",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rockland",
      "hct": "रॉकलॅंद"
    }, {
      "ac": "RDZ",
      "an": "Marcillac",
      "han": "मार्किलाक",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Rodez",
      "hct": "रोडेज़"
    }, {
      "ac": "RRG",
      "an": "Rodrigues Island",
      "han": "रॉद्रिग्स आयलॅन्ड",
      "cn": "Mauritius",
      "hcn": "मोरिशिय‌स‌",
      "cc": "MU",
      "ct": "Rodrigues Is",
      "hct": "रॉद्रिग्स इस"
    }, {
      "ac": "RVK",
      "an": "Ryumsjoen",
      "han": "र्युम्स्जोयन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Roervik",
      "hct": "रोयर्वीक"
    }, {
      "ac": "ROI",
      "an": "Roi Et",
      "han": "रोई एत",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Roi Et",
      "hct": "रोई एत"
    }, {
      "ac": "RMA",
      "an": "Roma",
      "han": "रोमा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Roma",
      "hct": "रोमा"
    }, {
      "ac": "ROO",
      "an": "Rondonopolis",
      "han": "रोन्दोनोपोलिस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Rondonopolis",
      "hct": "रोन्दोनोपोलिस"
    }, {
      "ac": "RNP",
      "an": "Rongelap Island",
      "han": "रोन्गेलप आयलॅन्ड",
      "cn": "Marshall Islands",
      "hcn": "मार्शल आयलॅन्ड्स",
      "cc": "MH",
      "ct": "Rongelap Island",
      "hct": "रोन्गेलप आयलॅन्ड"
    }, {
      "ac": "RNB",
      "an": "Kallinge",
      "han": "कललींगे",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Ronneby",
      "hct": "रोन्नेबी"
    }, {
      "ac": "RRS",
      "an": "Roros",
      "han": "रोरोस",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Roros",
      "hct": "रोरोस"
    }, {
      "ac": "ROS",
      "an": "Fisherton",
      "han": "फ़िशेर्टन",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Rosario",
      "hct": "रोसारिओ"
    }, {
      "ac": "RSJ",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rosario",
      "hct": "रोसारिओ"
    }, {
      "ac": "RET",
      "an": "Stolport",
      "han": "स्टोल्पोर्ट",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Rost",
      "hct": "रोस्त"
    }, {
      "ac": "RLG",
      "an": "Laage",
      "han": "लागे",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Laage",
      "hct": "लागे"
    }, {
      "ac": "ROV",
      "an": "Rostov",
      "han": "रोस्टोव",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Rostov",
      "hct": "रोस्टोव"
    }, {
      "ac": "ROW",
      "an": "Industrial Aircenter",
      "han": "ईन्दस्ट्रिअल एअरसेन्टर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Roswell",
      "hct": "रोस्वेल"
    }, {
      "ac": "ROP",
      "an": "Rota",
      "han": "रोटा",
      "cn": "Northern Mariana Islands",
      "hcn": "नोर्ठर्न मर्यना आयलॅन्ड्स",
      "cc": "MP",
      "ct": "Rota",
      "hct": "रोटा"
    }, {
      "ac": "ROT",
      "an": "Rotorua",
      "han": "रोटरुआ",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Rotorua",
      "hct": "रोटरुआ"
    }, {
      "ac": "RTM",
      "an": "Rotterdam",
      "han": "रोतर्दम",
      "cn": "Netherlands",
      "hcn": "नेद‌र‌लैंड‌",
      "cc": "NL",
      "ct": "Rotterdam",
      "hct": "रोतर्दम"
    }, {
      "ac": "RTA",
      "an": "Rotuma",
      "han": "रोटुमा",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Rotuma",
      "hct": "रोटुमा"
    }, {
      "ac": "URO",
      "an": "Boos",
      "han": "बूस",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Rouen",
      "hct": "रौएन"
    }, {
      "ac": "ZRJ",
      "an": "Round Lake (Weagamow Lake)",
      "han": "रौन्ड लेक (वीगमौ लेक)",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Round Lake",
      "hct": "रौन्ड लेक"
    }, {
      "ac": "YUY",
      "an": "Rouyn Noranda",
      "han": "रौय्न नोरंदा",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Rouyn",
      "hct": "रौय्न"
    }, {
      "ac": "RVN",
      "an": "Rovaniemi",
      "han": "रोवाणीएमी",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Rovaniemi",
      "hct": "रोवाणीएमी"
    }, {
      "ac": "RXS",
      "an": "Roxas",
      "han": "रोक्सस",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Roxas City",
      "hct": "रोक्सस सिटी"
    }, {
      "ac": "RBY",
      "an": "Ruby",
      "han": "रूबी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Ruby",
      "hct": "रूबी"
    }, {
      "ac": "RUK",
      "an": "Rukumkot",
      "han": "रुकूम्कोट",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Rukumkot",
      "hct": "रुकूम्कोट"
    }, {
      "ac": "RUM",
      "an": "Rumjatar",
      "han": "रुम्जातार",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Rumjatar",
      "hct": "रुम्जातार"
    }, {
      "ac": "RBQ",
      "an": "Rurrenabaque",
      "han": "रुर्रेनाबाक़",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Rurrenabaque",
      "hct": "रुर्रेनाबाक़"
    }, {
      "ac": "RSH",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Russian Mission",
      "hct": "रसीअन मिशन"
    }, {
      "ac": "RUT",
      "an": "Rutland",
      "han": "रुट्लांद",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Rutland",
      "hct": "रुट्लांद"
    }, {
      "ac": "RZE",
      "an": "Jasionka",
      "han": "जशन्का",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Rzeszow",
      "hct": "र्ज़ेस्ज़ौ"
    }, {
      "ac": "SCN",
      "an": "Saarbrucken",
      "han": "सार्ब्रुकेन",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Saarbruecken",
      "hct": "सार्ब्रुएकेन"
    }, {
      "ac": "SOB",
      "an": "",
      "han": "",
      "cn": "Hungary",
      "hcn": "हुन्गारी",
      "cc": "HU",
      "ct": "Saarmelleek",
      "hct": "सार्मेलीक"
    }, {
      "ac": "ZPB",
      "an": "Sachigo Lake",
      "han": "सचिगो लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sachigo Lake",
      "hct": "सचिगो लेक"
    }, {
      "ac": "YSY",
      "an": "Sachs Harbour",
      "han": "सच्स हर्बौर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sachs Harbour",
      "hct": "सच्स हर्बौर"
    }, {
      "ac": "SCK",
      "an": "Stockton Metro",
      "han": "स्टोक्टन मेट्रो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sacramento",
      "hct": "सक्रामेन्टो"
    }, {
      "ac": "SMF",
      "an": "Sacramento",
      "han": "सक्रामेन्टो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sacramento",
      "hct": "सक्रामेन्टो"
    }, {
      "ac": "HSG",
      "an": "Saga",
      "han": "सागा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Saga",
      "hct": "सागा"
    }, {
      "ac": "ACP",
      "an": "Sahand",
      "han": "सहेंद",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Maragheh",
      "hct": "मरघेह"
    }, {
      "ac": "SBR",
      "an": "Saibai Island",
      "han": "साइबाई आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Saibai Island",
      "hct": "साइबाई आयलॅन्ड"
    }, {
      "ac": "SDT",
      "an": "Saidu Sharif",
      "han": "सैदू शरीफ़",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Saidu Sharif",
      "hct": "सैदू शरीफ़"
    }, {
      "ac": "YCM",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Saint Catharines",
      "hct": "सेंट कथरीन्स"
    }, {
      "ac": "STC",
      "an": "Saint Cloud Municipal",
      "han": "सेंट क्लाउड म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Saint Cloud",
      "hct": "सेंट क्लाउड"
    }, {
      "ac": "RUN",
      "an": "Gillot",
      "han": "गिलोट",
      "cn": "Reunion",
      "hcn": "रेउनियोन",
      "cc": "RE",
      "ct": "St",
      "hct": "स्त्रीट"
    }, {
      "ac": "STG",
      "an": "St George Island",
      "han": "स्त्रीट जॉर्ज आयलॅन्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "St George Island",
      "hct": "स्त्रीट जॉर्ज आयलॅन्ड"
    }, {
      "ac": "SGU",
      "an": "Saint George Municipal",
      "han": "सेंट जॉर्ज म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Saint George",
      "hct": "सेंट जॉर्ज"
    }, {
      "ac": "YSJ",
      "an": "St John Municipal",
      "han": "स्त्रीट जॉन म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "SAINT JOHN",
      "hct": "सेंट जॉन"
    }, {
      "ac": "YYT",
      "an": "St Johns",
      "han": "स्त्रीट जॉन्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "St Johns",
      "hct": "स्त्रीट जॉन्स"
    }, {
      "ac": "XLS",
      "an": "Saint Louis",
      "han": "सेंट लुइस",
      "cn": "Senegal",
      "hcn": "सेनेगल",
      "cc": "SN",
      "ct": "St. Louis",
      "hct": "स्त्रीट. लुइस"
    }, {
      "ac": "KSM",
      "an": "St Marys",
      "han": "स्त्रीट मॅरीस़",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "St Mary's",
      "hct": "स्त्रीट मॅरीएस"
    }, {
      "ac": "SMK",
      "an": "St Michael",
      "han": "स्त्रीट माइकल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "St Michael",
      "hct": "स्त्रीट माइकल"
    }, {
      "ac": "SNP",
      "an": "St Paul Island",
      "han": "स्त्रीट पॉल आयलॅन्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "St. Paul Island",
      "hct": "स्त्रीट. पॉल आयलॅन्ड"
    }, {
      "ac": "SMS",
      "an": "Sainte Marie",
      "han": "सैंटे मॅरी",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Sainte Marie",
      "hct": "सैंटे मॅरी"
    }, {
      "ac": "SPN",
      "an": "Saipan",
      "han": "साईपान",
      "cn": "Northern Mariana Islands",
      "hcn": "नोर्ठर्न मर्यना आयलॅन्ड्स",
      "cc": "MP",
      "ct": "Saipan",
      "hct": "साईपान"
    }, {
      "ac": "SNO",
      "an": "Sakon Nakhon",
      "han": "सकोन नाखोन",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Sakon Nakhon",
      "hct": "सकोन नाखोन"
    }, {
      "ac": "SID",
      "an": "Amilcar Cabral",
      "han": "अमिलकार काब्राल",
      "cn": "Cape Verde",
      "hcn": "केप‌ व‌र्द",
      "cc": "CV",
      "ct": "Amilcar Cabral",
      "hct": "अमिलकार काब्राल"
    }, {
      "ac": "XYX",
      "an": "",
      "han": "",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Sala",
      "hct": "सेला"
    }, {
      "ac": "SLL",
      "an": "Salalah",
      "han": "सेलालाह",
      "cn": "Oman",
      "hcn": "ओमान",
      "cc": "OM",
      "ct": "Salalah",
      "hct": "सेलालाह"
    }, {
      "ac": "SLM",
      "an": "Salamanca",
      "han": "सलामन्का",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Salamanca",
      "hct": "सलामन्का"
    }, {
      "ac": "SLY",
      "an": "Salekhard",
      "han": "साळेखर्ड",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Salekhard",
      "hct": "सालेखर्ड"
    }, {
      "ac": "SLE",
      "an": "Mcnary Field",
      "han": "म्कनारी फ़ील्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Salem",
      "hct": "सालम"
    }, {
      "ac": "SCX",
      "an": "Salina Cruz Naval Air Station",
      "han": "शालिना क्रुज़ नवल एअर स्टेशन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Salina Cruz",
      "hct": "शालिना क्रुज़"
    }, {
      "ac": "SLN",
      "an": "Salina Municipal",
      "han": "शालिना म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Salina",
      "hct": "शालिना"
    }, {
      "ac": "SNC",
      "an": "General Ulpiano Paez",
      "han": "जनरल उल्पियानो पाएज़",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Salinas",
      "hct": "सालीनस"
    }, {
      "ac": "SBY",
      "an": "Wicomico Regional",
      "han": "विकोमिको रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Salisbury",
      "hct": "सैलिसबरी"
    }, {
      "ac": "YZG",
      "an": "Salluit",
      "han": "सलुइत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Salluit",
      "hct": "सलुइत"
    }, {
      "ac": "SMN",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Salmon",
      "hct": "शलमोन"
    }, {
      "ac": "SLC",
      "an": "Salt Lake City",
      "han": "सल्ट लेक सिटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Salt Lake City",
      "hct": "सल्ट लेक सिटी"
    }, {
      "ac": "SLA",
      "an": "Salta",
      "han": "सल्ता",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Salta",
      "hct": "सल्ता"
    }, {
      "ac": "SLW",
      "an": "Saltillo",
      "han": "सल्टिलो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Saltillo",
      "hct": "सल्टिलो"
    }, {
      "ac": "SSA",
      "an": "Luis E Magalhaes",
      "han": "लुईस इ मगल्हएस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Salvador",
      "hct": "साल्वेदोर"
    }, {
      "ac": "SZG",
      "an": "W A Mozart",
      "han": "डबलयू ए मोज़र्ट",
      "cn": "Austria",
      "hcn": "ऑस्ट्रिया",
      "cc": "AT",
      "ct": "Salzburg",
      "hct": "सेल्ज़्बूरग"
    }, {
      "ac": "KUF",
      "an": "Samara",
      "han": "समरा",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Samara",
      "hct": "समरा"
    }, {
      "ac": "SKD",
      "an": "Samarkand",
      "han": "समार्कंद",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Samarkand",
      "hct": "समार्कंद"
    }, {
      "ac": "SVB",
      "an": "Sambava",
      "han": "सम्बावा",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Sambava",
      "hct": "सम्बावा"
    }, {
      "ac": "UAS",
      "an": "Samburu South",
      "han": "सम्बुरू साउथ",
      "cn": "Kenya",
      "hcn": "केन्या",
      "cc": "KE",
      "ct": "Samburu South",
      "hct": "सम्बुरू साउथ"
    }, {
      "ac": "SMI",
      "an": "Samos",
      "han": "समोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Samos",
      "hct": "समोस"
    }, {
      "ac": "SZF",
      "an": "Samsun",
      "han": "संसन",
      "cn": "Aaramba",
      "hcn": "आरअम्बा",
      "cc": "Turkey",
      "ct": "TR",
      "hct": "ट्र"
    }, {
      "ac": "ADZ",
      "an": "Gustavo Rojas Pinilla",
      "han": "गुस्टवो रोजेस पिनीला",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "San Andres Island",
      "hct": "सेन आन्द्र्स आयलॅन्ड"
    }, {
      "ac": "SJT",
      "an": "Mathis Field",
      "han": "मथीस फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Angelo",
      "hct": "सेन एंजेलो"
    }, {
      "ac": "SAT",
      "an": "San Antonio",
      "han": "सेन एंटोनियो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Antonio",
      "hct": "सेन एंटोनियो"
    }, {
      "ac": "SVZ",
      "an": "San Antonio Del Tachira",
      "han": "सेन एंटोनियो देल ताचीरा",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "San Antonio",
      "hct": "सेन एंटोनियो"
    }, {
      "ac": "SRJ",
      "an": "Capit?n Av. German Quiroga G.",
      "han": "कापित?एन अव. जर्मन क़ुइरोगा जी.",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "San Borja",
      "hct": "सेन बोर्जा"
    }, {
      "ac": "BRC",
      "an": "San Carlos De Bariloche",
      "han": "सेन कार्लोस दे बरिलोच",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "San Carlos De Bariloch",
      "hct": "सेन कार्लोस दे बरिलोच"
    }, {
      "ac": "SCY",
      "an": "",
      "han": "",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "San Cristobal",
      "hct": "सेन क्रिस्टोबल"
    }, {
      "ac": "TQR",
      "an": "San Domino Island Heliport",
      "han": "सेन डॉमिनो आयलॅन्ड हेलिपोर्ट",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Tremiti Islands",
      "hct": "ट्रेमीती आयलॅन्ड्स"
    }, {
      "ac": "SFD",
      "an": "San Fernando De Apure",
      "han": "सेन फर्नांडो दे अपूरे",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "San Fernando De Apure",
      "hct": "सेन फर्नांडो दे अपूरे"
    }, {
      "ac": "SFE",
      "an": "San Fernando",
      "han": "सेन फर्नांडो",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "San Fernando",
      "hct": "सेन फर्नांडो"
    }, {
      "ac": "SJD",
      "an": "Los Cabos",
      "han": "लोस काबोस",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "San Jose Cabo",
      "hct": "सेन जोस काबो"
    }, {
      "ac": "SJE",
      "an": "Jorge E Gonzalez Torres",
      "han": "जोर्गे इ गोन्ज़ालेज़ टर्र्स",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "San Jose Del Guaviare",
      "hct": "सेन जोस देल गुआवियारे"
    }, {
      "ac": "UAQ",
      "an": "San Juan",
      "han": "सेन जुआन",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "San Julian",
      "hct": "सेन जुलीयन"
    }, {
      "ac": "SJU",
      "an": "Luiz Munoz Marin",
      "han": "लूइज़ मूनोज़ मरीन",
      "cn": "Puerto Rico",
      "hcn": "पुएर्टो रिको",
      "cc": "PR",
      "ct": "San Juan",
      "hct": "सेन जुआन"
    }, {
      "ac": "CSL",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Luis Obispo",
      "hct": "सेन लुईस ओबिस्पो"
    }, {
      "ac": "SBP",
      "an": "San Luis Obispo County",
      "han": "सेन लुईस ओबिस्पो कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "San Luis Obispo",
      "hct": "सेन लुईस ओबिस्पो"
    }, {
      "ac": "SLP",
      "an": "San Luis Potosi Municipal",
      "han": "सेन लुईस पोटोसी म्यूनिसिपल",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "San Luis Potosi",
      "hct": "सेन लुईस पोटोसी"
    }, {
      "ac": "LUQ",
      "an": "San Luis",
      "han": "सेन लुईस",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "San Luis",
      "hct": "सेन लुईस"
    }, {
      "ac": "CPC",
      "an": "Aviador C Campos",
      "han": "अवियाडोर सी कम्पोस",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "San Martin Des Andes",
      "hct": "सेन मार्टिन देस अन्डेस"
    }, {
      "ac": "AFA",
      "an": "San Rafael",
      "han": "सेन रफ़एल",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "San Rafael",
      "hct": "सेन रफ़एल"
    }, {
      "ac": "GMZ",
      "an": "",
      "han": "",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "San Sebas de la Gomera",
      "hct": "सेन सेबेस दे ला गोमेरा"
    }, {
      "ac": "EAS",
      "an": "San Sebastian",
      "han": "सेन सेबॅस्टियन",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "San Sebastian",
      "hct": "सेन सेबॅस्टियन"
    }, {
      "ac": "SOM",
      "an": "San Tome",
      "han": "सेन टोम",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "San Tome",
      "hct": "सेन टोम"
    }, {
      "ac": "SVI",
      "an": "Eduardo Falla Solano",
      "han": "एडुआर्डो फ़अला सोलानो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "San Vincente De Caguan",
      "hct": "सेन विन्सेंटे दे कगुआन"
    }, {
      "ac": "SAH",
      "an": "Sanaa",
      "han": "सना",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Sanaa",
      "hct": "सना"
    }, {
      "ac": "SDG",
      "an": "Sanandaj",
      "han": "सानंदाज",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Sanandaj",
      "hct": "सानंदाज"
    }, {
      "ac": "SDP",
      "an": "Sand Point",
      "han": "सण्द पोईंत",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sand Point",
      "hct": "सण्द पोईंत"
    }, {
      "ac": "SDK",
      "an": "Sandakan",
      "han": "सन्दकन",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Sandakan",
      "hct": "सन्दकन"
    }, {
      "ac": "SDN",
      "an": "Anda",
      "han": "अंडा",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Sandane",
      "hct": "सन्दणे"
    }, {
      "ac": "SSJ",
      "an": "Stokka",
      "han": "स्टोक्का",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Sandnessjoen",
      "hct": "संड्नेस्स्जोयन"
    }, {
      "ac": "YZP",
      "an": "Sandspit",
      "han": "संड्स्पित",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sandspit",
      "hct": "संड्स्पित"
    }, {
      "ac": "ZSJ",
      "an": "Sandy Lake",
      "han": "सैंडी लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sandy Lake",
      "hct": "सैंडी लेक"
    }, {
      "ac": "SFB",
      "an": "Orlando Sanford",
      "han": "ओर्लांदो सेनफोर्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sanford",
      "hct": "सेनफोर्ड"
    }, {
      "ac": "YSK",
      "an": "Sanikiluaq",
      "han": "साणिकीलुआक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sanikiluaq",
      "hct": "साणिकीलुआक़"
    }, {
      "ac": "SBL",
      "an": "",
      "han": "",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Santa Ana",
      "hct": "शांता अना"
    }, {
      "ac": "NNB",
      "an": "Santa Ana",
      "han": "शांता अना",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Santa Ana",
      "hct": "शांता अना"
    }, {
      "ac": "SNA",
      "an": "John Wayne",
      "han": "जॉन वेन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Santa Ana",
      "hct": "शांता अना"
    }, {
      "ac": "SBA",
      "an": "Santa Barbara",
      "han": "शांता बारबरा",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Santa Barbara",
      "hct": "शांता बारबरा"
    }, {
      "ac": "SPC",
      "an": "La Palma",
      "han": "ला पाल्मा",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Santa Cruz De La Palma",
      "hct": "शांता क्रुज़ दे ला पाल्मा"
    }, {
      "ac": "SCZ",
      "an": "Santa Cruz/Graciosa Bay/Luova",
      "han": "शांता क्रुज़/ग्रासियोसा बे/लुओवा",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Santa Cruz Graciosa Bay Luova",
      "hct": "शांता क्रुज़ ग्रासियोसा बे लुओवा"
    }, {
      "ac": "SRZ",
      "an": "Santa Cruz",
      "han": "शांता क्रुज़",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Santa Cruz",
      "hct": "शांता क्रुज़"
    }, {
      "ac": "VVI",
      "an": "Viru Viru",
      "han": "वीरु वीरु",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Santa Cruz",
      "hct": "शांता क्रुज़"
    }, {
      "ac": "SFN",
      "an": "Sauce Viejo",
      "han": "सऊक वीजो",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Santa Fe",
      "hct": "शांता फ़े"
    }, {
      "ac": "SAF",
      "an": "Santa Fe Municipal",
      "han": "शांता फ़े म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Santa Fe",
      "hct": "शांता फ़े"
    }, {
      "ac": "RIA",
      "an": "Santa Maria",
      "han": "शांता मारिआ",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Santa Maria",
      "hct": "शांता मारिआ"
    }, {
      "ac": "SMA",
      "an": "Santa Maria",
      "han": "शांता मारिआ",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Santa Maria (island)",
      "hct": "शांता मारिआ (आयलॅन्ड)"
    }, {
      "ac": "SMX",
      "an": "Santa Maria Public",
      "han": "शांता मारिआ पब्लिक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Santa Maria",
      "hct": "शांता मारिआ"
    }, {
      "ac": "SMR",
      "an": "Simon Bolivar",
      "han": "साइमन बोलीवार",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Santa Marta",
      "hct": "शांता मार्टा"
    }, {
      "ac": "RSA",
      "an": "Santa Rosa",
      "han": "शांता रोसा",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Santa Rosa",
      "hct": "शांता रोसा"
    }, {
      "ac": "SRA",
      "an": "Santa Rosa",
      "han": "शांता रोसा",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Santa Rosa",
      "hct": "शांता रोसा"
    }, {
      "ac": "STS",
      "an": "Sonoma County",
      "han": "सोनोमा कौंटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Santa Rosa",
      "hct": "शांता रोसा"
    }, {
      "ac": "SDR",
      "an": "Santander",
      "han": "संटनदर",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Santander",
      "hct": "संटनदर"
    }, {
      "ac": "STM",
      "an": "Santarem",
      "han": "संतरेम",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Santarem",
      "hct": "संतरेम"
    }, {
      "ac": "SCQ",
      "an": "Santiago",
      "han": "सॅन्टियागो",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Santiago",
      "hct": "सॅन्टियागो"
    }, {
      "ac": "SDE",
      "an": "Santiago Del Estero",
      "han": "सॅन्टियागो देल एस्टरो",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Santiago Del Estero",
      "hct": "सॅन्टियागो देल एस्टरो"
    }, {
      "ac": "SCL",
      "an": "Arturo Merino Benitez",
      "han": "आर्तरो मेरिनो बेनिटेज़",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Santiago",
      "hct": "सॅन्टियागो"
    }, {
      "ac": "GEL",
      "an": "Santo Angelo",
      "han": "संतो एंजेलो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Santo Angelo",
      "hct": "संतो एंजेलो"
    }, {
      "ac": "STD",
      "an": "Mayor Buenaventura Vivas",
      "han": "मयोर बुएनवन्टूरा विवास",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Santo Domingo",
      "hct": "संतो दोमिंगो"
    }, {
      "ac": "SYX",
      "an": "Phoenix",
      "han": "फोयनिक्स",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Sanya",
      "hct": "सनया"
    }, {
      "ac": "SFL",
      "an": "Sao Filipe",
      "han": "साव फ़िलिपे",
      "cn": " Fogo Island",
      "hcn": "फ़ोगो आयलॅन्ड",
      "cc": "CV",
      "ct": "Sao Filipe",
      "hct": "साव फ़िलिपे"
    }, {
      "ac": "SJZ",
      "an": "Sao Jorge",
      "han": "साव जोर्गे",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Sao Jorge Island",
      "hct": "साव जोर्गे आयलॅन्ड"
    }, {
      "ac": "SJP",
      "an": "Sao Jose Do Rio Preto",
      "han": "साव जोस दो रिओ प्रेटो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sao Jose Do Rio Preto",
      "hct": "साव जोस दो रिओ प्रेटो"
    }, {
      "ac": "SJK",
      "an": "Professor Urbano Ernesto Stumpf",
      "han": "प्रोफेसर उर्बानो एर्नेस्टो स्तुम्फ़",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sao Jose Dos Campos",
      "hct": "साव जोस दोस कम्पोस"
    }, {
      "ac": "SLZ",
      "an": "Marechal Cunha Machado",
      "han": "मारेचाल कुन्हा मचादो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sao Luis",
      "hct": "साव लुईस"
    }, {
      "ac": "SNE",
      "an": "Preguica",
      "han": "प्रेगुइका",
      "cn": "Cape Verde",
      "hcn": "केप‌ व‌र्द",
      "cc": "CV",
      "ct": "Sao Nocolau Island",
      "hct": "साव नोकोलऊ आयलॅन्ड"
    }, {
      "ac": "TMS",
      "an": "Sao Tome",
      "han": "साव टोम",
      "cn": "Sao Tome and Principe",
      "hcn": "साव टोम एण्द प्रिन्सिपे",
      "cc": "ST",
      "ct": "Sao Tome",
      "hct": "साव टोम"
    }, {
      "ac": "VXE",
      "an": "Sao Pedro",
      "han": "साव पेद्रो",
      "cn": "Cape Verde",
      "hcn": "केप‌ व‌र्द",
      "cc": "CV",
      "ct": "Sao Vicente Island",
      "hct": "साव विसेंटे आयलॅन्ड"
    }, {
      "ac": "SPK",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Sapporo",
      "hct": "सप्पोरो"
    }, {
      "ac": "CTS",
      "an": "Chitose",
      "han": "चिटोस",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Sapporo",
      "hct": "सप्पोरो"
    }, {
      "ac": "OKD",
      "an": "Okadama",
      "han": "ओकाडामा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Sapporo",
      "hct": "सप्पोरो"
    }, {
      "ac": "SSR",
      "an": "Sara",
      "han": "सारा",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Pentecost Island",
      "hct": "पेंटेकोस्त आयलॅन्ड"
    }, {
      "ac": "SJJ",
      "an": "Butmir",
      "han": "बुतमीर",
      "cn": "Bosnia and Herzegovina",
      "hcn": "बोसनिआ एण्द हेर्ज़ेगोवीना",
      "cc": "BA",
      "ct": "Sarajevo",
      "hct": "साराजेवो"
    }, {
      "ac": "SLK",
      "an": "Adirondack",
      "han": "अदिरोन्डक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Saranac Lake",
      "hct": "सारनेक लेक"
    }, {
      "ac": "RTW",
      "an": "Central",
      "han": "सेन्ट्रल",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Saratov",
      "hct": "सारातोव"
    }, {
      "ac": "RVE",
      "an": "Saravena",
      "han": "सारावेना",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Saravena",
      "hct": "सारावेना"
    }, {
      "ac": "SRY",
      "an": "Sari Dasht E Naz",
      "han": "सारी दाश्ट इ नाज़",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Dasht",
      "hct": "दाश्ट"
    }, {
      "ac": "YZR",
      "an": "Sarnia",
      "han": "सरनिआ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sarnia",
      "hct": "सरनिआ"
    }, {
      "ac": "YXE",
      "an": "Saskatoon Municipal",
      "han": "सास्कतून म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Saskatoon",
      "hct": "सास्कतून"
    }, {
      "ac": "SUJ",
      "an": "Satu Mare",
      "han": "सतू मारे",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Satu Mare",
      "hct": "सतू मारे"
    }, {
      "ac": "YAM",
      "an": "Sault Ste Marie",
      "han": "सौल्ट स्ते मॅरी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sault Ste Marie",
      "hct": "सौल्ट स्ते मॅरी"
    }, {
      "ac": "SSM",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sault Ste. Marie",
      "hct": "सौल्ट स्ते. मॅरी"
    }, {
      "ac": "CIU",
      "an": "Chippewa Cnty",
      "han": "चिपेवा क्न्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sault Ste Marie",
      "hct": "सौल्ट स्ते मॅरी"
    }, {
      "ac": "SAV",
      "an": "Savannah",
      "han": "सवन्नाह",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Savannah",
      "hct": "सवन्नाह"
    }, {
      "ac": "SVL",
      "an": "Savonlinna",
      "han": "सवोन्लीन्ना",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Savonlinna",
      "hct": "सवोन्लीन्ना"
    }, {
      "ac": "SVA",
      "an": "Savoonga",
      "han": "सवून्गा",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Savoonga",
      "hct": "सवून्गा"
    }, {
      "ac": "SVU",
      "an": "Savusavu",
      "han": "सवुसवू",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Savusavu",
      "hct": "सवुसवू"
    }, {
      "ac": "SCM",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Scammon Bay",
      "hct": "स्केमोन बे"
    }, {
      "ac": "YKL",
      "an": "Schefferville",
      "han": "शेफर्वील",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Schefferville",
      "hct": "शेफर्वील"
    }, {
      "ac": "BFF",
      "an": "Scottsbluff Municipal",
      "han": "स्कोत्स्ब्लुफ म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Scottsbluff",
      "hct": "स्कोत्स्ब्लुफ"
    }, {
      "ac": "AVP",
      "an": "Wilkes Barre Scranton",
      "han": "विळ्केस बार्रे स्क्रंटन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Scranton Wilkes",
      "hct": "स्क्रंटन विळ्केस"
    }, {
      "ac": "SYB",
      "an": "Seal Bay",
      "han": "सील बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Seal Bay",
      "hct": "सील बे"
    }, {
      "ac": "SEB",
      "an": "Sebha",
      "han": "से�ा",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Sebha",
      "hct": "से�ा"
    }, {
      "ac": "YHS",
      "an": "Sechelt",
      "han": "सेचेल्ट",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sechelt",
      "hct": "सेचेल्ट"
    }, {
      "ac": "EGM",
      "an": "Sege",
      "han": "सेगे",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Sege",
      "hct": "सेगे"
    }, {
      "ac": "SJY",
      "an": "Seinajoki",
      "han": "सैनजोकी",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Seinajoki , Ilmajoki",
      "hct": "सैनजोकी , इल्मजोकी"
    }, {
      "ac": "GXF",
      "an": "Sayun",
      "han": "सायून",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Sayun Intl",
      "hct": "सायून इंट्ल"
    }, {
      "ac": "WLK",
      "an": "Selawik",
      "han": "सेलौइक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Selawik",
      "hct": "सेलौइक"
    }, {
      "ac": "SOV",
      "an": "Seldovia",
      "han": "सेल्डोविआ",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Seldovia",
      "hct": "सेल्डोविआ"
    }, {
      "ac": "SRG",
      "an": "Achmad Yani",
      "han": "अचमद यानी",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Semarang",
      "hct": "सेमारंग"
    }, {
      "ac": "PLX",
      "an": "Semipalatinsk",
      "han": "सेमिपलाटिन्स्क",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Semiplatinsk",
      "hct": "सेमिप्लटिन्स्क"
    }, {
      "ac": "SDJ",
      "an": "Sendai",
      "han": "सेंडै",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Sendai",
      "hct": "सेंडै"
    }, {
      "ac": "YZV",
      "an": "Sept Iles Municipal",
      "han": "सेप्ट इल्स म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sept",
      "hct": "सेप्ट"
    }, {
      "ac": "SRX",
      "an": "Gardabya",
      "han": "गार्डाब्या",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Sirt",
      "hct": "सिर्ट"
    }, {
      "ac": "SVQ",
      "an": "San Pablo",
      "han": "सेन पॅब्लो",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Sevilla",
      "hct": "सेवीला"
    }, {
      "ac": "SFA",
      "an": "Thyna",
      "han": "ठ्य्ना",
      "cn": "Tunisia",
      "hcn": "ट्यूनीशिया",
      "cc": "TN",
      "ct": "Sfax",
      "hct": "स्फ़ाक्स"
    }, {
      "ac": "SHX",
      "an": "Shageluk",
      "han": "शागेलुक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Shageluk",
      "hct": "शागेलुक"
    }, {
      "ac": "CQD",
      "an": "",
      "han": "",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Shahre-Kord",
      "hct": "शाह्रे-कोर्ड"
    }, {
      "ac": "SKK",
      "an": "Shaktoolik",
      "han": "शक्टूलिक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Shaktoolik",
      "hct": "शक्टूलिक"
    }, {
      "ac": "ZTM",
      "an": "Shamattawa",
      "han": "शमट्टव",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Shamattawa",
      "hct": "शमट्टव"
    }, {
      "ac": "SNN",
      "an": "Shannon",
      "han": "शैनन",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Shannon",
      "hct": "शैनन"
    }, {
      "ac": "SWA",
      "an": "Wai Sha",
      "han": "वै शा",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Shantou",
      "hct": "शांटौ"
    }, {
      "ac": "SSH",
      "an": "Sharm El Sheikh",
      "han": "शर्म एल शेख",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Sharm El Sheikh",
      "hct": "शर्म एल शेख"
    }, {
      "ac": "SHW",
      "an": "Sharurah",
      "han": "शेरुरा",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Sharurah",
      "hct": "शेरुरा"
    }, {
      "ac": "SZD",
      "an": "Sheffield",
      "han": "शेफील्ड",
      "cn": "United Kingdom ",
      "hcn": "युनाईटेड किंग्डोम ",
      "cc": "GB",
      "ct": "Sheffield ",
      "hct": "शेफील्ड "
    }, {
      "ac": "SXP",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sheldon Point",
      "hct": "शेलडॉन पोईंत"
    }, {
      "ac": "SHE",
      "an": "Shenyang Taoxian",
      "han": "शेनयांग ताव्क्सियान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Shenyang",
      "hct": "शेनयांग"
    }, {
      "ac": "SHR",
      "an": "Sheridan Cty",
      "han": "शेरीदान क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sheridan",
      "hct": "शेरीदान"
    }, {
      "ac": "SDZ",
      "an": "Scatsta",
      "han": "स्कट्स्टा",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Scatsta",
      "hct": "स्कट्स्टा"
    }, {
      "ac": "LSI",
      "an": "Sumburgh",
      "han": "सूम्बूरघ",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Sumburgh",
      "hct": "सूम्बूरघ"
    }, {
      "ac": "SJW",
      "an": "Shijiazhuang Daguocun",
      "han": "शिजियाऴुआंग दागुओकुन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Shijiazhuang",
      "hct": "शिजियाझुआंग"
    }, {
      "ac": "HIL",
      "an": "Shilavo",
      "han": "शिलवो",
      "cn": "Ethiopia",
      "hcn": "ईथियोपिया",
      "cc": "ET",
      "ct": "Shilavo",
      "hct": "शिलवो"
    }, {
      "ac": "CIT",
      "an": "Shymkent",
      "han": "शीम्केंट",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Chimkent",
      "hct": "चिम्केंट"
    }, {
      "ac": "SHY",
      "an": "Shinyanga",
      "han": "शीन्यान्गा",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Shinyanga",
      "hct": "शीन्यान्गा"
    }, {
      "ac": "SHM",
      "an": "Nanki Shirahama",
      "han": "नानकी शीरहमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Nanki",
      "hct": "नानकी"
    }, {
      "ac": "SYZ",
      "an": "Shiraz Shahid Dastghaib",
      "han": "शिराज़ शहीद दस्ट्घैब",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Shiraz",
      "hct": "शिराज़"
    }, {
      "ac": "SHH",
      "an": "Shishmaref",
      "han": "शीश्मारेफ़",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Shishmaref",
      "hct": "शीश्मारेफ़"
    }, {
      "ac": "SYO",
      "an": "Shonai",
      "han": "शोनै",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Shonai",
      "hct": "शोनै"
    }, {
      "ac": "SOW",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Show Low",
      "hct": "शो लौ"
    }, {
      "ac": "SHV",
      "an": "Shreveport Regional",
      "han": "श्रेवेपोर्ट रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Shreveport",
      "hct": "श्रेवेपोर्ट"
    }, {
      "ac": "SHG",
      "an": "Shungnak",
      "han": "शुन्गनक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Shungnak",
      "hct": "शुन्गनक"
    }, {
      "ac": "SKT",
      "an": "Sialkot",
      "han": "स्यालकोट",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Sialkot",
      "hct": "स्यालकोट"
    }, {
      "ac": "SBZ",
      "an": "Sibiu",
      "han": "सिबीऊ",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Sibiu",
      "hct": "सिबीऊ"
    }, {
      "ac": "SBW",
      "an": "Sibu",
      "han": "सिबू",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Sibu",
      "hct": "सिबू"
    }, {
      "ac": "SDY",
      "an": "Richland Municipal",
      "han": "रिचलॅंद म्यूनिसिपल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sidney",
      "hct": "सिड्नी"
    }, {
      "ac": "REP",
      "an": "Siem Reap",
      "han": "सीम रीप",
      "cn": "Cambodia",
      "hcn": "कम्बोडिआ",
      "cc": "KH",
      "ct": "Siem Reap",
      "hct": "सीम रीप"
    }, {
      "ac": "SVC",
      "an": "Grant County",
      "han": "ग्रांट कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Silver City",
      "hct": "सिल्वर सिटी"
    }, {
      "ac": "SYM",
      "an": "Simao",
      "han": "सिमाव",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Simao",
      "hct": "सिमाव"
    }, {
      "ac": "SIF",
      "an": "Simara",
      "han": "सिमार",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Simara",
      "hct": "सिमार"
    }, {
      "ac": "SIP",
      "an": "Simferopol",
      "han": "सिम्फ़ेरोपोल",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Simferopol",
      "hct": "सिम्फ़ेरोपोल"
    }, {
      "ac": "IMK",
      "an": "Simikot",
      "han": "सिमीकोट",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Simikot",
      "hct": "सिमीकोट"
    }, {
      "ac": "OPS",
      "an": "",
      "han": "",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Sinop",
      "hct": "सिनोप"
    }, {
      "ac": "SUX",
      "an": "Sioux Gateway",
      "han": "सियोउक्स गेटवे",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sioux City",
      "hct": "सियोउक्स सिटी"
    }, {
      "ac": "FSD",
      "an": "Regional Joe Foss Field",
      "han": "रिजनल जो फ़ोस फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sioux Falls",
      "hct": "सियोउक्स फॉल्स"
    }, {
      "ac": "YXL",
      "an": "Sioux Lookout",
      "han": "सियोउक्स लूकौत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sioux Lookout",
      "hct": "सियोउक्स लूकौत"
    }, {
      "ac": "SXI",
      "an": "",
      "han": "",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Sirri Island",
      "hct": "सिरी आयलॅन्ड"
    }, {
      "ac": "JHS",
      "an": "Sisimiut",
      "han": "सिसिमीऊत",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Sisimiut",
      "hct": "सिसिमीऊत"
    }, {
      "ac": "JSH",
      "an": "Sitia",
      "han": "सितिआ",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Sitia",
      "hct": "सितिआ"
    }, {
      "ac": "SIT",
      "an": "Sitka",
      "han": "सिट्का",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sitka",
      "hct": "सिट्का"
    }, {
      "ac": "AKY",
      "an": "Sittwe",
      "han": "सित्वे",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Sittwe",
      "hct": "सित्वे"
    }, {
      "ac": "VAS",
      "an": "Sivas",
      "han": "सिवास",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Sivas",
      "hct": "सिवास"
    }, {
      "ac": "SGY",
      "an": "Skagway",
      "han": "स्कागवै",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Skagway",
      "hct": "स्कागवै"
    }, {
      "ac": "KDU",
      "an": "Skardu",
      "han": "स्कर्डू",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Skardu",
      "hct": "स्कर्डू"
    }, {
      "ac": "SFT",
      "an": "Skelleftea",
      "han": "स्केलेफ़्टी",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Skelleftea",
      "hct": "स्केलेफ़्टी"
    }, {
      "ac": "JSI",
      "an": "Alexandros Papadiamantis",
      "han": "अलेक्सान्द्रोस पापडिआमंटिस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Skiathos",
      "hct": "स्कियाथोस"
    }, {
      "ac": "SKE",
      "an": "Geiteryggen",
      "han": "गीटेर्य्ग्गेन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Skien",
      "hct": "स्कीन"
    }, {
      "ac": "SKU",
      "an": "Skiros",
      "han": "स्किरोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Skiros",
      "hct": "स्किरोस"
    }, {
      "ac": "SKP",
      "an": "Skopje",
      "han": "स्कोप्जे",
      "cn": "Macedonia",
      "hcn": "मेसेदोनिआ",
      "cc": "MK",
      "ct": "Skopje",
      "hct": "स्कोप्जे"
    }, {
      "ac": "KVB",
      "an": "Skovde",
      "han": "स्कोव्डे",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Skovde",
      "hct": "स्कोव्डे"
    }, {
      "ac": "SLQ",
      "an": "Sleetmute",
      "han": "स्लीट्मुटे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sleetmute",
      "hct": "स्लीट्मुटे"
    }, {
      "ac": "SLD",
      "an": "Sliac",
      "han": "स्लियाक",
      "cn": "Slovakia",
      "hcn": "स्लोवकिआ",
      "cc": "SK",
      "ct": "Sliac",
      "hct": "स्लियाक"
    }, {
      "ac": "SXL",
      "an": "Sligo",
      "han": "स्लिगो",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Sligo",
      "hct": "स्लिगो"
    }, {
      "ac": "YSH",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Smith Falls",
      "hct": "स्मिथ फॉल्स"
    }, {
      "ac": "YYD",
      "an": "Smithers",
      "han": "स्मीठर्ज़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Smithers",
      "hct": "स्मीठर्ज़"
    }, {
      "ac": "YFJ",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Snare Lake",
      "hct": "सनरे लेक"
    }, {
      "ac": "DWB",
      "an": "Soalala",
      "han": "सोआलाला",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Soalala",
      "hct": "सोआलाला"
    }, {
      "ac": "SCT",
      "an": "Socotra",
      "han": "सोकोत्रा",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Socotra",
      "hct": "सोकोत्रा"
    }, {
      "ac": "SOO",
      "an": "Soderhamn",
      "han": "सोडेर्हेम्न",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Soderhamn",
      "hct": "सोडेर्हेम्न"
    }, {
      "ac": "SOF",
      "an": "Sofia Vrazhdebna",
      "han": "सोफिया व्राझदेब्ना",
      "cn": "Bulgaria",
      "hcn": "बूलगारिआ",
      "cc": "BG",
      "ct": "Sofia",
      "hct": "सोफिया"
    }, {
      "ac": "SOG",
      "an": "Sogndal",
      "han": "सोग्न्डल",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Sogndal",
      "hct": "सोग्न्डल"
    }, {
      "ac": "SLH",
      "an": "Sola",
      "han": "सोला",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Sola",
      "hct": "सोला"
    }, {
      "ac": "SOC",
      "an": "Adi Sumarmo Wiryokusumo",
      "han": "आदि सूमर्मो विर्योकुसुमो",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Solo City",
      "hct": "सोलो सिटी"
    }, {
      "ac": "CSH",
      "an": "Solovki",
      "han": "सोलोव्की",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Solovetsky Islands",
      "hct": "सोलोवेट्स्की आयलॅन्ड्स"
    }, {
      "ac": "SGD",
      "an": "Sonderborg",
      "han": "सोन्डर्बोर्ग",
      "cn": "Denmark",
      "hcn": "डेन्मार्क",
      "cc": "DK",
      "ct": "Sonderborg",
      "hct": "सोन्डर्बोर्ग"
    }, {
      "ac": "JZH",
      "an": "Jiuzhaigou Huanglong",
      "han": "जीऊझ़ैगौ हुआंग्लोन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Jiuzhaigou",
      "hct": "जीऊझ़ैगौ"
    }, {
      "ac": "SOJ",
      "an": "Sorkjosen",
      "han": "सोर्क्जोसेन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Sorkjosen",
      "hct": "सोर्क्जोसेन"
    }, {
      "ac": "SOQ",
      "an": "Jefman",
      "han": "जेफ़्मॅन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Sorong",
      "hct": "सोरोन्ग"
    }, {
      "ac": "SBN",
      "an": "Michiana Regional",
      "han": "मिचिआना रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "South Bend",
      "hct": "साउथ बेंद"
    }, {
      "ac": "XSI",
      "an": "South �ारत‌n Lake",
      "han": "साउथ इंडियन लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "South Indian Lake",
      "hct": "साउथ इंडियन लेक"
    }, {
      "ac": "WSN",
      "an": "South Naknek",
      "han": "साउथ नक्णेक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "South Naknek",
      "hct": "साउथ नक्णेक"
    }, {
      "ac": "SWJ",
      "an": "Southwest Bay",
      "han": "साउथवॅस्ट बे",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Malekula Island",
      "hct": "मालेकुला आयलॅन्ड"
    }, {
      "ac": "SOU",
      "an": "Southampton",
      "han": "सौथंप्टन",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Southampton",
      "hct": "सौथंप्टन"
    }, {
      "ac": "SEN",
      "an": "Southend",
      "han": "सौथेंद",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Southend",
      "hct": "सौथेंद"
    }, {
      "ac": "SPU",
      "an": "Split",
      "han": "स्प्लित",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Split",
      "hct": "स्प्लित"
    }, {
      "ac": "GEG",
      "an": "Spokane",
      "han": "स्पोकाणे",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Spokane",
      "hct": "स्पोकाणे"
    }, {
      "ac": "SGF",
      "an": "Springfield Branson Regional",
      "han": "स्प्रिंगफ़ील्ड ब्रनसन रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "SPRINGFIELD",
      "hct": "स्प्रिंगफ़ील्ड"
    }, {
      "ac": "SPI",
      "an": "Capital",
      "han": "कॅपिटल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Springfield",
      "hct": "स्प्रिंगफ़ील्ड"
    }, {
      "ac": "ADX",
      "an": "Leuchars",
      "han": "लेउचर्स",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Leuchars",
      "hct": "लेउचर्स"
    }, {
      "ac": "YAY",
      "an": "St Anthony",
      "han": "स्त्रीट एंथोनी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "St. Anthony",
      "hct": "स्त्रीट. एंथोनी"
    }, {
      "ac": "SGO",
      "an": "St George",
      "han": "स्त्रीट जॉर्ज",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "St George ",
      "hct": "स्त्रीट जॉर्ज "
    }, {
      "ac": "STL",
      "an": "Lambert St Louis",
      "han": "लाम्बेर्ट स्त्रीट लुइस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "St. Louis",
      "hct": "स्त्रीट. लुइस"
    }, {
      "ac": "SNR",
      "an": "Montoir",
      "han": "मोन्टोईर",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "St.",
      "hct": "स्त्रीट."
    }, {
      "ac": "FSP",
      "an": "St Pierre",
      "han": "स्त्रीट पिएर",
      "cn": "Saint Pierre and Miquelon",
      "hcn": "सेंट पिएर एण्द मिक़्लोन",
      "cc": "PM",
      "ct": "St.",
      "hct": "स्त्रीट."
    }, {
      "ac": "SCE",
      "an": "University Park",
      "han": "युनिवर्सिती पार्क",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "State College",
      "hct": "स्टेट कॉलेज"
    }, {
      "ac": "SHD",
      "an": "Shenandoah Valley",
      "han": "शेनंदोआ वॅली",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Staunton",
      "hct": "स्टौंटन"
    }, {
      "ac": "SVG",
      "an": "Sola",
      "han": "सोला",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Stavanger",
      "hct": "स्टवंगर"
    }, {
      "ac": "STW",
      "an": "Shpakovskoye",
      "han": "श्पकोव्स्कोये",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Stavropol",
      "hct": "स्टव्रोपोल"
    }, {
      "ac": "YST",
      "an": "St. Theresa Point",
      "han": "स्त्रीट. थरेसा पोईंत",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "St. Theresa Point",
      "hct": "स्त्रीट. थरेसा पोईंत"
    }, {
      "ac": "WBB",
      "an": "Stebbins",
      "han": "स्तेब्बीन्स",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Stebbins",
      "hct": "स्तेब्बीन्स"
    }, {
      "ac": "YJT",
      "an": "Stephenville",
      "han": "स्टीफनविले",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Stephenville",
      "hct": "स्टीफनविले"
    }, {
      "ac": "SVS",
      "an": "Stevens Village",
      "han": "स्तेवन्स विलेज",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Stevens Village",
      "hct": "स्तेवन्स विलेज"
    }, {
      "ac": "SKN",
      "an": "Skagen",
      "han": "स्काजन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Stokmarknes",
      "hct": "स्टोक्मार्क्ण्स"
    }, {
      "ac": "SRV",
      "an": "Stony River",
      "han": "स्टोनी रिवर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Stony River",
      "hct": "स्टोनी रिवर"
    }, {
      "ac": "SRP",
      "an": "Sorstokken",
      "han": "सोर्स्टोक्केन",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Stord",
      "hct": "स्टर्ड"
    }, {
      "ac": "SYY",
      "an": "Stornoway",
      "han": "स्टर्नौऐ",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Stornoway",
      "hct": "स्टर्नौऐ"
    }, {
      "ac": "SQO",
      "an": "Storuman",
      "han": "स्टरमॅन",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Mohed",
      "hct": "मोहेड"
    }, {
      "ac": "SXB",
      "an": "Enzheim",
      "han": "एन्ऴेइम",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Strasbourg",
      "hct": "स्ट्रास्बौर्ग"
    }, {
      "ac": "VAO",
      "an": "Suavanao",
      "han": "सुआवनाव",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Suavanao",
      "hct": "सुआवनाव"
    }, {
      "ac": "SCV",
      "an": "Stefan Cel Mare",
      "han": "स्टीफन सेल मारे",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Suceava",
      "hct": "सकीवा"
    }, {
      "ac": "SRE",
      "an": "Juana Azurduy De Padilla",
      "han": "जुआना अज़ूरडुय दे पेडिला",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Sucre",
      "hct": "सक्रे"
    }, {
      "ac": "YSB",
      "an": "Sudbury Municipal",
      "han": "सूदबूरई म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Sudbury",
      "hct": "सूदबूरई"
    }, {
      "ac": "SYU",
      "an": "Warraber Island",
      "han": "वार्राबेर आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Sue Islet",
      "hct": "सुए इसलेट"
    }, {
      "ac": "SUL",
      "an": "Sui",
      "han": "सुइ",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Sui",
      "hct": "सुइ"
    }, {
      "ac": "THS",
      "an": "Sukhothai",
      "han": "सुखोथै",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Sukhothai",
      "hct": "सुखोथै"
    }, {
      "ac": "SKC",
      "an": "Suki",
      "han": "सुकी",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Suki",
      "hct": "सुकी"
    }, {
      "ac": "SKZ",
      "an": "Sukkur",
      "han": "सुक्कुर",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Sukkur",
      "hct": "सुक्कुर"
    }, {
      "ac": "SUR",
      "an": "Summer Beaver",
      "han": "सूममेर बीवर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Summer Beaver",
      "hct": "सूममेर बीवर"
    }, {
      "ac": "SSC",
      "an": "Shaw Afb",
      "han": "शॉ अफ़्ब",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Sumter",
      "hct": "सूम्टेर"
    }, {
      "ac": "SDL",
      "an": "Sundsvall",
      "han": "सुन्ड्सवाल",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Sundsvall",
      "hct": "सुन्ड्सवाल"
    }, {
      "ac": "MCY",
      "an": "Maroochydore",
      "han": "मरूच्य्डोरे",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Sunshine Coast",
      "hct": "सनशाईन कोस्ट"
    }, {
      "ac": "SUB",
      "an": "Juanda",
      "han": "जुआंडा",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Surabaya",
      "hct": "सूराबय"
    }, {
      "ac": "URT",
      "an": "Surat Thani",
      "han": "सूरत ठाणी",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Surat Thani",
      "hct": "सूरत ठाणी"
    }, {
      "ac": "SGC",
      "an": "Surgut",
      "han": "सूरगुट",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Surgut",
      "hct": "सूरगुट"
    }, {
      "ac": "SUG",
      "an": "Surigao",
      "han": "सूरिगाव",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Sangley Point",
      "hct": "सांगले पोईंत"
    }, {
      "ac": "SKH",
      "an": "Surkhet",
      "han": "सुरखेट",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Surkhet",
      "hct": "सुरखेट"
    }, {
      "ac": "SUV",
      "an": "Nausori",
      "han": "नऊशौरी",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Suva",
      "hct": "सुवा"
    }, {
      "ac": "EVG",
      "an": "Sveg",
      "han": "स्वेग",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Sveg",
      "hct": "स्वेग"
    }, {
      "ac": "SVJ",
      "an": "Helle",
      "han": "हेल",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Svolvaer",
      "hct": "स्वोल्वर"
    }, {
      "ac": "SWS",
      "an": "Swansea",
      "han": "स्वानसी",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Swansea",
      "hct": "स्वानसी"
    }, {
      "ac": "SCW",
      "an": "Syktyvkar",
      "han": "स्य्क्ट्य्वकर",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Syktyvkar",
      "hct": "स्य्क्ट्य्वकर"
    }, {
      "ac": "ZYL",
      "an": "Osmany",
      "han": "ऊस्मानी",
      "cn": "Bangladesh",
      "hcn": "बांग्लादेश",
      "cc": "BD",
      "ct": "Sylhet Osmani",
      "hct": "सिल्हेत ऊसमानी"
    }, {
      "ac": "SYR",
      "an": "Hancock",
      "han": "हन्कोक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Syracuse",
      "hct": "सिरकस"
    }, {
      "ac": "JSY",
      "an": "Syros Island",
      "han": "सिरोस आयलॅन्ड",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Syros Island",
      "hct": "सिरोस आयलॅन्ड"
    }, {
      "ac": "SZZ",
      "an": "Goleniow",
      "han": "गोलेनियोव",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Szczecin",
      "hct": "स्ज़्क्ज़ेकिन"
    }, {
      "ac": "TCP",
      "an": "Taba",
      "han": "ताब",
      "cn": "Egypt",
      "hcn": "मिस्र",
      "cc": "EG",
      "ct": "Taba",
      "hct": "ताब"
    }, {
      "ac": "TBZ",
      "an": "Tabriz",
      "han": "ताब्रिज़",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Tabriz",
      "hct": "ताब्रिज़"
    }, {
      "ac": "TBG",
      "an": "Tabubil",
      "han": "टाबुबिल",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Tabubil",
      "hct": "टाबुबिल"
    }, {
      "ac": "TUU",
      "an": "Tabuk",
      "han": "ताबुक",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Tabuk",
      "hct": "ताबुक"
    }, {
      "ac": "TCG",
      "an": "Tocache",
      "han": "टोकचे",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Tacheng",
      "hct": "ताचेन्ग"
    }, {
      "ac": "THL",
      "an": "Tachileik",
      "han": "ताचीलीक",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Tachilek",
      "hct": "ताचीलेक"
    }, {
      "ac": "TAC",
      "an": "Daniel Z Romualdez",
      "han": "डॅनिअल झेड रोमुआल्डेज़",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Tacloban",
      "hct": "तक्लोबन"
    }, {
      "ac": "TCQ",
      "an": "Coronel Carlos Ciriani Santa Rosa",
      "han": "कोरोनेल कार्लोस सिरिआणी शांता रोसा",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Tacna",
      "hct": "तक्ना"
    }, {
      "ac": "XTL",
      "an": "Tadoule Lake",
      "han": "तादौले लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Tadoule Lake",
      "hct": "तादौले लेक"
    }, {
      "ac": "TAG",
      "an": "Tagbilaran",
      "han": "तेगबिलारण",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Tagbilaran",
      "hct": "तेगबिलारण"
    }, {
      "ac": "RMQ",
      "an": "",
      "han": "",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Taichung",
      "hct": "ताईचुंग"
    }, {
      "ac": "TIF",
      "an": "Taif",
      "han": "ताईफ़",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Taif",
      "hct": "ताईफ़"
    }, {
      "ac": "TNN",
      "an": "Tainan",
      "han": "ताईनान",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Tainan",
      "hct": "ताईनान"
    }, {
      "ac": "TTT",
      "an": "Fengnin",
      "han": "फ़ेन्ग्निन",
      "cn": "Taiwan",
      "hcn": "ताईवान‌",
      "cc": "TW",
      "ct": "Fengnin",
      "hct": "फ़ेन्ग्निन"
    }, {
      "ac": "TYN",
      "an": "Wusu",
      "han": "वुसू",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Taiyuan",
      "hct": "ताईयुआन"
    }, {
      "ac": "TAI",
      "an": "Taiz",
      "han": "तैज़",
      "cn": "Yemen",
      "hcn": "य‌म‌न‌",
      "cc": "YE",
      "ct": "Taiz",
      "hct": "तैज़"
    }, {
      "ac": "TAK",
      "an": "Takamatsu",
      "han": "तकामट्सू",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Takamatsu",
      "hct": "तकामट्सू"
    }, {
      "ac": "TCT",
      "an": "Takotna",
      "han": "ताकोट्ना",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Takotna",
      "hct": "ताकोट्ना"
    }, {
      "ac": "TYL",
      "an": "Capitan Montes",
      "han": "कापीतान मोन्ट्स",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Talara",
      "hct": "तालरा"
    }, {
      "ac": "TLW",
      "an": "",
      "han": "",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Talasea",
      "hct": "तालसी"
    }, {
      "ac": "TLH",
      "an": "Tallahassee Municipal",
      "han": "तालहस्सी म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tallahassee",
      "hct": "तालहस्सी"
    }, {
      "ac": "TLL",
      "an": "Ulemiste",
      "han": "उलेमीस्ते",
      "cn": "Estonia",
      "hcn": "ईथियोपिया",
      "cc": "EE",
      "ct": "Tallinn",
      "hct": "तालीन्न"
    }, {
      "ac": "YYH",
      "an": "Taloyoak",
      "han": "तालोयोआक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Spence Bay",
      "hct": "स्पेंस बे"
    }, {
      "ac": "TMM",
      "an": "Toamasina",
      "han": "टोआमसीना",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Toamasina",
      "hct": "टोआमसीना"
    }, {
      "ac": "TUD",
      "an": "Tambacounda",
      "han": "तम्बकौन्डा",
      "cn": "Senegal",
      "hcn": "सेनेगल",
      "cc": "SN",
      "ct": "Tambacounda",
      "hct": "तम्बकौन्डा"
    }, {
      "ac": "TMC",
      "an": "Tambolaka",
      "han": "तम्बोलका",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Waikabubak",
      "hct": "वैकाबूबक"
    }, {
      "ac": "TBW",
      "an": "Tambow",
      "han": "तम्बौ",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Tambow",
      "hct": "तम्बौ"
    }, {
      "ac": "TME",
      "an": "Tame",
      "han": "तमे",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Tame",
      "hct": "तमे"
    }, {
      "ac": "VCL",
      "an": "Chu Lai",
      "han": "चू लाय",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Chu Lai",
      "hct": "चू लाय"
    }, {
      "ac": "TMP",
      "an": "Tampere Pirkkala",
      "han": "तम्परे पिर्ककला",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Tampere",
      "hct": "तम्परे"
    }, {
      "ac": "TAM",
      "an": "General F Javier Mina",
      "han": "जनरल एफ़ जेवियर मीना",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Tampico",
      "hct": "तंपीको"
    }, {
      "ac": "TMW",
      "an": "Tamworth",
      "han": "तम्वर्थ",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Tamworth",
      "hct": "तम्वर्थ"
    }, {
      "ac": "TTA",
      "an": "Plage Blanche",
      "han": "पलागे ब्लांचे",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Tan Tan",
      "hct": "तन तन"
    }, {
      "ac": "TAL",
      "an": "Ralph Calhoun",
      "han": "राल्फ कल्हौन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tanana",
      "hct": "तनना"
    }, {
      "ac": "TNG",
      "an": "Boukhalef",
      "han": "बौखलेफ़",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Tangier",
      "hct": "तंगिएर"
    }, {
      "ac": "TJQ",
      "an": "H As Hanandjoeddin",
      "han": "एच अस हानंद्जोयददीन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Tanjung Pandan",
      "hct": "तनजंग पंदन"
    }, {
      "ac": "TJG",
      "an": "Warukin",
      "han": "वरुकिन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Tanjung",
      "hct": "तनजंग"
    }, {
      "ac": "TAH",
      "an": "Tanna island",
      "han": "तन्ना आयलॅन्ड",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Tanna",
      "hct": "तन्ना"
    }, {
      "ac": "TAP",
      "an": "Tapachula",
      "han": "तपाचुला",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Tapachula",
      "hct": "तपाचुला"
    }, {
      "ac": "TPJ",
      "an": "Taplejung",
      "han": "ताप्लेजुन्ग",
      "cn": "Nepal",
      "hcn": "नेपाल",
      "cc": "NP",
      "ct": "Taplejung",
      "hct": "ताप्लेजुन्ग"
    }, {
      "ac": "TRK",
      "an": "Juwata",
      "han": "जुवता",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Taraken",
      "hct": "तारकेन"
    }, {
      "ac": "TPP",
      "an": "Cadete Guillermo Del Castillo Paredes",
      "han": "केडेटे गुइलर्मो देल कास्तिलो पेरेडेस",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Tarapoto",
      "hct": "तारापोटो"
    }, {
      "ac": "TRW",
      "an": "Bonriki",
      "han": "बोन्रिकी",
      "cn": "Kiribati",
      "hcn": "किरिबती",
      "cc": "KI",
      "ct": "Tarawa",
      "hct": "तारवा"
    }, {
      "ac": "TAQ",
      "an": "",
      "han": "",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Tarcoola",
      "hct": "तर्कूला"
    }, {
      "ac": "TRO",
      "an": "Taree",
      "han": "टरी",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Taree ",
      "hct": "टरी "
    }, {
      "ac": "TIZ",
      "an": "Tari",
      "han": "टारी",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Tari",
      "hct": "टारी"
    }, {
      "ac": "TJA",
      "an": "Capitan Oriel Lea Plaza",
      "han": "कापीतान ओरीएल ली प्लाज़ा",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Tarija",
      "hct": "टारिजा"
    }, {
      "ac": "AGM",
      "an": "",
      "han": "",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Tasiilaq",
      "hct": "तासिइलाक़"
    }, {
      "ac": "YTQ",
      "an": "Tasiujaq",
      "han": "तासीऊजक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Tasiujaq",
      "hct": "तासीऊजक़"
    }, {
      "ac": "TLJ",
      "an": "Tatalina Lrrs",
      "han": "टाटालीना ल्र्र्स",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tatalina",
      "hct": "टाटालीना"
    }, {
      "ac": "TUO",
      "an": "Taupo",
      "han": "तऊपो",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Taupo",
      "hct": "तऊपो"
    }, {
      "ac": "TRG",
      "an": "Tauranga",
      "han": "टौरंगा",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Tauranga",
      "hct": "टौरंगा"
    }, {
      "ac": "TVU",
      "an": "Matei",
      "han": "माटी",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Matei",
      "hct": "माटी"
    }, {
      "ac": "TWU",
      "an": "Tawau",
      "han": "टवऊ",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Tawau",
      "hct": "टवऊ"
    }, {
      "ac": "TWT",
      "an": "",
      "han": "",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Tawitawi",
      "hct": "टौइतौई"
    }, {
      "ac": "TCH",
      "an": "Tchibanga",
      "han": "च्चिबंगा",
      "cn": "Gabon",
      "hcn": "गाबोन",
      "cc": "GA",
      "ct": "Tchibanga",
      "hct": "च्चिबंगा"
    }, {
      "ac": "MME",
      "an": "Durham Tees Valley",
      "han": "दर्हम टीस वॅली",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Teesside",
      "hct": "टीससाईड"
    }, {
      "ac": "THR",
      "an": "Mehrabad",
      "han": "मेहराबाद",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Tehran",
      "hct": "तेहरान"
    }, {
      "ac": "KTS",
      "an": "Brevig Mission",
      "han": "ब्रेविग मिशन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Teller Mission",
      "hct": "टेलर मिशन"
    }, {
      "ac": "TLA",
      "an": "Teller",
      "han": "टेलर",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Teller",
      "hct": "टेलर"
    }, {
      "ac": "TEX",
      "an": "Telluride Municipal",
      "han": "टेलुरिड म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Telluride",
      "hct": "टेलुरिड"
    }, {
      "ac": "TIM",
      "an": "Moses Kilangin",
      "han": "मोसिस कीलंगिन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Timika",
      "hct": "टिमिका"
    }, {
      "ac": "ZCO",
      "an": "Manquehue",
      "han": "मन्क़्हुए",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Temuco",
      "hct": "टेमुको"
    }, {
      "ac": "TKE",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tenakee Springs",
      "hct": "तेनकी स्प्रिंग्स"
    }, {
      "ac": "TCI",
      "an": "All airports",
      "han": "ऑल एअरपॉर्ट्स",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Tenerife",
      "hct": "तनेरिफ़े"
    }, {
      "ac": "TFN",
      "an": "Tenerife Norte Los Rodeos",
      "han": "तनेरिफ़े नोरटे लोस रोदेव्स",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Tenerife",
      "hct": "तनेरिफ़े"
    }, {
      "ac": "TFS",
      "an": "Reina Sofia",
      "han": "रैना सोफिया",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Tenerife",
      "hct": "तनेरिफ़े"
    }, {
      "ac": "TPQ",
      "an": "Tepic",
      "han": "टेपिक",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Tepic",
      "hct": "टेपिक"
    }, {
      "ac": "TER",
      "an": "Lajes",
      "han": "लाजेस",
      "cn": "Portugal",
      "hcn": "पोर्तुगल",
      "cc": "PT",
      "ct": "Lajes (terceira Island)",
      "hct": "लाजेस (टर्केरा आयलॅन्ड)"
    }, {
      "ac": "THE",
      "an": "Senador Petronio Portella",
      "han": "सेनडोर पेट्रोनियो पोर्टेला",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Teresina",
      "hct": "तेरेसीना"
    }, {
      "ac": "TMJ",
      "an": "Termez",
      "han": "टेर्मेज़",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Termez",
      "hct": "टेर्मेज़"
    }, {
      "ac": "TTE",
      "an": "Sultan Babullah",
      "han": "सुल्तान बाबूल्ला",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Ternate",
      "hct": "टेर्नाटे"
    }, {
      "ac": "YXT",
      "an": "Terrace Municipal",
      "han": "टेरस म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "TERRACE",
      "hct": "टेरस"
    }, {
      "ac": "TET",
      "an": "Tete Chingodzi",
      "han": "टेटे चींगोड्ज़ी",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Tete",
      "hct": "टेटे"
    }, {
      "ac": "ZTB",
      "an": "Tete A La Baleine",
      "han": "टेटे ए ला बालैन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Tete A La Baleine",
      "hct": "टेटे ए ला बालैन"
    }, {
      "ac": "TEB",
      "an": "Teterboro",
      "han": "टेटर्बोरो",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Teterboro",
      "hct": "टेटर्बोरो"
    }, {
      "ac": "TTU",
      "an": "Saniat Rmel",
      "han": "सानिआट र्मेल",
      "cn": "Morocco",
      "hcn": "मोरोक्को",
      "cc": "MA",
      "ct": "Tetouan",
      "hct": "टेटौआन"
    }, {
      "ac": "TXK",
      "an": "Texarkana Municipal",
      "han": "टेक्सर्कना म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Texarkana",
      "hct": "टेक्सर्कना"
    }, {
      "ac": "SNW",
      "an": "Thandwe",
      "han": "ठंड्वे",
      "cn": "Burma",
      "hcn": "बर्मा",
      "cc": "MM",
      "ct": "Thandwe",
      "hct": "ठंड्वे"
    }, {
      "ac": "XTG",
      "an": "Thargomindah",
      "han": "ठार्गोमिंदाह",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Thargomindah ",
      "hct": "ठार्गोमिंदाह "
    }, {
      "ac": "YQD",
      "an": "The Pas Municipal",
      "han": "धी पास म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "The Pas",
      "hct": "धी पास"
    }, {
      "ac": "SKG",
      "an": "Makedonia",
      "han": "मकेदोनिआ",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Thessaloniki",
      "hct": "थेस्सलोनिकी"
    }, {
      "ac": "YTD",
      "an": "",
      "han": "",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Thicket Portage",
      "hct": "थिकेट पोरतागे"
    }, {
      "ac": "TVF",
      "an": "Thief River Falls",
      "han": "थीफ़ रिवर फॉल्स",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Thief River Falls",
      "hct": "थीफ़ रिवर फॉल्स"
    }, {
      "ac": "JTR",
      "an": "Santorini",
      "han": "संटोरिनी",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Thira",
      "hct": "थीरा"
    }, {
      "ac": "YTH",
      "an": "Thompson",
      "han": "थॉम्पसन",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Thompson",
      "hct": "थॉम्पसन"
    }, {
      "ac": "KTB",
      "an": "Thorne Bay",
      "han": "ठोर्णे बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Thorne Bay",
      "hct": "ठोर्णे बे"
    }, {
      "ac": "THO",
      "an": "Thorshofn",
      "han": "ठोर्शोफ़्न",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Thorshofn",
      "hct": "ठोर्शोफ़्न"
    }, {
      "ac": "YQT",
      "an": "Thunder Bay",
      "han": "थुनदर बे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "THUNDER BAY",
      "hct": "थुनदर बे"
    }, {
      "ac": "TIS",
      "an": "Thursday Island",
      "han": "थूरसदय आयलॅन्ड",
      "cn": "Australia ",
      "hcn": "औस्ट्रालिया ",
      "cc": "AU",
      "ct": "Thursday Island ",
      "hct": "थूरसदय आयलॅन्ड "
    }, {
      "ac": "TSN",
      "an": "Binhai",
      "han": "बीन्हाई",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Tianjin",
      "hct": "टिआन्जिन"
    }, {
      "ac": "TGJ",
      "an": "Tiga",
      "han": "तिगा",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Tiga",
      "hct": "तिगा"
    }, {
      "ac": "TIJ",
      "an": "General Abelardo L Rodriguez",
      "han": "जनरल आबेलार्डो एल रोड्रिग्ज़",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Tijuana",
      "hct": "टिजुआना"
    }, {
      "ac": "IKS",
      "an": "Tiksi",
      "han": "टिक्सी",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Tiksi",
      "hct": "टिक्सी"
    }, {
      "ac": "TIU",
      "an": "Timaru",
      "han": "टिमारू",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Timaru",
      "hct": "टिमारू"
    }, {
      "ac": "TSR",
      "an": "Traian Vuia",
      "han": "ट्राईन वुइआ",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Timisoara",
      "hct": "टिमीसोआरा"
    }, {
      "ac": "YTS",
      "an": "Timmins Municipal",
      "han": "टिम्मिन्स म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Timmins",
      "hct": "टिम्मिन्स"
    }, {
      "ac": "TNC",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tin City",
      "hct": "तीन सिटी"
    }, {
      "ac": "TIQ",
      "an": "Tinian",
      "han": "टीनियान",
      "cn": "Northern Mariana Islands",
      "hcn": "नोर्ठर्न मर्यना आयलॅन्ड्स",
      "cc": "MP",
      "ct": "West Tinian",
      "hct": "वॅस्ट टीनियान"
    }, {
      "ac": "TOD",
      "an": "Pulau Tiओमान",
      "han": "पुलऊ टियोमॅन",
      "cn": "Malaysia",
      "hcn": "मलेशिया",
      "cc": "MY",
      "ct": "Tioman",
      "hct": "टियोमॅन"
    }, {
      "ac": "TIA",
      "an": "Rinas",
      "han": "रिनस",
      "cn": "Albania",
      "hcn": "अल्बानिआ",
      "cc": "AL",
      "ct": "Tirana",
      "hct": "तिरना"
    }, {
      "ac": "TRE",
      "an": "Tiree",
      "han": "तिरी",
      "cn": "United Kingdom",
      "hcn": "युनाईटेड किंग्डोम",
      "cc": "GB",
      "ct": "Tiree",
      "hct": "तिरी"
    }, {
      "ac": "TGM",
      "an": "Transilvania Targu Mures",
      "han": "ट्रान्सिल्वाणिआ टार्गू मुर्स",
      "cn": "Romania",
      "hcn": "रोमेनिआ",
      "cc": "RO",
      "ct": "Tirgu Mures",
      "hct": "तिर्गू मुर्स"
    }, {
      "ac": "TIV",
      "an": "Tivat",
      "han": "टिवट",
      "cn": "Montenegro",
      "hcn": "मोन्टनेग्रो",
      "cc": "ME",
      "ct": "Tivat",
      "hct": "टिवट"
    }, {
      "ac": "TOB",
      "an": "Gamal Abdel Nasser",
      "han": "गमल अब्देल नासर",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Tobruk",
      "hct": "टोब्रुक"
    }, {
      "ac": "TOG",
      "an": "Togiak",
      "han": "टोगियाक",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Togiak Village",
      "hct": "टोगियाक विलेज"
    }, {
      "ac": "TKJ",
      "an": "Tok",
      "han": "तोक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tok",
      "hct": "तोक"
    }, {
      "ac": "OOK",
      "an": "Toksook Bay",
      "han": "तोक्सूक बे",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Toksook Bay",
      "hct": "तोक्सूक बे"
    }, {
      "ac": "TKN",
      "an": "Tokunoshima",
      "han": "तोकूनोशिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Tokunoshima",
      "hct": "तोकूनोशिमा"
    }, {
      "ac": "TKS",
      "an": "Tokushima",
      "han": "तोकुशिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Tokushima",
      "hct": "तोकुशिमा"
    }, {
      "ac": "TOL",
      "an": "Toledo Express",
      "han": "तॉलेदो एक्स्प्रेस",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Toledo",
      "hct": "तॉलेदो"
    }, {
      "ac": "TLC",
      "an": "Licenciado Adolfo Lopez Mateos",
      "han": "लिसेन्शीडो आडोल्फ़ो लोपेज़ माटियोस",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Toluca",
      "hct": "तॉलका"
    }, {
      "ac": "TOM",
      "an": "Tombouctou",
      "han": "टोंबौक्टौ",
      "cn": "Mali",
      "hcn": "माली",
      "cc": "ML",
      "ct": "Tombouctou",
      "hct": "टोंबौक्टौ"
    }, {
      "ac": "TOF",
      "an": "Tomsk Bogashevo",
      "han": "टोम्स्क बोगाशेवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Tomsk",
      "hct": "टोम्स्क"
    }, {
      "ac": "TGO",
      "an": "Tongliao",
      "han": "टोन्ग्लिआओ",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Tongliao",
      "hct": "टोन्ग्लिआओ"
    }, {
      "ac": "TGH",
      "an": "Tongoa Island",
      "han": "टोन्गोआ आयलॅन्ड",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Tongoa Island",
      "hct": "टोन्गोआ आयलॅन्ड"
    }, {
      "ac": "TEN",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Tongren",
      "hct": "टोन्ग्रेन"
    }, {
      "ac": "TWB",
      "an": "Toowoomba",
      "han": "तूवूंबा",
      "cn": "Australia ",
      "hcn": "औस्ट्रालिया ",
      "cc": "AU",
      "ct": "Toowoomba ",
      "hct": "तूवूंबा "
    }, {
      "ac": "TRC",
      "an": "Francisco Sarabia",
      "han": "फ़्रान्सिस्को सारबिआ",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Torreon",
      "hct": "टर्रियोन"
    }, {
      "ac": "TOH",
      "an": "Torres Airstrip",
      "han": "टर्र्स एर्स्ट्रिप",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Loh Linua",
      "hct": "लोह लीनुआ"
    }, {
      "ac": "TYF",
      "an": "Torsby",
      "han": "टर्स्बी",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Torsby",
      "hct": "टर्स्बी"
    }, {
      "ac": "TTJ",
      "an": "Tottori",
      "han": "टोतोरी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Tottori",
      "hct": "टोतोरी"
    }, {
      "ac": "TOU",
      "an": "Touho",
      "han": "टौहो",
      "cn": "New Caledonia",
      "hcn": "न्यू कैलडोनिया",
      "cc": "NC",
      "ct": "Touho",
      "hct": "टौहो"
    }, {
      "ac": "TLN",
      "an": "Le Palyvestre",
      "han": "ले पाल्य्वेस्त्रे",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Hyeres",
      "hct": "ह्येर्स"
    }, {
      "ac": "TLS",
      "an": "Blagnac",
      "han": "ब्लग्नेक",
      "cn": "France",
      "hcn": "फ्रांस",
      "cc": "FR",
      "ct": "Toulouse",
      "hct": "टौलौस"
    }, {
      "ac": "TSV",
      "an": "Townsville",
      "han": "टौन्स्वील",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Townsville",
      "hct": "टौन्स्वील"
    }, {
      "ac": "TOY",
      "an": "Toyama",
      "han": "तोयामा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Toyama",
      "hct": "तोयामा"
    }, {
      "ac": "TOE",
      "an": "Nefta",
      "han": "नेफ़्त",
      "cn": "Tunisia",
      "hcn": "ट्यूनीशिया",
      "cc": "TN",
      "ct": "Tozeur",
      "hct": "टोज़ेउर"
    }, {
      "ac": "TZX",
      "an": "Trabzon",
      "han": "ट्राब्ज़ोन",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Trabzon",
      "hct": "ट्राब्ज़ोन"
    }, {
      "ac": "TST",
      "an": "Trang",
      "han": "टरंग",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Trang",
      "hct": "टरंग"
    }, {
      "ac": "TPS",
      "an": "Trapani Birgi",
      "han": "ट्रपाणी बीरगी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Trapani",
      "hct": "ट्रपाणी"
    }, {
      "ac": "TDX",
      "an": "Trat",
      "han": "ट्रट",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Trat",
      "hct": "ट्रट"
    }, {
      "ac": "TVC",
      "an": "Cherry Capital",
      "han": "शेर्री कॅपिटल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Traverse City",
      "hct": "ट्रावेर्से सिटी"
    }, {
      "ac": "REL",
      "an": "Almइरानte Zar",
      "han": "अल्मीरंटे ज़ार",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Trelew",
      "hct": "ट्रेलेव"
    }, {
      "ac": "TRS",
      "an": "Ronchi Dei Legionari",
      "han": "रोन्ची देई लेगियोनारी",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Trieste",
      "hct": "ट्रिएस्ते"
    }, {
      "ac": "TDD",
      "an": "Tte Av Jorge Henrich Arauz",
      "han": "ते अव जोर्गे हेन्रिच आरऊज़",
      "cn": "Bolivia",
      "hcn": "बोलिविया",
      "cc": "BO",
      "ct": "Trinidad",
      "hct": "ट्रिनिडाद"
    }, {
      "ac": "TIP",
      "an": "Tripoli",
      "han": "त्रिपोली",
      "cn": "Libya",
      "hcn": "लिबिया",
      "cc": "LY",
      "ct": "Tripoli",
      "hct": "त्रिपोली"
    }, {
      "ac": "THN",
      "an": "Trollhattan Vanersborg",
      "han": "ट्रोल्हटन वाणर्स्बोर्ग",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Trollhattan",
      "hct": "ट्रोल्हटन"
    }, {
      "ac": "TMT",
      "an": "Trombetas",
      "han": "ट्रोंबेटास",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Trombetas",
      "hct": "ट्रोंबेटास"
    }, {
      "ac": "TOS",
      "an": "Langnes",
      "han": "लंग्न्स",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Tromso",
      "hct": "ट्रोम्सो"
    }, {
      "ac": "TRD",
      "an": "Trondheim Vaernes",
      "han": "ट्रोन्धीम वर्न्स",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Trondheim",
      "hct": "ट्रोन्धीम"
    }, {
      "ac": "TRU",
      "an": "Capitan Carlos Martinez De Pinillos",
      "han": "कापीतान कार्लोस मार्तिनेज़ दे पिनीलोस",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Trujillo",
      "hct": "त्रुजिलो"
    }, {
      "ac": "WTS",
      "an": "Tsiroanओमानdidy",
      "han": "ट्सिरोआनोमंडिदी",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Tsiroanomandidy",
      "hct": "ट्सिरोआनोमंडिदी"
    }, {
      "ac": "TSJ",
      "an": "Tsushima",
      "han": "ट्सुशिमा",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Tsushima",
      "hct": "ट्सुशिमा"
    }, {
      "ac": "TUS",
      "an": "Tucson",
      "han": "तुकसन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tucson",
      "hct": "तुकसन"
    }, {
      "ac": "TUC",
      "an": "Teniente Benjamin Matienzo",
      "han": "तेनींटे बेन्जमिन मतीन्ज़ो",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Tucuman",
      "hct": "तुकमॅन"
    }, {
      "ac": "TUR",
      "an": "Tucurui",
      "han": "तुकरुई",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Tucurui",
      "hct": "तुकरुई"
    }, {
      "ac": "TFI",
      "an": "Tufi",
      "han": "तुफ़ी",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Tufi",
      "hct": "तुफ़ी"
    }, {
      "ac": "TUG",
      "an": "Tuguegarao",
      "han": "टुगुएगराव",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Tuguegarao",
      "hct": "टुगुएगराव"
    }, {
      "ac": "YUB",
      "an": "Tuktoyaktuk",
      "han": "तुक्टोयक्टुक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Tuktoyaktuk",
      "hct": "तुक्टोयक्टुक"
    }, {
      "ac": "TUA",
      "an": "Teniente Coronel Luis A Mantilla",
      "han": "तेनींटे कोरोनेल लुईस ए मंतिला",
      "cn": "Ecuador",
      "hcn": "इक्वादोर‌",
      "cc": "EC",
      "ct": "Tulcan",
      "hct": "तूलकन"
    }, {
      "ac": "TLE",
      "an": "Toliara",
      "han": "तॉलिरा",
      "cn": "Madagascar",
      "hcn": "मादागॅसकार",
      "cc": "MG",
      "ct": "Toliara",
      "hct": "तॉलिरा"
    }, {
      "ac": "ZFN",
      "an": "Tulita",
      "han": "तुलिता",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Tulita",
      "hct": "तुलिता"
    }, {
      "ac": "TUL",
      "an": "Tulsa",
      "han": "तुलसा",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tulsa",
      "hct": "तुलसा"
    }, {
      "ac": "TLT",
      "an": "Tuluksak",
      "han": "तुलुक्सक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tuluksak",
      "hct": "तुलुक्सक"
    }, {
      "ac": "TCO",
      "an": "La Florida",
      "han": "ला फ़्लोरिदा",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Tumaco",
      "hct": "तुमाको"
    }, {
      "ac": "TBP",
      "an": "Pedro Canga",
      "han": "पेद्रो कंगा",
      "cn": "Peru",
      "hcn": "पेरू",
      "cc": "PE",
      "ct": "Tumbes",
      "hct": "तुम्बेस"
    }, {
      "ac": "TUN",
      "an": "Carthage",
      "han": "कर्थागे",
      "cn": "Tunisia",
      "hcn": "ट्यूनीशिया",
      "cc": "TN",
      "ct": "Tunis",
      "hct": "टुनीस"
    }, {
      "ac": "WTL",
      "an": "Tuntutuliak",
      "han": "टुंटुटुलिआक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tuntutuliak",
      "hct": "टुंटुटुलिआक"
    }, {
      "ac": "TNK",
      "an": "Tununak",
      "han": "टुनुनक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tununak",
      "hct": "टुनुनक"
    }, {
      "ac": "TXN",
      "an": "Tunxi",
      "han": "टुन्क्सी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Huangshan",
      "hct": "हुआंगशान"
    }, {
      "ac": "TUP",
      "an": "C D Lemons Municipal",
      "han": "सी डी लेमोन्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tupelo",
      "hct": "टुपेलो"
    }, {
      "ac": "TUI",
      "an": "Turaif",
      "han": "तूराईफ़",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Turaif",
      "hct": "तूराईफ़"
    }, {
      "ac": "TUK",
      "an": "Turbat",
      "han": "तुरबॅट",
      "cn": "Pakistan",
      "hcn": "पाकिस्तान",
      "cc": "PK",
      "ct": "Turbat",
      "hct": "तुरबॅट"
    }, {
      "ac": "TRN",
      "an": "Torino Caselle",
      "han": "टोरिनो केसेल",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Turin",
      "hct": "तूरिन"
    }, {
      "ac": "TKU",
      "an": "Turku",
      "han": "तूरकू",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Turku",
      "hct": "तूरकू"
    }, {
      "ac": "TGZ",
      "an": "Angel Albino Corzo",
      "han": "एंजल आल्बीनो कोर्ज़ो",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Tuxtla Gutierrez",
      "hct": "तुक्स्ट्ला गुतिएर्रेज़"
    }, {
      "ac": "TBB",
      "an": "Dong Tac",
      "han": "डोंग तक",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Tuy Hoa",
      "hct": "तुय हो"
    }, {
      "ac": "TWF",
      "an": "Twin Falls City County",
      "han": "ट्विन फॉल्स सिटी कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Twin Falls",
      "hct": "ट्विन फॉल्स"
    }, {
      "ac": "TWA",
      "an": "Twin Hills",
      "han": "ट्विन हिल्लस",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Twin Hills",
      "hct": "ट्विन हिल्लस"
    }, {
      "ac": "TYR",
      "an": "Pounds Field",
      "han": "पौन्ड्स फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Tyler",
      "hct": "त्यलर"
    }, {
      "ac": "TJM",
      "an": "Roschino",
      "han": "रोस्खिनो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Tyumen",
      "hct": "त्युमेन"
    }, {
      "ac": "UBJ",
      "an": "Yamaguchi Ube",
      "han": "यमगुची उबे",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Yamaguchi",
      "hct": "यमगुची"
    }, {
      "ac": "UBA",
      "an": "Uberaba",
      "han": "उबेरब",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Uberaba",
      "hct": "उबेरब"
    }, {
      "ac": "UDI",
      "an": "Ten Cel Av Cesar Bombonato",
      "han": "तेन सेल अव सेसर बोम्बोनातो",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Uberlandia",
      "hct": "उबेर्लांडिआ"
    }, {
      "ac": "UBP",
      "an": "Ubon Ratchathani",
      "han": "उबोन रत्च्थाणी",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Ubon Ratchathani",
      "hct": "उबोन रत्च्थाणी"
    }, {
      "ac": "UTH",
      "an": "Udon Thani",
      "han": "उडन ठाणी",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Udon Thani",
      "hct": "उडन ठाणी"
    }, {
      "ac": "UFA",
      "an": "Ufa",
      "han": "उफ़ा",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Ufa",
      "hct": "उफ़ा"
    }, {
      "ac": "UGI",
      "an": "Uganik",
      "han": "उगाणीक",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Uganik",
      "hct": "उगाणीक"
    }, {
      "ac": "UJA",
      "an": "",
      "han": "",
      "cn": "India",
      "hcn": "�ारत‌",
      "cc": "IN",
      "ct": "Ujjain",
      "hct": "उज्जैन"
    }, {
      "ac": "UPG",
      "an": "Hasanudin",
      "han": "हसनुडीन",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Ujung Pandang",
      "hct": "उजुन्ग पंदंग"
    }, {
      "ac": "UCT",
      "an": "Ukhta",
      "han": "उख्ता",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Ukhta",
      "hct": "उख्ता"
    }, {
      "ac": "ULN",
      "an": "Buyant Uhaa",
      "han": "बुयांत उहा",
      "cn": "Mongolia",
      "hcn": "मोंगोलिआ",
      "cc": "MN",
      "ct": "Ulaanbaatar",
      "hct": "उलान्बातार"
    }, {
      "ac": "UUD",
      "an": "Mukhino",
      "han": "मूखिनो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Ulan",
      "hct": "उलन"
    }, {
      "ac": "HLH",
      "an": "Ulanhot",
      "han": "उलनहॉट",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Ulanhot",
      "hct": "उलनहॉट"
    }, {
      "ac": "ULB",
      "an": "Ulai",
      "han": "उलै",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Ambryn Island",
      "hct": "अम्ब्र्य्न आयलॅन्ड"
    }, {
      "ac": "USN",
      "an": "Ulsan",
      "han": "उलसन",
      "cn": "Korea",
      "hcn": "कोरिया",
      "cc": "KR",
      "ct": "Ulsan",
      "hct": "उलसन"
    }, {
      "ac": "ULY",
      "an": "Ulyanovsk East",
      "han": "उल्यानोव्स्क ईस्ट",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Ulyanovsk",
      "hct": "उल्यानोव्स्क"
    }, {
      "ac": "UME",
      "an": "Umea",
      "han": "ऊमी",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Umea",
      "hct": "ऊमी"
    }, {
      "ac": "YUD",
      "an": "Umiujaq",
      "han": "ऊमीऊजक़",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Umiujaq",
      "hct": "ऊमीऊजक़"
    }, {
      "ac": "UTT",
      "an": "Umtata",
      "han": "ऊम्ताटा",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Umtata",
      "hct": "ऊम्ताटा"
    }, {
      "ac": "UNK",
      "an": "Unalakleet",
      "han": "उनालक्ळीत",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Unalakleet",
      "hct": "उनालक्ळीत"
    }, {
      "ac": "UTN",
      "an": "Upington",
      "han": "उपींग्टन",
      "cn": "South Africa",
      "hcn": "द‌क्षिण‌ अफ़्रीका",
      "cc": "ZA",
      "ct": "Upington",
      "hct": "उपींग्टन"
    }, {
      "ac": "URJ",
      "an": "Uraj",
      "han": "उराज",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Uraj",
      "hct": "उराज"
    }, {
      "ac": "URA",
      "an": "Uralsk",
      "han": "उरल्स्क",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Uralsk",
      "hct": "उरल्स्क"
    }, {
      "ac": "UGC",
      "an": "Urgench",
      "han": "उर्गेन्च",
      "cn": "Uzbekistan",
      "hcn": "उज़्बेकिस्तान‌",
      "cc": "UZ",
      "ct": "Urgench",
      "hct": "उर्गेन्च"
    }, {
      "ac": "OMH",
      "an": "Urmieh",
      "han": "उर्मीह",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Urmieh",
      "hct": "उर्मीह"
    }, {
      "ac": "UPN",
      "an": "Licenciado Y Gen Ignacio Lopez Rayon",
      "han": "लिसेन्शीडो वाय जन इग्नेसियो लोपेज़ रेओन",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Uruapan",
      "hct": "उरुपन"
    }, {
      "ac": "URG",
      "an": "Rubem Berta",
      "han": "रुबेम बेर्ता",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Uruguaiana",
      "hct": "उरुगुआइयाना"
    }, {
      "ac": "URC",
      "an": "Diwopu",
      "han": "दिवोपू",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Urumqi",
      "hct": "उरुम्क़ी"
    }, {
      "ac": "USQ",
      "an": "Usak",
      "han": "उसक",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Usak",
      "hct": "उसक"
    }, {
      "ac": "USH",
      "an": "Ushuaia Malvinas आर्जेंटीनाs",
      "han": "उशुआइया माल्वीनस आर्गेन्टीनस",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Ushuaia",
      "hct": "उशुआइया"
    }, {
      "ac": "USK",
      "an": "Usinsk",
      "han": "उसीन्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Usinsk",
      "hct": "उसीन्स्क"
    }, {
      "ac": "UKK",
      "an": "Ust Kamenogorsk",
      "han": "उस्ट कमेनोगोर्स्क",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Ust Kamenogorsk",
      "hct": "उस्ट कमेनोगोर्स्क"
    }, {
      "ac": "UTP",
      "an": "U Taphao",
      "han": "यू तफव",
      "cn": "Thailand",
      "hcn": "थाईलैंड‌",
      "cc": "TH",
      "ct": "Pattaya",
      "hct": "पटय"
    }, {
      "ac": "UMD",
      "an": "Uummannaq",
      "han": "ऊम्मन्नक़",
      "cn": "Greenland",
      "hcn": "ग्रीन‌लैंड‌",
      "cc": "GL",
      "ct": "Uummannaq",
      "hct": "ऊम्मन्नक़"
    }, {
      "ac": "UDJ",
      "an": "Uzhhorod",
      "han": "उऴ्होरोड",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Uzhgorod",
      "hct": "उझ़गोरोड"
    }, {
      "ac": "VAA",
      "an": "Vaasa",
      "han": "वासा",
      "cn": "Finland",
      "hcn": "फ़िनलॅंड",
      "cc": "FI",
      "ct": "Vaasa",
      "hct": "वासा"
    }, {
      "ac": "VDS",
      "an": "Vadso",
      "han": "वाड्सो",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Vadso",
      "hct": "वाड्सो"
    }, {
      "ac": "VRY",
      "an": "",
      "han": "",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Vaeroy",
      "hct": "वराय"
    }, {
      "ac": "EGE",
      "an": "Eagle County",
      "han": "ईगल कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Vail Eagle",
      "hct": "वैल ईगल"
    }, {
      "ac": "YVO",
      "an": "Val d Or Municipal",
      "han": "वाल डी ओर म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Val D''Or",
      "hct": "वाल डीओर"
    }, {
      "ac": "VDZ",
      "an": "Valdez Municipal",
      "han": "वल्डेज़ म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Valdez",
      "hct": "वल्डेज़"
    }, {
      "ac": "ZAL",
      "an": "Pichoy",
      "han": "पिचोय",
      "cn": "Chile",
      "hcn": "चिली",
      "cc": "CL",
      "ct": "Valdivia",
      "hct": "वालदिव्या"
    }, {
      "ac": "VLD",
      "an": "Valdosta Regional",
      "han": "वल्डोस्टा रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Valdosta",
      "hct": "वल्डोस्टा"
    }, {
      "ac": "VLC",
      "an": "Valencia",
      "han": "वालेनसिया",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Valencia",
      "hct": "वालेनसिया"
    }, {
      "ac": "VLN",
      "an": "Valenica",
      "han": "वालेनिका",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Valencia",
      "hct": "वालेनसिया"
    }, {
      "ac": "VLV",
      "an": "Dr Antonio Nicolas Briceno",
      "han": "ड्र एंटोनियो निकोलास ब्रिसेनो",
      "cn": "Venezuela",
      "hcn": "वेनेज़्वेला",
      "cc": "VE",
      "ct": "Valera",
      "hct": "वालेरा"
    }, {
      "ac": "VLS",
      "an": "Valesdir",
      "han": "वालेस्डिर",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Valesdir",
      "hct": "वालेस्डिर"
    }, {
      "ac": "VLL",
      "an": "Valladolid",
      "han": "वलाडोलिद",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Valladolid",
      "hct": "वलाडोलिद"
    }, {
      "ac": "VUP",
      "an": "Alfonso Lopez Pumarejo",
      "han": "आल्फान्सो लोपेज़ पूमरेजो",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Valledupar",
      "hct": "वालेदुपेर"
    }, {
      "ac": "VPS",
      "an": "Ft Walton Beach",
      "han": "फ़्ट वाल्टन बीच",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Valparaiso",
      "hct": "वल्परइसो"
    }, {
      "ac": "VDE",
      "an": "Hierro",
      "han": "हिएर्रो",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Hierro",
      "hct": "हिएर्रो"
    }, {
      "ac": "VAN",
      "an": "Van",
      "han": "वन",
      "cn": "Turkey",
      "hcn": "तुर्की",
      "cc": "TR",
      "ct": "Van",
      "hct": "वन"
    }, {
      "ac": "VAI",
      "an": "Vanimo",
      "han": "वाणीमो",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Vanimo",
      "hct": "वाणीमो"
    }, {
      "ac": "VBV",
      "an": "Vanua Balavu",
      "han": "वनुआ बालवू",
      "cn": "Fiji",
      "hcn": "फ़िजी",
      "cc": "FJ",
      "ct": "Vanua Balavu",
      "hct": "वनुआ बालवू"
    }, {
      "ac": "VAW",
      "an": "Vardoe",
      "han": "वर्डोय",
      "cn": "Norway",
      "hcn": "नॉर्वे",
      "cc": "NO",
      "ct": "Vardoe",
      "hct": "वर्डोय"
    }, {
      "ac": "VAR",
      "an": "Varna",
      "han": "वर्ना",
      "cn": "Bulgaria",
      "hcn": "बूलगारिआ",
      "cc": "BG",
      "ct": "Varna",
      "hct": "वर्ना"
    }, {
      "ac": "VAV",
      "an": "Vavau",
      "han": "ववऊ",
      "cn": "Tonga",
      "hcn": "टोंगा",
      "cc": "TO",
      "ct": "Vava'u",
      "hct": "ववायू"
    }, {
      "ac": "VXO",
      "an": "Vaxjo",
      "han": "वक्स्जो",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Vaxjo",
      "hct": "वक्स्जो"
    }, {
      "ac": "VEE",
      "an": "Venetie",
      "han": "वेनेटी",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Venetie",
      "hct": "वेनेटी"
    }, {
      "ac": "VER",
      "an": "Las Bajadas General Heriberto Jara",
      "han": "लस बाजादास जनरल हेरिबेर्टो जरा",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Veracruz",
      "hct": "वेराक्रुज़"
    }, {
      "ac": "VEL",
      "an": "Vernal",
      "han": "वरनल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Vernal",
      "hct": "वरनल"
    }, {
      "ac": "VRN",
      "an": "Verona",
      "han": "वेरोना",
      "cn": "Italy",
      "hcn": "इटली",
      "cc": "IT",
      "ct": "Verona",
      "hct": "वेरोना"
    }, {
      "ac": "VEY",
      "an": "Vestmannaeyjar",
      "han": "वेस्ट्मन्नाईजर",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Vestmannaeyjar",
      "hct": "वेस्ट्मन्नाईजर"
    }, {
      "ac": "VFA",
      "an": "Victoria Falls",
      "han": "विक्टोरिआ फॉल्स",
      "cn": "Zimbabwe",
      "hcn": "ज़िम्बाब्वे",
      "cc": "ZW",
      "ct": "Victoria Falls",
      "hct": "विक्टोरिआ फॉल्स"
    }, {
      "ac": "YWH",
      "an": "Victoria Inner Harbour",
      "han": "विक्टोरिआ इन्नर हर्बौर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Victoria",
      "hct": "विक्टोरिआ"
    }, {
      "ac": "YYJ",
      "an": "Victoria",
      "han": "विक्टोरिआ",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "VICTORIA",
      "hct": "विक्टोरिआ"
    }, {
      "ac": "VCT",
      "an": "Victoria Regional",
      "han": "विक्टोरिआ रिजनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Victoria",
      "hct": "विक्टोरिआ"
    }, {
      "ac": "VDM",
      "an": "Gobernador Castello",
      "han": "गोबेर्नडोर कॅस्टेलो",
      "cn": "Argentina",
      "hcn": "आर्जेंटीना",
      "cc": "AR",
      "ct": "Viedma",
      "hct": "वीद्मा"
    }, {
      "ac": "VTE",
      "an": "Wattay",
      "han": "वटे",
      "cn": "Laos",
      "hcn": "लाव्स",
      "cc": "LA",
      "ct": "Vientiane",
      "hct": "वीन्टिआने"
    }, {
      "ac": "VGO",
      "an": "Vigo",
      "han": "विगो",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Vigo",
      "hct": "विगो"
    }, {
      "ac": "VNX",
      "an": "Vilankulo",
      "han": "वीलन्कुलो",
      "cn": "Mozambique",
      "hcn": "मोजा़म्बीक‌",
      "cc": "MZ",
      "ct": "Vilankulu",
      "hct": "वीलनकुल्लु"
    }, {
      "ac": "VHM",
      "an": "Vilhelmina",
      "han": "वील्हेलमिना",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Vilhelmina",
      "hct": "वील्हेलमिना"
    }, {
      "ac": "BVH",
      "an": "Vilhena",
      "han": "वील्हेना",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Vilhena",
      "hct": "वील्हेना"
    }, {
      "ac": "VGZ",
      "an": "Villagarzon",
      "han": "वीलगर्ज़ोन",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Villagarzon",
      "hct": "वीलगर्ज़ोन"
    }, {
      "ac": "VSA",
      "an": "C P A Carlos Rovirosa",
      "han": "सी पी ए कार्लोस रोविरोसा",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Villahermosa",
      "hct": "वीलहेर्मोसा"
    }, {
      "ac": "VVC",
      "an": "Vanguardia",
      "han": "वंगुरदिआ",
      "cn": "Colombia",
      "hcn": "कोलंबिया",
      "cc": "CO",
      "ct": "Villavicencio",
      "hct": "वीलविसेन्सियो"
    }, {
      "ac": "VNO",
      "an": "Vilnius",
      "han": "वील्नीऊस",
      "cn": "Lithuania",
      "hcn": "लिठुआणिआ",
      "cc": "LT",
      "ct": "Vilnius",
      "hct": "वील्नीऊस"
    }, {
      "ac": "VII",
      "an": "Vinh",
      "han": "विन्ह",
      "cn": "Vietnam",
      "hcn": "विय‌त‌नाम‌",
      "cc": "VN",
      "ct": "Vinh",
      "hct": "विन्ह"
    }, {
      "ac": "VRC",
      "an": "Virac",
      "han": "विरक",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Virac",
      "hct": "विरक"
    }, {
      "ac": "VIS",
      "an": "Visalia",
      "han": "विसालिया",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Visalia",
      "hct": "विसालिया"
    }, {
      "ac": "VBY",
      "an": "Visby",
      "han": "विस्बी",
      "cn": "Sweden",
      "hcn": "स्वीड‌न‌",
      "cc": "SE",
      "ct": "Visby",
      "hct": "विस्बी"
    }, {
      "ac": "VDC",
      "an": "Vitoria Da Cnquis",
      "han": "विटोरिआ दा क्न्क़ुइस",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Vitoria Da Cnquis",
      "hct": "विटोरिआ दा क्न्क़ुइस"
    }, {
      "ac": "VIX",
      "an": "Eurico Sales",
      "han": "एउरिको सेल्स",
      "cn": "Brazil",
      "hcn": "ब्राज़ील",
      "cc": "BR",
      "ct": "Vitoria",
      "hct": "विटोरिआ"
    }, {
      "ac": "VIT",
      "an": "Vitoria",
      "han": "विटोरिआ",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Vitoria",
      "hct": "विटोरिआ"
    }, {
      "ac": "OGZ",
      "an": "Beslan",
      "han": "बेस्लन",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Beslan",
      "hct": "बेस्लन"
    }, {
      "ac": "VVO",
      "an": "Vladivostok",
      "han": "व्लादिवोस्टोक",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "VLADIVOSTOK",
      "hct": "व्लादिवोस्टोक"
    }, {
      "ac": "VOG",
      "an": "Gumrak",
      "han": "गूम्रक",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Volgograd",
      "hct": "वोल्गोग्रेद"
    }, {
      "ac": "VOL",
      "an": "Nea Anchialos",
      "han": "नी अंचियालोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Nea Anghialos",
      "hct": "नी अंघियालोस"
    }, {
      "ac": "VPN",
      "an": "Vopnafjordur",
      "han": "वोप्नाफ़्जोर्डर",
      "cn": "Iceland",
      "hcn": "आईसलॅंद",
      "cc": "IS",
      "ct": "Vopnafjordur",
      "hct": "वोप्नाफ़्जोर्डर"
    }, {
      "ac": "VKT",
      "an": "Vorkuta",
      "han": "वोर्कूता",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Vorkuta",
      "hct": "वोर्कूता"
    }, {
      "ac": "VOZ",
      "an": "Chertovitskoye",
      "han": "चर्टोवित्स्कोये",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Voronezh",
      "hct": "वोरोनेझ़"
    }, {
      "ac": "YWK",
      "an": "Wabush",
      "han": "वाबुश",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Wabush",
      "hct": "वाबुश"
    }, {
      "ac": "ACT",
      "an": "Madison Cooper",
      "han": "मॅदीसन कूपर",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Waco",
      "hct": "वाको"
    }, {
      "ac": "WAE",
      "an": "",
      "han": "",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Wadi Ad Dawasir",
      "hct": "वाडी ऍद दवासर"
    }, {
      "ac": "WHF",
      "an": "",
      "han": "",
      "cn": "Sudan",
      "hcn": "सुदान",
      "cc": "SD",
      "ct": "Wadi Halfa",
      "hct": "वाडी हल्फ़ा"
    }, {
      "ac": "WGA",
      "an": "Forrest Hill",
      "han": "फ़ोर्रेस्ट हील",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Wagga",
      "hct": "वाग्गा"
    }, {
      "ac": "WGP",
      "an": "Mau Hau",
      "han": "मऊ हऊ",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Waingapu",
      "hct": "वैन्गापू"
    }, {
      "ac": "AIN",
      "an": "Wainwright",
      "han": "वायन्व्रिघ्ट",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wainwright",
      "hct": "वायन्व्रिघ्ट"
    }, {
      "ac": "NTQ",
      "an": "Noto",
      "han": "नोटो",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Wajima",
      "hct": "वाजीमा"
    }, {
      "ac": "WKJ",
      "an": "Wakkanai",
      "han": "वक्कनाई",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Wakkanai",
      "hct": "वक्कनाई"
    }, {
      "ac": "WLH",
      "an": "Walaha",
      "han": "वालहा",
      "cn": "Vanuatu",
      "hcn": "वानुआतु",
      "cc": "VU",
      "ct": "Walaha",
      "hct": "वालहा"
    }, {
      "ac": "WAA",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wales",
      "hct": "वालेस"
    }, {
      "ac": "ALW",
      "an": "Walla Walla City County",
      "han": "वाला वाला सिटी कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Walla Walla",
      "hct": "वाला वाला"
    }, {
      "ac": "WLS",
      "an": "Wallis",
      "han": "वालिस",
      "cn": "Wallis and Futuna",
      "hcn": "वालिस‌ और‌ फुतुना द्वीप‌स‌मूह‌",
      "cc": "WF",
      "ct": "Wallis",
      "hct": "वालिस"
    }, {
      "ac": "WVB",
      "an": "Walvis Bay",
      "han": "वालविस बे",
      "cn": "Namibia",
      "hcn": "नामीबिया",
      "cc": "NA",
      "ct": "Walvis Bay",
      "hct": "वाल्विस बे"
    }, {
      "ac": "WKA",
      "an": "Wanaka",
      "han": "वानका",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Wanaka",
      "hct": "वानका"
    }, {
      "ac": "WAG",
      "an": "Wanganui",
      "han": "वांगनुइ",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Wanganui",
      "hct": "वांगनुइ"
    }, {
      "ac": "WXN",
      "an": "Wanxian",
      "han": "वन्क्सियान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Wanxian",
      "hct": "वन्क्सियान"
    }, {
      "ac": "WBM",
      "an": "Wapenamanda",
      "han": "वपेनामंडा",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Wapenamanda",
      "hct": "वपेनामंडा"
    }, {
      "ac": "YKQ",
      "an": "Waskaganish",
      "han": "वास्कागाणीश",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Waskaganish",
      "hct": "वास्कागाणीश"
    }, {
      "ac": "KWF",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Waterfall",
      "hct": "वाटर्फ़ल"
    }, {
      "ac": "WAT",
      "an": "Waterford",
      "han": "वाटरफोर्ड",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Waterford",
      "hct": "वाटरफोर्ड"
    }, {
      "ac": "ALO",
      "an": "Livingston Betsworth Fld",
      "han": "लिवींग्स्टन बेट्स्वर्थ फ़्ल्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Waterloo",
      "hct": "वाटर्लू"
    }, {
      "ac": "ART",
      "an": "Watertown",
      "han": "वाटरटाउन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Watertown",
      "hct": "वाटरटाउन"
    }, {
      "ac": "ATY",
      "an": "Watertown Municipal",
      "han": "वाटरटाउन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "WATERTOWN",
      "hct": "वाटरटाउन"
    }, {
      "ac": "CWA",
      "an": "Central Wisconsin",
      "han": "सेन्ट्रल वीस्कन्सीन",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wausau",
      "hct": "वऊसऊ"
    }, {
      "ac": "YWP",
      "an": "Webequie",
      "han": "वेबेक़ुई",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Webequie",
      "hct": "वेबेक़ुई"
    }, {
      "ac": "EJH",
      "an": "Wejh",
      "han": "वेझ",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Wejh",
      "hct": "वेझ"
    }, {
      "ac": "WEF",
      "an": "Weifang",
      "han": "वीफ़ंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Weifang",
      "hct": "वीफ़ंग"
    }, {
      "ac": "WEH",
      "an": "Weihai",
      "han": "वीहाई",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Weihai",
      "hct": "वीहाई"
    }, {
      "ac": "WEI",
      "an": "Weipa",
      "han": "वीपा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Weipa",
      "hct": "वीपा"
    }, {
      "ac": "YNC",
      "an": "Wemindji",
      "han": "वेमिन्डजी",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Wemindji",
      "hct": "वेमिन्डजी"
    }, {
      "ac": "EAT",
      "an": "Pangborn Memorial",
      "han": "पंग्बोर्न मेमोरिल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wenatchee",
      "hct": "वेनाची"
    }, {
      "ac": "WNZ",
      "an": "Wenzhou Yongqiang",
      "han": "वेन्झ़ै योन्ग्क़ियांग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Wenzhou",
      "hct": "वेन्झ़ै"
    }, {
      "ac": "PBI",
      "an": "Palm Beach",
      "han": "पाल्म बीच",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "West Palm Beach",
      "hct": "वॅस्ट पाल्म बीच"
    }, {
      "ac": "KWP",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "West Point",
      "hct": "वॅस्ट पोईंत"
    }, {
      "ac": "WYS",
      "an": "Yellowstone",
      "han": "येलौस्टोन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "West Yellowstone",
      "hct": "वॅस्ट येलौस्टोन"
    }, {
      "ac": "HPN",
      "an": "Westchester County",
      "han": "वॅस्टचेस्टर कौंटी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Westchester County",
      "hct": "वॅस्टचेस्टर कौंटी"
    }, {
      "ac": "GWT",
      "an": "Westerland Sylt",
      "han": "वेस्टर्लांद सिल्ट",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Westerland",
      "hct": "वेस्टर्लांद"
    }, {
      "ac": "WST",
      "an": "Weston",
      "han": "वेस्तन",
      "cn": "Ireland",
      "hcn": "आय‌र‌लैंड‌",
      "cc": "IE",
      "ct": "Leixlip",
      "hct": "लीक्स्लिप"
    }, {
      "ac": "WSZ",
      "an": "Westport",
      "han": "वॅस्टप़ॉर्ट",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Westport",
      "hct": "वॅस्टप़ॉर्ट"
    }, {
      "ac": "WSX",
      "an": "Westsound",
      "han": "वॅस्टसौन्ड",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Westsound",
      "hct": "वॅस्टसौन्ड"
    }, {
      "ac": "WWK",
      "an": "Wewak",
      "han": "वेवक",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Wewak",
      "hct": "वेवक"
    }, {
      "ac": "YLE",
      "an": "Lac La Martre",
      "han": "लेक ला मार्ट्रे",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Lac La Martre",
      "hct": "लेक ला मार्ट्रे"
    }, {
      "ac": "WHK",
      "an": "Whakatane",
      "han": "व्हकतणे",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Whakatane",
      "hct": "व्हकतणे"
    }, {
      "ac": "YXN",
      "an": "Whale Cove",
      "han": "व्हाले कोव",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Whale Cove",
      "hct": "व्हाले कोव"
    }, {
      "ac": "WWP",
      "an": "Whale Pass",
      "han": "व्हाले पास्स",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Whale Pass",
      "hct": "व्हाले पास्स"
    }, {
      "ac": "WRE",
      "an": "Whangarei",
      "han": "व्हांगरी",
      "cn": "New Zealand",
      "hcn": "न्यूज़ीलैंड‌",
      "cc": "NZ",
      "ct": "Whangarei",
      "hct": "व्हांगरी"
    }, {
      "ac": "WMO",
      "an": "White Mountain",
      "han": "वाइट मौंतैन",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "White Mountain",
      "hct": "वाइट मौंतैन"
    }, {
      "ac": "YXY",
      "an": "Whitehorse",
      "han": "व्हिटेहोर्स",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Whitehorse",
      "hct": "व्हिटेहोर्स"
    }, {
      "ac": "WYA",
      "an": "Whyalla",
      "han": "व्ह्याला",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Whyalla",
      "hct": "व्ह्याला"
    }, {
      "ac": "SPS",
      "an": "Wichita Falls Municipal",
      "han": "विचिता फॉल्स म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wichita Falls",
      "hct": "विचिता फॉल्स"
    }, {
      "ac": "ICT",
      "an": "Mid Continent",
      "han": "मिड कोन्टिनेंट",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wichita",
      "hct": "विचिता"
    }, {
      "ac": "UWE",
      "an": "",
      "han": "",
      "cn": "Germany",
      "hcn": "ज‌र्म‌नी",
      "cc": "DE",
      "ct": "Wiesbaden",
      "hct": "वीस्बद्न"
    }, {
      "ac": "YWM",
      "an": "Williams Harbour",
      "han": "विलियम्स हर्बौर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Williams Harbour",
      "hct": "विलियम्स हर्बौर"
    }, {
      "ac": "YWL",
      "an": "Williams Lake Municipal",
      "han": "विलियम्स लेक म्यूनिसिपल",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Williams Lake",
      "hct": "विलियम्स लेक"
    }, {
      "ac": "IPT",
      "an": "Williamsport Lycoming Municipal",
      "han": "विलियम्सप़ॉर्ट ल्य्कोमिंग म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Williamsport",
      "hct": "विलियम्सप़ॉर्ट"
    }, {
      "ac": "ISN",
      "an": "Sloulin Field",
      "han": "स्लौलीन फ़ील्ड",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Williston",
      "hct": "विलिस्टन"
    }, {
      "ac": "ILM",
      "an": "New Hanover Cty",
      "han": "न्यू हानोवर क्टी",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "WILMINGTON",
      "hct": "विल्मिंग्टन"
    }, {
      "ac": "WUN",
      "an": "Wiluna",
      "han": "विलुना",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Wiluna ",
      "hct": "विलुना "
    }, {
      "ac": "ERS",
      "an": "Eros",
      "han": "एरोस",
      "cn": "Namibia",
      "hcn": "नामीबिया",
      "cc": "NA",
      "ct": "Windhoek",
      "hct": "विन्ढोयक"
    }, {
      "ac": "WDH",
      "an": "Hosea Kutako",
      "han": "होसी कूटाको",
      "cn": "Namibia",
      "hcn": "नामीबिया",
      "cc": "NA",
      "ct": "Windhoek",
      "hct": "विन्ढोयक"
    }, {
      "ac": "WNR",
      "an": "Windorah",
      "han": "विन्दोरा",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Windorah ",
      "hct": "विन्दोरा "
    }, {
      "ac": "BDL",
      "an": "Bradley",
      "han": "ब्रेडले",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Windsor Locks",
      "hct": "विंड्सर लोक्स"
    }, {
      "ac": "YQG",
      "an": "Windsor",
      "han": "विंड्सर",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Windsor",
      "hct": "विंड्सर"
    }, {
      "ac": "WIN",
      "an": "Winton",
      "han": "विन्टन",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Winton ",
      "hct": "विन्टन "
    }, {
      "ac": "WPM",
      "an": "Wipim",
      "han": "विपिम",
      "cn": "Papua New Guinea",
      "hcn": "पापुआ न्यू गिनी",
      "cc": "PG",
      "ct": "Wipim",
      "hct": "विपिम"
    }, {
      "ac": "WJA",
      "an": "Woja",
      "han": "वोजा",
      "cn": "Marshall Islands",
      "hcn": "मार्शल आयलॅन्ड्स",
      "cc": "MH",
      "ct": "Majuro Atoll",
      "hct": "मजुरो अटोल"
    }, {
      "ac": "OLF",
      "an": "Wolf Point Int'l",
      "han": "वोल्फ़ पोईंत इंतएल",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wolf Point",
      "hct": "वोल्फ़ पोईंत"
    }, {
      "ac": "WOL",
      "an": "Wollongong",
      "han": "वोलोन्गोन्ग",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Wollongong ",
      "hct": "वोलोन्गोन्ग "
    }, {
      "ac": "WJU",
      "an": "Wonju",
      "han": "वोन्जू",
      "cn": "South Korea",
      "hcn": "द‌क्षिण‌ कोरिया",
      "cc": "KR",
      "ct": "Wonju",
      "hct": "वोन्जू"
    }, {
      "ac": "WRL",
      "an": "Worland Municipal",
      "han": "वर्लांद म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Worland",
      "hct": "वर्लांद"
    }, {
      "ac": "WRG",
      "an": "Wrangell",
      "han": "व्रन्जेल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Wrangell",
      "hct": "व्रन्जेल"
    }, {
      "ac": "WRO",
      "an": "Strachowice",
      "han": "स्ट्रचौइस",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Wroclaw",
      "hct": "व्रोक्लौ"
    }, {
      "ac": "WUA",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Wu Hai",
      "hct": "वू हाई"
    }, {
      "ac": "WUH",
      "an": "Tianhe",
      "han": "टिआन्हे",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Wuhan",
      "hct": "वुहान"
    }, {
      "ac": "WNN",
      "an": "Wunnumin Lake",
      "han": "वुन्नूमिन लेक",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Wunnumin Lake",
      "hct": "वुन्नूमिन लेक"
    }, {
      "ac": "WUX",
      "an": "Wuxi",
      "han": "वुक्सी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Wuxi",
      "hct": "वुक्सी"
    }, {
      "ac": "WUS",
      "an": "Nanping Wuyishan",
      "han": "नाण्पींग वुयिशान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Wuyishan",
      "hct": "वुयिशान"
    }, {
      "ac": "WUZ",
      "an": "Changzhoudao",
      "han": "चंग्झैदाव",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Wuzhou",
      "hct": "वुऴौ"
    }, {
      "ac": "XMN",
      "an": "Xiamen",
      "han": "क्सियामेन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xiamen",
      "hct": "क्सियामेन"
    }, {
      "ac": "SIA",
      "an": "Xiguan",
      "han": "क्सिगुआन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xi An (Xiguan)",
      "hct": "क्सी अन (क्सिगुआन)"
    }, {
      "ac": "XIY",
      "an": "Xianyang",
      "han": "क्सियाण्यांग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xian",
      "hct": "क्सियान"
    }, {
      "ac": "XFN",
      "an": "Xiangfan",
      "han": "क्सियांग्फ़न",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xiangfan",
      "hct": "क्सियांग्फ़न"
    }, {
      "ac": "XIC",
      "an": "Qingshan",
      "han": "क़िंगशान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xichang",
      "hct": "क्सिचंग"
    }, {
      "ac": "XKH",
      "an": "Xieng Khouang",
      "han": "क्सीन्ग खौंग",
      "cn": "Laos",
      "hcn": "लाव्स",
      "cc": "LA",
      "ct": "Phon Savan",
      "hct": "फोन सावन"
    }, {
      "ac": "XIL",
      "an": "Xilinhot",
      "han": "क्सिलीन्होट",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xilinhot",
      "hct": "क्सिलीन्होट"
    }, {
      "ac": "ACX",
      "an": "",
      "han": "",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xingyi",
      "hct": "क्सिंग्यी"
    }, {
      "ac": "XNN",
      "an": "Xining Caojiabu",
      "han": "क्सिनिन्ग काव्जियाबू",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xining",
      "hct": "क्सिनिन्ग"
    }, {
      "ac": "XUZ",
      "an": "Xuzhou Guanyin",
      "han": "क्सुझ़ै गुआन्यिन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Xuzhou",
      "hct": "क्सुझ़ै"
    }, {
      "ac": "YKM",
      "an": "Yakima Terminal",
      "han": "याकिमा टर्मिनल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Yakima",
      "hct": "याकिमा"
    }, {
      "ac": "YAK",
      "an": "Yakutat",
      "han": "याकूतट",
      "cn": "USA",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Yakutat",
      "hct": "याकूतट"
    }, {
      "ac": "YKS",
      "an": "Yakutsk",
      "han": "याकूत्स्क",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Yakutsk",
      "hct": "याकूत्स्क"
    }, {
      "ac": "XMY",
      "an": "Yam Island",
      "han": "यम आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Yam Island",
      "hct": "यम आयलॅन्ड"
    }, {
      "ac": "GAJ",
      "an": "Yamagata",
      "han": "यमेगता",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Yamagata",
      "hct": "यमेगता"
    }, {
      "ac": "ENY",
      "an": "Yan'an",
      "han": "यानअन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yan'an",
      "hct": "यानअन"
    }, {
      "ac": "YNB",
      "an": "Yenbo",
      "han": "येन्बो",
      "cn": "Saudi Arabia",
      "hcn": "स‌ऊदी अर‌ब‌",
      "cc": "SA",
      "ct": "Yenbo",
      "hct": "येन्बो"
    }, {
      "ac": "YNZ",
      "an": "Yancheng",
      "han": "यांचेन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yancheng",
      "hct": "यांचेन्ग"
    }, {
      "ac": "XYA",
      "an": "Yandina",
      "han": "यानदीना",
      "cn": "Solomon Islands",
      "hcn": "सोलोम‌न‌ द्वीप‌",
      "cc": "SB",
      "ct": "Yandina",
      "hct": "यानदीना"
    }, {
      "ac": "RGN",
      "an": "Mingaladon",
      "han": "मिंगलाडन",
      "cn": "Myanmar (Burma)",
      "hcn": "म्यांमार‌ (बर्मा)",
      "cc": "MM",
      "ct": "Yangon",
      "hct": "यांगोन"
    }, {
      "ac": "YNY",
      "an": "Yangyang",
      "han": "यांगयांग",
      "cn": "South Korea",
      "hcn": "द‌क्षिण‌ कोरिया",
      "cc": "KR",
      "ct": "Sokcho   Gangneung",
      "hct": "सोक्चो   गंग्नेउन्ग"
    }, {
      "ac": "YNJ",
      "an": "Yanji",
      "han": "यानजी",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yanji",
      "hct": "यानजी"
    }, {
      "ac": "YNT",
      "an": "Laishan",
      "han": "लायशान",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yantai",
      "hct": "यानताई"
    }, {
      "ac": "YES",
      "an": "",
      "han": "",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Yasouj",
      "hct": "यासौज"
    }, {
      "ac": "AZD",
      "an": "Yazd Shahid Sadooghi",
      "han": "याज़्ड शहीद सडूघी",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Yazd",
      "hct": "याज़्ड"
    }, {
      "ac": "YZF",
      "an": "Yellowknife",
      "han": "येलौक्निफ़े",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "Yellowknife",
      "hct": "येलौक्निफ़े"
    }, {
      "ac": "RSU",
      "an": "Yeosu",
      "han": "येओसू",
      "cn": "South Korea",
      "hcn": "द‌क्षिण‌ कोरिया",
      "cc": "KR",
      "ct": "Yeosu",
      "hct": "येओसू"
    }, {
      "ac": "EVN",
      "an": "Yerevan",
      "han": "येरेवन",
      "cn": "Armenia",
      "hcn": "अर्मेनिआ",
      "cc": "AM",
      "ct": "Yerevan",
      "hct": "येरेवन"
    }, {
      "ac": "YBP",
      "an": "Yibin",
      "han": "यिबीन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yibin",
      "hct": "यिबीन"
    }, {
      "ac": "YIH",
      "an": "Yichang",
      "han": "यिचंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yichang",
      "hct": "यिचंग"
    }, {
      "ac": "INC",
      "an": "Yinchuan",
      "han": "यिन्चुआन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yinchuan",
      "hct": "यिन्चुआन"
    }, {
      "ac": "YIN",
      "an": "Yining",
      "han": "यिनिन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yining",
      "hct": "यिनिन्ग"
    }, {
      "ac": "YIW",
      "an": "Yiwu",
      "han": "यिवू",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yiwu",
      "hct": "यिवू"
    }, {
      "ac": "JOG",
      "an": "Adi Sutjipto",
      "han": "आदि सुट्जिप्टो",
      "cn": "Indonesia",
      "hcn": "इंडोनेशिया",
      "cc": "ID",
      "ct": "Yogyakarta",
      "hct": "योग्यकर्ता"
    }, {
      "ac": "YGJ",
      "an": "Miho",
      "han": "मिहो",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Miho",
      "hct": "मिहो"
    }, {
      "ac": "OGN",
      "an": "Yonaguni",
      "han": "योनागुणी",
      "cn": "Japan",
      "hcn": "जापान",
      "cc": "JP",
      "ct": "Yonaguni Jima",
      "hct": "योनागुणी जिमा"
    }, {
      "ac": "ZAC",
      "an": "York Landing",
      "han": "योर्क लांदींग",
      "cn": "Canada",
      "hcn": "कैनेडा",
      "cc": "CA",
      "ct": "York Landing",
      "hct": "योर्क लांदींग"
    }, {
      "ac": "OKR",
      "an": "Yorke Island",
      "han": "योर्के आयलॅन्ड",
      "cn": "Australia",
      "hcn": "औस्ट्रालिया",
      "cc": "AU",
      "ct": "Yorke Island",
      "hct": "योर्के आयलॅन्ड"
    }, {
      "ac": "YNG",
      "an": "Youngstown Municipal",
      "han": "यून्ग्स्टौन म्यूनिसिपल",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Youngstown",
      "hct": "यून्ग्स्टौन"
    }, {
      "ac": "UYN",
      "an": "Yulin",
      "han": "युलीन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Yulin",
      "hct": "युलीन"
    }, {
      "ac": "YUM",
      "an": "Yuma",
      "han": "युमा",
      "cn": "United States",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Yuma",
      "hct": "युमा"
    }, {
      "ac": "UUS",
      "an": "Khomutovo",
      "han": "खोमुटोवो",
      "cn": "Russia",
      "hcn": "रूस",
      "cc": "RU",
      "ct": "Yuzhno",
      "hct": "युझ़़नो"
    }, {
      "ac": "ACZ",
      "an": "",
      "han": "",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Zabol",
      "hct": "ज़ाबोल"
    }, {
      "ac": "ZCL",
      "an": "Zacatecas",
      "han": "ज़कटेकास",
      "cn": "Mexico",
      "hcn": "मेक्सिको",
      "cc": "MX",
      "ct": "Zacatecas",
      "hct": "ज़कटेकास"
    }, {
      "ac": "KZB",
      "an": "",
      "han": "",
      "cn": "US",
      "hcn": "संयुक्त‌ राज्य‌ अम‌रीका",
      "cc": "US",
      "ct": "Zachar Bay",
      "hct": "ज़चर बे"
    }, {
      "ac": "ZAD",
      "an": "Zadar",
      "han": "ज़दार",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Zadar",
      "hct": "ज़दार"
    }, {
      "ac": "ZAG",
      "an": "Zagreb",
      "han": "ज़ाग्रेब",
      "cn": "Croatia",
      "hcn": "क्रोएशिया",
      "cc": "HR",
      "ct": "Zagreb",
      "hct": "ज़ाग्रेब"
    }, {
      "ac": "ZAH",
      "an": "Zahedan",
      "han": "ज़हेडान",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Zahedan",
      "hct": "ज़हेडान"
    }, {
      "ac": "ZTH",
      "an": "Zakinthos",
      "han": "ज़किंठोस",
      "cn": "Greece",
      "hcn": "ग्रीस",
      "cc": "GR",
      "ct": "Zakinthos Is",
      "hct": "ज़किंठोस इस"
    }, {
      "ac": "ZAM",
      "an": "Zamboanga",
      "han": "ज़ाम्बोआंगा",
      "cn": "Philippines",
      "hcn": "फ़िलीपीन‌",
      "cc": "PH",
      "ct": "Zamboanga",
      "hct": "ज़ाम्बोआंगा"
    }, {
      "ac": "JWN",
      "an": "",
      "han": "",
      "cn": "Iran",
      "hcn": "इरान",
      "cc": "IR",
      "ct": "Zanjan",
      "hct": "ज़ानजान"
    }, {
      "ac": "ZNZ",
      "an": "Kisauni",
      "han": "किसौनी",
      "cn": "Tanzania",
      "hcn": "तांज़ानिया",
      "cc": "TZ",
      "ct": "Zanzibar",
      "hct": "ज़ान्ज़िबर"
    }, {
      "ac": "OZH",
      "an": "Zaporozhye",
      "han": "ज़ापोरोझ़्ये ",
      "cn": "Ukraine",
      "hcn": "यूक्रेन‌",
      "cc": "UA",
      "ct": "Zaporozhye",
      "hct": "ज़ापोरोऴ्ये"
    }, {
      "ac": "ZAZ",
      "an": "Zaragoza Ab",
      "han": "ज़रगोज़ा अब",
      "cn": "Spain",
      "hcn": "स्पेन‌",
      "cc": "ES",
      "ct": "Zaragoza",
      "hct": "ज़रगोज़ा"
    }, {
      "ac": "DMB",
      "an": "Taraz",
      "han": "ताराज़",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Dzhambul",
      "hct": "डझ़म्बुल"
    }, {
      "ac": "ZHA",
      "an": "Zhanjiang",
      "han": "झ़न्जिआंग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Zhanjiang",
      "hct": "झ़न्जिआंग"
    }, {
      "ac": "ZAT",
      "an": "Zhaotong",
      "han": "झ़व्टोन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Zhaotong",
      "hct": "झ़व्टोन्ग"
    }, {
      "ac": "CGO",
      "an": "Xinzheng",
      "han": "क्सिन्ऴेन्ग",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Zhengzhou",
      "hct": "झ़ेन्ग्झै"
    }, {
      "ac": "DZN",
      "an": "Zhezkazgan",
      "han": "झ़ेज़्कज़गन",
      "cn": "Kazakhstan",
      "hcn": "क‌ज़ाक‌स्तान‌",
      "cc": "KZ",
      "ct": "Zhezkazgan",
      "hct": "झ़ेज़्कज़गन"
    }, {
      "ac": "HSN",
      "an": "Zhoushan",
      "han": "झ़ौशन",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Zhoushan",
      "hct": "झ़ैशन"
    }, {
      "ac": "ZUH",
      "an": "Zhuhai",
      "han": "झ़ूहाई",
      "cn": "China",
      "hcn": "चीन‌",
      "cc": "CN",
      "ct": "Zhuhai",
      "hct": "झुहाई"
    }, {
      "ac": "IEG",
      "an": "Babimost",
      "han": "बबीमोस्ट",
      "cn": "Poland",
      "hcn": "पोलैंड‌",
      "cc": "PL",
      "ct": "Zielona Gora",
      "hct": "ज़ीलोना गोरा"
    }, {
      "ac": "ZIG",
      "an": "Ziguinchor",
      "han": "ज़िगुइन्चोर",
      "cn": "Senegal",
      "hcn": "सेनेगल",
      "cc": "SN",
      "ct": "Ziguinchor",
      "hct": "ज़िगुइन्चोर"
    }, {
      "ac": "ILZ",
      "an": "Zilina",
      "han": "ज़िलीना",
      "cn": "Slovakia",
      "hcn": "स्लोवकिआ",
      "cc": "SK",
      "ct": "Zilina",
      "hct": "ज़िलीना"
    }, {
      "ac": "OUZ",
      "an": "Zouerate",
      "han": "ज़ौएरेटे",
      "cn": "Mauritania",
      "hcn": "मौरिटानिआ",
      "cc": "MR",
      "ct": "Zouerate",
      "hct": "ज़ौएरेटे"
    }, {
      "ac": "DWC",
      "an": "Dubai World Central - Al Maktoum International Airport",
      "han": "दुबाई वर्ल्ड सेन्ट्रल - अल मक्टौम इंटरनेशनल एअरप़ॉर्ट",
      "cn": "United Arab Emirates",
      "hcn": "संयुक्त‌ अर‌ब‌ अमीरात‌",
      "cc": "AE",
      "ct": "Dubai",
      "hct": "दुबई"
    }, {
      "ac": "HRI",
      "an": "Mattala Rajapaksa International Airport",
      "han": "मटअला राजपक्सा इंटरनेशनल एअरप़ॉर्ट",
      "cn": "Sri Lanka",
      "hcn": "श्री लंका",
      "cc": "LK",
      "ct": "Sri Lanka",
      "hct": "श्री लंका"
    }, {
      "ac": "PTY",
      "an": "Tocumen",
      "han": "टोक्यूमें",
      "cn": "Panama",
      "hcn": "पनामा",
      "cc": "PA",
      "ct": "Panama City",
      "hct": "पनामा सिटी"
    }, {
      "ac": "ISU",
      "an": "Sulaymaniyah International Airport",
      "han": "सुलयमनियह इंटरनेशनल एयरपोर्ट",
      "cn": "Iraq",
      "hcn": "इराक",
      "cc": "IQ",
      "ct": "Sulaymaniyah",
      "hct": "सुलयमनियह"
    }];
  }

  getAirlines() {
    return [
      {
        "no": "1",
        "airline": "Ada Air",
        "iata": "ZY",
        "code": "121",
        "icao": "ADE"
      },
      {
        "no": "136",
        "airline": "Forward Air International Airlines, Inc.",
        "iata": "BN",
        "code": ".",
        "icao": ""
      },
      {
        "no": "2",
        "airline": "Adria Airways - The Airline of Slovenia",
        "iata": "JP",
        "code": "165",
        "icao": "ADR"
      },
      {
        "no": "137",
        "airline": "Garuda Indonesia",
        "iata": "GA",
        "code": "126",
        "icao": "GIA"
      },
      {
        "no": "3",
        "airline": "Aer Lingus Limited",
        "iata": "EI",
        "code": "53",
        "icao": "EIN"
      },
      {
        "no": "138",
        "airline": "GB Airways Ltd.",
        "iata": "GT",
        "code": "171",
        "icao": "GBL"
      },
      {
        "no": "4",
        "airline": "Aero Asia International (Private) Ltd.",
        "iata": "E4",
        "code": "532",
        "icao": "."
      },
      {
        "no": "139",
        "airline": "Ghana Airways Corp.",
        "iata": "GH",
        "code": "237",
        "icao": "GHA"
      },
      {
        "no": "5",
        "airline": "Aero California",
        "iata": "JR",
        "code": "78",
        "icao": "SER"
      },
      {
        "no": "140",
        "airline": "Gill Aviation Ltd.",
        "iata": "9C",
        "code": "786",
        "icao": "GIL"
      },
      {
        "no": "6",
        "airline": "Aero Zambia",
        "iata": "Z9",
        "code": "509",
        "icao": "."
      },
      {
        "no": "141",
        "airline": "Gujarat Airways Limited",
        "iata": "G8",
        "code": ".",
        "icao": "GUJ"
      },
      {
        "no": "7",
        "airline": "Aeroflot-Russian International Airlines",
        "iata": "SU",
        "code": "555",
        "icao": "AFL"
      },
      {
        "no": "142",
        "airline": "Gulf Air Company G.S.C.",
        "iata": "GF",
        "code": "72",
        "icao": "GFA"
      },
      {
        "no": "8",
        "airline": "Aerolineas Argentinas",
        "iata": "AR",
        "code": "44",
        "icao": "ARG"
      },
      {
        "no": "143",
        "airline": "Hapag Lloyd Fluggesellschaft",
        "iata": "HF",
        "code": "617",
        "icao": "HLF"
      },
      {
        "no": "9",
        "airline": "Aerolineas Centrales de Colombia",
        "iata": "VX",
        "code": "137",
        "icao": "AES"
      },
      {
        "no": "144",
        "airline": "Hazelton Airlines",
        "iata": "ZL",
        "code": "899",
        "icao": "HZL"
      },
      {
        "no": "10",
        "airline": "Aeromexico",
        "iata": "AM",
        "code": "139",
        "icao": "AMX"
      },
      {
        "no": "145",
        "airline": "Hemus Air",
        "iata": "DU",
        "code": "748",
        "icao": ""
      },
      {
        "no": "11",
        "airline": "Aeromexpress S.A. de C.V.",
        "iata": "QO",
        "code": ".",
        "icao": "MPX"
      },
      {
        "no": "146",
        "airline": "Hong Kong Dragon Airlines Limited",
        "iata": "KA",
        "code": "43",
        "icao": "HDA"
      },
      {
        "no": "12",
        "airline": "Aeroperu - Empresa de Transportes",
        "iata": "PL",
        "code": "210",
        "icao": "PLI"
      },
      {
        "no": "147",
        "airline": "Iberia - Lineas Aereas de Espana",
        "iata": "IB",
        "code": "75",
        "icao": "IBE"
      },
      {
        "no": "13",
        "airline": "Aerosvit Airlines",
        "iata": "VV",
        "code": "870",
        "icao": "AEW"
      },
      {
        "no": "148",
        "airline": "Icelandair",
        "iata": "FI",
        "code": "108",
        "icao": "ICE"
      },
      {
        "no": "14",
        "airline": "Aerovias Venezolanas, S.A.",
        "iata": "VE",
        "code": "128",
        "icao": "AVE"
      },
      {
        "no": "149",
        "airline": "Indian Airlines",
        "iata": "IC",
        "code": "58",
        "icao": "IAC"
      },
      {
        "no": "15",
        "airline": "Affretair (Private) Limited",
        "iata": "ZL",
        "code": ".",
        "icao": "AFM"
      },
      {
        "no": "150",
        "airline": "Inter-Aviation Services dba Inter-Air",
        "iata": "D6",
        "code": "625",
        "icao": "INL"
      },
      {
        "no": "16",
        "airline": "African Joint Air Services",
        "iata": "Y2",
        "code": "693",
        "icao": "."
      },
      {
        "no": "151",
        "airline": "Iran Air",
        "iata": "IR",
        "code": "96",
        "icao": "IRA"
      },
      {
        "no": "17",
        "airline": "Air Afrique",
        "iata": "RK",
        "code": "92",
        "icao": "RKA"
      },
      {
        "no": "152",
        "airline": "Iran Aseman Airlines",
        "iata": "EP",
        "code": "815",
        "icao": "IRC"
      },
      {
        "no": "18",
        "airline": "Air Algerie",
        "iata": "AH",
        "code": "124",
        "icao": "DAH"
      },
      {
        "no": "153",
        "airline": "Iraqi Airways",
        "iata": "IA",
        "code": "73",
        "icao": "IAW"
      },
      {
        "no": "19",
        "airline": "Air Austral",
        "iata": "UU",
        "code": "760",
        "icao": "REU"
      },
      {
        "no": "154",
        "airline": "Jamahiriya Libyan Arab Airlines",
        "iata": "LN",
        "code": "148",
        "icao": "LAA"
      },
      {
        "no": "20",
        "airline": "Air Baltic Corporation S.A.",
        "iata": "BT",
        "code": "657",
        "icao": "."
      },
      {
        "no": "155",
        "airline": "Japan Air System Co., Ltd.",
        "iata": "JD",
        "code": "234",
        "icao": "JAS"
      },
      {
        "no": "21",
        "airline": "Air Berlin GmbH & Co. Luftverkehrs KG",
        "iata": "AB",
        "code": "745",
        "icao": "BER"
      },
      {
        "no": "156",
        "airline": "Japan Airlines Company Ltd.",
        "iata": "JL",
        "code": "131",
        "icao": "JAL"
      },
      {
        "no": "22",
        "airline": "Air Bosna",
        "iata": "JA",
        "code": "995",
        "icao": "."
      },
      {
        "no": "157",
        "airline": "Jersey European Airways Ltd.",
        "iata": "JY",
        "code": "267",
        "icao": "JEA"
      },
      {
        "no": "23",
        "airline": "Air Botswana Corporation",
        "iata": "BP",
        "code": "636",
        "icao": "BOT"
      },
      {
        "no": "158",
        "airline": "Jet Airways (India) Limited",
        "iata": "9W",
        "code": "589",
        "icao": ""
      },
      {
        "no": "24",
        "airline": "Air Caledonie International",
        "iata": "SB",
        "code": "63",
        "icao": "ACI"
      },
      {
        "no": "159",
        "airline": "Jugoslovenski Aerotransport (JAT)",
        "iata": "JU",
        "code": "115",
        "icao": "JAT"
      },
      {
        "no": "25",
        "airline": "Air Canada",
        "iata": "AC",
        "code": "14",
        "icao": "ACA"
      },
      {
        "no": "160",
        "airline": "Kendell Airlines",
        "iata": "KD",
        "code": "678",
        "icao": "KDA"
      },
      {
        "no": "26",
        "airline": "Air China International Corporation",
        "iata": "CA",
        "code": "999",
        "icao": "CCA"
      },
      {
        "no": "161",
        "airline": "Kenya Airways",
        "iata": "KQ",
        "code": "706",
        "icao": "KQA"
      },
      {
        "no": "27",
        "airline": "Air Contractors (UK) Limited",
        "iata": "AG",
        "code": ".",
        "icao": "ABR"
      },
      {
        "no": "162",
        "airline": "KLM Royal Dutch Airlines",
        "iata": "KL",
        "code": "74",
        "icao": "KLM"
      },
      {
        "no": "28",
        "airline": "Air Europa Lineas Aereas, S.A.",
        "iata": "UX",
        "code": "996",
        "icao": "AEA"
      },
      {
        "no": "163",
        "airline": "KLM uk Ltd.",
        "iata": "UK",
        "code": "130",
        "icao": "UKA"
      },
      {
        "no": "29",
        "airline": "Air France",
        "iata": "AF",
        "code": "57",
        "icao": "AFR"
      },
      {
        "no": "164",
        "airline": "Korean Air Lines Co. Ltd.",
        "iata": "KE",
        "code": "180",
        "icao": "KAL"
      },
      {
        "no": "30",
        "airline": "Air Gabon",
        "iata": "GN",
        "code": "185",
        "icao": "AGN"
      },
      {
        "no": "165",
        "airline": "Kuwait Airways",
        "iata": "KU",
        "code": "229",
        "icao": "KAC"
      },
      {
        "no": "31",
        "airline": "Air Jamaica",
        "iata": "JM",
        "code": "201",
        "icao": "AJM"
      },
      {
        "no": "166",
        "airline": "L.B. Limited",
        "iata": "7Z",
        "code": "569",
        "icao": "LBH"
      },
      {
        "no": "32",
        "airline": "Air Jamaica Express Limited",
        "iata": "JQ",
        "code": "100",
        "icao": "JMX"
      },
      {
        "no": "167",
        "airline": "LADECO Cargo S.A.",
        "iata": "UC",
        "code": ".",
        "icao": ""
      },
      {
        "no": "33",
        "airline": "Air Kazakstan",
        "iata": "9Y",
        "code": "452",
        "icao": "KZK"
      },
      {
        "no": "168",
        "airline": "Ladeco S.A. dba Ladeco Airlines",
        "iata": "UC",
        "code": "145",
        "icao": "LCO"
      },
      {
        "no": "34",
        "airline": "Air Koryo",
        "iata": "JS",
        "code": "120",
        "icao": "KOR"
      },
      {
        "no": "169",
        "airline": "LAM - Linhas Aereas de Mocambique",
        "iata": "TM",
        "code": "68",
        "icao": "LAM"
      },
      {
        "no": "35",
        "airline": "Air Liberte",
        "iata": "IJ",
        "code": "718",
        "icao": "LIB"
      },
      {
        "no": "170",
        "airline": "Lan Peru S.A.",
        "iata": "LP",
        "code": "544",
        "icao": ""
      },
      {
        "no": "36",
        "airline": "Air Littoral",
        "iata": "FU",
        "code": "659",
        "icao": "LIT"
      },
      {
        "no": "171",
        "airline": "Lauda Air Luftfahrt AG",
        "iata": "NG",
        "code": "231",
        "icao": "LDA"
      },
      {
        "no": "37",
        "airline": "Air Madagascar",
        "iata": "MD",
        "code": "258",
        "icao": "MDG"
      },
      {
        "no": "172",
        "airline": "Linea Aerea Nacional-Chile S.A.",
        "iata": "LA",
        "code": "45",
        "icao": "LAN"
      },
      {
        "no": "38",
        "airline": "Air Malawi Limited",
        "iata": "QM",
        "code": "167",
        "icao": "AML"
      },
      {
        "no": "173",
        "airline": "Lineas Aereas Costarricenses S.A.",
        "iata": "LR",
        "code": "133",
        "icao": "LRC"
      },
      {
        "no": "39",
        "airline": "Air Maldives Limited",
        "iata": "L6",
        "code": "900",
        "icao": "AMI"
      },
      {
        "no": "174",
        "airline": "Lineas Aereas Privadas Argentinas (LAPA)",
        "iata": "MJ",
        "code": "69",
        "icao": "LPR"
      },
      {
        "no": "40",
        "airline": "Air Malta p.l.c.",
        "iata": "KM",
        "code": "643",
        "icao": "AMC"
      },
      {
        "no": "175",
        "airline": "Lithuanian Airlines",
        "iata": "TE",
        "code": "874",
        "icao": "LIL"
      },
      {
        "no": "41",
        "airline": "Air Marshall Islands, Inc.",
        "iata": "CW",
        "code": "778",
        "icao": "MRS"
      },
      {
        "no": "176",
        "airline": "Lloyd Aereo Boliviano S.A. (LAB)",
        "iata": "LB",
        "code": "51",
        "icao": "LLB"
      },
      {
        "no": "42",
        "airline": "Air Mauritius",
        "iata": "MK",
        "code": "239",
        "icao": "MAU"
      },
      {
        "no": "177",
        "airline": "LOT - Polish Airlines",
        "iata": "LO",
        "code": "80",
        "icao": "LOT"
      },
      {
        "no": "43",
        "airline": "Air Moldova International S.A.",
        "iata": "RM",
        "code": "283",
        "icao": "MLV"
      },
      {
        "no": "178",
        "airline": "LTU International Airways",
        "iata": "LT",
        "code": "266",
        "icao": "LTU"
      },
      {
        "no": "44",
        "airline": "Air Namibia",
        "iata": "SW",
        "code": "186",
        "icao": "NMB"
      },
      {
        "no": "179",
        "airline": "Lufthansa Cargo AG",
        "iata": "LH",
        "code": ".",
        "icao": "GEC"
      },
      {
        "no": "45",
        "airline": "Air New Zealand Limited",
        "iata": "NZ",
        "code": "86",
        "icao": "ANZ"
      },
      {
        "no": "180",
        "airline": "Lufthansa CityLine GmbH",
        "iata": "CL",
        "code": "683",
        "icao": "CLH"
      },
      {
        "no": "46",
        "airline": "Air Niugini Pty Limited",
        "iata": "PX",
        "code": "656",
        "icao": "ANG"
      },
      {
        "no": "181",
        "airline": "Luxair",
        "iata": "LG",
        "code": "149",
        "icao": "LGL"
      },
      {
        "no": "47",
        "airline": "Air Nostrum L.A.M.S.A.",
        "iata": "YW",
        "code": "694",
        "icao": "ANS"
      },
      {
        "no": "182",
        "airline": "Macedonian Airlnes",
        "iata": "IN",
        "code": "367",
        "icao": "MAK"
      },
      {
        "no": "48",
        "airline": "Air Pacific Ltd.",
        "iata": "FJ",
        "code": "260",
        "icao": "FJI"
      },
      {
        "no": "183",
        "airline": "Maersk Air A/S",
        "iata": "DM",
        "code": "349",
        "icao": "DAN"
      },
      {
        "no": "49",
        "airline": "Air Sask Aviation 1991",
        "iata": "7W",
        "code": "94",
        "icao": "ASK"
      },
      {
        "no": "184",
        "airline": "Maersk Air Ltd.",
        "iata": "VB",
        "code": "702",
        "icao": ""
      },
      {
        "no": "50",
        "airline": "Air Seychelles Limited",
        "iata": "HM",
        "code": "61",
        "icao": "SEY"
      },
      {
        "no": "185",
        "airline": "Malaysian Airline System Berhad",
        "iata": "MH",
        "code": "232",
        "icao": "MAS"
      },
      {
        "no": "51",
        "airline": "Air Tahiti",
        "iata": "VT",
        "code": "135",
        "icao": "VTA"
      },
      {
        "no": "186",
        "airline": "Malev Hungarian Airlines Public Limited",
        "iata": "MA",
        "code": "182",
        "icao": "MAH"
      },
      {
        "no": "52",
        "airline": "Air Tanzania Corporation",
        "iata": "TC",
        "code": "197",
        "icao": "ATC"
      },
      {
        "no": "187",
        "airline": "Manx Airlines",
        "iata": "JE",
        "code": "916",
        "icao": "MNX"
      },
      {
        "no": "53",
        "airline": "Air Ukraine",
        "iata": "6U",
        "code": "891",
        "icao": "UKR"
      },
      {
        "no": "188",
        "airline": "Meridiana S.p.A.",
        "iata": "IG",
        "code": "191",
        "icao": "ISS"
      },
      {
        "no": "54",
        "airline": "Air Vanuatu (Operations) Limited",
        "iata": "NF",
        "code": "218",
        "icao": "AVN"
      },
      {
        "no": "189",
        "airline": "Merpati Nusantara Airlines",
        "iata": "MZ",
        "code": "621",
        "icao": "MNA"
      },
      {
        "no": "55",
        "airline": "Air Zimbabwe Corporation",
        "iata": "UM",
        "code": "168",
        "icao": "AZW"
      },
      {
        "no": "190",
        "airline": "MIAT - Mongolian Airlines",
        "iata": "OM",
        "code": "289",
        "icao": "MGL"
      },
      {
        "no": "56",
        "airline": "Air-India Limited",
        "iata": "AI",
        "code": "98",
        "icao": "AIC"
      },
      {
        "no": "191",
        "airline": "Middle East Airlines AirLiban",
        "iata": "ME",
        "code": "76",
        "icao": "MEA"
      },
      {
        "no": "57",
        "airline": "Alaska Airlines Inc.",
        "iata": "AS",
        "code": "27",
        "icao": "ASA"
      },
      {
        "no": "192",
        "airline": "Nigeria Airways Ltd.",
        "iata": "WT",
        "code": "87",
        "icao": "NGA"
      },
      {
        "no": "58",
        "airline": "Albanian Airlines MAK S.H.P.K.",
        "iata": "LV",
        "code": "639",
        "icao": "LBC"
      },
      {
        "no": "193",
        "airline": "Nippon Cargo Airlines",
        "iata": "KZ",
        "code": ".",
        "icao": "NCA"
      },
      {
        "no": "59",
        "airline": "Alitalia - Linee Aeree Italiane",
        "iata": "AZ",
        "code": "55",
        "icao": "AZA"
      },
      {
        "no": "194",
        "airline": "Northwest Airlines, Inc.",
        "iata": "NW",
        "code": "12",
        "icao": "NWA"
      },
      {
        "no": "60",
        "airline": "All Nippon Airways Co. Ltd.",
        "iata": "NH",
        "code": "205",
        "icao": "ANA"
      },
      {
        "no": "195",
        "airline": "Olympic Airways S.A.",
        "iata": "OA",
        "code": "50",
        "icao": "OAL"
      },
      {
        "no": "61",
        "airline": "ALM 1997 Airline Inc., dba Air ALM",
        "iata": "LM",
        "code": "119",
        "icao": "ALM"
      },
      {
        "no": "196",
        "airline": "Oman Aviation Services Co. (SAOG)",
        "iata": "WY",
        "code": "910",
        "icao": "OAS"
      },
      {
        "no": "62",
        "airline": "Aloha Airlines Inc.",
        "iata": "AQ",
        "code": "327",
        "icao": "AAH"
      },
      {
        "no": "197",
        "airline": "P.T. Sempati Air",
        "iata": "SG",
        "code": "821",
        "icao": "SSR"
      },
      {
        "no": "63",
        "airline": "ALPI Eagles S.p.A.",
        "iata": "E8",
        "code": "789",
        "icao": "ELG"
      },
      {
        "no": "198",
        "airline": "Pacific Airways Corporation (Pacificair)",
        "iata": "GX",
        "code": ".",
        "icao": ""
      },
      {
        "no": "64",
        "airline": "America West Airlines Inc.",
        "iata": "HP",
        "code": "401",
        "icao": "AWE"
      },
      {
        "no": "199",
        "airline": "Pakistan International Airlines",
        "iata": "PK",
        "code": "214",
        "icao": "PIA"
      },
      {
        "no": "65",
        "airline": "American Airlines Inc.",
        "iata": "AA",
        "code": "1",
        "icao": "AAL"
      },
      {
        "no": "200",
        "airline": "Philippine Airlines, Inc.",
        "iata": "PR",
        "code": "79",
        "icao": "PAL"
      },
      {
        "no": "66",
        "airline": "Ansett Australia",
        "iata": "AN",
        "code": "90",
        "icao": "AAA"
      },
      {
        "no": "201",
        "airline": "Polynesian Limited",
        "iata": "PH",
        "code": "162",
        "icao": "PAO"
      },
      {
        "no": "67",
        "airline": "Ansett New Zealand",
        "iata": "ZQ",
        "code": "941",
        "icao": "."
      },
      {
        "no": "202",
        "airline": "Portugalia - Companhia Portuguesa de",
        "iata": "NI",
        "code": "685",
        "icao": "PGA"
      },
      {
        "no": "68",
        "airline": "AOM-Minerve S.A.",
        "iata": "IW",
        "code": "646",
        "icao": "AOM"
      },
      {
        "no": "203",
        "airline": "Primeras Lineas Uruguayas de",
        "iata": "PU",
        "code": "286",
        "icao": "PUA"
      },
      {
        "no": "69",
        "airline": "Ariana Afghan Airlines",
        "iata": "FG",
        "code": "255",
        "icao": "AFG"
      },
      {
        "no": "204",
        "airline": "Qantas Airways Ltd.",
        "iata": "QF",
        "code": "81",
        "icao": "QFA"
      },
      {
        "no": "70",
        "airline": "Arkia - Israeli Airlines Ltd",
        "iata": "IZ",
        "code": "238",
        "icao": "AIZ"
      },
      {
        "no": "205",
        "airline": "Qatar Airways (W.L.L.)",
        "iata": "QR",
        "code": "157",
        "icao": "QTR"
      },
      {
        "no": "71",
        "airline": "Armenian Airlines",
        "iata": "R3",
        "code": "956",
        "icao": "."
      },
      {
        "no": "206",
        "airline": "Red Sea Air",
        "iata": "7R",
        "code": "593",
        "icao": "ERS"
      },
      {
        "no": "72",
        "airline": "Atlas Air, Inc.",
        "iata": "5Y",
        "code": ".",
        "icao": "."
      },
      {
        "no": "207",
        "airline": "Regional Airlines",
        "iata": "VM",
        "code": "982",
        "icao": "RGI"
      },
      {
        "no": "73",
        "airline": "Augsburg Airways GmbH",
        "iata": "IQ",
        "code": "614",
        "icao": "AUB"
      },
      {
        "no": "208",
        "airline": "Riga Airlines",
        "iata": "GV",
        "code": "248",
        "icao": "RIG"
      },
      {
        "no": "74",
        "airline": "Austrian Airlines",
        "iata": "OS",
        "code": "257",
        "icao": "AUA"
      },
      {
        "no": "209",
        "airline": "Royal Air Maroc",
        "iata": "AT",
        "code": "147",
        "icao": "RAM"
      },
      {
        "no": "75",
        "airline": "Avant Airlines S.A.",
        "iata": "OT",
        "code": "246",
        "icao": "."
      },
      {
        "no": "210",
        "airline": "Royal Brunei Airlines Sdn. Bhd.",
        "iata": "BI",
        "code": "672",
        "icao": "RBA"
      },
      {
        "no": "76",
        "airline": "Aviacion y Comercio S.A. (AVIACO)",
        "iata": "AO",
        "code": "110",
        "icao": "AYC"
      },
      {
        "no": "211",
        "airline": "Royal Jordanian",
        "iata": "RJ",
        "code": "512",
        "icao": "RJA"
      },
      {
        "no": "77",
        "airline": "Avianca - Aerovias Nacionales de",
        "iata": "AV",
        "code": "134",
        "icao": "AVA"
      },
      {
        "no": "212",
        "airline": "Royal Swazi National Airways Corp.",
        "iata": "ZC",
        "code": "141",
        "icao": "RSN"
      },
      {
        "no": "78",
        "airline": "AVIATECA, S.A.",
        "iata": "GU",
        "code": "240",
        "icao": "GUG"
      },
      {
        "no": "213",
        "airline": "Royal Tongan Airlines",
        "iata": "WR",
        "code": "971",
        "icao": "HRH"
      },
      {
        "no": "79",
        "airline": "Avioimpex A.D. p.o.",
        "iata": "M4",
        "code": "743",
        "icao": "AXX"
      },
      {
        "no": "214",
        "airline": "Ryanair Ltd.",
        "iata": "FR",
        "code": "224",
        "icao": "RYR"
      },
      {
        "no": "80",
        "airline": "Azerbaijan Hava Yollary",
        "iata": "J2",
        "code": "771",
        "icao": "AHY"
      },
      {
        "no": "215",
        "airline": "SA Airlink",
        "iata": "4Z",
        "code": "749",
        "icao": ""
      },
      {
        "no": "81",
        "airline": "Balkan - Bulgarian Airlines",
        "iata": "LZ",
        "code": "196",
        "icao": "LAZ"
      },
      {
        "no": "216",
        "airline": "SABENA",
        "iata": "SN",
        "code": "82",
        "icao": "SAB"
      },
      {
        "no": "82",
        "airline": "Belavia",
        "iata": "B2",
        "code": "628",
        "icao": "BRU"
      },
      {
        "no": "217",
        "airline": "Safair (Proprietary) Ltd.",
        "iata": "FA",
        "code": ".",
        "icao": "SFR"
      },
      {
        "no": "83",
        "airline": "Bellview Airlines Ltd.",
        "iata": "B3",
        "code": "208",
        "icao": "BLV"
      },
      {
        "no": "218",
        "airline": "Sahara Airlines Limited",
        "iata": "S2",
        "code": "705",
        "icao": ""
      },
      {
        "no": "84",
        "airline": "Biman Bangladesh Airlines",
        "iata": "BG",
        "code": "997",
        "icao": "BBC"
      },
      {
        "no": "219",
        "airline": "Samara Airlines",
        "iata": "E5",
        "code": "906",
        "icao": "BRZ"
      },
      {
        "no": "85",
        "airline": "Braathens ASA",
        "iata": "BU",
        "code": "154",
        "icao": "BRA"
      },
      {
        "no": "220",
        "airline": "SATA - Air Acores",
        "iata": "SP",
        "code": "737",
        "icao": "SAT"
      },
      {
        "no": "86",
        "airline": "British Airways p.l.c.",
        "iata": "BA",
        "code": "125",
        "icao": "BAW"
      },
      {
        "no": "221",
        "airline": "Saudi Arabian Airlines",
        "iata": "SV",
        "code": "65",
        "icao": "SVA"
      },
      {
        "no": "87",
        "airline": "British Midland Airways Ltd.",
        "iata": "BD",
        "code": "236",
        "icao": "BMA"
      },
      {
        "no": "222",
        "airline": "Scandinavian Airlines System (SAS)",
        "iata": "SK",
        "code": "117",
        "icao": "SAS"
      },
      {
        "no": "88",
        "airline": "BWIA International Airways Limited",
        "iata": "BW",
        "code": "106",
        "icao": "BWA"
      },
      {
        "no": "223",
        "airline": "Shanghai Airlines",
        "iata": "FM",
        "code": "774",
        "icao": ""
      },
      {
        "no": "89",
        "airline": "Cameroon Airlines",
        "iata": "UY",
        "code": "604",
        "icao": "UYC"
      },
      {
        "no": "224",
        "airline": "Sierra National Airlines",
        "iata": "LJ",
        "code": "690",
        "icao": "SLA"
      },
      {
        "no": "90",
        "airline": "Canadian Airlines International Limited",
        "iata": "CP",
        "code": "18",
        "icao": "CDN"
      },
      {
        "no": "225",
        "airline": "Singapore Airlines",
        "iata": "SQ",
        "code": "618",
        "icao": "SIA"
      },
      {
        "no": "91",
        "airline": "Cargolux Airlines International S.A.",
        "iata": "CV",
        "code": "",
        "icao": "CLX"
      },
      {
        "no": "226",
        "airline": "Skyways AB",
        "iata": "JZ",
        "code": "752",
        "icao": ""
      },
      {
        "no": "92",
        "airline": "Cathay Pacific Airways Ltd.",
        "iata": "CX",
        "code": "160",
        "icao": "CPA"
      },
      {
        "no": "227",
        "airline": "Solomon Airlines",
        "iata": "IE",
        "code": "193",
        "icao": "SOL"
      },
      {
        "no": "93",
        "airline": "China Eastern Airlines",
        "iata": "MU",
        "code": "781",
        "icao": "CES"
      },
      {
        "no": "228",
        "airline": "South African Airways",
        "iata": "SA",
        "code": "83",
        "icao": "SAA"
      },
      {
        "no": "94",
        "airline": "China Northern Airlines",
        "iata": "CJ",
        "code": "782",
        "icao": "CBF"
      },
      {
        "no": "229",
        "airline": "Southern Winds S.A.",
        "iata": "A4",
        "code": "242",
        "icao": ""
      },
      {
        "no": "95",
        "airline": "China Northwest Airlines",
        "iata": "WH",
        "code": "783",
        "icao": "CNW"
      },
      {
        "no": "230",
        "airline": "Spanair S.A.",
        "iata": "JK",
        "code": "680",
        "icao": "JKK"
      },
      {
        "no": "96",
        "airline": "China Southern Airlines",
        "iata": "CZ",
        "code": "784",
        "icao": "CSN"
      },
      {
        "no": "231",
        "airline": "SriLankan Airlines Limited",
        "iata": "UL",
        "code": "603",
        "icao": "ALK"
      },
      {
        "no": "97",
        "airline": "China Southwest Airlines",
        "iata": "SZ",
        "code": "785",
        "icao": "CXN"
      },
      {
        "no": "232",
        "airline": "Sudan Airways Co. Ltd.",
        "iata": "SD",
        "code": "200",
        "icao": "SUD"
      },
      {
        "no": "98",
        "airline": "China Xinjiang Airlines",
        "iata": "XO",
        "code": "651",
        "icao": "CXJ"
      },
      {
        "no": "233",
        "airline": "Sunflower Airlines Ltd.",
        "iata": "PI",
        "code": "252",
        "icao": "SUF"
      },
      {
        "no": "99",
        "airline": "China Yunnan Airlines",
        "iata": "3Q",
        "code": "592",
        "icao": "CYH"
      },
      {
        "no": "234",
        "airline": "Surinam Airways Ltd.",
        "iata": "PY",
        "code": "192",
        "icao": "SLM"
      },
      {
        "no": "100",
        "airline": "Cielos del Sur S.A.",
        "iata": "AU",
        "code": "143",
        "icao": "AUT"
      },
      {
        "no": "235",
        "airline": "Swiss Air Transport Co. Ltd. (Swissair)",
        "iata": "SR",
        "code": "85",
        "icao": "SWR"
      },
      {
        "no": "101",
        "airline": "Cityjet",
        "iata": "WX",
        "code": "689",
        "icao": "BCY"
      },
      {
        "no": "236",
        "airline": "Syrian Arab Airlines",
        "iata": "RB",
        "code": "70",
        "icao": "SYR"
      },
      {
        "no": "102",
        "airline": "Comair Ltd.",
        "iata": "MN",
        "code": "161",
        "icao": "CAW"
      },
      {
        "no": "237",
        "airline": "T.A.T. European Airlines",
        "iata": "VD",
        "code": "936",
        "icao": "TAT"
      },
      {
        "no": "103",
        "airline": "Compagnie Aerienne Corse",
        "iata": "XK",
        "code": "146",
        "icao": "."
      },
      {
        "no": "238",
        "airline": "TAAG - Linhas Aereas de Angola",
        "iata": "DT",
        "code": "118",
        "icao": "DTA"
      },
      {
        "no": "104",
        "airline": "Compagnie Africaine d'Aviation \"CAA\"",
        "iata": "E9",
        "code": ".",
        "icao": "."
      },
      {
        "no": "239",
        "airline": "Taca International Airlines, S.A.",
        "iata": "TA",
        "code": "202",
        "icao": "TAI"
      },
      {
        "no": "105",
        "airline": "Compania Boliviana de Transporte Aereo",
        "iata": "5L",
        "code": "275",
        "icao": ""
      },
      {
        "no": "240",
        "airline": "TAM - Transportes Aereos",
        "iata": "JJ",
        "code": "957",
        "icao": "BLC"
      },
      {
        "no": "106",
        "airline": "Compania Mexicana de Aviacion",
        "iata": "MX",
        "code": "132",
        "icao": "MXA"
      },
      {
        "no": "241",
        "airline": "TAM - Transportes Aereos del",
        "iata": "PZ",
        "code": "692",
        "icao": "LAP"
      },
      {
        "no": "107",
        "airline": "Continental Airlines, Inc.",
        "iata": "CO",
        "code": "5",
        "icao": "COA"
      },
      {
        "no": "242",
        "airline": "TAM - Transportes Aereos Regionais S.A.",
        "iata": "KK",
        "code": "877",
        "icao": "TAM"
      },
      {
        "no": "108",
        "airline": "Continental Micronesia, Inc.",
        "iata": "CS",
        "code": "596",
        "icao": "CMI"
      },
      {
        "no": "243",
        "airline": "TAP - Air Portugal",
        "iata": "TP",
        "code": "47",
        "icao": "TAP"
      },
      {
        "no": "109",
        "airline": "COPA - Compania Panamena de",
        "iata": "CM",
        "code": "230",
        "icao": "CMP"
      },
      {
        "no": "244",
        "airline": "TAROM - Romanian Air Transport",
        "iata": "RO",
        "code": "281",
        "icao": "ROT"
      },
      {
        "no": "110",
        "airline": "Croatia Airlines",
        "iata": "OU",
        "code": "831",
        "icao": "CTN"
      },
      {
        "no": "245",
        "airline": "Thai Airways International Public",
        "iata": "TG",
        "code": "217",
        "icao": "THA"
      },
      {
        "no": "111",
        "airline": "Cronus Airlines",
        "iata": "X5",
        "code": "198",
        "icao": "CUS"
      },
      {
        "no": "246",
        "airline": "The Mount Cook Group Ltd.",
        "iata": "NM",
        "code": "445",
        "icao": "NZM"
      },
      {
        "no": "112",
        "airline": "Crossair Limited Company for European",
        "iata": "LX",
        "code": "724",
        "icao": "CRX"
      },
      {
        "no": "247",
        "airline": "Tower Air Inc.",
        "iata": "FF",
        "code": "305",
        "icao": "TOW"
      },
      {
        "no": "113",
        "airline": "Cubana de Aviacion S.A.",
        "iata": "CU",
        "code": "136",
        "icao": "CUB"
      },
      {
        "no": "248",
        "airline": "Trans World Airlines Inc.",
        "iata": "TW",
        "code": "15",
        "icao": "TWA"
      },
      {
        "no": "114",
        "airline": "Cyprus Airways",
        "iata": "CY",
        "code": "48",
        "icao": "CYP"
      },
      {
        "no": "249",
        "airline": "Trans-Mediterranean Airways",
        "iata": "TL",
        "code": ".",
        "icao": "TMA"
      },
      {
        "no": "115",
        "airline": "Czech Airlines a.s. , CSA",
        "iata": "OK",
        "code": "64",
        "icao": "CSA"
      },
      {
        "no": "250",
        "airline": "Transaero Airlines",
        "iata": "UN",
        "code": "670",
        "icao": "TSO"
      },
      {
        "no": "116",
        "airline": "Debonair Airways Ltd.",
        "iata": "2G",
        "code": "578",
        "icao": "DEB"
      },
      {
        "no": "251",
        "airline": "Transavia Airlines",
        "iata": "HV",
        "code": "979",
        "icao": "TRA"
      },
      {
        "no": "117",
        "airline": "Delta Air Lines Inc.",
        "iata": "DL",
        "code": "6",
        "icao": "DAL"
      },
      {
        "no": "252",
        "airline": "Transbrasil S.A., Linhas Aereas",
        "iata": "TR",
        "code": "653",
        "icao": "TBA"
      },
      {
        "no": "118",
        "airline": "Deutsche BA Luftfahrtgesellschaft mbH",
        "iata": "DI",
        "code": "944",
        "icao": "BAG"
      },
      {
        "no": "253",
        "airline": "Transportes Aereos Ejecutivos S.A. de CV",
        "iata": "GD",
        "code": "838",
        "icao": ""
      },
      {
        "no": "119",
        "airline": "Deutsche Lufthansa AG",
        "iata": "LH",
        "code": "220",
        "icao": "DLH"
      },
      {
        "no": "254",
        "airline": "Tunisair",
        "iata": "TU",
        "code": "199",
        "icao": "TAR"
      },
      {
        "no": "120",
        "airline": "DHL International E.C.",
        "iata": "ES",
        "code": ".",
        "icao": "DHX"
      },
      {
        "no": "255",
        "airline": "Turkish Airlines Inc.",
        "iata": "TK",
        "code": "235",
        "icao": "THY"
      },
      {
        "no": "121",
        "airline": "Dinar Lineas Aereas S.A.",
        "iata": "D7",
        "code": "429",
        "icao": "RDN"
      },
      {
        "no": "256",
        "airline": "Turkmenistan Airlines",
        "iata": "T5",
        "code": "542",
        "icao": "TUA"
      },
      {
        "no": "122",
        "airline": "Eagle Aviation Ltd.",
        "iata": "Y4",
        "code": "67",
        "icao": "EQA"
      },
      {
        "no": "257",
        "airline": "Ukraine International Airlines",
        "iata": "PS",
        "code": "566",
        "icao": "AUI"
      },
      {
        "no": "123",
        "airline": "East West Travel Trade Links Ltd.",
        "iata": "4S",
        "code": "804",
        "icao": "."
      },
      {
        "no": "258",
        "airline": "United Airlines, Inc.",
        "iata": "UA",
        "code": "16",
        "icao": "UAL"
      },
      {
        "no": "124",
        "airline": "Ecuatoriana de Aviacion S.A.",
        "iata": "EU",
        "code": "341",
        "icao": "EEA"
      },
      {
        "no": "259",
        "airline": "UPS",
        "iata": "5X",
        "code": ".",
        "icao": "UPS"
      },
      {
        "no": "125",
        "airline": "Egyptair",
        "iata": "MS",
        "code": "77",
        "icao": "MSR"
      },
      {
        "no": "260",
        "airline": "US Airways, Inc.",
        "iata": "US",
        "code": "37",
        "icao": "USA"
      },
      {
        "no": "126",
        "airline": "El Al Israel Airlines Ltd.",
        "iata": "LY",
        "code": "114",
        "icao": "ELY"
      },
      {
        "no": "261",
        "airline": "Varig S.A.",
        "iata": "RG",
        "code": "42",
        "icao": "VRG"
      },
      {
        "no": "127",
        "airline": "Emirates",
        "iata": "EK",
        "code": "176",
        "icao": "UAE"
      },
      {
        "no": "262",
        "airline": "Viacao Aerea Sao Paulo, S.A. (VASP)",
        "iata": "VP",
        "code": "343",
        "icao": "VSP"
      },
      {
        "no": "128",
        "airline": "Estonian Air",
        "iata": "OV",
        "code": "960",
        "icao": "ELL"
      },
      {
        "no": "263",
        "airline": "Virgin Atlantic Airways Limited",
        "iata": "VS",
        "code": "932",
        "icao": "VIR"
      },
      {
        "no": "129",
        "airline": "Ethiopian Airlines Enterprise",
        "iata": "ET",
        "code": "71",
        "icao": "ETH"
      },
      {
        "no": "264",
        "airline": "Volare Airlines S.P.A.",
        "iata": "8D",
        "code": "263",
        "icao": "VLE"
      },
      {
        "no": "130",
        "airline": "European Air Transport",
        "iata": "QY",
        "code": ".",
        "icao": "BCS"
      },
      {
        "no": "265",
        "airline": "Wideroe's Flyveselskap A.S.",
        "iata": "WF",
        "code": "701",
        "icao": "WIF"
      },
      {
        "no": "131",
        "airline": "Eurowings AG",
        "iata": "EW",
        "code": "104",
        "icao": "EWG"
      },
      {
        "no": "266",
        "airline": "Xiamen Airlines",
        "iata": "MF",
        "code": "731",
        "icao": "CXA"
      },
      {
        "no": "132",
        "airline": "Falcon Air AB",
        "iata": "IH",
        "code": "759",
        "icao": "FCN"
      },
      {
        "no": "267",
        "airline": "Yemenia - Yemen Airways",
        "iata": "IY",
        "code": "635",
        "icao": "IYE"
      },
      {
        "no": "133",
        "airline": "FedEx",
        "iata": "FX",
        "code": ".",
        "icao": "FDX"
      },
      {
        "no": "268",
        "airline": "Zambian Airways",
        "iata": "Q3",
        "code": "391",
        "icao": "MAZ"
      },
      {
        "no": "134",
        "airline": "Finnair Oyj",
        "iata": "AY",
        "code": "105",
        "icao": "FIN"
      },
      {
        "no": "269",
        "airline": "Zimbabwe Express Airlines",
        "iata": "Z7",
        "code": "247",
        "icao": ""
      },
      {
        "no": "135",
        "airline": "Flight West Airlines Pty Ltd.",
        "iata": "YC",
        "code": "60",
        "icao": "FWQ"
      },
      {
        "no": ".",
        "airline": ".",
        "iata": ".",
        "code": ".",
        "icao": ""
      }
    ];
  }

}
